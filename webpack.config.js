const path = require('path')

module.exports = {
    mode: 'development',
    entry: {
        common: path.resolve(__dirname, './modules/common/src/Ui/Common/views/main.js'),
    },

    output: {
        path: path.resolve(__dirname, './public/dist'),
        filename: '[name].bundle.js',
    }
}
