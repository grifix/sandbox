#!/bin/sh
docker-compose down
rm -rf var/*
docker volume rm -f sandbox_db
docker volume rm -f sandbox_rabbit
docker-compose up --build -d --remove-orphans

set -e
set -x
docker-compose exec -T php composer install

docker-compose exec -T php bin/console doctrine:migrations:migrate --no-interaction
docker-compose exec -T php bin/console sandbox:common:reset-fixtures
set +x
echo "Setup completed"
