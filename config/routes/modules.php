<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function(RoutingConfigurator $configurator){
    $configurator->import('../../modules/*/configs/routes.php');
};
