<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Grifix\Framework\Application\CommandBusInterface;
use Grifix\Framework\Infrastructure\Application\CommandBus;
use Grifix\ObjectRegistry\MemoryObjectRegistry;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Test\Contexts\HttpErrorContext;
use Grifix\Test\Contexts\HttpResponseContext;
use Grifix\Test\Contexts\SetUpContext;
use Grifix\Test\Contexts\TimeContext;
use Grifix\Test\DatabaseManager;
use Grifix\Test\EventStore\MessageBrokerFake;
use Grifix\Test\EventStore\TestCommandBus;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Test\TestHttpClient;
use Grifix\Test\TestMessageBus;
use Sandbox\Catalog\CatalogApplicationClient;
use Sandbox\Catalog\CatalogClientInterface;
use Sandbox\Security\Client\ApplicationSecurityClient;
use Sandbox\Security\Client\SecurityClientInterface;
use Sandbox\SharedKernel\Ui\Middlewares\SecurityMiddleware;
use Sandbox\SharedKernel\Ui\Security\CachedSecurity;
use Sandbox\SharedKernel\Ui\Security\Security;
use Sandbox\SharedKernel\Ui\Security\SecurityInterface;
use Sandbox\SharedKernel\Ui\SharedExceptionConverterProvider;

return static function (ContainerConfigurator $configurator) {
    $configurator->import('../modules/*/configs/services.php');
    $services = $configurator->services();
    $services->defaults()->autoconfigure()->autowire();
    $services->set(CatalogClientInterface::class, CatalogApplicationClient::class);
    $services->set(SecurityClientInterface::class, ApplicationSecurityClient::class);
    $services->set(SharedExceptionConverterProvider::class);
    $services->set(Security::class);
    $services->set(SecurityInterface::class, CachedSecurity::class)
        ->args([
            '$security' => service(Security::class),
        ]);
    $services->set(FixtureFactory::class)->args([
        '$container' => service('service_container'),
    ])->public();
    $services->set(ObjectRegistryInterface::class, MemoryObjectRegistry::class);

    $services->set(DatabaseManager::class)->args([
        '$rootDir' => '%kernel.project_dir%',
        '$env' => '%kernel.environment%',
    ])->public();

    $services->set(SecurityMiddleware::class)
        ->tag(
            'kernel.event_listener',
            [
                'event' => 'kernel.request',
            ],
        );

    if ($configurator->env() === 'test') {
        $services->set(TestHttpClient::class);

        $services->set(CommandBus::class);
        $services->set(MessageBrokerInterface::class, MessageBrokerFake::class);
        $services->set(TestMessageBus::class);
        $services->set(CommandBusInterface::class, TestCommandBus::class)->args([
            '$commandBus' => service(CommandBus::class),
        ]);

        $services->set(SetUpContext::class);
        $services->set(HttpErrorContext::class);
        $services->set(HttpResponseContext::class);
        $services->set(TimeContext::class);
        $services->set(ClockInterface::class, FrozenClock::class);
    }
};
