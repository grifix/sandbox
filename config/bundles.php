<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Grifix\MemoryBundle\GrifixMemoryBundle::class => ['all' => true],
    Grifix\ClockBundle\GrifixClockBundle::class => ['all' => true],
    Grifix\WorkerBundle\GrifixWorkerBundle::class => ['all' => true],
    Grifix\NormalizerBundle\GrifixNormalizerBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Grifix\EventStoreBundle\GrifixEventStoreBundle::class => ['all' => true],
    Grifix\FrameworkBundle\GrifixFrameworkBundle::class => ['all' => true],
    Grifix\EntityManagerBundle\GrifixEntityManagerBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Grifix\TypesBundle\GrifixTypesBundle::class => ['all' => true],
    FriendsOfBehat\SymfonyExtension\Bundle\FriendsOfBehatSymfonyExtensionBundle::class => ['test' => true],
    Grifix\ErrorPresenterBundle\GrifixErrorPresenterBundle::class => ['all' => true],
    Grifix\ViewBundle\GrifixViewBundle::class => ['all' => true],
    Grifix\JwtBundle\GrifixJwtBundle::class => ['all' => true],
    Grifix\EncryptorBundle\GrifixEncryptorBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
];
