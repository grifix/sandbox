import {Injectable} from "@angular/core";
export class ObjectRegistry {
  private objects: Map<string, any> = new Map<string, any>();

  public add(object: any, alias: string): void {
    if(this.objects.has(alias)) {
      throw new Error(`Object with alias ${alias} already exists!`);
    }
    this.objects.set(alias, object);
  }

  public get(alias: string): any {
    if(!this.objects.has(alias)) {
      throw new Error(`Object with alias ${alias} does not exist!`);
    }
    return this.objects.get(alias);
  }
}
