import {FixtureFactory} from "./fixture-factory";
import {FixtureRegistry} from "./fixture-registry";
import {ApplicationInterface} from "../../app/common/application/ports/application/application-interface";
import {AuthenticateUserCommand} from "../../app/security/application/user/commands/authentiacate/authenticate-user-command";
import {Email} from "../../libraries/types/email/email";
import {ClearTokensCommand} from "../../app/security/application/user/commands/clear-tokes/clear-tokens-command";
import {Uuid} from "../../libraries/types/uuid/uuid";

export abstract class AbstractFixture {
  private isBuilt: boolean = false;
  protected id: Uuid|null = null;

  public constructor(
    protected fixtureFactory: FixtureFactory,
    protected fixtureRegistry: FixtureRegistry,
    protected application: ApplicationInterface,
    private admin: { email: string, password: string },
  ) {
    this.init();
  }

  protected init(): void {

  }

  protected abstract create(): Promise<Uuid>;

  protected abstract addToRegistry(alias: string): void;

  protected assertIsBuilt(): void {
    if (!this.isBuilt) {
      throw new Error(`Fixture ${this.constructor.name} is not built!`);
    }
  }

  protected assertIsNotBuilt(): void {
    if (this.isBuilt) {
      throw new Error(`Fixture ${this.constructor.name} is already built!`);
    }
  }

  public async build<T>(alias: string | null = null): Promise<T> {
    this.assertIsNotBuilt();
    await this.application.tell(new AuthenticateUserCommand(Email.fromString(this.admin.email), this.admin.password));
    this.id = await this.create();
    await this.application.tell(new ClearTokensCommand());
    if (alias) {
      this.addToRegistry(alias);
    }
    this.isBuilt = true;
    return this as unknown as T;
  }

  public getId(): Uuid {
    if (this.id === null) {
      throw new Error('Fixture ['+this.constructor.name+'] not created yet');
    }
    return this.id;
  }
}
