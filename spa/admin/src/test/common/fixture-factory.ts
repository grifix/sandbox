import {ApplicationInterface} from "../../app/common/application/ports/application/application-interface";
import {UserFixture} from "../security/fixtures/user-fixture";
import {FixtureRegistry} from "./fixture-registry";
import {AbstractFixture} from "./abstract-fixture";

export class FixtureFactory {
  constructor(
    private readonly admin: { email: string, password: string },
    private readonly application: ApplicationInterface,
    private readonly fixtureRegistry: FixtureRegistry,
  ) {
  }

  public createSecurityUserFixture(): UserFixture {
    return new UserFixture(this, this.fixtureRegistry, this.application, this.admin);
  }

  public createFixture<T extends AbstractFixture>(fixtureClass: new (fixtureFactory: FixtureFactory, fixtureRegistry: FixtureRegistry, application: ApplicationInterface, admin: { email: string, password: string }) => T): T {
    return new fixtureClass(this, this.fixtureRegistry, this.application, this.admin);
  }
}
