import {ApplicationInterface} from "../../app/common/application/ports/application/application-interface";
import {FixtureFactory} from "./fixture-factory";
import {FixtureRegistry} from "./fixture-registry";
import {TestClient} from "../../libraries/test/client/test-client";
import {
  AuthenticateUserCommand
} from "../../app/security/application/user/commands/authentiacate/authenticate-user-command";
import {Email} from "../../libraries/types/email/email";
import {ClearTokensCommand} from "../../app/security/application/user/commands/clear-tokes/clear-tokens-command";

export class TestHelper {
  public readonly fixtureFactory: FixtureFactory;
  public readonly fixtureRegistry: FixtureRegistry;

  constructor(
    private readonly admin: { email: string, password: string },
    private readonly application: ApplicationInterface,
    private readonly testClient: TestClient,
  ) {
    this.fixtureRegistry = new FixtureRegistry();
    this.fixtureFactory = new FixtureFactory(this.admin, this.application, this.fixtureRegistry);
  }

  public async resetFixtures(): Promise<void> {
    await this.testClient.resetFixtures();
    return new Promise(resolve => setTimeout(resolve, 1000));
  }

  public async authenticateAdmin(): Promise<void> {
    await this.application.tell(new AuthenticateUserCommand(
      Email.fromString('admin@example.com'),
      'AdmiN!123'
    ));
  }

  public async unauthenticateAdmin(): Promise<void> {
    await this.application.tell(new ClearTokensCommand());
  }

  public async sleep(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

