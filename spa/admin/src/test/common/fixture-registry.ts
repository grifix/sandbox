import {Injectable} from "@angular/core";
import {UserFixture} from "../security/fixtures/user-fixture";
import {ObjectRegistry} from "./object-registry";
import {RoleFixture} from "../security/fixtures/role-fixture";

@Injectable({
  providedIn: 'root'
})
export class FixtureRegistry {

  private readonly securityUserFixtures: ObjectRegistry = new ObjectRegistry();
  private readonly securityRoleFixtures: ObjectRegistry = new ObjectRegistry();

  public addSecurityUserFixture(fixture: UserFixture, alias: string): void {
    this.securityUserFixtures.add(fixture, alias);
  }

  public addSecurityRoleFixture(fixture: RoleFixture, alias: string): void {
    this.securityRoleFixtures.add(fixture, alias);
  }

  public getSecurityUserFixture(alias: string): UserFixture {
    return this.securityUserFixtures.get(alias);
  }

  public getSecurityRoleFixture(alias: string): RoleFixture {
    return this.securityRoleFixtures.get(alias);
  }
}
