import {AbstractFixture} from "../../common/abstract-fixture";
import {Email} from "../../../libraries/types/email/email";
import {v4} from "uuid";
import {CreateUserCommand} from "../../../app/security/application/user/commands/create/create-user-command";
import {AuthenticateUserCommand} from "../../../app/security/application/user/commands/authentiacate/authenticate-user-command";
import {Uuid} from "../../../libraries/types/uuid/uuid";
import {AssignRoleCommand} from "../../../app/security/application/user/commands/assing-role/assign-role-command";

export class UserFixture extends AbstractFixture {

  private email: Email = Email.fromString('user-' + v4() + '@example.com');
  private password: string = 'A$dfjis$2323fdisf;i;';

  public withEmail(email: Email): UserFixture {
    this.assertIsNotBuilt()
    this.email = email;
    return this;
  }

  public withPassword(password: string): UserFixture {
    this.assertIsNotBuilt()
    this.password = password;
    return this;
  }

  protected addToRegistry(alias: string): void {
    this.fixtureRegistry.addSecurityUserFixture(this, alias);
  }

  protected async create(): Promise<Uuid> {
    return await this.application.tell(new CreateUserCommand(this.email, this.password));
  }

  public async authenticate(): Promise<UserFixture> {
    await this.application.tell(new AuthenticateUserCommand(this.email, this.password));
    return this;
  }

  public async assingRole(roleId: Uuid): Promise<UserFixture> {
    await this.application.tell(new AssignRoleCommand(this.getId(), roleId));
    return this;
  }

}
