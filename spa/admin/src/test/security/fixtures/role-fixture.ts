import {AbstractFixture} from "../../common/abstract-fixture";
import {v4} from "uuid";
import {CreateRoleCommand} from "../../../app/security/application/role/commands/create/create-role-command";
import {
  GrantRolePermissionCommand
} from "../../../app/security/application/role/commands/grantPermission/grant-role-permission-command";
import {Uuid} from "../../../libraries/types/uuid/uuid";

export class RoleFixture extends AbstractFixture {

  private name: string = 'role-' + v4();

  protected addToRegistry(alias: string): void {
    this.fixtureRegistry.addSecurityRoleFixture(this, alias)
  }

  protected async create(): Promise<Uuid> {
    return await this.application.tell(new CreateRoleCommand(this.name))
  }

  public async grantPermission(permission: string): Promise<RoleFixture> {
    await this.application.tell(new GrantRolePermissionCommand(this.getId(), permission))
    return this;
  }
}
