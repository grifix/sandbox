import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from "./common/ui/components/main/main.component";
import {AuthComponent} from "./security/ui/components/auth/auth.component";
import {UserTableComponent} from "./security/ui/components/user-table/user-table.component";
import {SecurityRoutes} from "./security/ui/components/security-routes";

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: SecurityRoutes.auth,
    component: AuthComponent
  },
  {
    path: SecurityRoutes.users,
    component: UserTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
