import {Component, Injector} from '@angular/core';
import {ApplicationInterface} from "./common/application/ports/application/application-interface";
import {environment} from "../environments/environment";
import {TestHelper} from "../test/common/test-helper";
import {TestClient} from "../libraries/test/client/test-client";
import {AuthenticateUserCommand} from "./security/application/user/commands/authentiacate/authenticate-user-command";
import {
  AuthenticateUserCommandHandler
} from "./security/application/user/commands/authentiacate/authenticate-user-command-handler";
import {ClearTokensCommand} from "./security/application/user/commands/clear-tokes/clear-tokens-command";
import {ClearTokensCommandHandler} from "./security/application/user/commands/clear-tokes/clear-tokens-command-handler";
import {GetMeQuery} from "./security/application/user/queries/get-me/get-me-query";
import {GetMeQueryHandler} from "./security/application/user/queries/get-me/get-me-query-handler";
import {RefreshTokensCommand} from "./security/application/user/commands/refresh-tokens/refresh-tokens-command";
import {
  RefreshTokensCommandHandler
} from "./security/application/user/commands/refresh-tokens/refresh-tokens-command-handler";
import {IsAuthenticatedQuery} from "./common/application/Queries/is-authenticated/is-authenticated-query";
import {
  IsAuthenticatedQueryHandler
} from "./common/application/Queries/is-authenticated/is-authenticated-query-handler";
import {CreateUserCommand} from "./security/application/user/commands/create/create-user-command";
import {CreateUserCommandHandler} from "./security/application/user/commands/create/create-user-command-handler";
import {AssignRoleCommand} from "./security/application/user/commands/assing-role/assign-role-command";
import {AssignRoleCommandHandler} from "./security/application/user/commands/assing-role/assign-role-command-handler";
import {CreateRoleCommand} from "./security/application/role/commands/create/create-role-command";
import {CreateRoleCommandHandler} from "./security/application/role/commands/create/create-role-command-handler";
import {
  GrantRolePermissionCommand
} from "./security/application/role/commands/grantPermission/grant-role-permission-command";
import {
  GrantRolePermissionCommandHandler
} from "./security/application/role/commands/grantPermission/grant-role-permission-command-handler";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'admin';


  constructor(
    application: ApplicationInterface,
    injector: Injector,
  ) {
    application.registerCommandHandler(AuthenticateUserCommand, injector.get(AuthenticateUserCommandHandler));
    application.registerCommandHandler(ClearTokensCommand, injector.get(ClearTokensCommandHandler));
    application.registerCommandHandler(RefreshTokensCommand, injector.get(RefreshTokensCommandHandler));
    application.registerCommandHandler(CreateUserCommand, injector.get(CreateUserCommandHandler));
    application.registerCommandHandler(AssignRoleCommand, injector.get(AssignRoleCommandHandler));
    application.registerCommandHandler(CreateRoleCommand, injector.get(CreateRoleCommandHandler));
    application.registerCommandHandler(GrantRolePermissionCommand, injector.get(GrantRolePermissionCommandHandler));

    application.registerQueryHandler(GetMeQuery, injector.get(GetMeQueryHandler));
    application.registerQueryHandler(IsAuthenticatedQuery, injector.get(IsAuthenticatedQueryHandler));



    if (environment.testHelper.enabled) {
      (window as any).testHelper = new TestHelper(
        {
          email: environment.testHelper.admin.email,
          password: environment.testHelper.admin.password,
        },
        application,
        injector.get(TestClient),
      );
      console.log(window);
    }
  }
}
