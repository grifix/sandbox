import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from './common/ui/components/main/main.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from "@angular/material/dialog";
import {SecurityClientInterface} from "../libraries/security/client/security-client-interface";
import {MessageComponent} from './common/ui/components/message/message.component';
import {AuthComponent} from './security/ui/components/auth/auth.component';
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {ReactiveFormsModule} from "@angular/forms";
import {TokenStorageInterface} from "../libraries/tokenStorage/token-storage-interface";
import {LocalTokenStorage} from "../libraries/tokenStorage/local-token-storage";
import {HttpClientModule} from "@angular/common/http";
import {UserTableComponent} from './security/ui/components/user-table/user-table.component';
import {AuthenticatorInterface} from "./common/application/ports/authenticator/authenticator-interface";
import {Authenticator} from "./common/infrastructure/ports/authenticator";
import {HttpClientInterface} from "../libraries/http/http-client-interface";
import {AngularHttpClient} from "../libraries/http/angular-http-client";
import {NgEventBus} from "ng-event-bus";
import {ApplicationInterface} from "./common/application/ports/application/application-interface";
import {Application} from "./common/infrastructure/ports/application";
import {UserManagerInterface} from "./security/application/user/ports/user-manager/user-manager-interface"
import {UserManager} from "./security/infrastructure/user/ports/user-manager";
import {GlobalErrorHandler} from "./common/ui/global-error-handler";
import {PanelComponent} from './common/ui/components/panel/panel.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {HttpSecurityClient} from "../libraries/security/client/http-security-client";
import {EventBusInterface} from "./common/application/ports/event-bus/event-bus-interface";
import {AngularEventBus} from "./common/infrastructure/ports/angular-event-bus";
import {MatIconModule} from "@angular/material/icon";
import {environment} from "../environments/environment";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {RoleManagerInterface} from "./security/application/role/ports/role-manager/role-manager-interface";
import {RoleManager} from "./security/infrastructure/role/ports/role-manager";

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MessageComponent,
    AuthComponent,
    UserTableComponent,
    PanelComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatDialogModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatListModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatToolbarModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
    ],
  providers: [
    NgEventBus,
    {
      provide: EventBusInterface,
      useClass: AngularEventBus
    },
    {
      provide: ApplicationInterface,
      useClass: Application,
    },
    {
      provide: SecurityClientInterface,
      useClass: HttpSecurityClient
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    {
      provide: TokenStorageInterface,
      useClass: LocalTokenStorage,
    },
    {
      provide: 'apiUrl',
      useValue: environment.apiUrl,
    },
    {
      provide: AuthenticatorInterface,
      useClass: Authenticator,
    },
    {
      provide: HttpClientInterface,
      useClass: AngularHttpClient
    },
    {
      provide: UserManagerInterface,
      useClass: UserManager
    },
    {
      provide: RoleManagerInterface,
      useClass: RoleManager
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
