import {Injectable} from "@angular/core";
import {MetaData, NgEventBus} from "ng-event-bus";
import {EventBusInterface} from "../../application/ports/event-bus/event-bus-interface";
import {AbstractEvent} from "../../application/ports/event-bus/abstract-event";

@Injectable()
export class AngularEventBus implements EventBusInterface {
  constructor(
    private readonly eventBus: NgEventBus,
  ) {
  }

  emit(event: AbstractEvent): void {
    this.eventBus.cast(event.getAlias(), event);
  }

  subscribe<T>(eventType: any, callback: (event: T) => void): void {
    const event = Object.create(eventType.prototype);
    if (!(event instanceof AbstractEvent)) {
      throw new Error(`${event.constructor.name} is not an instance of Event!`);
    }
    this.eventBus.on(event.getAlias()).subscribe((meta: MetaData) => callback(meta.data));
  }
}
