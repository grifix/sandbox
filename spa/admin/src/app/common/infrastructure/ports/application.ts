import {AuthenticatorInterface} from "../../application/ports/authenticator/authenticator-interface";
import {Injectable} from "@angular/core";
import {ApplicationInterface} from "../../application/ports/application/application-interface";
import {QueryHandlerInterface} from "../../application/ports/application/query-handler-interface";
import {CommandHandlerInterface} from "../../application/ports/application/command-handler-interface";
import {AbstractQuery} from "../../application/ports/application/abstract-query";
import {AbstractCommand} from "../../application/ports/application/abstract-command";
import {
  ExpiredAccessTokenException
} from "../../../../libraries/security/client/exceptions/expired-access-token-exception";

@Injectable()
export class Application implements ApplicationInterface {

  private queryHandlers = new Map<Object, QueryHandlerInterface>();

  private commandHandlers = new Map<Object, CommandHandlerInterface>();

  constructor(private authenticator: AuthenticatorInterface) {

  }

  async ask<R>(query: AbstractQuery): Promise<R> {
    try {
      return await this.getQueryHandler(query).handle(query)
    } catch (error) {
      if (error instanceof ExpiredAccessTokenException) {
        await this.authenticator.refreshTokens();
        return await this.getQueryHandler(query).handle(query)
      }
      throw error;
    }

  }

  registerQueryHandler(queryType: any, handler: QueryHandlerInterface): void {
    const query = Object.create(queryType.prototype);
    if (!(query instanceof AbstractQuery)) {
      throw new Error(`${query.constructor.name} is not an instance of Query!`);
    }
    this.queryHandlers.set(query.getAlias(), handler);
  }

  private getQueryHandler(query: AbstractQuery): QueryHandlerInterface {
    let queryHandler = this.queryHandlers.get(query.getAlias());
    if (!queryHandler) {
      throw new Error(`No query handler registered for the query [${query.getAlias()}]!`);
    }
    return queryHandler;
  }

  private getCommandHandler(command: AbstractCommand): CommandHandlerInterface {
    let commandHandler = this.commandHandlers.get(command.getAlias());
    if (!commandHandler) {
      throw new Error(`No command handler registered for the command [${command.getAlias()}]!`);
    }
    return commandHandler;
  }

  registerCommandHandler(commandType: any, handler: CommandHandlerInterface): void {
    const command = Object.create(commandType.prototype);
    if (!(command instanceof AbstractCommand)) {
      throw new Error(`${command.constructor.name} is not an instance of Command!`);
    }
    this.commandHandlers.set(command.getAlias(), handler);
  }

  async tell<R = void>(command: AbstractCommand): Promise<R> {
    try {
      return await this.getCommandHandler(command).handle(command);
    } catch (error) {
      if (error instanceof ExpiredAccessTokenException) {
        await this.authenticator.refreshTokens();
        return await this.getCommandHandler(command).handle(command);
      }
      throw error;
    }
  }
}
