import {Injectable} from "@angular/core";
import {Email} from "../../../../libraries/types/email/email";
import {TokenStorageInterface} from "../../../../libraries/tokenStorage/token-storage-interface";
import {AuthenticatorInterface} from "../../application/ports/authenticator/authenticator-interface";
import {
  AuthenticationFailedException
} from "../../application/ports/authenticator/exceptions/authentication-failed-exception";
import {NotAuthenticatedException} from "../../application/ports/authenticator/exceptions/not-authenticated-exception";
import {SecurityClientInterface} from "../../../../libraries/security/client/security-client-interface";

@Injectable()
export class Authenticator implements AuthenticatorInterface {
  constructor(
    private readonly client: SecurityClientInterface,
    private readonly tokenStorage: TokenStorageInterface,
  ) {
  }

  /**
   * @inheritDoc
   */
  async authenticate(email: Email, password: string): Promise<void> {
    try {
      let tokens = await this.client.authenticate(email, password);
      this.tokenStorage.storeAccessToken(tokens.accessToken);
      this.tokenStorage.storeRefreshToken(tokens.refreshToken);
    } catch (error) {
      if (error instanceof Error) {
        throw new AuthenticationFailedException(error.message);
      }
      throw new AuthenticationFailedException();
    }
  }

  getAccessToken(): string {
    if (this.tokenStorage.hasAccessToken()) {
      return this.tokenStorage.getAccessToken();
    }
    throw new NotAuthenticatedException();

  }

  async refreshTokens(): Promise<void> {
    let tokens = await this.client.createRefreshAccessSession(this.tokenStorage.getRefreshToken());
    this.tokenStorage.storeAccessToken(tokens.accessToken);
    this.tokenStorage.storeRefreshToken(tokens.refreshToken);
  }

  clearTokens(): void {
    this.tokenStorage.clear();
  }

  public isAuthenticated(): boolean {
    return this.tokenStorage.hasAccessToken();
  }
}
