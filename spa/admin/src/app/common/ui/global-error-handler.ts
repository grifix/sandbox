import {ErrorHandler, Injectable} from "@angular/core";
import {UiHelper} from "./ui-helper";
import {HttpErrorResponse, HttpStatusCode} from "@angular/common/http";
import {Location} from "@angular/common";
import {Navigator} from "./navigator";
import {ApplicationInterface} from "../application/ports/application/application-interface";
import {RefreshTokensCommand} from "../../security/application/user/commands/refresh-tokens/refresh-tokens-command";
import {SecurityRoutes} from "../../security/ui/components/security-routes";
import {environment} from "../../../environments/environment";
import {ActivatedRoute} from "@angular/router";
import {NotAuthenticatedException} from "../application/ports/authenticator/exceptions/not-authenticated-exception";
import {MeDoesNotExistException} from "../../security/application/user/ports/user-manager/me-does-no-exist-exception";
import {
  InvalidRefreshTokenException
} from "../../../libraries/security/client/exceptions/invalid-refresh-token-exception";
import {
  RefreshAccessSessionDoesNotExistException
} from "../../../libraries/security/client/exceptions/refresh-access-session-does-not-exist-exception";
import {
  ExpiredRefreshTokenException
} from "../../../libraries/security/client/exceptions/expired-refresh-token-exception";
import {
  ExpiredAccessTokenException
} from "../../../libraries/security/client/exceptions/expired-access-token-exception";
import {
  InvalidAccessTokenException
} from "../../../libraries/security/client/exceptions/invalid-access-token-exception";

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(
    private uiHelper: UiHelper,
    private navigator: Navigator,
    private location: Location,
    private application: ApplicationInterface,
    private route: ActivatedRoute,
  ) {
  }

  async handleError(error: any): Promise<void> {
    if (environment.debugErrors) {
      console.error(error);
    }
    if ((error instanceof Error) && ("rejection" in error)) {
      return await this.handleError(error.rejection);
    }
    if (error instanceof HttpErrorResponse && error.status === HttpStatusCode.Unauthorized) {
      return await this.redirectToAuth();
    }
    if (error instanceof NotAuthenticatedException) {
      return await this.redirectToAuth();
    }
    if (error instanceof MeDoesNotExistException) {
      return await this.redirectToAuth();
    }
    if (error instanceof InvalidRefreshTokenException) {
      return await this.redirectToAuth();
    }
    if (error instanceof RefreshAccessSessionDoesNotExistException) {
      return await this.redirectToAuth();
    }
    if (error instanceof ExpiredRefreshTokenException) {
      return await this.redirectToAuth();
    }
    if (error instanceof ExpiredAccessTokenException) {
      return await this.refreshTokens();
    }
    if (error instanceof InvalidAccessTokenException) {
      return await this.refreshTokens();
    }
    if (error instanceof Error) {
      this.uiHelper.showError(error.message, $localize`Error`);
      return;
    }
  }

  private async refreshTokens(): Promise<void> {
    try {
      await this.application.tell(new RefreshTokensCommand());
    } catch (error) {
      await this.handleError(error);
    }
  }

  private async redirectToAuth(): Promise<void> {
    let redirectTo = this.route.snapshot.queryParams['redirectTo'];
    if (!redirectTo) {
      redirectTo = this.location.path();
    }
    await this.navigator.navigate(
      [SecurityRoutes.auth],
      {queryParams: {redirectTo: redirectTo}}
    );
  }
}
