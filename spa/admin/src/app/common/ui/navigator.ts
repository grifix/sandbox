import {Router} from "@angular/router";
import {Injectable, NgZone} from "@angular/core";
@Injectable({
  providedIn: 'root'
})
export class Navigator {
  constructor(
    private readonly router: Router,
    private readonly zone: NgZone,
  ) {
  }

  public navigate(commands: any[], extras?: any): Promise<boolean> {
    return this.zone.run(() => this.router.navigate(commands, extras));
  }
}
