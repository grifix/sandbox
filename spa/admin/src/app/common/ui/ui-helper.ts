import {Injectable, NgZone} from "@angular/core";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ComponentType} from "@angular/cdk/overlay";
import {MessageComponent} from "./components/message/message.component";

@Injectable({
  providedIn: 'root'
})
export class UiHelper {
  constructor(
    private dialog: MatDialog,
    private zone: NgZone,
  ) {
  }

  openDialog<T, D = any>(component: ComponentType<T>, config?: MatDialogConfig<D>): void {
    this.zone.run(() => {
      this.dialog.open(
        component,
        config
      );
    });
  }

  showError(message: string, title?: string): void {
    this.openDialog(
      MessageComponent,
      {
        data: {
          message,
          title,
        }
      }
    );
  }
}
