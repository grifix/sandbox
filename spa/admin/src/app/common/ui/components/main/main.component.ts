import {Component} from '@angular/core';
import {SecurityRoutes} from "../../../../security/ui/components/security-routes";
import {EventBusInterface} from "../../../application/ports/event-bus/event-bus-interface";
import {ComponentCreatedEvent} from "../../events/component-created-event";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  public items: Item[];

  constructor(
    eventBus: EventBusInterface,
  ) {
    this.items = [
      new Item(
        'Users',
        'person',
        SecurityRoutes.users
      ),
      new Item(
        'Roles',
        'group',
        SecurityRoutes.users
      ),
      new Item(
        'Sessions',
        'access_time',
        SecurityRoutes.users
      ),
    ];
    eventBus.emit(new ComponentCreatedEvent('Main'));
  }
}

class Item {

  constructor(public label: string, public icon: string, public routerLink: string) {

  }

}
