import {AfterViewInit, Component, OnInit} from '@angular/core';
import {EventBusInterface} from "../../../application/ports/event-bus/event-bus-interface";
import {Navigator} from "../../navigator";
import {SecurityRoutes} from "../../../../security/ui/components/security-routes";
import {ApplicationInterface} from "../../../application/ports/application/application-interface";
import {ComponentCreatedEvent} from "../../events/component-created-event";
import {User} from "../../../../security/application/user/ports/user-manager/user";
import {UserManagerInterface} from "../../../../security/application/user/ports/user-manager/user-manager-interface";
import {
  UserAuthenticatedEvent
} from "../../../../security/application/user/commands/authentiacate/user-authenticated-event";
import {TokensClearedEvent} from "../../../../security/application/user/commands/clear-tokes/tokens-cleared-event";
import {
  TokensRefreshedEvent
} from "../../../../security/application/user/commands/refresh-tokens/tokens-refreshed-event";
import {IsAuthenticatedQueryResult} from "../../../application/Queries/is-authenticated/is-authenticated-query-result";
import {IsAuthenticatedQuery} from "../../../application/Queries/is-authenticated/is-authenticated-query";

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  public me: User | null = null;
  public title: string = 'Sandbox';
  public subtitle: string | null = null;

  constructor(
    private readonly UserManager: UserManagerInterface,
    private readonly eventBus: EventBusInterface,
    private readonly navigator: Navigator,
    private readonly application: ApplicationInterface,
  ) {
    this.eventBus.subscribe(UserAuthenticatedEvent, () => this.refreshMe());
    this.eventBus.subscribe(TokensClearedEvent, () => this.me = null);
    this.eventBus.subscribe(TokensRefreshedEvent, () => this.refreshMe());
    this.eventBus.subscribe(ComponentCreatedEvent, (event: ComponentCreatedEvent) => {
      this.subtitle = event.title;
    });
  }

  private async refreshMe(): Promise<void> {
    await this.UserManager.getMe().then((me: User) => this.me = me);
  }

  public logout(): void {
    this.me = null;
    this.navigator.navigate([SecurityRoutes.auth]).then()
  }

  public async goToMain(): Promise<void> {
    if (!await this.isAuthenticated()) {
      this.navigator.navigate([SecurityRoutes.auth]).then()
      return
    }
    this.navigator.navigate(['/']).then()
  }

  private async isAuthenticated(): Promise<boolean> {
    const result = await this.application.ask<IsAuthenticatedQueryResult>(new IsAuthenticatedQuery());
    return result.isAuthenticated;
  }


  async ngOnInit(): Promise<void> {
    await this.refreshMe();
  }
}
