import {AbstractEvent} from "../../application/ports/event-bus/abstract-event";

export class ComponentCreatedEvent extends AbstractEvent {

  constructor(
    public title: string,
  ) {
    super();
  }

  getAlias(): string {
    return "common." + this.constructor.name
  }

}
