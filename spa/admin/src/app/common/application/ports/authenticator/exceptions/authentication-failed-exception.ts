export class AuthenticationFailedException extends Error {
  constructor(message?: string) {
    message = message || 'Authentication failed!';
    super(message);
  }
}
