import {Email} from "../../../../../libraries/types/email/email";

export abstract class AuthenticatorInterface {
  /**
   *
   * @throws AuthenticationFailedException
   */
  public abstract authenticate(email: Email, password: string): Promise<void>

  /**
   * @throws NotAuthenticatedException
   */
  public abstract getAccessToken(): string

  public abstract refreshTokens(): Promise<void>

  public abstract clearTokens(): void

  public abstract isAuthenticated(): boolean
}





