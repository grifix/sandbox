export class NotAuthenticatedException extends Error {
  constructor() {
    super('Not authenticated!');
  }
}
