import {AbstractCommand} from "./abstract-command";

export interface CommandHandlerInterface {
  handle(command: AbstractCommand): Promise<any>;
}
