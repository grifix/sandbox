export abstract class AbstractQuery {
  public getAlias(): string {
    return this.getNamespace() + '.' + this.constructor.name;
  }

  abstract getNamespace(): string;
}
