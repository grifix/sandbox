export abstract class AbstractCommand {
  public getAlias(): string {
    return this.getNamespace() + '.' + this.constructor.name;
  }

  abstract getNamespace(): string;
}
