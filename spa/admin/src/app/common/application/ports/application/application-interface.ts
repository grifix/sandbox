import {AbstractQuery} from "./abstract-query";
import {AbstractCommand} from "./abstract-command";
import {QueryHandlerInterface} from "./query-handler-interface";
import {CommandHandlerInterface} from "./command-handler-interface";

export abstract class ApplicationInterface {
  public abstract ask<R>(query: AbstractQuery): Promise<R>;

  public abstract tell<R = void>(command: AbstractCommand): Promise<R>;

  public abstract registerQueryHandler(queryType: any, handler: QueryHandlerInterface): void;

  public abstract registerCommandHandler(commandType: any, handler: CommandHandlerInterface): void;
}









