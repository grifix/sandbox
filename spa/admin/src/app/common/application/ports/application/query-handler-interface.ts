import {AbstractQuery} from "./abstract-query";

export interface QueryHandlerInterface {
  handle(query: AbstractQuery): Promise<any>;
}
