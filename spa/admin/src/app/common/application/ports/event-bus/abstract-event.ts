export abstract class AbstractEvent {
  abstract getAlias(): string;
}
