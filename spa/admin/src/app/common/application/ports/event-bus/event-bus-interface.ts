export abstract class EventBusInterface {
  public abstract emit(event: any): void;

  public abstract subscribe(eventType: any, callback: (event: any) => void): void;
}

