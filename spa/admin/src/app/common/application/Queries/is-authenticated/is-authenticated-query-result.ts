export type IsAuthenticatedQueryResult = {
  isAuthenticated: boolean
}
