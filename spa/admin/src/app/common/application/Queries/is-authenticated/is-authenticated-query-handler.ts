import {Injectable} from "@angular/core";
import {AuthenticatorInterface} from "../../ports/authenticator/authenticator-interface";
import {IsAuthenticatedQuery} from "./is-authenticated-query";
import {IsAuthenticatedQueryResult} from "./is-authenticated-query-result";
import {QueryHandlerInterface} from "../../ports/application/query-handler-interface";

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedQueryHandler implements QueryHandlerInterface {
  constructor(
    private readonly authenticator: AuthenticatorInterface,
  ) {
  }

  async handle(query: IsAuthenticatedQuery): Promise<IsAuthenticatedQueryResult> {
    return {isAuthenticated: this.authenticator.isAuthenticated()};
  }
}
