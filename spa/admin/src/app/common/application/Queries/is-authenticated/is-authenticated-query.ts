import {AbstractQuery} from "../../ports/application/abstract-query";
import {CommonNamespace} from "../../../common-namespace";

export class IsAuthenticatedQuery extends AbstractQuery {
  override getNamespace(): string {
    return CommonNamespace;
  }
}




