import {Injectable} from "@angular/core";
import {RoleManagerInterface} from "../../../application/role/ports/role-manager/role-manager-interface";
import {Role} from "../../../application/role/ports/role-manager/role";
import {AuthenticatorInterface} from "../../../../common/application/ports/authenticator/authenticator-interface";
import {SecurityClientInterface} from "../../../../../libraries/security/client/security-client-interface";
import {Uuid} from "../../../../../libraries/types/uuid/uuid";
import {RoleFilter} from "../../../application/role/ports/role-manager/role-filter";

@Injectable()
export class RoleManager implements RoleManagerInterface {

  constructor(
    private readonly authenticator: AuthenticatorInterface,
    private readonly securityClient: SecurityClientInterface,
  ) {
  }

  async create(name: string): Promise<Uuid> {
    return await this.securityClient.createRole(name, this.authenticator.getAccessToken());
  }

  async find(filter: RoleFilter): Promise<Role[]> {
    return (await this.securityClient.findRoles(filter, this.authenticator.getAccessToken())).roles;
  }

  async grantPermission(roleId: Uuid, permission: string): Promise<void> {
    await this.securityClient.grantPermission(roleId, permission, this.authenticator.getAccessToken());
  }
}
