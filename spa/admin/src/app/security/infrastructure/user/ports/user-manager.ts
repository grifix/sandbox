import {Injectable} from "@angular/core";
import {AuthenticatorInterface} from "../../../../common/application/ports/authenticator/authenticator-interface";
import {Email} from "../../../../../libraries/types/email/email";
import {Uuid} from "../../../../../libraries/types/uuid/uuid";
import {UserManagerInterface} from "../../../application/user/ports/user-manager/user-manager-interface";
import {User} from "../../../application/user/ports/user-manager/user";
import {MeDoesNotExistException} from "../../../application/user/ports/user-manager/me-does-no-exist-exception";
import {UserFilter} from "../../../application/user/ports/user-manager/user-filter";
import {FindUsersResult} from "../../../application/user/ports/user-manager/find-users-result";
import {SecurityClientInterface} from "../../../../../libraries/security/client/security-client-interface";
import {
  UserDoesNotExistException
} from "../../../../../libraries/security/client/exceptions/user-does-not-exist-exception";


@Injectable()
export class UserManager implements UserManagerInterface {
  constructor(
    private readonly authenticator: AuthenticatorInterface,
    private readonly securityClient: SecurityClientInterface,
  ) {
  }

  async getMe(): Promise<User> {
    try {
      return await this.securityClient.getMe(this.authenticator.getAccessToken());
    } catch (e) {
      if (e instanceof UserDoesNotExistException) {
        throw new MeDoesNotExistException();
      }
      throw e;
    }

  }

  async create(email: Email, password: string): Promise<Uuid> {
    return await this.securityClient.createUser(email, password, this.authenticator.getAccessToken());
  }

  async find(filter: UserFilter): Promise<FindUsersResult> {
    return (await this.securityClient.findUsers(filter, this.authenticator.getAccessToken()));
  }

  async assignRole(userId: Uuid, roleId: Uuid): Promise<void> {
    await this.securityClient.assignRole(userId, roleId, this.authenticator.getAccessToken());
  }
}
