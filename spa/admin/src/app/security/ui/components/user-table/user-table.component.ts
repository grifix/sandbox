import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {EventBusInterface} from "../../../../common/application/ports/event-bus/event-bus-interface";
import {ComponentCreatedEvent} from "../../../../common/ui/events/component-created-event";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {User} from "../../../application/user/ports/user-manager/user";
import {UserManagerInterface} from "../../../application/user/ports/user-manager/user-manager-interface";

@Component({
  selector: 'app-users',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css'],
})
export class UserTableComponent implements AfterViewInit {
  public displayedColumns: string[] = [
    'id',
    'email',
    'disabled',
    'createdAt',
    'resumeDate',
    'registrationCompleted',
    'passwordResetInitiated'
  ];
  dataSource = new MatTableDataSource<User>();
  total: number = 0;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private eventBus: EventBusInterface,
    private userManager: UserManagerInterface,
  ) {
    this.eventBus.emit(new ComponentCreatedEvent('Users'));
  }


  async ngAfterViewInit(): Promise<void> {
    await this.loadRecords(0, this.paginator.pageSize);
  }

  async onChangePage(event: PageEvent): Promise<void> {
    await this.loadRecords(event.pageIndex * event.pageSize, event.pageSize);
  }

  async loadRecords(offset: number, limit:number): Promise<void> {
    const result = await this.userManager.find({limit, offset, countTotal: true});
    this.dataSource = new MatTableDataSource<User>(result.users);
    this.total = result.total!;
  }
}
