import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Email} from "../../../../../libraries/types/email/email";
import {ApplicationInterface} from "../../../../common/application/ports/application/application-interface";
import {ClearTokensCommand} from "../../../application/user/commands/clear-tokes/clear-tokens-command";
import {AuthenticateUserCommand} from "../../../application/user/commands/authentiacate/authenticate-user-command";
import {EventBusInterface} from "../../../../common/application/ports/event-bus/event-bus-interface";
import {ComponentCreatedEvent} from "../../../../common/ui/events/component-created-event";

@Component({
  selector: 'security-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  form = this.formBuilder.group({
    email: ['', [Validators.email, Validators.required]],
    password: ['', Validators.required],
  });

  private redirectTo: string | null = null;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly application: ApplicationInterface,
    private readonly eventBus: EventBusInterface,
  ) {
    this.application.tell(new ClearTokensCommand()).then();
    this.eventBus.emit(new ComponentCreatedEvent('Authentication'));
  }


  async onSubmit(): Promise<void> {
    const email = Email.fromString(this.form.value.email?.toString() || '');
    await this.application.tell(
      new AuthenticateUserCommand(
        email,
        this.form.value.password?.toString() || ''
      )
    )
    if (this.redirectTo && !this.redirectTo.startsWith('/security/auth')) {
      await this.router.navigate([this.redirectTo]);
      return;
    }
    await this.router.navigate(['/'])
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.redirectTo = params['redirectTo'];
    });
  }
}
