import {Uuid} from "../../../../../../libraries/types/uuid/uuid";

export type RoleFilter = {
  id?: Uuid | null;
  name?: string | null;
  limit?: number | null;
  offset?: number | null;
  withPermissions?: boolean | null;
  countTotal?: boolean | null;
  sortBy?: 'created_at' | 'name' | null;
}
