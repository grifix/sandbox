import {Uuid} from "../../../../../../libraries/types/uuid/uuid";
import {Forbidden} from "../../../../../../libraries/types/forbidden";

export type Role = {
  id: Uuid | Forbidden,
  name: string | Forbidden,
  permissions: string[] | Forbidden,
}
