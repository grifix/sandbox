import {Uuid} from "../../../../../../libraries/types/uuid/uuid";
import {RoleFilter} from "./role-filter";
import {Role} from "./role";

export abstract class RoleManagerInterface {
  public abstract create(name: string): Promise<Uuid>;

  public abstract find(filter: RoleFilter): Promise<Role[]>;

  public abstract grantPermission(roleId: Uuid, permission: string): Promise<void>;
}







