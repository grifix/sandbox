import {Injectable} from "@angular/core";
import {RoleManagerInterface} from "../../ports/role-manager/role-manager-interface";
import {CreateRoleCommand} from "./create-role-command";

@Injectable({
  providedIn: 'root'
})
export class CreateRoleCommandHandler {

  constructor(
    private readonly roleManager: RoleManagerInterface
  ){

  }
  async handle(command: CreateRoleCommand): Promise<void> {
    await this.roleManager.create(command.name);
  }
}
