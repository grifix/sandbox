import {RoleManagerInterface} from "../../ports/role-manager/role-manager-interface";
import {GrantRolePermissionCommand} from "./grant-role-permission-command";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class GrantRolePermissionCommandHandler {
  constructor(
    private readonly roleManager: RoleManagerInterface
  ) {

  }

  async handle(command: GrantRolePermissionCommand): Promise<void> {
    await this.roleManager.grantPermission(command.roleId, command.permission);
  }
}
