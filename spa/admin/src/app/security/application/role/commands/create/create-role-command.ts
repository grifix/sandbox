import {RoleNamespace} from "../../../../role-namespace";
import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";

export class CreateRoleCommand extends AbstractCommand {
  constructor(
    public readonly name: string,
  ) {
    super();
  }

  getNamespace(): string {
    return RoleNamespace;
  }

}

