import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";
import {RoleNamespace} from "../../../../role-namespace";
import {Uuid} from "../../../../../../libraries/types/uuid/uuid";

export class GrantRolePermissionCommand extends AbstractCommand {

  constructor(
    public readonly roleId: Uuid,
    public readonly permission: string,
  ) {
    super();
  }

  getNamespace(): string {
    return RoleNamespace;
  }

}
