import {Injectable} from "@angular/core";
import {UserManagerInterface} from "../../ports/user-manager/user-manager-interface";
import {GetMeQuery} from "./get-me-query";
import {GetMeQueryResult} from "./get-me-query-result";
import {QueryHandlerInterface} from "../../../../../common/application/ports/application/query-handler-interface";

@Injectable({
  providedIn: 'root'
})
export class GetMeQueryHandler implements QueryHandlerInterface {
  constructor(
    private userManager: UserManagerInterface
  ) {
  }

  async handle(action: GetMeQuery): Promise<GetMeQueryResult> {
    return {user: await this.userManager.getMe()}
  }
}
