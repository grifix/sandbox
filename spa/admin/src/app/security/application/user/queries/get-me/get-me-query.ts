import {UserNamespace} from "../../../../user-namespace";
import {AbstractQuery} from "../../../../../common/application/ports/application/abstract-query";


export class GetMeQuery extends AbstractQuery {
  override getNamespace(): string {
    return UserNamespace
  }
}




