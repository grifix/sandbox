import {User} from "../../ports/user-manager/user";

export type GetMeQueryResult = {
  user: User
}
