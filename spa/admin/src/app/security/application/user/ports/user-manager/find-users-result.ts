import {User} from "./user";

export type FindUsersResult = {
  users: User[],
  total: number | null,
}
