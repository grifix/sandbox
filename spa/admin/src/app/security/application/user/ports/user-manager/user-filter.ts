import {Uuid} from "../../../../../../libraries/types/uuid/uuid";
import {Email} from "../../../../../../libraries/types/email/email";

export type UserFilter = {
  id?: Uuid | null;
  email?: Email | null;
  limit?: number | null;
  offset?: number | null;
  withPermissions?: boolean | null;
  countTotal?: boolean | null;
  sortBy?: 'created_at' | 'email' | null;
}
