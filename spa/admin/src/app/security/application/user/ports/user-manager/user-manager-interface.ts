import {Uuid} from "../../../../../../libraries/types/uuid/uuid";
import {Email} from "../../../../../../libraries/types/email/email";
import {User} from "./user";
import {UserFilter} from "./user-filter";
import {FindUsersResult} from "./find-users-result";

export abstract class UserManagerInterface {
  public abstract getMe(): Promise<User>;

  public abstract create(email: Email, password: string): Promise<Uuid>;

  public abstract find(filter: UserFilter): Promise<FindUsersResult>;

  public abstract assignRole(userId: Uuid, roleId: Uuid): Promise<void>;
}




