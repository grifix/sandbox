export class MeDoesNotExistException extends Error {
  constructor() {
    super('Me does not exist!');
  }
}
