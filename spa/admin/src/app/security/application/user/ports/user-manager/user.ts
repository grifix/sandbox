import {Uuid} from "../../../../../../libraries/types/uuid/uuid";
import {Forbidden} from "../../../../../../libraries/types/forbidden";
import {Email} from "../../../../../../libraries/types/email/email";

export type User = {
  id: Uuid | Forbidden,
  email: Email | Forbidden,
  disabled: boolean | Forbidden,
  createdAt: Date | Forbidden,
  resumeDate: Date | Forbidden | null,
  permissions: string[] | Forbidden,
  registrationCompleted: boolean | Forbidden,
  passwordResetInitiated: boolean | Forbidden,
}
