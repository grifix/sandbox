import {Email} from "../../../../../../libraries/types/email/email";
import {UserNamespace} from "../../../../user-namespace";
import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";

export class AuthenticateUserCommand extends AbstractCommand {
  constructor(
    public readonly email: Email,
    public readonly password: string,
  ) {
    super();
  }

  public getNamespace(): string {
    return UserNamespace;
  }
}






