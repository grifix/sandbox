import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";
import {UserNamespace} from "../../../../user-namespace";
import {Uuid} from "../../../../../../libraries/types/uuid/uuid";

export class AssignRoleCommand extends AbstractCommand {

  constructor(
    public readonly userId: Uuid,
    public readonly roleId: Uuid
  ) {
    super();
  }

  getNamespace(): string {
    return UserNamespace;
  }

}
