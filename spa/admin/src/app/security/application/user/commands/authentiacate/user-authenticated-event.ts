import {UserNamespace} from "../../../../user-namespace";
import {AbstractEvent} from "../../../../../common/application/ports/event-bus/abstract-event";

export class UserAuthenticatedEvent extends AbstractEvent {
  public getAlias(): string {
    return UserNamespace + this.constructor.name;
  }
}
