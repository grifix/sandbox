import {UserNamespace} from "../../../../user-namespace";
import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";

export class RefreshTokensCommand extends AbstractCommand {
  getNamespace() {
    return UserNamespace;
  }
}


