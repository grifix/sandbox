import {Injectable} from "@angular/core";
import {UserManagerInterface} from "../../ports/user-manager/user-manager-interface";
import {CreateUserCommand} from "./create-user-command";
import {CommandHandlerInterface} from "../../../../../common/application/ports/application/command-handler-interface";
import {Uuid} from "../../../../../../libraries/types/uuid/uuid";

@Injectable({
  providedIn: 'root'
})
export class CreateUserCommandHandler implements CommandHandlerInterface{

  constructor(
    private readonly userManager: UserManagerInterface
  ) {
  }

  async handle(command: CreateUserCommand): Promise<Uuid> {
    return await this.userManager.create(command.email, command.password);
  }
}
