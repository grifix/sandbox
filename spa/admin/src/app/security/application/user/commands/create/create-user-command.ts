import {UserNamespace} from "../../../../user-namespace";
import {Email} from "../../../../../../libraries/types/email/email";
import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";


export class CreateUserCommand extends AbstractCommand {

  constructor(
    public readonly email: Email,
    public readonly password: string,
  ) {
    super();
  }

  getNamespace(): string {
    return UserNamespace;
  }
}


