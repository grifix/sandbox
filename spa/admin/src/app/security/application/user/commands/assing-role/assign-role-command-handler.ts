import {CommandHandlerInterface} from "../../../../../common/application/ports/application/command-handler-interface";
import {AssignRoleCommand} from "./assign-role-command";
import {UserManagerInterface} from "../../ports/user-manager/user-manager-interface";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AssignRoleCommandHandler implements CommandHandlerInterface {

  constructor(
    private readonly userManager: UserManagerInterface,
  ) {
  }

  handle(command: AssignRoleCommand): Promise<void> {
    return this.userManager.assignRole(command.userId, command.roleId);
  }
}
