import {UserNamespace} from "../../../../user-namespace";
import {AbstractCommand} from "../../../../../common/application/ports/application/abstract-command";
export class ClearTokensCommand extends AbstractCommand {

  getNamespace(): string {
    return UserNamespace
  }
}



