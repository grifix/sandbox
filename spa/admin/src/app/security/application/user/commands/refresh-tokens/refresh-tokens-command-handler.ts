import {Injectable} from "@angular/core";
import {AuthenticatorInterface} from "../../../../../common/application/ports/authenticator/authenticator-interface";
import {EventBusInterface} from "../../../../../common/application/ports/event-bus/event-bus-interface";
import {RefreshTokensCommand} from "./refresh-tokens-command";
import {TokensRefreshedEvent} from "./tokens-refreshed-event";
import {CommandHandlerInterface} from "../../../../../common/application/ports/application/command-handler-interface";

@Injectable({
  providedIn: 'root'
})
export class RefreshTokensCommandHandler implements CommandHandlerInterface {
  constructor(
    private readonly authenticator: AuthenticatorInterface,
    private readonly eventBus: EventBusInterface,
  ) {
  }

  async handle<R = void>(command: RefreshTokensCommand): Promise<void> {
    await this.authenticator.refreshTokens();
    this.eventBus.emit(new TokensRefreshedEvent());
  }
}
