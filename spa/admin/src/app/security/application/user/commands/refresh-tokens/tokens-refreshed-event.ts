import {UserNamespace} from "../../../../user-namespace";
import {AbstractEvent} from "../../../../../common/application/ports/event-bus/abstract-event";

export class TokensRefreshedEvent extends AbstractEvent {
  getAlias() {
    return UserNamespace + this.constructor.name;
  }
}
