import {Injectable} from "@angular/core";
import {AuthenticatorInterface} from "../../../../../common/application/ports/authenticator/authenticator-interface";
import {ClearTokensCommand} from "./clear-tokens-command";
import {EventBusInterface} from "../../../../../common/application/ports/event-bus/event-bus-interface";
import {TokensClearedEvent} from "./tokens-cleared-event";
import {CommandHandlerInterface} from "../../../../../common/application/ports/application/command-handler-interface";

@Injectable({
  providedIn: 'root'
})
export class ClearTokensCommandHandler implements CommandHandlerInterface{
  constructor(
    private authenticator: AuthenticatorInterface,
    private readonly eventBus: EventBusInterface,
  ) {
  }

  async handle(command: ClearTokensCommand): Promise<void> {
    await this.authenticator.clearTokens();
    this.eventBus.emit(new TokensClearedEvent());
  }
}
