import {Injectable} from "@angular/core";
import {AuthenticatorInterface} from "../../../../../common/application/ports/authenticator/authenticator-interface";
import {EventBusInterface} from "../../../../../common/application/ports/event-bus/event-bus-interface";
import {UserAuthenticatedEvent} from "./user-authenticated-event";
import {AuthenticateUserCommand} from "./authenticate-user-command";
import {CommandHandlerInterface} from "../../../../../common/application/ports/application/command-handler-interface";

@Injectable({
  providedIn: 'root'
})
export class AuthenticateUserCommandHandler implements CommandHandlerInterface {
  constructor(
    private readonly authenticator: AuthenticatorInterface,
    private readonly eventBus: EventBusInterface,
  ) {
  }

  async handle(command: AuthenticateUserCommand): Promise<void> {
    await this.authenticator.authenticate(
      command.email,
      command.password,
    );
    this.eventBus.emit(new UserAuthenticatedEvent());
  }
}
