import {Uuid} from "../../../types/uuid/uuid";
import {Forbidden} from "../../../types/forbidden";

export type Role = {
  id: Uuid | Forbidden,
  name: string | Forbidden,
  permissions: string[] | Forbidden,
}
