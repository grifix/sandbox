import {Uuid} from "../../../types/uuid/uuid";
import {Forbidden} from "../../../types/forbidden";
import {Email} from "../../../types/email/email";

export type User = {
  id: Uuid | Forbidden,
  email: Email | Forbidden,
  disabled: boolean | Forbidden,
  createdAt: Date | Forbidden,
  resumeDate: Date | Forbidden | null,
  permissions: string[] | Forbidden,
  registrationCompleted: boolean | Forbidden,
  passwordResetInitiated: boolean | Forbidden,
}
