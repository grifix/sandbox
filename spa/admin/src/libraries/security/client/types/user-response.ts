export type UserResponse = {
  id: string,
  email: string,
  disabled: boolean | string,
  createdAt: string,
  resumeDate: string,
  permissions: string[] | string,
  registrationCompleted: boolean | string,
  passwordResetInitiated: boolean | string,
}
