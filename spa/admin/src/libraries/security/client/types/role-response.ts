export type RoleResponse = {
  id: string,
  name: string,
  permissions: string[] | string,
}
