import {Uuid} from "../../../types/uuid/uuid";
import {Email} from "../../../types/email/email";

export type FindUsersRequest = {
  id?: Uuid | null;
  email?: Email | null;
  limit?: number | null;
  offset?: number | null;
  withPermissions?: boolean | null;
  countTotal?: boolean | null;
  sortBy?: 'created_at' | 'email' | null;
}
