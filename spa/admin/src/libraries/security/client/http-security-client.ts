import {Inject, Injectable} from "@angular/core";
import {HttpErrorResponse} from "@angular/common/http";
import {Email} from "../../types/email/email";
import {HttpClientInterface} from "../../http/http-client-interface";
import {Uuid} from "../../types/uuid/uuid";
import {Forbidden} from "../../types/forbidden";
import {DateFactory} from "../../date/date-factory";
import {SecurityClientInterface} from "./security-client-interface";
import {Tokens} from "./types/tokens";
import {SecurityServerException} from "./exceptions/security-server-exception";
import {ErrorCodes} from "./error-codes";
import {InvalidAccessTokenException} from "./exceptions/invalid-access-token-exception";
import {ExpiredAccessTokenException} from "./exceptions/expired-access-token-exception";
import {ExpiredRefreshTokenException} from "./exceptions/expired-refresh-token-exception";
import {InvalidRefreshTokenException} from "./exceptions/invalid-refresh-token-exception";
import {RefreshAccessSessionDoesNotExistException} from "./exceptions/refresh-access-session-does-not-exist-exception";
import {UserDoesNotExistException} from "./exceptions/user-does-not-exist-exception";
import {User} from "./types/user";
import {Role} from "./types/role";
import {FindUsersRequest} from "./types/find-users-request";
import {FindUsersResult} from "./types/find-users-result";
import {FindRolesRequest} from "./exceptions/find-roles-request";
import {FindRolesResult} from "./exceptions/find-roles-result";
import {UserResponse} from "./types/user-response";
import {RoleResponse} from "./types/role-response";

@Injectable()
export class HttpSecurityClient implements SecurityClientInterface {

  constructor(
    @Inject('apiUrl') private readonly apiUrl: string,
    private readonly httpClient: HttpClientInterface,
  ) {
  }

  /**
   * @inheritDoc
   */
  public async authenticate(email: Email, password: string): Promise<Tokens> {

    try {
      return await this.httpClient.post<Tokens>(
        this.apiUrl + 'security/users/authenticate',
        {
          email: email.toString(),
          password: password,
        }
      );
    } catch (error) {
      throw this.createError(error, 'Cannot authenticate!');
    }
  }


  /**
   * @inheritDoc
   */
  private createError(error: any, defaultMessage: string = 'Unknown error!'): SecurityServerException {
    if (error instanceof HttpErrorResponse && error.error && error.error.error && error.error.error.code) {
      switch (error.error.error.code) {
        case ErrorCodes.INVALID_ACCESS_TOKEN:
          return new InvalidAccessTokenException();
        case ErrorCodes.EXPIRED_ACCESS_TOKEN:
          return new ExpiredAccessTokenException();
        case ErrorCodes.EXPIRED_REFRESH_TOKEN:
          return new ExpiredRefreshTokenException();
        case ErrorCodes.INVALID_REFRESH_TOKEN:
          return new InvalidRefreshTokenException();
        case ErrorCodes.REFRESH_ACCESS_SESSION_DOES_NOT_EXIST:
          return new RefreshAccessSessionDoesNotExistException();
        case ErrorCodes.USER_DOES_NOT_EXIST:
          return new UserDoesNotExistException();
        default:
          return new SecurityServerException(error.error.error.message, error);
      }
    }
    return new SecurityServerException(defaultMessage);
  }

  async getMe(accessToken: string): Promise<User> {
    try {
      const user = await
        this.httpClient.get<UserResponse>(
          this.apiUrl + 'security/users/me',
          null,
          this.prepareAccessHeaders(accessToken)
        );

      return this.createUserObject(user);

    } catch (error) {
      throw this.createError(error, 'Cannot get me!');
    }
  }

  private createUserObject(
    user: UserResponse,
  ): User {
    return {
      id: Forbidden.isForbidden(user.id) ? Forbidden.create() : Uuid.fromString(user.id),
      email: Forbidden.isForbidden(user.email) ? Forbidden.create() : Email.fromString(user.email),
      disabled: Forbidden.isForbidden(user.disabled) ? Forbidden.create() : Boolean(user.disabled),
      createdAt: Forbidden.isForbidden(user.createdAt) ? Forbidden.create() : new Date(user.createdAt),
      resumeDate: Forbidden.isForbidden(user.resumeDate) ? Forbidden.create() : DateFactory.createDate(user.resumeDate),
      permissions: Forbidden.isForbidden(user.permissions) ? Forbidden.create() : user.permissions,
      registrationCompleted: Forbidden.isForbidden(user.registrationCompleted) ? Forbidden.create() : Boolean(user.registrationCompleted),
      passwordResetInitiated: Forbidden.isForbidden(user.passwordResetInitiated) ? Forbidden.create() : Boolean(user.passwordResetInitiated),
    };
  }

  private createRoleObject(
    role: RoleResponse,
  ): Role {
    return {
      id: Forbidden.isForbidden(role.id) ? Forbidden.create() : Uuid.fromString(role.id),
      name: Forbidden.isForbidden(role.name) ? Forbidden.create() : role.name,
      permissions: Forbidden.isForbidden(role.permissions) ? Forbidden.create() : role.permissions,
    };
  }

  private prepareAccessHeaders(accessToken?: string): any {
    if (accessToken) {
      return {
        'Authorization': 'Bearer ' + accessToken,
      };
    }
    return {};
  }

  async createRefreshAccessSession(refreshToken: string): Promise<Tokens> {
    try {
      return await this.httpClient.post<Tokens>(
        this.apiUrl + 'security/refreshAccessSession',
        {
          refreshToken: refreshToken,
        }
      );
    } catch (error) {
      throw this.createError(error, 'Cannot refresh access session!');
    }
  }

  async createUser(email: Email, password: string, accessToken?: string): Promise<Uuid> {
    try {
      const response = await this.httpClient.post<{ userId: string }>(
        this.apiUrl + 'security/users',
        {
          email: email.toString(),
          password: password,
        },
        this.prepareAccessHeaders(accessToken)
      );
      return Uuid.fromString(response.userId);
    } catch (error) {
      throw this.createError(error, 'Cannot create user!');
    }
  }

  async createRole(name: string, accessToken?: string): Promise<Uuid> {
    try {
      const response = await this.httpClient.post<{ roleId: string }>(
        this.apiUrl + 'security/roles',
        {
          name: name,
        },
        this.prepareAccessHeaders(accessToken)
      );
      return Uuid.fromString(response.roleId);
    } catch (error) {
      throw this.createError(error, 'Cannot create role!');
    }
  }

  async findUsers(request: FindUsersRequest, accessToken?: string): Promise<FindUsersResult> {
    try {
      let params = this.prepareSearchParams(request);
      if (request.id != null) {
        params.id = request.id.toString();
      }
      if (request.email != null) {
        params.email = request.email.toString();
      }

      const response = await this.httpClient.get<{ users: UserResponse[], total: number | null }>(
        this.apiUrl + 'security/users',
        params,
        this.prepareAccessHeaders(accessToken)
      );

      let users: User[] = [];
      response.users.forEach((user) => {
        users.push(this.createUserObject(user));
      });
      return {
        users: users,
        total: response.total,
      };
    } catch (error) {
      throw this.createError(error, 'Cannot find users!');
    }
  }

  async findRoles(request: FindRolesRequest, accessToken?: string): Promise<FindRolesResult> {
    try {

      let params = this.prepareSearchParams(request);
      if (request.id != null) {
        params.id = request.id.toString();
      }
      if (request.name != null) {
        params.email = request.name.toString();
      }

      const response = await this.httpClient.get<{ roles: RoleResponse[] }>(
        this.apiUrl + 'security/roles',
        params,
        this.prepareAccessHeaders(accessToken)
      );

      let roles: Role[] = [];
      response.roles.forEach((role) => {
          roles.push(this.createRoleObject(role));
        }
      );
      return {roles: roles};
    } catch (error) {
      throw this.createError(error, 'Cannot find roles!');
    }
  }

  private prepareSearchParams(request: any): any {
    let result: any = {};
    if (request.countTotal != null) {
      result.countTotal = request.countTotal;
    }
    if (request.offset != null) {
      result.offset = request.offset;
    }
    if (request.limit != null) {
      result.limit = request.limit;
    }
    if (request.sortBy != null) {
      result.sortBy = request.sortBy;
    }
    return result;
  }

  async grantPermission(roleId: Uuid, permission: string, accessToken: string): Promise<void> {
    try {
      const response = await this.httpClient.post<{ roleId: string }>(
        this.apiUrl + 'security/roles/' + roleId.toString() + '/permissions',
        {
          permission: permission,
        },
        this.prepareAccessHeaders(accessToken)
      );
    } catch (error) {
      throw this.createError(error, 'Cannot grant permission!');
    }
  }

  async revokePermission(roleId: Uuid, permission: string, accessToken: string): Promise<void> {
    try {
      const response = await this.httpClient.delete<{ roleId: string }>(
        this.apiUrl + 'security/roles/' + roleId.toString() + '/permissions',
        {
          permission: permission,
        },
        this.prepareAccessHeaders(accessToken)
      );
    } catch (error) {
      throw this.createError(error, 'Cannot delete permission!');
    }
  }

  async assignRole(userId: Uuid, roleId: Uuid, accessToken: string): Promise<void> {
    try {
      const response = await this.httpClient.delete<{ roleId: string }>(
        this.apiUrl + 'security/users/' + userId.toString() + '/roles',
        {
          roleId: roleId.toString(),
        },
        this.prepareAccessHeaders(accessToken)
      );
    } catch (error) {
      throw this.createError(error, 'Cannot assign role!');
    }
  }
}


