import {Email} from "../../types/email/email";
import {Uuid} from "../../types/uuid/uuid";
import {Tokens} from "./types/tokens";
import {User} from "./types/user";
import {FindUsersRequest} from "./types/find-users-request";
import {FindUsersResult} from "./types/find-users-result";
import {FindRolesRequest} from "./exceptions/find-roles-request";
import {FindRolesResult} from "./exceptions/find-roles-result";

export abstract class SecurityClientInterface {
  /**
   * @throws SecurityServerException
   */
  public abstract authenticate(email: Email, password: string): Promise<Tokens>;

  /**
   * @throws SecurityServerException
   */
  public abstract getMe(accessToken: string): Promise<User>;

  /**
   * @throws SecurityServerException
   */
  public abstract createRefreshAccessSession(refreshToken: string): Promise<Tokens>;

  /**
   * @throws SecurityServerException
   */
  public abstract createUser(email: Email, password: string, accessToken?: string): Promise<Uuid>;

  /**
   * @throws SecurityServerException
   */
  public abstract findUsers(request: FindUsersRequest, accessToken?: string): Promise<FindUsersResult>;

  /**
   * @throws SecurityServerException
   */
  public abstract findRoles(request: FindRolesRequest, accessToken?: string): Promise<FindRolesResult>;

  /**
   * @throws SecurityServerException
   */
  public abstract createRole(name: string, accessToken: string): Promise<Uuid>;

  /**
   * @throws SecurityServerException
   */
  public abstract grantPermission(roleId: Uuid, permission: string, accessToken: string): Promise<void>;

  /**
   * @throws SecurityServerException
   */
  public abstract revokePermission(roleId: Uuid, permission: string, accessToken: string): Promise<void>;

  /**
   * @throws SecurityServerException
   */
  public abstract assignRole(userId: Uuid, roleId: Uuid, accessToken: string): Promise<void>;
}


























