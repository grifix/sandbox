import {SecurityServerException} from "./security-server-exception";

export class RefreshAccessSessionDoesNotExistException extends SecurityServerException {
  constructor() {
    super('Refresh access session does not exist!');
  }
}
