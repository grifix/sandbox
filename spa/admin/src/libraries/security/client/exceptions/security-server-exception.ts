export class SecurityServerException extends Error {
  constructor(message?: string, private readonly previous?: Error) {
    message = message || 'Security client error!';
    super(message);
  }
}
