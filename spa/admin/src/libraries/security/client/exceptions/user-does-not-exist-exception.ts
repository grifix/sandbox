import {SecurityServerException} from "./security-server-exception";

export class UserDoesNotExistException extends SecurityServerException {
  constructor() {
    super('User does not exist!');
  }
}
