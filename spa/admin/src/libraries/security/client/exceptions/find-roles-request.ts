import {Uuid} from "../../../types/uuid/uuid";

export type FindRolesRequest = {
  id?: Uuid | null;
  name?: string | null;
  limit?: number | null;
  offset?: number | null;
  withPermissions?: boolean | null;
  countTotal?: boolean | null;
  sortBy?: 'created_at' | 'name' | null;
}
