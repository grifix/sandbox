import {SecurityServerException} from "./security-server-exception";

export class ExpiredAccessTokenException extends SecurityServerException {
  constructor() {
    super('Expired access token!');
  }
}
