import {SecurityServerException} from "./security-server-exception";

export class InvalidRefreshTokenException extends SecurityServerException {
  constructor() {
    super('Invalid refresh token!');
  }
}
