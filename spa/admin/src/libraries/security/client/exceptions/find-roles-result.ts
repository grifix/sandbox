import {Role} from "../types/role";

export type FindRolesResult = {
  roles: Role[],
}
