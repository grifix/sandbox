import {SecurityServerException} from "./security-server-exception";

export class InvalidAccessTokenException extends SecurityServerException {
  constructor() {
    super('Invalid access token!');
  }
}
