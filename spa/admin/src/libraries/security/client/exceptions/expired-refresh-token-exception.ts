import {SecurityServerException} from "./security-server-exception";

export class ExpiredRefreshTokenException extends SecurityServerException {
  constructor() {
    super('Expired refresh token!');
  }
}
