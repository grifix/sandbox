import {Inject, Injectable} from "@angular/core";
import {HttpClientInterface} from "../../http/http-client-interface";

@Injectable({
  providedIn: 'root'
})
export class TestClient{
  constructor(
    @Inject('apiUrl') private readonly apiUrl: string,
    private readonly httpClient: HttpClientInterface,
  ) {
  }

  public async resetFixtures(): Promise<void> {
      await this.httpClient.post<void>(this.apiUrl + 'reset-fixtures', {});
  }
}
