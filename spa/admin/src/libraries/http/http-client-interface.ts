export abstract class HttpClientInterface {

  public abstract get<T>(url: string, params?: any, headers?: any): Promise<T>;

  public abstract post<T>(url: string, data: any, headers?: any): Promise<T>;

  public abstract put<T>(url: string, data: any, headers?: any): Promise<T>;

  public abstract delete<T>(url: string, data: any,  headers?: any): Promise<T>;

}
