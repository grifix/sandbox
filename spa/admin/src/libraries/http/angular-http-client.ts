import {HttpClientInterface} from "./http-client-interface";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {firstValueFrom} from "rxjs";

@Injectable()
export class AngularHttpClient implements HttpClientInterface {

  constructor(
    private readonly httpClient: HttpClient
  ) {
  }

  async delete<T>(url: string, data: any,  headers?: any): Promise<T> {
    return this.sendRequest('DELETE', url, data, headers);
  }

  async get<T>(url: string, params?:any, headers?: any): Promise<T> {
    return await this.sendRequest<T>('GET', url, null, headers, params);
  }

  async post<T>(url: string, data: any, headers?: any): Promise<T> {
    return this.sendRequest<T>('POST', url, data, headers);
  }

  async put<T>(url: string, data: any, headers?: any): Promise<T> {
    return this.sendRequest('PUT', url, data, headers);
  }

  private async sendRequest<T>(method: string, url: string, data: any, headers?: any, params?: any): Promise<T> {

    if(!params){
      params = {};
    }
    if(!headers){
      headers = {};
    }

    let httpParams = new HttpParams();
    for (let param in params) {
      if (params.hasOwnProperty(param) && params[param] !== null) {
          httpParams = httpParams.set(param, params[param]);
      }
    }
    return await firstValueFrom(this.httpClient.request<T>(
      method, url, {
        body: data,
        headers: new HttpHeaders(headers),
        params: httpParams,
      }
    )) as Promise<T>;
  }
}
