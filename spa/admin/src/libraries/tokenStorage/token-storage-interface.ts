export abstract class TokenStorageInterface {
  /** @throws TokenNotFoundException */
  public abstract getAccessToken(): string;

  public abstract storeAccessToken(token: string): void;

  /** @throws TokenNotFoundException */
  public abstract getRefreshToken(): string;

  public abstract storeRefreshToken(token: string): void;

  public abstract hasAccessToken(): boolean;

  public abstract hasRefreshToken(): boolean;

  public abstract clear(): void;
}


