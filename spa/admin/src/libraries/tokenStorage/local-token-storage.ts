import {TokenStorageInterface} from "./token-storage-interface";
import {TokenNotFoundException} from "./token-not-found-exception";

export class LocalTokenStorage implements TokenStorageInterface {

  private readonly REFRESH_TOKEN_KEY = 'refreshToken';
  private readonly ACCESS_TOKEN_KEY = 'accessToken';

  getAccessToken(): string {
    if (!this.hasAccessToken()) {
      throw new TokenNotFoundException()
    }
    return localStorage.getItem(this.ACCESS_TOKEN_KEY) as string;
  }

  getRefreshToken(): string {
    if (!this.hasRefreshToken()) {
      throw new TokenNotFoundException()
    }
    return localStorage.getItem(this.REFRESH_TOKEN_KEY) as string;
  }

  hasAccessToken(): boolean {
    return typeof localStorage.getItem(this.ACCESS_TOKEN_KEY) === 'string';
  }

  hasRefreshToken(): boolean {
    return typeof localStorage.getItem(this.REFRESH_TOKEN_KEY) === 'string';
  }

  storeAccessToken(accessToken: string): void {
    localStorage.setItem(this.ACCESS_TOKEN_KEY, accessToken);
  }

  storeRefreshToken(refreshToken: string): void {
    localStorage.setItem(this.REFRESH_TOKEN_KEY, refreshToken);
  }

  clear(): void {
    localStorage.removeItem(this.ACCESS_TOKEN_KEY);
    localStorage.removeItem(this.REFRESH_TOKEN_KEY);
  }
}
