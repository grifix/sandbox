export abstract class DateFactory {
  public static createDate(date: string | null): Date | null {
    if (null === date) {
      return null;
    }
    return new Date(date);
  }
}
