import * as EmailValidator from 'email-validator';
import {InvalidEmailException} from "./invalid-email-exception";

export class Email {

  private constructor(private readonly value: string) {
    if (!EmailValidator.validate(value)) {
      throw new InvalidEmailException();
    }
  }

  public toString(): string {
    return this.value;
  }

  public static fromString(value: string): Email {
    return new Email(value);
  }
}


