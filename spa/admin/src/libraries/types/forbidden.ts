export class Forbidden {
  private static readonly value:string = '__FORBIDDEN_PROPERTY__';
  public toString(): string {
    return Forbidden.value;
  }

  public static create(): Forbidden {
    return new Forbidden();
  }

  private constructor() {

  }

  public static isForbidden(value: any): boolean {
    return value === Forbidden.value;
  }
}
