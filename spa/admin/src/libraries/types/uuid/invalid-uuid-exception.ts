export class InvalidUuidException extends Error {
  constructor() {
    super('Invalid uuid!');
  }
}
