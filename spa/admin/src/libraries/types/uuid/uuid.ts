import {v4, validate} from 'uuid';
import {InvalidUuidException} from "./invalid-uuid-exception";

export class Uuid {

  private constructor(private readonly value: string) {
    if (!validate(value)) {
      throw new InvalidUuidException();
    }
  }

  public static generate(): Uuid {
    return new Uuid(v4());
  }

  public static fromString(value: string): Uuid {
    return new Uuid(value);
  }

  public toString(): string {
    return this.value;
  }
}


