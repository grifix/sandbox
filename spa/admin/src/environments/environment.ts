export const environment = {
  debugErrors: false,
  testHelper: {
    enabled: false,
    admin: {
      email: '',
      password: '',
    }
  },
  apiUrl: 'http://localhost:57001/api/v1/',
};
