export const environment = {
  debugErrors: true,
  testHelper:{
    enabled: true,
    admin: {
      email: 'admin@example.com',
      password: 'AdmiN!123'
    }
  },
  apiUrl: 'http://localhost:57001/api/v1/',
};
