import {UserFixture} from "../../../src/test/security/fixtures/user-fixture";
import {CypressHelper} from "../../cypress-helper";
import {RoleFixture} from "../../../src/test/security/fixtures/role-fixture";

describe.only('user-table', () => {

  function prepareFixtures() {
    CypressHelper.disableTimout();
    return cy.window().then(async win => {

      await win.testHelper.resetFixtures();
      await win.testHelper.authenticateAdmin();

      for (let i = 0; i < 15; i++) {
        await win.testHelper.fixtureFactory.createFixture(UserFixture).build();
      }

      const roleFixture = await (await win.testHelper.fixtureFactory.createFixture(RoleFixture)
        .build<RoleFixture>())
        .grantPermission('security.role.read');

      const userFixture = await win.testHelper.fixtureFactory.createFixture(UserFixture).build<UserFixture>();
      await userFixture.assingRole(roleFixture.getId());
      await win.testHelper.unauthenticateAdmin();
      await userFixture.authenticate();
    });
  }

  it.only('display users', () => {
    cy.visit('/');
    prepareFixtures().then(
      () => {
        cy.visit('/security/users');
      }
    );
  });
});
