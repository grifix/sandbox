/// <reference types="cypress" />
import {UserFixture} from "../../../src/test/security/fixtures/user-fixture";
import {CypressHelper} from "../../cypress-helper";

describe('logging out', () => {

  function prepareFixtures() {

    return cy.window().then(async win => {
      await win.testHelper.resetFixtures();
      await (await win.testHelper.fixtureFactory
        .createFixture(UserFixture)
        .build<UserFixture>())
        .authenticate();
    });
  }

  it.only('log out', () => {
    cy.visit('/');
    prepareFixtures().then(
      () => {
        cy.visit('/');
        cy.get('[data-role="home"]').click();
        CypressHelper.assertPageTitleContains('Main');
        cy.get('[data-role="log-out"]').click();
        CypressHelper.assertPageTitleContains('Authentication');
      }
    );
  });
});
