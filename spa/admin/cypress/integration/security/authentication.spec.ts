/// <reference types="cypress" />
import {Email} from "../../../src/libraries/types/email/email";
import {CypressHelper} from "../../cypress-helper";
import {UserFixture} from "../../../src/test/security/fixtures/user-fixture";

describe.only('authentication', () => {

  const email = Email.fromString('user@example.com');
  const password = 'AdmiN!123';

  function prepareFixtures() {

    cy.window().then(async win => {

      await win.testHelper.resetFixtures();
      await win.testHelper.fixtureFactory
          .createFixture(UserFixture)
          .withEmail(email)
          .withPassword(password)
          .build()
    });
  }

  function authenticate(login: string, password: string) {
    cy.get('[data-role="email"]').type(login);
    cy.get('[data-role="password"]').type(password);
    cy.get('[data-role="submit"]').click();
  }

  it.only('authenticate', () => {
    cy.visit('/');
    CypressHelper.assertPageTitleContains('Authentication');
    authenticate('admin@example.com', 'AdmiN!123');
    CypressHelper.assertPageTitleContains('Main');
    CypressHelper.assertIsLoggedAs('admin@example.com');
  });

  it.only('authenticate and redirect', () => {
    cy.visit('security/users');
    prepareFixtures();
    CypressHelper.assertPageTitleContains('Authentication');
    authenticate(email.toString(), password);
    CypressHelper.assertPageTitleContains('Users');
    CypressHelper.assertIsLoggedAs(email.toString());
  })

  it.only('wrong credentials', () => {
    cy.visit('security/users');
    prepareFixtures();
    authenticate('wrong-user@example.com', password);
    CypressHelper.assertMessageIsDisplayed('Error', 'User does not exist!');
  })
});
