/// <reference types="cypress" />

import {environment} from "../src/environments/environment";

export abstract class CypressHelper {
  static assertPageTitleContains(text: string) {
    cy.get('[data-role="page-title"]').should('contain', text);
  }

  static assertMessageIsDisplayed(title: string, content: string) {
    cy.get('[data-role="message-title"]').should('contain', title);
    cy.get('[data-role="message-content"]').should('contain', content);
  }

  static assertIsLoggedAs(email: string) {
    cy.get('[data-role="me"]').should('contain', email);
  }

  static disableTimout() {
    Cypress.config('defaultCommandTimeout', 99999999999);
  }

  static resetFixtures() {
    CypressHelper.disableTimout();
    cy.request('POST', environment.apiUrl + 'reset-fixtures');
    CypressHelper.enableTimout();
  }

  static enableTimout() {
    Cypress.config('defaultCommandTimeout', 5000);
  }
}
