import {TestHelper} from "../../src/test/common/test-helper";

declare global {
  interface Window {
    testHelper: TestHelper;  // Replace 'any' with the actual type of your service
  }
}
