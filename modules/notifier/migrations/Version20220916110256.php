<?php

declare(strict_types=1);

namespace Notifier;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220916110256 extends AbstractMigration
{

    public function up(Schema $schema): void
    {
        $this->addSql('create schema notifier');
        $this->createNotificationTable();
    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table notifier.notifications');
        $this->addSql('drop schema notifier');
    }

    private function createNotificationTable(): void
    {
        $sql = <<<SQL
create table notifier.notifications
(
    text varchar
);
SQL;
        $this->addSql($sql);
    }
}
