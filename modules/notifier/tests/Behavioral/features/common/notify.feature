Feature: notify

  Scenario: notify about new product
    When message about new product "Book" creation received
    Then there should ba a notification "Product Book was created"
