<?php

declare(strict_types=1);

namespace Modules\Notifier\Tests\Behavioral\Contexts\Common;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Test\TestMessageBus;
use Grifix\Uuid\Uuid;
use Modules\Notifier\Application\Common\NotificationService;
use PHPUnit\Framework\Assert;
use Sandbox\Catalog\Messages\ProductCreatedEventMessage;

final class NotifierContext implements Context
{
    public function __construct(
        private readonly TestMessageBus $messageBus,
        private readonly NotificationService $notificationService
    ) {
    }


    /**
     * @When /^message about new product "([^"]*)" creation received$/
     */
    public function messageAboutNewProductCreationReceived(string $productName)
    {
        $this->messageBus->sendMessage(
            'sandbox.catalog',
            Uuid::createRandom(),
            new ProductCreatedEventMessage(
                Uuid::createRandom(),
                $productName,
                Money::pln(500)
            )
        );
    }

    /**
     * @Then /^there should ba a notification "([^"]*)"$/
     */
    public function thereShouldBaANotification(string $expectedMessage): void
    {
        Assert::assertContains($expectedMessage, $this->notificationService->getNotifications());
    }
}
