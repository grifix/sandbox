<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Framework\Config\ModuleServicesConfigurator;
use Modules\Notifier\Application\Common\NotificationService;

return static function (ContainerConfigurator $configurator) {
    ModuleServicesConfigurator::create($configurator)
        ->configure('notifier', 'Notifier');

    $services = $configurator->services();
    $services->defaults()->autowire()->autoconfigure();
    $services->set(NotificationService::class);
};
