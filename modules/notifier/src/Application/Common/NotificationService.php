<?php

declare(strict_types=1);

namespace Modules\Notifier\Application\Common;

use Doctrine\DBAL\Connection;

final class NotificationService
{
    private const TABLE = 'notifier.notifications';

    public function __construct(private readonly Connection $connection)
    {
    }

    public function createNotification(string $text): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'text' => $text
            ]
        );
    }

    public function getNotifications(): array
    {
        return $this->connection->createQueryBuilder()
            ->select('text')
            ->from(self::TABLE)
            ->fetchFirstColumn();
    }
}
