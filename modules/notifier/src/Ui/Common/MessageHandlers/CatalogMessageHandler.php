<?php

declare(strict_types=1);

namespace Modules\Notifier\Ui\Common\MessageHandlers;

use Modules\Notifier\Application\Common\NotificationService;
use Sandbox\Catalog\Messages\ProductCreatedEventMessage;

final class CatalogMessageHandler
{

    public function __construct(private readonly NotificationService $notificationService)
    {
    }

    public function onProductCreated(ProductCreatedEventMessage $message): void
    {
        $this->notificationService->createNotification(sprintf('Product %s was created', $message->name));
    }
}
