<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Framework\Config\ModuleServicesConfigurator;
use Modules\Draft\Application\Person\Ports\Projector\PersonProjectorInterface;
use Modules\Draft\Application\Person\Ports\Repository\PersonRepositoryInterface;
use Modules\Draft\Domain\Person\PersonOutsideInterface;
use Modules\Draft\Infrastructure\Application\Person\Ports\PersonProjector;
use Modules\Draft\Infrastructure\Application\Person\Ports\PersonRepository;
use Modules\Draft\Infrastructure\Domain\Person\PersonOutside;

return static function (ContainerConfigurator $configurator) {
    ModuleServicesConfigurator::create($configurator)
        ->configure('draft', 'Draft');

    $configurator->services()
        ->defaults()->autowire()->autoconfigure()
        ->set(PersonRepositoryInterface::class, PersonRepository::class)
        ->set(PersonOutsideInterface::class, PersonOutside::class)
        ->set(PersonProjectorInterface::class, PersonProjector::class);
};
