<?php

declare(strict_types=1);

use Modules\Draft\Ui\Person\RequestHandlers\Create\CreatePersonRequestHandler;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return static function (RoutingConfigurator $configurator) {
    $configurator->add(
        'draft-person-create',
        '/draft/person/create'
    )->controller(CreatePersonRequestHandler::class, '__invoke');
};
