<?php

declare(strict_types=1);

namespace Modules\Draft\Ui\Person\RequestHandlers\Create;

use Grifix\Uuid\Uuid;
use Modules\Draft\Application\Person\Commands\Create\CreatePersonCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreatePersonRequestHandler extends AbstractRequestHandler
{
    public function __invoke(Request $request): Response
    {
        $personId = Uuid::createRandom();
        $this->application->tell(
            new CreatePersonCommand(
                $personId,
                'John',
                'Connor'
            )
        );
        return new JsonResponse(['personId' => $personId->toString()], Response::HTTP_CREATED);
    }
}
