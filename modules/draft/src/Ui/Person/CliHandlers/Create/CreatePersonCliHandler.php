<?php

declare(strict_types=1);

namespace Modules\Draft\Ui\Person\CliHandlers\Create;

use Grifix\Framework\Ui\AbstractCliHandler;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreatePersonCliHandler extends AbstractCliHandler
{
    private const NAME = 'draft:person:create';

    protected function configure()
    {
        $this->setName(self::NAME);
        $this->setDescription('Creates a new person');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output):int{
        return self::SUCCESS;
    }
}
