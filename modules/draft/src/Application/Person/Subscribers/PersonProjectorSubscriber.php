<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Subscribers;

use Modules\Draft\Application\Person\Ports\Projector\PersonDto;
use Modules\Draft\Application\Person\Ports\Projector\PersonProjectorInterface;
use Modules\Draft\Domain\Person\Events\PersonCreatedEvent;

final class PersonProjectorSubscriber
{
    public function __construct(private readonly PersonProjectorInterface $personProjector)
    {
    }

    public function onPersonCreated(PersonCreatedEvent $event): void
    {
        $this->personProjector->add(
            new PersonDto($event->id, $event->firstName, $event->lastName)
        );
    }
}
