<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Ports\Projector;

use Grifix\Uuid\Uuid;

interface PersonProjectorInterface
{
    public function add(PersonDto $person): void;

    public function find(PersonFilter $find): array;

    public function findOne(PersonFilter $find): ?PersonDto;

    public function get(Uuid $personId): PersonDto;
}
