<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Ports\Projector\Exceptions;

use Grifix\Uuid\Uuid;

final class PersonDoesNotExistException extends \Exception
{
    public function __construct(Uuid $id)
    {
        parent::__construct(sprintf('Person with id [%s] does not exist.', $id->toString()));
    }
}
