<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Ports\Projector;

use Grifix\Uuid\Uuid;

final class PersonDto
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $firstName,
        public readonly string $lastName
    )
    {
    }
}
