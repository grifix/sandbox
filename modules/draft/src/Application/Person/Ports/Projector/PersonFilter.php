<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Ports\Projector;

use Grifix\Uuid\Uuid;

final class PersonFilter
{
    private ?Uuid $id = null;
    private ?string $firstName = null;
    private ?string $lastName = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }
}
