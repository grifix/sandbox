<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Draft\Application\Person\Ports\Repository\Exceptions\PersonDoesNotExistException;
use Modules\Draft\Domain\Person\Person;

interface PersonRepositoryInterface
{
    /**
     * @throws PersonDoesNotExistException
     */
    public function get(Uuid $personId): Person;

    public function add(Person $person): void;
}
