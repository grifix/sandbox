<?php

declare(strict_types=1);

namespace Modules\Draft\Application\Person\Commands\Create;

use Modules\Draft\Application\Person\Ports\Repository\PersonRepositoryInterface;
use Modules\Draft\Domain\Person\PersonFactory;

final class CreatePersonCommandHandler
{
    public function __construct(
        private readonly PersonRepositoryInterface $personRepository,
        private readonly PersonFactory $personFactory
    ) {
    }

    public function __invoke(CreatePersonCommand $command): void
    {
        $this->personRepository->add(
            $this->personFactory->createPerson(
                $command->id,
                $command->firstName,
                $command->lastName,
            )
        );
    }
}
