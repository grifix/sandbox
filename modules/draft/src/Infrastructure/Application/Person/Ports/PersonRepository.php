<?php

declare(strict_types=1);

namespace Modules\Draft\Infrastructure\Application\Person\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Draft\Application\Person\Ports\Repository\Exceptions\PersonDoesNotExistException;
use Modules\Draft\Application\Person\Ports\Repository\PersonRepositoryInterface;
use Modules\Draft\Domain\Person\Person;

final class PersonRepository extends AbstractRepository implements PersonRepositoryInterface
{

    public function get(Uuid $personId): Person
    {
        return $this->doGet($personId, PersonDoesNotExistException::class, Person::class);
    }

    public function add(Person $person): void
    {
        $this->doAdd($person);
    }
}
