<?php

declare(strict_types=1);

namespace Modules\Draft\Infrastructure\Application\Person\Ports;

use Doctrine\DBAL\Connection;
use Grifix\Uuid\Uuid;
use Modules\Draft\Application\Person\Ports\Projector\PersonDto;
use Modules\Draft\Application\Person\Ports\Projector\PersonFilter;
use Modules\Draft\Application\Person\Ports\Projector\PersonProjectorInterface;

final class PersonProjector implements PersonProjectorInterface
{
    private const TABLE = 'draft.person_projections';

    public function __construct(private readonly Connection $connection)
    {
    }

    public function add(PersonDto $person): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $person->id->toString(),
                '"firstName"' => $person->firstName,
                '"lastName"' => $person->lastName,
            ]
        );
    }

    public function find(PersonFilter $find): array
    {
        // TODO: Implement find() method.
    }

    public function findOne(PersonFilter $find): ?PersonDto
    {
        // TODO: Implement findOne() method.
    }

    public function get(Uuid $personId): PersonDto
    {
        // TODO: Implement get() method.
    }
}
