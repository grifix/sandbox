<?php

declare(strict_types=1);

namespace Modules\Draft\Infrastructure\Domain\Person;

use Grifix\EventStore\EventStoreInterface;
use Modules\Draft\Domain\Person\Events\PersonEventInterface;
use Modules\Draft\Domain\Person\Person;
use Modules\Draft\Domain\Person\PersonOutsideInterface;

final class PersonOutside implements PersonOutsideInterface
{

    public function __construct(private readonly EventStoreInterface $eventStore)
    {
    }

    public function publishEvent(PersonEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, Person::class, $event->getId());
    }
}
