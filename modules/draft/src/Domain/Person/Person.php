<?php

declare(strict_types=1);

namespace Modules\Draft\Domain\Person;

use Grifix\Uuid\Uuid;
use Modules\Draft\Domain\Person\Events\PersonCreatedEvent;

final class Person
{
    public function __construct(
        private readonly PersonOutsideInterface $outside,
        private readonly Uuid $id,
        private readonly string $firstName,
        private readonly string $lastName
    ) {
        $this->outside->publishEvent(
            new PersonCreatedEvent(
                $this->id,
                $this->firstName,
                $this->lastName
            )
        );
    }
}
