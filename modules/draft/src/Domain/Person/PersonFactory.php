<?php

declare(strict_types=1);

namespace Modules\Draft\Domain\Person;

use Grifix\Uuid\Uuid;

final class PersonFactory
{
    public function __construct(private readonly PersonOutsideInterface $outside)
    {
    }

    public function createPerson(Uuid $id, string $firstName, string $lastName): Person
    {
        return new Person(
            $this->outside,
            $id,
            $firstName,
            $lastName
        );
    }
}
