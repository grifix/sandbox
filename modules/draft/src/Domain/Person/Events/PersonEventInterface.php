<?php

declare(strict_types=1);

namespace Modules\Draft\Domain\Person\Events;

use Grifix\Uuid\Uuid;

interface PersonEventInterface
{
    public function getId():Uuid;
}
