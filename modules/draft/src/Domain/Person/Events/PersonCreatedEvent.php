<?php

declare(strict_types=1);

namespace Modules\Draft\Domain\Person\Events;

use Grifix\Uuid\Uuid;

final class PersonCreatedEvent implements PersonEventInterface
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $firstName,
        public readonly string $lastName
    ) {
    }

    public function getId(): Uuid
    {
        return $this->id;
    }
}
