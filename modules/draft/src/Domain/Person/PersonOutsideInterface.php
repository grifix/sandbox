<?php

declare(strict_types=1);

namespace Modules\Draft\Domain\Person;

use Modules\Draft\Domain\Person\Events\PersonEventInterface;

interface PersonOutsideInterface
{
    public function publishEvent(PersonEventInterface $event): void;
}
