<?php

declare(strict_types=1);

namespace Draft;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20220810064152 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('create schema draft');
        $this->createPersonTable();
    }

    public function down(Schema $schema): void
    {
        $this->dropPersonTable();
        $this->addSql('drop schema draft');
    }

    private function createPersonTable(): void
    {
        $sql = <<<SQL
create table if not exists draft.persons
(
    id      uuid
        constraint draft_persons_pk
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;
        $this->addSql($sql);
    }

    private function dropPersonTable():void{
        $this->addSql('drop table draft.persons');
    }
}
