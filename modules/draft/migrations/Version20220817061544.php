<?php

declare(strict_types=1);

namespace Draft;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220817061544 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $sql = <<<SQL
create table draft.person_projections
(
    id          uuid    not null
        constraint id
            primary key,
    "firstName" varchar not null,
    "lastName"  varchar not null
);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table draft.person_projections');
    }
}
