<?php

declare(strict_types=1);

namespace Modules\Cart\Ui\Cart\RequestHandlers\AddItem;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\UuidInputType;
use Grifix\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\Collection;

final class AddItemInput extends AbstractInput
{
    public function getProductId(): Uuid
    {
        return $this->getValue('productId', UuidInputType::class)->toUuid();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'productId' => UuidInputType::createConstraint(),
            ],
        ]);
    }
}
