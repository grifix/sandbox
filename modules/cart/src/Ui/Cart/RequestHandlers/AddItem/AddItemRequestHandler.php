<?php

declare(strict_types=1);

namespace Modules\Cart\Ui\Cart\RequestHandlers\AddItem;

use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Commands\AddItem\AddItemCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AddItemRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/cart/carts/{cartId}/items',
        methods: ['POST'],
    )]
    public function __invoke(Request $request, string $cartId): Response
    {
        $cartId = Uuid::createFromString($cartId);
        $input = $this->createInputFromBody($request, AddItemInput::class);
        $this->application->tell(
            new AddItemCommand(
                $cartId,
                $input->getProductId(),
            ),
        );

        return new JsonResponse();
    }
}
