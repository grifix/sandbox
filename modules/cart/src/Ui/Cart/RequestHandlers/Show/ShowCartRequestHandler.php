<?php

declare(strict_types=1);

namespace Modules\Cart\Ui\Cart\RequestHandlers\Show;

use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Projector\CartFilter;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartItemDto;
use Modules\Cart\Application\Cart\Queries\Get\GetCartQuery;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ShowCartRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/cart/carts/{cartId}',
        methods: ['GET'],
    )]
    public function __invoke(string $cartId): Response
    {
        $cartId = Uuid::createFromString($cartId);
        return new JsonResponse([
            'cart' => $this->createJson(
                $this->application->ask(
                    new GetCartQuery(CartFilter::create()->setCartId($cartId)),
                )->cart,
            ),
        ]);
    }

    private function createJson(CartDto $cart): array
    {
        $items = [];
        foreach ($cart->items as $item) {
            $items[] = $this->createItemJson($item);
        }
        return [
            'id' => $cart->id->toString(),
            'total' => [
                'amount' => $cart->total->getAmount(),
                'currency' => $cart->total->getCurrency()->getCode(),
            ],
            'items' => $items,

        ];
    }

    private function createItemJson(CartItemDto $cartItem): array
    {
        return [
            'productId' => $cartItem->productId->toString(),
            'productName' => $cartItem->productName,
            'price' => [
                'amount' => $cartItem->price->getAmount(),
                'currency' => $cartItem->price->getCurrency()->getCode(),
            ],
        ];
    }
}
