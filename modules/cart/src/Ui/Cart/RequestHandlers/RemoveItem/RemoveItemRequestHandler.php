<?php

declare(strict_types=1);

namespace Modules\Cart\Ui\Cart\RequestHandlers\RemoveItem;

use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Commands\RemoveItem\RemoveItemCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class RemoveItemRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/cart/carts/{cartId}/items/{productId}',
        methods: ['DELETE'],
    )]
    public function __invoke(Request $request, string $cartId, string $productId): Response
    {
        $cartId = Uuid::createFromString($cartId);
        $productId = Uuid::createFromString($productId);

        $this->application->tell(
            new RemoveItemCommand(
                $cartId,
                $productId,
            ),
        );

        return new JsonResponse();
    }
}
