<?php

declare(strict_types=1);

namespace Modules\Cart\Ui\Cart\RequestHandlers\Create;

use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Commands\Create\CreateCartCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateCartRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/cart/carts',
        methods: ['POST'],
    )]
    public function __invoke(): Response
    {
        $cartId = Uuid::createRandom();
        $this->application->tell(new CreateCartCommand($cartId));
        return new JsonResponse(
            [
                'cartId' => $cartId->toString(),
            ],
            Response::HTTP_CREATED,
        );
    }
}
