<?php

declare(strict_types=1);

namespace Modules\Cart\Ui;

use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Modules\Cart\Application\Cart\Ports\Projector\Exceptions\CartDoesNotExistException as CartDtoDoesNotExistException;
use Modules\Cart\Application\Cart\Ports\Repository\Exceptions\CartDoesNotExistException;
use Modules\Cart\Domain\Cart\Exceptions\ItemDoesNotExistException;

final class CartExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(CartDoesNotExistException::class, httpCode: 404),
            ExceptionConverter::create(CartDtoDoesNotExistException::class, httpCode: 404),
            ExceptionConverter::create(ItemDoesNotExistException::class, httpCode: 404),
            ExceptionConverter::create('Modules\Cart\Domain', httpCode: 400),
        ];
    }
}
