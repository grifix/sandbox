<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart\Item;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class CartItem
{
    public function __construct(public readonly Uuid $productId, public readonly Money $price)
    {
    }
}
