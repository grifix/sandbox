<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart\Exceptions;

final class ItemLimitExceedException extends \Exception
{

    public function __construct(int $limit)
    {
        parent::__construct(sprintf('Cart cannot contain more than %s items!', $limit));
    }
}
