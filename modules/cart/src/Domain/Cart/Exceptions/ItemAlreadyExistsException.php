<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart\Exceptions;

use Grifix\Uuid\Uuid;

final class ItemAlreadyExistsException extends \Exception
{

    public function __construct(Uuid $productId)
    {
        parent::__construct(
            sprintf(
                'Item with product id [%s] already exists in the cart!',
                $productId->toString()
            )
        );
    }
}
