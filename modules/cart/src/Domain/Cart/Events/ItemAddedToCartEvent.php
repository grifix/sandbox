<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart\Events;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ItemAddedToCartEvent implements CartEventInterface
{

    public function __construct(
        public readonly Uuid $cartId,
        public readonly Uuid $productId,
        public readonly Money $productPrice,
        public readonly Money $total
    )
    {
    }

    public function getCartId(): Uuid
    {
        return $this->cartId;
    }
}
