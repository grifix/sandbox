<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart\Events;

use Grifix\Uuid\Uuid;

interface CartEventInterface
{
    public function getCartId(): Uuid;
}
