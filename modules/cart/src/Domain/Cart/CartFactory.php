<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart;

use Grifix\Uuid\Uuid;

final class CartFactory
{

    public function __construct(private readonly CartOutsideInterface $outside)
    {
    }

    public function createCart(Uuid $id): Cart
    {
        return new Cart($this->outside, $id);
    }
}
