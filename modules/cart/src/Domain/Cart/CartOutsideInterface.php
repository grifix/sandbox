<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\CartEventInterface;

interface CartOutsideInterface
{
    public function getProductPrice(Uuid $productId): Money;

    public function getItemLimit(): int;

    public function publishEvent(CartEventInterface $event): void;
}
