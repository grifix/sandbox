<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\CartCreatedEvent;
use Modules\Cart\Domain\Cart\Events\ItemAddedToCartEvent;
use Modules\Cart\Domain\Cart\Events\ItemRemovedFromCartEvent;
use Modules\Cart\Domain\Cart\Exceptions\ItemAlreadyExistsException;
use Modules\Cart\Domain\Cart\Exceptions\ItemDoesNotExistException;
use Modules\Cart\Domain\Cart\Exceptions\ItemLimitExceedException;
use Modules\Cart\Domain\Cart\Item\CartItem;

final class Cart
{
    /**
     * @var CartItem[]
     */
    private array $items = [];

    private Money $total;

    public function __construct(private readonly CartOutsideInterface $outside, private readonly Uuid $id)
    {
        $this->total = Money::pln(0);
        $this->outside->publishEvent(new CartCreatedEvent($this->id, $this->total));
    }

    public function addItem(Uuid $productId): void
    {
        $this->assertItemLimitDoesNotExceeded();
        $this->assertHasNoItem($productId);
        $productPrice = $this->outside->getProductPrice($productId);
        $this->items[] = new CartItem($productId, $productPrice);
        $this->total = $this->total->add($productPrice);
        $this->outside->publishEvent(
            new ItemAddedToCartEvent(
                $this->id,
                $productId,
                $productPrice,
                $this->total
            )
        );
    }

    public function removeItem(Uuid $productId): void
    {
        $this->assertHasItem($productId);
        foreach ($this->items as $i => $item) {
            if ($item->productId->isEqualTo($productId)) {
                unset($this->items[$i]);
                $this->total = $this->total->subtract($item->price);
                $this->outside->publishEvent(
                    new ItemRemovedFromCartEvent(
                        $this->id,
                        $productId,
                        $this->total
                    )
                );
                return;
            }
        }
    }

    private function assertHasItem(Uuid $productId): void
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                return;
            }
        }
        throw new ItemDoesNotExistException($productId);
    }

    private function assertItemLimitDoesNotExceeded(): void
    {
        if (count($this->items) >= $this->outside->getItemLimit()) {
            throw new ItemLimitExceedException($this->outside->getItemLimit());
        }
    }

    private function assertHasNoItem(Uuid $productId): void
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                throw new ItemAlreadyExistsException($productId);
            }
        }
    }
}
