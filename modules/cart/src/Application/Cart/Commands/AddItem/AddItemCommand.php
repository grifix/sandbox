<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\AddItem;

use Grifix\Uuid\Uuid;

final class AddItemCommand
{
    public function __construct(
        public readonly Uuid $cartId,
        public readonly Uuid $productId
    ) {
    }
}
