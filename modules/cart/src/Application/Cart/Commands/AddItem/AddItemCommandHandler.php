<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\AddItem;

use Modules\Cart\Application\Cart\Ports\Repository\CartRepositoryInterface;

final class AddItemCommandHandler
{
    public function __construct(
        private readonly CartRepositoryInterface $repository
    ) {
    }

    public function __invoke(AddItemCommand $command): void
    {
        $this->repository->get($command->cartId)->addItem($command->productId);
    }
}
