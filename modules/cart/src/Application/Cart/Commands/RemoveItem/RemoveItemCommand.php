<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\RemoveItem;

use Grifix\Uuid\Uuid;

final class RemoveItemCommand
{
    public function __construct(
        public readonly Uuid $cartId,
        public readonly Uuid $productId
    )
    {
    }
}
