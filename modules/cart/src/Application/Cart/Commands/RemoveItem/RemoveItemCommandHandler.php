<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\RemoveItem;

use Modules\Cart\Application\Cart\Ports\Repository\CartRepositoryInterface;

final class RemoveItemCommandHandler
{
    public function __construct(
        private readonly CartRepositoryInterface $repository
    ) {
    }

    public function __invoke(RemoveItemCommand $command): void
    {
        $this->repository->get($command->cartId)->removeItem($command->productId);
    }
}
