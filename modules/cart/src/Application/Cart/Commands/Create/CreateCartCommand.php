<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\Create;

use Grifix\Uuid\Uuid;

final class CreateCartCommand
{
    public function __construct(
        public readonly Uuid $cartId
    ) {
    }
}
