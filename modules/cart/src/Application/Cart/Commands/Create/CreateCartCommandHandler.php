<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Commands\Create;

use Modules\Cart\Application\Cart\Ports\Repository\CartRepositoryInterface;
use Modules\Cart\Domain\Cart\CartFactory;

final class CreateCartCommandHandler
{
    public function __construct(
        private readonly CartRepositoryInterface $repository,
        private readonly CartFactory $factory
    ) {
    }

    public function __invoke(CreateCartCommand $command): void
    {
        $this->repository->add($this->factory->createCart($command->cartId));
    }
}
