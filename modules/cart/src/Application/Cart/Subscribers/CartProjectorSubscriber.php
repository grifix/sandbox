<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Subscribers;

use Modules\Cart\Application\Cart\Ports\Projector\CartProjectorInterface;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;
use Modules\Cart\Domain\Cart\Events\CartCreatedEvent;
use Modules\Cart\Domain\Cart\Events\ItemAddedToCartEvent;
use Modules\Cart\Domain\Cart\Events\ItemRemovedFromCartEvent;

final readonly class CartProjectorSubscriber
{
    public function __construct(private CartProjectorInterface $projector)
    {
    }

    public function onCartCreated(CartCreatedEvent $event): void
    {
        $this->projector->createCart(
            new CartDto(
                $event->cartId,
                $event->total,
                [],
            ),
        );
    }

    public function onItemAdded(ItemAddedToCartEvent $event): void
    {
        $this->projector->addItem($event->cartId, $event->productId, $event->productPrice, $event->total);
    }

    public function onItemRemoved(ItemRemovedFromCartEvent $event): void
    {
        $this->projector->removeItem($event->cartId, $event->productId, $event->total);
    }
}
