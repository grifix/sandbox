<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Repository\Exceptions;

use Grifix\Uuid\Uuid;

final class CartDoesNotExistException extends \Exception
{
    public function __construct(Uuid $cartId)
    {
        parent::__construct(sprintf('Cart with id [%s] does not exist!', $cartId->toString()));
    }
}
