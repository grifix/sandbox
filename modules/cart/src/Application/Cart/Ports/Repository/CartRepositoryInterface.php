<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Repository\Exceptions\CartDoesNotExistException;
use Modules\Cart\Domain\Cart\Cart;

interface CartRepositoryInterface
{
    public function add(Cart $cart): void;

    /**
     * @throws CartDoesNotExistException
     */
    public function get(Uuid $id): Cart;
}
