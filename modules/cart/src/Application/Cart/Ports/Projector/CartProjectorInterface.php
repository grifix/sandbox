<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Projector;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;

interface CartProjectorInterface
{
    public function createCart(CartDto $cart): void;

    public function addItem(Uuid $cartId, Uuid $productId, Money $productPrice, Money $total): void;

    public function removeItem(Uuid $cartId, Uuid $productId, Money $total): void;

    /**
     * @return CartDto[]
     */
    public function find(CartFilter $filter): array;

    public function getOne(CartFilter $filter): CartDto;

    public function findOne(CartFilter $filter): ?CartDto;
}
