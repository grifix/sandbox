<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Projector;

use Grifix\Uuid\Uuid;

final class CartFilter
{
    private ?Uuid $cartId = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self;
    }

    public function getCartId(): ?Uuid
    {
        return $this->cartId;
    }

    public function setCartId(?Uuid $cartId): self
    {
        $this->cartId = $cartId;
        return $this;
    }
}
