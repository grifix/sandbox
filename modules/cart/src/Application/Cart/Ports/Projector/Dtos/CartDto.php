<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Projector\Dtos;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class CartDto
{
    /**
     * @param CartItemDto[] $items
     */
    public function __construct(
        public readonly Uuid $id,
        public readonly Money $total,
        public readonly array $items
    ) {
    }
}
