<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Projector\Dtos;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class CartItemDto
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $productName,
        public readonly Money $price
    ) {
    }
}
