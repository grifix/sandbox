<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Ports\Projector\Exceptions;

use Grifix\Uuid\Uuid;

final class CartDoesNotExistException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Cart does not exist!');
    }
}
