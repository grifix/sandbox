<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Queries\Get;

use Modules\Cart\Application\Cart\Ports\Projector\CartFilter;

final class GetCartQuery
{

    public function __construct(public readonly CartFilter $filter)
    {
    }
}
