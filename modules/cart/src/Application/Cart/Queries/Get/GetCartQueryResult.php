<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Queries\Get;

use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;

final class GetCartQueryResult
{

    public function __construct(public readonly CartDto $cart)
    {
    }
}
