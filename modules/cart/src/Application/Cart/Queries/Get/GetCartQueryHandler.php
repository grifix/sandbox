<?php

declare(strict_types=1);

namespace Modules\Cart\Application\Cart\Queries\Get;

use Modules\Cart\Application\Cart\Ports\Projector\CartProjectorInterface;

final class GetCartQueryHandler
{
    public function __construct(private readonly CartProjectorInterface $projector)
    {
    }

    public function __invoke(GetCartQuery $query): GetCartQueryResult
    {
        return new GetCartQueryResult(
            $this->projector->getOne($query->filter)
        );
    }
}
