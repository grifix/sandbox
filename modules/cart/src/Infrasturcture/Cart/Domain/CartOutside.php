<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Cart\Domain;

use Grifix\EventStore\EventStoreInterface;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Cart;
use Modules\Cart\Domain\Cart\CartOutsideInterface;
use Modules\Cart\Domain\Cart\Events\CartEventInterface;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;

final class CartOutside implements CartOutsideInterface
{
    public function __construct(
        private readonly int $itemLimit,
        private readonly CatalogConnectorInterface $catalogConnector,
        private readonly EventStoreInterface $eventStore
    ) {
    }

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->catalogConnector->getProductPrice($productId);
    }

    public function getItemLimit(): int
    {
        return $this->itemLimit;
    }

    public function publishEvent(CartEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, Cart::class, $event->getCartId());
    }
}
