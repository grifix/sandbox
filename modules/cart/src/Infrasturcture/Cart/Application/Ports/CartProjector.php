<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Cart\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Money\Money\Money;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Projector\CartFilter;
use Modules\Cart\Application\Cart\Ports\Projector\CartProjectorInterface;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartItemDto;
use Modules\Cart\Application\Cart\Ports\Projector\Exceptions\CartDoesNotExistException;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;

final class CartProjector implements CartProjectorInterface
{

    private const TABLE = 'cart.cart_projections';

    public function __construct(
        private readonly Connection $connection,
        private readonly CatalogConnectorInterface $catalogConnector,
        private readonly NormalizerInterface $normalizer
    ) {
    }

    public function createCart(CartDto $cart): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $cart->id->toString(),
                'total_amount' => $cart->total->getAmount(),
                'total_currency' => $cart->total->getCurrency()->getCode(),
                'items' => json_encode([])
            ]
        );
    }

    public function addItem(Uuid $cartId, Uuid $productId, Money $productPrice, Money $total): void
    {
        $row = $this->createQueryBuilder(CartFilter::create()->setCartId($cartId))->fetchAssociative();
        $items = json_decode($row['items'], true);
        $items[] = $this->normalizer->normalize(
            new CartItemDto(
                $productId,
                $this->catalogConnector->getProductName($productId),
                $productPrice
            )
        );
        $this->connection->update(
            self::TABLE,
            [
                'items' => json_encode($items),
                'total_amount' => $total->getAmount(),
                'total_currency' => $total->getCurrency()->getCode()
            ],
            [
                'id' => $cartId->toString()
            ]
        );
    }

    public function removeItem(Uuid $cartId, Uuid $productId, Money $total): void
    {
        $row = $this->createQueryBuilder(CartFilter::create()->setCartId($cartId))->fetchAssociative();
        $items = json_decode($row['items'], true);
        foreach ($items as $i => $item) {
            if ($item['productId']['value'] == $productId->toString()) {
                unset($items[$i]);
            }
        }
        $this->connection->update(
            self::TABLE,
            [
                'items' => json_encode($items),
                'total_amount' => $total->getAmount(),
                'total_currency' => $total->getCurrency()->getCode()
            ],
            [
                'id' => $cartId->toString()
            ]
        );
    }

    public function find(CartFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $this->createDto($row);
        }
        return $result;
    }

    public function getOne(CartFilter $filter): CartDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            throw new CartDoesNotExistException();
        }
        return $this->createDto($row);
    }

    public function findOne(CartFilter $filter): ?CartDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            return null;
        }
        return $this->createDto($row);
    }

    private function createQueryBuilder(CartFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('*')->from(self::TABLE);

        if ($filter->getCartId()) {
            $result->andWhere('id = :id')->setParameter('id', $filter->getCartId()->toString());
        }

        return $result;
    }

    private function createDto(array $row): CartDto
    {
        $items = [];
        foreach (json_decode($row['items'], true) as $item) {
            $items[] = $this->normalizer->denormalize($item);
        }
        return new CartDto(
            Uuid::createFromString($row['id']),
            Money::withCurrency($row['total_amount'], $row['total_currency']),
            $items
        );
    }
}
