<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Cart\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Repository\CartRepositoryInterface;
use Modules\Cart\Application\Cart\Ports\Repository\Exceptions\CartDoesNotExistException;
use Modules\Cart\Domain\Cart\Cart;

final class CartRepository extends AbstractRepository implements CartRepositoryInterface
{
    public function add(Cart $cart): void
    {
        $this->doAdd($cart);
    }

    public function get(Uuid $id): Cart
    {
       return $this->doGet($id, CartDoesNotExistException::class, Cart::class);
    }
}
