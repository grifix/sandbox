<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Common\Connectors\Catalog;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

interface CatalogConnectorInterface
{
    public function getProductPrice(Uuid $productId): Money;

    public function getProductName(Uuid $productId): string;
}
