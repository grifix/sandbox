<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Common\Connectors\Catalog;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Sandbox\Catalog\CatalogClientInterface;

final class CatalogConnector implements CatalogConnectorInterface
{
    public function __construct(private readonly CatalogClientInterface $client)
    {
    }

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->client->getProduct($productId)->price;
    }

    public function getProductName(Uuid $productId): string
    {
        return $this->client->getProduct($productId)->name;
    }
}
