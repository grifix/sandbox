<?php

declare(strict_types=1);

namespace Cart;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220915070050 extends AbstractMigration
{

    public function up(Schema $schema): void
    {
        $this->addSql('create schema cart');
        $this->createCartTable();
        $this->createCartProjectionTable();
    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table cart.carts');
        $this->addSql('drop table cart.cart_projections');
        $this->addSql('drop schema cart');
    }

    private function createCartTable(): void
    {
        $sql = <<<SQL
create table if not exists cart.carts
(
    id      uuid
        constraint cart_carts_pk
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;
        $this->addSql($sql);
    }

    protected function createCartProjectionTable():void{
        $sql = <<<SQL
create table cart.cart_projections
(
    id             uuid    not null
        constraint cart_projections_pk
            primary key,
    total_amount   bigint  not null,
    total_currency varchar not null,
    items          jsonb   not null
);
SQL;
        $this->addSql($sql);
    }
}
