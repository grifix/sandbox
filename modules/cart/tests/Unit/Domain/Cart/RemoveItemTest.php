<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Unit\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\ItemRemovedFromCartEvent;
use Modules\Cart\Domain\Cart\Exceptions\ItemDoesNotExistException;
use Modules\Cart\Tests\Unit\Domain\Cart\Builders\CartBuilder;
use PHPUnit\Framework\TestCase;

final class RemoveItemTest extends TestCase
{
    public function testItRemoves(): void
    {
        $cartId = Uuid::createFromString('f0adbecf-90cb-446d-9de3-ae1c5264cca2');
        $productId = Uuid::createFromString('0d0426a6-6c18-4df5-8630-818f12bd0864');
        $cart = CartBuilder::create()
            ->setId($cartId)
            ->withAddedItem($productId)
            ->build();

        $cart->removeItem($productId);
        $events = EventCollector::getEvents($cart);
        self::assertCount(1, $events);
        self::assertEquals(
            new ItemRemovedFromCartEvent(
                $cartId,
                $productId,
                Money::pln(0)
            ),
            array_shift($events)
        );
    }

    /**
     * @dataProvider failsDataProvider
     */
    public function testItFailsToRemove(
        CartBuilder $builder,
        Uuid $productId,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $builder->build()->removeItem($productId);
    }

    public function failsDataProvider(): array
    {
        return [
            'not existed item' => [
                CartBuilder::create(),
                Uuid::createFromString('261c403f-d746-4c72-a866-d9d72873845d'),
                ItemDoesNotExistException::class,
                'Item with product id [261c403f-d746-4c72-a866-d9d72873845d] does not exist in the cart!'
            ]
        ];
    }
}
