<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Unit\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\ItemAddedToCartEvent;
use Modules\Cart\Domain\Cart\Exceptions\ItemAlreadyExistsException;
use Modules\Cart\Domain\Cart\Exceptions\ItemLimitExceedException;
use Modules\Cart\Tests\Unit\Domain\Cart\Builders\CartBuilder;
use Modules\Cart\Tests\Unit\Domain\Cart\Builders\CartOutsideBuilder;
use PHPUnit\Framework\TestCase;

final class AddItemTest extends TestCase
{
    public function testItAddsItem(): void
    {
        $cartId = Uuid::createFromString('911e5f07-4e70-4a44-ba85-917e6c085227');
        $product1Id = Uuid::createFromString('e08accb2-44b6-445b-a578-34d8f9412067');
        $product2Id = Uuid::createFromString('2adb7698-3711-454d-9506-0a8ce2ffa147');

        $cart = CartBuilder::create()
            ->setId($cartId)
            ->setOutside(
                CartOutsideBuilder::create()
                    ->setProductPrice(Money::pln(10000))
                    ->build()
            )->build();

        $cart->addItem($product1Id);
        $cart->addItem($product2Id);

        $events = EventCollector::getEvents($cart);
        self::assertCount(2, $events);
        self::assertEquals(
            new ItemAddedToCartEvent(
                $cartId,
                $product1Id,
                Money::pln(10000),
                Money::pln(10000)
            ),
            array_shift($events)
        );
        self::assertEquals(
            new ItemAddedToCartEvent(
                $cartId,
                $product2Id,
                Money::pln(10000),
                Money::pln(20000)
            ),
            array_shift($events)
        );
    }

    /**
     * @dataProvider failsDataProvider
     */
    public function testItFailsToAddItem(
        CartBuilder $builder,
        Uuid $productId,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $builder->build()->addItem($productId);
    }

    public function failsDataProvider(): array
    {
        return [
            'items limit exceeded' => [
                CartBuilder::create()
                    ->withAddedItemQuantity(3)
                    ->setOutside(
                        CartOutsideBuilder::create()
                            ->setItemLimit(3)
                            ->build()
                    ),
                Uuid::createRandom(),
                ItemLimitExceedException::class,
                'Cart cannot contain more than 3 items!'
            ],
            'product already added' => [
                CartBuilder::create()
                    ->withAddedItem(Uuid::createFromString('f53fabc4-f08e-4f4f-b165-b173886d3af3'))
                    ->setOutside(
                        CartOutsideBuilder::create()
                            ->setItemLimit(3)
                            ->build()
                    ),
                Uuid::createFromString('f53fabc4-f08e-4f4f-b165-b173886d3af3'),
                ItemAlreadyExistsException::class,
                'Item with product id [f53fabc4-f08e-4f4f-b165-b173886d3af3] already exists in the cart!'
            ]
        ];
    }
}
