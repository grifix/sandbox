<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Unit\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Cart;
use Modules\Cart\Domain\Cart\Events\CartCreatedEvent;
use Modules\Cart\Tests\Unit\Domain\Cart\Builders\CartOutsideBuilder;
use PHPUnit\Framework\TestCase;

final class CreateCartTest extends TestCase
{
    public function testItCreates(): void
    {
        $cart = new Cart(
            CartOutsideBuilder::create()->build(),
            Uuid::createFromString('9dc72b10-cdfe-44b3-89a7-b911034961a6')
        );

        $events = EventCollector::getEvents($cart);
        self::assertCount(1, $events);
        self::assertEquals(
            new CartCreatedEvent(
                Uuid::createFromString('9dc72b10-cdfe-44b3-89a7-b911034961a6'),
                Money::pln(0)
            ),
            array_shift($events)
        );
    }
}
