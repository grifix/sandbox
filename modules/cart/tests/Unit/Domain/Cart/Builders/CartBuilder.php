<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Unit\Domain\Cart\Builders;

use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Cart;
use Modules\Cart\Domain\Cart\CartOutsideInterface;

final class CartBuilder
{
    private Uuid $id;
    private CartOutsideInterface $outside;
    private ?int $addedItemQuantity = null;
    private ?Uuid $addedItemProductId = null;

    private function __construct()
    {
        $this->id = Uuid::createFromString('8d984cf8-d86c-45be-9051-81fe522f0261');
        $this->outside = CartOutsideBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function setOutside(CartOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function withAddedItem(Uuid $addedItemProductId): self
    {
        $this->addedItemProductId = $addedItemProductId;
        return $this;
    }

    public function withAddedItemQuantity(int $addedItemQuantity): self
    {
        $this->addedItemQuantity = $addedItemQuantity;
        return $this;
    }

    public function build(): Cart
    {
        $result = new Cart($this->outside, $this->id);
        if (null !== $this->addedItemQuantity) {
            for ($i = 0; $i < $this->addedItemQuantity; $i++) {
                $result->addItem(Uuid::createRandom());
            }
        }
        if (null !== $this->addedItemProductId) {
            $result->addItem($this->addedItemProductId);
        }
        EventCollector::clearEvents($result);
        return $result;
    }
}
