<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Unit\Domain\Cart\Builders;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Modules\Cart\Domain\Cart\CartOutsideInterface;

final class CartOutsideBuilder
{
    private Money $productPrice;

    private int $itemLimit = 3;

    private function __construct()
    {
        $this->productPrice = Money::pln(1000);
    }

    public static function create(): self
    {
        return new self;
    }

    public function setProductPrice(Money $productPrice): self
    {
        $this->productPrice = $productPrice;
        return $this;
    }

    public function setItemLimit(int $itemLimit): self
    {
        $this->itemLimit = $itemLimit;
        return $this;
    }

    public function build(): CartOutsideInterface
    {
        $result = \Mockery::mock(CartOutsideInterface::class);
        $result->allows('getItemLimit')->andReturn($this->itemLimit);
        $result->allows('getProductPrice')->andReturn($this->productPrice);
        EventCollector::injectTo($result);
        return $result;
    }
}
