<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Fixtures;

use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;
use Modules\Cart\Tests\Behavioral\Stubs\CatalogConnectorStub;

final class ProductFixture extends AbstractFixture
{
    private Uuid $id;
    private string $name;
    private Money $price;

    protected function init(): void
    {
        $this->id = Uuid::createRandom();
        $this->name = uniqid('Product_');
        $this->price = Money::pln(1000);
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }

    public function setPrice(Money $price): self
    {
        $this->price = $price;
        return $this;
    }
    protected function create(): void
    {
        /** @var CatalogConnectorStub $connector */
        $connector = $this->container->get(CatalogConnectorInterface::class);
        $connector->createProduct($this);
    }
}
