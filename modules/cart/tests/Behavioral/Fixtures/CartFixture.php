<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Fixtures;

use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Commands\AddItem\AddItemCommand;
use Modules\Cart\Application\Cart\Commands\Create\CreateCartCommand;

final class CartFixture extends AbstractFixture
{
    private Uuid $id;

    private ?int $addedItemQuantity = null;

    /**
     * @var string[]
     */
    private array $addedProductsAliases = [];

    /**
     * @var Uuid[]
     */
    private array $addedProductsIds = [];

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function withAddedItemQuantity(int $addedItemQuantity): self
    {
        $this->addedItemQuantity = $addedItemQuantity;
        return $this;
    }

    /**
     * @param string[] $addedProductsAliases
     */
    public function withAddedProducts(array $addedProductsAliases): self
    {
        $this->addedProductsAliases = $addedProductsAliases;
        return $this;
    }

    /**
     * @param Uuid[] $addedProductsIds
     */
    public function withAddedProductsIds(array $addedProductsIds): self
    {
        $this->addedProductsIds = $addedProductsIds;
        return $this;
    }

    protected function init(): void
    {
        $this->id = Uuid::createRandom();
    }

    protected function create(): void
    {
        $this->application->tell(new CreateCartCommand($this->id));
    }

    protected function afterCreate(): void
    {
        if (null !== $this->addedItemQuantity) {
            for ($i = 0; $i < $this->addedItemQuantity; $i++) {
                $product = $this->fixtureFactory->createFixture(ProductFixture::class)->build();
                $this->application->tell(new AddItemCommand($this->id, $product->getId()));
            }
        }

        if (!empty($this->addedProductsAliases)) {
            foreach ($this->addedProductsAliases as $addedProductsAlias) {
                $this->application->tell(
                    new AddItemCommand(
                        $this->id,
                        $this->objectRegistry->getObject(ProductFixture::class, $addedProductsAlias)->getId(),
                    ),
                );
            }
        }

        if (!empty($this->addedProductsIds)) {
            foreach ($this->addedProductsIds as $addedProductsId) {
                $this->application->tell(
                    new AddItemCommand(
                        $this->id,
                        $addedProductsId,
                    ),
                );
            }
        }
    }
}
