<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Stubs;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;

final class CatalogConnectorStub implements CatalogConnectorInterface
{
    /**
     * @var ProductFixture[]
     */
    private array $products = [];

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->findProduct($productId)->getPrice();
    }

    public function getProductName(Uuid $productId): string
    {
        return $this->findProduct($productId)->getName();
    }

    private function findProduct(Uuid $productId): ProductFixture
    {
        foreach ($this->products as $product) {
            if ($product->getId()->isEqualTo($productId)) {
                return $product;
            }
        }
        throw new \Exception('Product does not exist!');
    }

    public function createProduct(ProductFixture $product): void
    {
        $this->products[] = $product;
    }
}
