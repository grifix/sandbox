Feature: show cart

  Scenario: show cart
    Given there is a cart "My cart" with id "6ef5e5d4-beeb-4505-a70d-93743bb9b7a9" and products:
      | id                                   | name                  | price   |
      | ecdbc5e1-d998-4150-86bb-1581e3470e79 | Foundation            | 100 PLN |
      | 1923fef8-edb3-4a66-a6a3-5d799edb1d72 | Foundation and Empire | 110 PLN |
      | 8fd51def-c3f0-4676-8230-3b9be3cb1ed0 | Second Foundation     | 105 PLN |

    When I show the cart "My cart"
    Then I should see the following response:
    """
    cart:
      id: 6ef5e5d4-beeb-4505-a70d-93743bb9b7a9
      total:
        amount: 31500
        currency: PLN
      items:
        - productId: ecdbc5e1-d998-4150-86bb-1581e3470e79
          productName: Foundation
          price:
            amount: 10000
            currency: PLN
        - productId: 1923fef8-edb3-4a66-a6a3-5d799edb1d72
          productName: Foundation and Empire
          price:
            amount: 11000
            currency: PLN
        - productId: 8fd51def-c3f0-4676-8230-3b9be3cb1ed0
          productName: Second Foundation
          price:
            amount: 10500
            currency: PLN
    """

  Scenario: show not existing cart
    When I show not existing cart
    Then there should be an error that "Cart does not exist!"
