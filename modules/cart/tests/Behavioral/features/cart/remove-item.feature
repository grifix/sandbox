Feature: remove product

  Background:
    Given there are the following products:
      | alias     | price    |
      | Book      | 100 PLN  |
      | Magazine  | 5 PLN    |
      | Newspaper | 0.60 PLN |
    And there is a cart "My cart" with items:
    """
    - Book
    - Magazine
    - Newspaper
    """

  Scenario: remove product from cart
    When I remove the product "Magazine" from the cart "My cart"
    Then cart "My cart" should contain the following products:
    """
    - Book
    - Newspaper
    """
    And the cart "My cart" total should be "100.60 PLN"

  Scenario: remove non existing product from cart
    Given there is a product "Calendar"
    When I remove the product "Calendar" from the cart "My cart"
    Then there should be an error that "Item with product id [%s] does not exist in the cart!"

  Scenario: remove product from not existing cart
    When I remove product from not existing cart
    Then there should be an error that "Cart with id [%s] does not exist!"
