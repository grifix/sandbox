Feature: add item

  Background:
    Given there is a product "Book" with price "100 PLN"
    And there is a product "Book2" with price "120 PLN"

  Scenario: add item to the cart
    Given there is a cart "My cart"
    When I add product "Book" to the cart "My cart"
    And I add product "Book2" to the cart "My cart"
    Then cart "My cart" should contain the following products:
    """
    - Book
    - Book2
    """
    And the cart "My cart" total should be "220 PLN"

  Scenario: add more then 3 items
    Given the item cart limit is 3
    And there is a cart "My cart" with 3 items
    When I add product to the cart "My cart"
    Then there should be an error that "Cart cannot contain more than 3 items!"

  Scenario: add existing product
    Given there is a cart "My cart" with product "Book"
    When I add product "Book" to the cart "My cart"
    Then there should be an error that "Item with product id [%s] already exists in the cart!"

  Scenario: add product to the not existing cart
    When I add product to the not existing cart
    Then there should be an error that "Cart with id [%s] does not exist!"
