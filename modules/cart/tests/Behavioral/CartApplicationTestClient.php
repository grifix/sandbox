<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Projector\CartFilter;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;
use Modules\Cart\Application\Cart\Queries\Get\GetCartQuery;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;

final readonly class CartApplicationTestClient
{
    public function __construct(
        private ApplicationInterface $application,
        private ObjectRegistryInterface $objectRegistry,
    ) {
    }

    public function getCartById(Uuid $cartId): CartDto
    {
        return $this->application->ask(new GetCartQuery(CartFilter::create()->setCartId($cartId)))->cart;
    }

    public function getCartByAlias(string $cartAlias): CartDto
    {
        return $this->getCartById($this->objectRegistry->getObject(CartFixture::class, $cartAlias)->getId());
    }
}
