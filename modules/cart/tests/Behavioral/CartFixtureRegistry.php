<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral;

use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;

final class CartFixtureRegistry
{
    public function __construct(private readonly ObjectRegistryInterface $objectRegistry)
    {
    }

    public function getCartFixture(string $alias): CartFixture
    {
        return $this->objectRegistry->getObject(CartFixture::class, $alias);
    }


    public function getProductFixture(string $alias): ProductFixture
    {
        return $this->objectRegistry->getObject(ProductFixture::class, $alias);
    }
}
