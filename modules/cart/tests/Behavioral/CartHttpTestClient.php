<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Test\TestHttpClient;
use Grifix\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final class CartHttpTestClient
{
    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }

    public function createCart(): Response
    {
        return $this->httpClient->post(
            '/api/v1/cart/carts'
        );
    }

    public function addItem(Uuid $cartId, array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/cart/carts/' . $cartId->toString() . '/items',
            json_encode($payload)
        );
    }

    public function removeItem(Uuid $cartId, Uuid $productId): Response
    {
        return $this->httpClient->delete(
            sprintf(
                '/api/v1/cart/carts/%s/items/%s',
                $cartId->toString(),
                $productId->toString()
            )
        );
    }

    public function showCart(Uuid $cartId): Response
    {
        return $this->httpClient->get(
            sprintf(
                '/api/v1/cart/carts/%s',
                $cartId->toString(),
            )
        );
    }

    public function getLastResponse(): Response
    {
        return $this->httpClient->getLastResponse();
    }

    public function getLastResponseJson(): ArrayWrapper
    {
        return $this->httpClient->getLastResponseJson();
    }

    public function getLastResponseStatusCode(): int
    {
        return $this->httpClient->getLastResponse()->getStatusCode();
    }
}
