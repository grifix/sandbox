<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Contexts\Cart\Http;

use Behat\Behat\Context\Context;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Uuid\Uuid;
use Modules\Cart\Tests\Behavioral\CartFixtureRegistry;
use Modules\Cart\Tests\Behavioral\CartHttpTestClient;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final class ShowCartFeatureHttpContext implements Context
{
    public function __construct(
        private readonly CartHttpTestClient $httpClient,
        private readonly CartFixtureRegistry $fixtureRegistry
    ) {
    }


    /**
     * @When /^I show the cart "([^"]*)"$/
     */
    public function iShowTheCart(string $cartAlias)
    {
        $this->httpClient->showCart($this->fixtureRegistry->getCartFixture($cartAlias)->getId());
    }

    /**
     * @Then I should see the following response:
     */
    public function iShouldSeeTheFollowingItemsInTheCart(string $expectedResponse): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(Yaml::parse($expectedResponse), $this->httpClient->getLastResponseJson()->getWrapped());
    }

    /**
     * @When /^I show not existing cart$/
     */
    public function iShowNotExistingCart()
    {
        $this->httpClient->showCart(Uuid::createRandom());
    }
}
