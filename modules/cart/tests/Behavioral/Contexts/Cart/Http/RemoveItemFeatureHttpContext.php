<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Contexts\Cart\Http;

use Behat\Behat\Context\Context;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Uuid\Uuid;
use Modules\Cart\Tests\Behavioral\CartFixtureRegistry;
use Modules\Cart\Tests\Behavioral\CartHttpTestClient;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;

final class RemoveItemFeatureHttpContext implements Context
{
    public function __construct(
        private readonly CartHttpTestClient $httpClient,
        private readonly CartFixtureRegistry $fixtureRegistry,
        private readonly FixtureFactory $fixtureFactory
    ) {
    }


    /**
     * @When /^I remove the product "([^"]*)" from the cart "([^"]*)"$/
     */
    public function iRemoveTheProductFromTheCart(string $productAlias, string $cartAlias): void
    {
        $this->httpClient->removeItem(
            $this->fixtureRegistry->getCartFixture($cartAlias)->getId(),
            $this->fixtureRegistry->getProductFixture($productAlias)->getId()
        );
    }

    /**
     * @When /^I remove product from not existing cart$/
     */
    public function iRemoveProductFromNotExistingCart(): void
    {
        $this->httpClient->removeItem(
            Uuid::createRandom(),
            $this->fixtureFactory->createFixture(ProductFixture::class)->build()->getId()
        );
    }
}
