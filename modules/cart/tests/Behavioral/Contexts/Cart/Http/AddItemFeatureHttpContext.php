<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Contexts\Cart\Http;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Uuid\Uuid;
use Modules\Cart\Tests\Behavioral\CartApplicationTestClient;
use Modules\Cart\Tests\Behavioral\CartFixtureRegistry;
use Modules\Cart\Tests\Behavioral\CartHttpTestClient;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;
use PHPUnit\Framework\Assert;
use Symfony\Component\Yaml\Yaml;

final class AddItemFeatureHttpContext implements Context
{
    public function __construct(
        private readonly CartApplicationTestClient $applicationClient,
        private readonly CartHttpTestClient $httpClient,
        private readonly CartFixtureRegistry $fixtureRegistry,
        private readonly FixtureFactory $fixtureFactory
    ) {
    }


    /**
     * @When /^I add product "([^"]*)" to the cart "([^"]*)"$/
     */
    public function iAddProductAliasToTheCart(string $productAlias, string $cartAlias)
    {
        $this->httpClient->addItem(
            $this->fixtureRegistry->getCartFixture($cartAlias)->getId(),
            [
                'productId' => $this->fixtureRegistry
                    ->getProductFixture($productAlias)
                    ->getId()
                    ->toString()
            ]
        );
    }

    /**
     * @Then /^cart "([^"]*)" should contain the following products:$/
     */
    public function cartShouldContainTheFollowingProducts(string $cartAlias, string $products): void
    {
        $products = Yaml::parse($products);
        $cart = $this->applicationClient->getCartByAlias($cartAlias);
        foreach ($products as $i => $productAlias) {
            Assert::assertEquals(
                $this->fixtureRegistry->getProductFixture($productAlias)->getId(),
                $cart->items[$i]->productId

            );
        }
        Assert::assertEquals(count($products), count($cart->items));
    }

    /**
     * @Given /^the cart "([^"]*)" total should be "([^"]*)"$/
     */
    public function theCartTotalShouldBe(string $cartAlias, string $total): void
    {
        Assert::assertEquals(

            Money::createFromString($total),
            $this->applicationClient->getCartByAlias($cartAlias)->total
        );
    }

    /**
     * @Given /^the item cart limit is (\d+)$/
     */
    public function theItemCartLimitIs($arg1)
    {
    }

    /**
     * @When /^I add product to the cart "([^"]*)"$/
     */
    public function iAddProductToTheCart(string $cartAlias): void
    {
        $this->httpClient->addItem(
            $this->fixtureRegistry->getCartFixture($cartAlias)->getId(),
            [
                'productId' => $this->fixtureFactory
                    ->createFixture(ProductFixture::class)
                    ->build()
                    ->getId()
                    ->toString()
            ]

        );
    }

    /**
     * @When /^I add product to the not existing cart$/
     */
    public function iAddProductToTheNotExistingCart()
    {
        $this->httpClient->addItem(
            Uuid::createRandom(),
            [
                'productId' => $this->fixtureFactory->createFixture(ProductFixture::class)
                    ->build()
                    ->getId()
                    ->toString()
            ]
        );
    }
}
