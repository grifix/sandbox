<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Contexts\Cart\Http;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Ports\Projector\Dtos\CartDto;
use Modules\Cart\Tests\Behavioral\CartApplicationTestClient;
use Modules\Cart\Tests\Behavioral\CartHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class CreateCartFeatureHttpContext implements Context
{

    private ?Uuid $createdCartId = null;

    public function __construct(
        private readonly CartHttpTestClient $httpClient,
        private readonly CartApplicationTestClient $applicationClient
    ) {
    }


    /**
     * @When /^I create a new cart$/
     */
    public function iCreateANewCart()
    {
        $this->httpClient->createCart();
        if ($this->httpClient->getLastResponseStatusCode() === Response::HTTP_CREATED) {
            $this->createdCartId = Uuid::createFromString(
                $this->httpClient->getLastResponseJson()->getElement('cartId')
            );
        }
    }

    /**
     * @Then /^a new cart should be created$/
     */
    public function aNewCartShouldBeCreated()
    {
        Assert::assertEquals(Response::HTTP_CREATED, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(
            $this->createdCartId->toString(),
            $this->httpClient->getLastResponseJson()->getElement('cartId')
        );
        Assert::assertEquals(
            new CartDto(
                $this->createdCartId,
                Money::pln(0),
                []
            ),
            $this->applicationClient->getCartById($this->createdCartId)
        );
    }
}
