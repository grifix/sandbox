<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Contexts\Cart;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Uuid\Uuid;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;
use Symfony\Component\Yaml\Yaml;

final class CartFixtureContext implements Context
{
    public function __construct(private readonly FixtureFactory $fixtureFactory)
    {
    }


    /**
     * @Given /^there is a cart "([^"]*)"$/
     */
    public function thereIsACart(string $cartAlias): void
    {
        $this->fixtureFactory->createFixture(CartFixture::class)->build($cartAlias);
    }

    /**
     * @Given there is a product :productAlias with price :price
     */
    public function thereIsAProductWithPrice(string $productAlias, string $price): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setPrice(Money::createFromString($price))->build($productAlias);
    }

    /**
     * @Given /^there is a cart "([^"]*)" with (\d+) items$/
     */
    public function thereIsACartWithItems(string $cartAlias, int $addedItemsQuantity): void
    {
        $this->fixtureFactory->createFixture(CartFixture::class)
            ->withAddedItemQuantity($addedItemsQuantity)
            ->build($cartAlias);
    }

    /**
     * @Given /^there is a cart "([^"]*)" with product "([^"]*)"$/
     */
    public function thereIsACartWithProduct(string $cartAlias, string $productAlias): void
    {
        $this->fixtureFactory->createFixture(CartFixture::class)
            ->withAddedProducts([$productAlias])
            ->build($cartAlias);
    }

    /**
     * @Given /^there is a cart "([^"]*)" with items:$/
     */
    public function thereIsACartAliasWithItems(string $cartAlias, string $items): void
    {
        $this->fixtureFactory->createFixture(CartFixture::class)
            ->withAddedProducts(Yaml::parse($items))->build($cartAlias);
    }

    /**
     * @Given /^there are the following products:$/
     */
    public function thereAreTheFollowingProducts(TableNode $products)
    {
        foreach ($products as $product) {
            $this->fixtureFactory->createFixture(ProductFixture::class)
                ->setPrice(Money::createFromString($product['price']))
                ->build($product['alias']);
        }
    }

    /**
     * @Given there is a product :productAlias
     */
    public function thereIsAProductAlias(string $productAlias): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)->build($productAlias);
    }

    /**
     * @Given there is a cart :cartAlias with id :id and products:
     */
    public function thereIsACartWithProducts(string $cartAlias, string $id, TableNode $items)
    {
        $productIds = [];
        foreach ($items as $item) {
            $productIds[] = $this->fixtureFactory->createFixture(ProductFixture::class)
                ->setId(Uuid::createFromString($item['id']))
                ->setPrice(Money::createFromString($item['price']))
                ->setName($item['name'])
                ->build()
                ->getId();
        }
        $this->fixtureFactory->createFixture(CartFixture::class)
            ->setId(Uuid::createFromString($id))
            ->withAddedProductsIds($productIds)
            ->build($cartAlias);
    }
}
