<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Framework\Config\ModuleServicesConfigurator;
use Modules\Cart\Application\Cart\Ports\Projector\CartProjectorInterface;
use Modules\Cart\Application\Cart\Ports\Repository\CartRepositoryInterface;
use Modules\Cart\Domain\Cart\CartOutsideInterface;
use Modules\Cart\Infrasturcture\Cart\Application\Ports\CartProjector;
use Modules\Cart\Infrasturcture\Cart\Application\Ports\CartRepository;
use Modules\Cart\Infrasturcture\Cart\Domain\CartOutside;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnector;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;
use Modules\Cart\Tests\Behavioral\CartApplicationTestClient;
use Modules\Cart\Tests\Behavioral\CartFixtureRegistry;
use Modules\Cart\Tests\Behavioral\CartHttpTestClient;
use Modules\Cart\Tests\Behavioral\Stubs\CatalogAdapterStub;
use Modules\Cart\Tests\Behavioral\Stubs\CatalogConnectorStub;
use Modules\Cart\Ui\CartExceptionConverterProvider;

return static function (ContainerConfigurator $configurator) {
    ModuleServicesConfigurator::create($configurator)
        ->configure('cart', 'Cart');

    $services = $configurator->services();
    $services->defaults()->autowire()->autoconfigure();
    $services->set(CartRepositoryInterface::class, CartRepository::class);
    $services->set(CartOutsideInterface::class, CartOutside::class)->args([
        '$itemLimit' => 3,
    ]);
    $services->set(CatalogConnectorInterface::class, CatalogConnector::class);
    $services->set(CartProjectorInterface::class, CartProjector::class);
    $services->set(CartExceptionConverterProvider::class);

    if ($configurator->env() === 'test') {
        $services->set(CartHttpTestClient::class);
        $services->set(CartApplicationTestClient::class);
        $services->set(CatalogConnectorInterface::class, CatalogConnectorStub::class)->public();
        $services->set(CartFixtureRegistry::class);
    }
};
