<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\RefreshAccessSession;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionCreatedEvent;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSession;
use Modules\Security\Tests\Unit\Domain\RefreshAccessSession\Builders\RefreshAccessSessionOutsideBuilder;

final class CreateRefreshAccessSessionTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $accessSessionId = Uuid::createFromString('f6e21697-f630-4547-b05c-fd62db8d80e3');
        $userId = Uuid::createFromString('38a1c677-2c5e-4ace-ba47-69bd0bedb0e8');
        $currentTime = DateTime::create(2001, 1, 1);

        $accessSession = new RefreshAccessSession(
            RefreshAccessSessionOutsideBuilder::create()->withGetCurrentTimeResult($currentTime)->build(),
            $accessSessionId,
            $userId,
        );

        self::assertEventsWerePublished(
            $accessSession,
            new RefreshAccessSessionCreatedEvent(
                $accessSessionId,
                $userId,
                $currentTime->add(DateInterval::create(days: 30)),
            ),
        );
    }
}
