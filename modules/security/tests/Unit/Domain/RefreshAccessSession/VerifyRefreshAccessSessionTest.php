<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\RefreshAccessSession;

use Grifix\Ip\IpAddress;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionVerifiedEvent;
use Modules\Security\Domain\RefreshAccessSession\Exceptions\RefreshAccessSessionExpiredException;
use Modules\Security\Domain\RefreshAccessSession\Exceptions\RefreshAccessSessionRevokedException;
use Modules\Security\Tests\Unit\Domain\RefreshAccessSession\Builders\RefreshAccessSessionBuilder;

final class VerifyRefreshAccessSessionTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $sessionId = Uuid::createFromString('b34c3abb-ab22-4692-8275-007895782c09');
        $ipAddress = IpAddress::create('127.0.0.1');
        $session = RefreshAccessSessionBuilder::create()->withId($sessionId)->build();
        $session->verify($ipAddress);
        $this->assertEventsWerePublished(
            $session,
            new RefreshAccessSessionVerifiedEvent(
                $sessionId,
                $ipAddress,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        RefreshAccessSessionBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->verify();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'revoked' => [
                RefreshAccessSessionBuilder::create()->shouldBeRevoked(),
                RefreshAccessSessionRevokedException::class,
                'Refresh access session is revoked!',
            ],
            'expired' => [
                RefreshAccessSessionBuilder::create()->shouldBeExpired(),
                RefreshAccessSessionExpiredException::class,
                'Refresh access session is expired!',
            ],
        ];
    }
}
