<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\RefreshAccessSession\Builders;

use Grifix\Date\DateTime\DateTime;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Mockery\Mock;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSession;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSessionOutsideInterface;

final class RefreshAccessSessionBuilder
{
    private Uuid $id;

    private RefreshAccessSessionOutsideInterface $outside;

    private Uuid $userId;

    private bool $shouldBeRevoked = false;

    private bool $shouldBeExpired = false;

    private function __construct()
    {
        $this->id = Uuid::createFromString('5aad4c92-ec33-4516-b049-39e6115aab1d');
        $this->userId = Uuid::createFromString('8ca392e3-7638-407e-bde6-5e0be53a271b');
        $this->outside = RefreshAccessSessionOutsideBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function withId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function withOutside(RefreshAccessSessionOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function withUserId(Uuid $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    public function shouldBeRevoked(): self
    {
        $this->shouldBeRevoked = true;
        return $this;
    }

    public function shouldBeExpired(): self
    {
        $this->shouldBeExpired = true;
        return $this;
    }

    public function build(): RefreshAccessSession
    {
        if ($this->shouldBeExpired) {
            /** @var Mock $outside */
            $outside = $this->outside;
            /** @phpstan-ignore-next-line */
            $outside->allows('getCurrentTime')->andReturn(
                DateTime::create(2010, 1, 1),
                DateTime::create(2011, 1, 1),
            );
        }

        $result = new RefreshAccessSession(
            $this->outside,
            $this->id,
            $this->userId,
        );

        if ($this->shouldBeRevoked) {
            $result->revoke();
        }

        EventCollector::clearEvents($result);

        return $result;
    }
}
