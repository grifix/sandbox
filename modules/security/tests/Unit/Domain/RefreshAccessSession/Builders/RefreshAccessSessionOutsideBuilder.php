<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\RefreshAccessSession\Builders;

use Grifix\Date\DateTime\DateTime;
use Grifix\Test\AbstractOutsideBuilder;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSessionOutsideInterface;

final class RefreshAccessSessionOutsideBuilder extends AbstractOutsideBuilder
{
    protected DateTime $getCurrentTimeResult;

    protected function __construct()
    {
        $this->getCurrentTimeResult = DateTime::create(2001, 1, 1);
    }

    public function withGetCurrentTimeResult(DateTime $getCurrentTimeResult): self
    {
        $this->getCurrentTimeResult = $getCurrentTimeResult;
        return $this;
    }

    public function build(): RefreshAccessSessionOutsideInterface
    {
        return $this->doBuild(RefreshAccessSessionOutsideInterface::class);
    }
}
