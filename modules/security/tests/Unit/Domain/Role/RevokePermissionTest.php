<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\Role;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Role\Events\RolePermissionRevokedEvent;
use Modules\Security\Domain\Role\Exceptions\PermissionNotGrantedException;
use Modules\Security\Tests\Unit\Domain\Role\Builders\RoleBuilder;

final class RevokePermissionTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $roleId = Uuid::createFromString('4344ee27-9d10-4505-9991-7d9739071fe8');
        $permission = 'items.delete';
        $role = RoleBuilder::create()
            ->shouldHavePermission($permission)
            ->withId($roleId)
            ->build();
        $role->revokePermission($permission);
        self::assertEventsWerePublished(
            $role,
            new RolePermissionRevokedEvent(
                $roleId,
                $permission,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        RoleBuilder $builder,
        string $permission,
        string $expectedException,
        string $expectedExceptionMessage,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->revokePermission($permission);
    }

    public function failsProvider(): array
    {
        return [
            'revoke not granted permission' => [
                RoleBuilder::create(),
                'items.delete',
                PermissionNotGrantedException::class,
                'Permission is not granted!',
            ],
        ];
    }
}
