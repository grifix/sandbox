<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\Role\Builders;

use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Role\Role;
use Modules\Security\Domain\Role\RoleOutsideInterface;

final class RoleBuilder
{
    private Uuid $id;

    private string $name = 'tester';

    private RoleOutsideInterface $outside;

    private ?string $shouldHavePermission = null;

    private function __construct()
    {
        $this->id = Uuid::createFromString('20a54fb3-4385-4818-a333-a5b3c9ca3289');
        $this->outside = RoleOutsideBuilder::create()->build();
    }

    public static function create(): RoleBuilder
    {
        return new self();
    }

    public function withId(Uuid $id): RoleBuilder
    {
        $this->id = $id;
        return $this;
    }

    public function withName(string $name): RoleBuilder
    {
        $this->name = $name;
        return $this;
    }

    public function withOutside(RoleOutsideInterface $outside): RoleBuilder
    {
        $this->outside = $outside;
        return $this;
    }

    public function shouldHavePermission(string $permission): RoleBuilder
    {
        $this->shouldHavePermission = $permission;
        return $this;
    }

    public function build(): Role
    {
        $result = new Role(
            $this->outside,
            $this->id,
            $this->name,
        );

        if ($this->shouldHavePermission) {
            $result->grantPermission($this->shouldHavePermission);
        }

        EventCollector::clearEvents($result);
        return $result;
    }
}
