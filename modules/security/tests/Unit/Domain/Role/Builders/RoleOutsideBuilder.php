<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\Role\Builders;

use Grifix\Test\AbstractOutsideBuilder;
use Modules\Security\Domain\Role\RoleOutsideInterface;

final class RoleOutsideBuilder extends AbstractOutsideBuilder
{
    protected bool $doesRoleWithSameNameExistResult = false;

    public function withDoesRoleWithSameNameExistResult(bool $doesRoleWithSameNameExistResult): RoleOutsideBuilder
    {
        $this->doesRoleWithSameNameExistResult = $doesRoleWithSameNameExistResult;
        return $this;
    }

    public function build(): RoleOutsideInterface
    {
        return $this->doBuild(RoleOutsideInterface::class);
    }
}
