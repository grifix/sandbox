<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\Role;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Role\Events\RoleCreatedEvent;
use Modules\Security\Domain\Role\Exceptions\RoleWithSameNameAlreadyExistsException;
use Modules\Security\Domain\Role\Role;
use Modules\Security\Tests\Unit\Domain\Role\Builders\RoleBuilder;
use Modules\Security\Tests\Unit\Domain\Role\Builders\RoleOutsideBuilder;

final class CreateRoleTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $roleId = Uuid::createFromString('8d37c530-783a-4f26-b9b2-5444256a4160');
        $name = 'admin';

        $role = new Role(
            RoleOutsideBuilder::create()->build(),
            $roleId,
            $name,
        );

        self::assertEventsWerePublished(
            $role,
            new RoleCreatedEvent(
                $roleId,
                $name,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        RoleBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build();
    }

    public function failsProvider(): array
    {
        return [
            'not unique name' => [
                RoleBuilder::create()
                    ->withOutside(
                        RoleOutsideBuilder::create()
                            ->withDoesRoleWithSameNameExistResult(true)
                            ->build(),
                    ),
                RoleWithSameNameAlreadyExistsException::class,
                'Role with the same name already exists!',
            ],
        ];
    }
}
