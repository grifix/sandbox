<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\Role;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Role\Events\RolePermissionGrantedEvent;
use Modules\Security\Domain\Role\Exceptions\PermissionAlreadyGrantedException;
use Modules\Security\Tests\Unit\Domain\Role\Builders\RoleBuilder;

final class GrantPermissionTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $roleId = Uuid::createFromString('0d6c00f0-7055-4388-95ba-3c44351a3c89');
        $permission = 'items.delete';
        $role = RoleBuilder::create()->withId($roleId)->build();
        $role->grantPermission($permission);
        self::assertEventsWerePublished(
            $role,
            new RolePermissionGrantedEvent(
                $roleId,
                $permission,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        RoleBuilder $builder,
        string $permission,
        string $expectedException,
        string $expectedExceptionMessage,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->grantPermission($permission);
    }

    public function failsProvider(): array
    {
        return [
            'already granted permission' => [
                RoleBuilder::create()->shouldHavePermission('items.delete'),
                'items.delete',
                PermissionAlreadyGrantedException::class,
                'Permission is already granted!',
            ],
        ];
    }
}
