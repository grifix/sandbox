<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserRoleUnAssignedEvent;
use Modules\Security\Domain\User\Exceptions\UserHasNoRoleException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class UnAssignRoleTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('2def3b0f-8ff3-4b68-8582-fd82dcc700c9');
        $roleId = Uuid::createFromString('f1a918d5-dafc-46f9-89d5-c8888445058c');

        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveAssignedRole($roleId)
            ->build();

        $user->unAssignRole($roleId);

        self::assertEventsWerePublished(
            $user,
            new UserRoleUnAssignedEvent(
                $userId,
                $roleId
            )
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        Uuid $roleId,
        string $expectedException,
        ?string $expectedExceptionMessage = null
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->unAssignRole($roleId);
    }

    public function failsProvider(): array
    {
        return [
            'role is not assigned' => [
                UserBuilder::create(),
                Uuid::createFromString('fbf8d314-b6df-46ce-8e36-ac38a57a2db5'),
                UserHasNoRoleException::class,
                'User has no role!'
            ]
        ];
    }
}
