<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserPasswordResetInitiatedEvent;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class InitiateResetPasswordTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('6f9fda9e-4ea9-4b22-8dc6-7a69036f0811');
        $user = UserBuilder::create()
            ->withId($userId)
            ->build();

        $user->initiatePasswordReset();

        self::assertEventsWerePublished(
            $user,
            new UserPasswordResetInitiatedEvent($userId),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->initiatePasswordReset();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'suspend user' => [
                UserBuilder::create()
                    ->shouldBeSuspended(),
                UserIsSuspendedException::class,
            ],
            'disabled user' => [
                UserBuilder::create()
                    ->shouldBeDisabled(),
                UserIsDisabledException::class,
            ],
        ];
    }
}
