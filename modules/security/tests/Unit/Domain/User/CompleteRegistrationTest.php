<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserRegistrationCompletedEvent;
use Modules\Security\Domain\User\Exceptions\RegistrationIsAlreadyCompletedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class CompleteRegistrationTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('74667409-c1b9-4d01-bca2-a86a2d74fc93');
        $user = UserBuilder::create()
            ->withId($userId)
            ->build();

        $user->completeRegistration();

        self::assertEventsWerePublished(
            $user,
            new UserRegistrationCompletedEvent($userId),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->completeRegistration();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'already completed' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration(),
                RegistrationIsAlreadyCompletedException::class,
                'Registration is already completed!',
            ],
        ];
    }
}
