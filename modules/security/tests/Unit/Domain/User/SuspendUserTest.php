<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserSuspendedEvent;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class SuspendUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('a3adf8b3-af94-435a-b4b0-55655af77017');
        $suspendPeriod = DateInterval::create(minutes: 5);
        $currentDate = DateTime::create(2001, 1, 15);
        $wakeUpDate = $currentDate->add($suspendPeriod);
        $user = UserBuilder::create()
            ->withId($userId)
            ->withOutside(
                UserOutsideBuilder::create()
                    ->withGetCurrentDateTimeResult($currentDate)
                    ->build(),
            )
            ->build();

        $user->suspend($suspendPeriod);

        self::assertEventsWerePublished(
            $user,
            new UserSuspendedEvent($userId, $wakeUpDate),
        );
    }
}
