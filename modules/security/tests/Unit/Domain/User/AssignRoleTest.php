<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserRoleAssignedEvent;
use Modules\Security\Domain\User\Exceptions\RoleAlreadyAssignedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class AssignRoleTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('7fa4c9f9-5470-45a2-9bf8-615d035bcdff');
        $roleId = Uuid::createFromString('2d534684-8586-4dfb-aef8-ae46de1626f4');

        $user = UserBuilder::create()->withId($userId)->build();

        $user->assignRole($roleId);

        self::assertEventsWerePublished(
            $user,
            new UserRoleAssignedEvent(
                $userId,
                $roleId,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        Uuid $roleId,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->assignRole($roleId);
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'role already assigned' => [
                UserBuilder::create()->shouldHaveAssignedRole(
                    Uuid::createFromString('fbf8d314-b6df-46ce-8e36-ac38a57a2db5'),
                ),
                Uuid::createFromString('fbf8d314-b6df-46ce-8e36-ac38a57a2db5'),
                RoleAlreadyAssignedException::class,
                'Role already assigned!',
            ],
        ];
    }
}
