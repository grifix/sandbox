<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Email\Email;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Email\Exceptions\Email\Exceptions\UserEmailAlreadyConfirmedException;
use Modules\Security\Domain\User\Email\Exceptions\WrongEmailConfirmationTokenException;
use Modules\Security\Domain\User\Email\UserEmailIsNotUniqueException;
use Modules\Security\Domain\User\Events\UserEmailChangeConfirmedEvent;
use Modules\Security\Domain\User\Exceptions\EmailChangeIsNotInitiatedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class ConfirmUserEmailChangeTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $email = Email::create('user@example.com');
        $userId = Uuid::createFromString('28b40ce5-2226-4cd1-8acf-3ff68069b0e6');
        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveInitiatedEmailChange($email)
            ->build();

        $user->confirmEmailChange();
        self::assertEventsWerePublished(
            $user,
            new UserEmailChangeConfirmedEvent(
                $userId,
                $email,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->confirmEmailChange();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'has no initiated email change' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration(),
                EmailChangeIsNotInitiatedException::class,
                'Email change is not initiated!',
            ],
            'is suspended' => [
                UserBuilder::create()
                    ->shouldBeSuspended(),
                UserIsSuspendedException::class,
            ],
            'is disabled' => [
                UserBuilder::create()
                    ->shouldBeDisabled(),
                UserIsDisabledException::class,
            ],
            'not unique email' => [
                UserBuilder::create()
                    ->shouldHaveInitiatedEmailChange()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withDoesUserWithEmailExistResult(true)
                            ->build(),
                    ),
                UserEmailIsNotUniqueException::class,
            ],
        ];
    }
}
