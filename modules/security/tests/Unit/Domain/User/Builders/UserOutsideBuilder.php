<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User\Builders;

use Grifix\Date\DateTime\DateTime;
use Grifix\Test\AbstractOutsideBuilder;
use Mockery\MockInterface;
use Modules\Security\Domain\User\UserOutsideInterface;

final class UserOutsideBuilder extends AbstractOutsideBuilder
{
    public const isPasswordValid = 'isPasswordValid';
    public const getCurrentDateTime = 'getCurrentDateTime';

    protected string $generatePasswordResult = '94NvMCUIfdfs7878Dkjfisdkfjl';

    protected bool $isPasswordSecureResult = true;

    protected string $hashPasswordResult = '268f85305a4e47b7bb345ee63fae837e';

    protected bool $isPasswordValidResult = true;

    protected bool $doesUserWithEmailExistResult = false;

    protected bool $hasRoleAccessResult = true;

    protected DateTime $getCurrentDateTimeResult;

    protected function __construct()
    {
        $this->getCurrentDateTimeResult = DateTime::create(2000, 1, 1);
    }

    public function setGeneratePasswordResult(string $generatePasswordResult): UserOutsideBuilder
    {
        $this->generatePasswordResult = $generatePasswordResult;
        return $this;
    }

    public function withIsPasswordSecureResult(bool $isPasswordSecureResult): UserOutsideBuilder
    {
        $this->isPasswordSecureResult = $isPasswordSecureResult;
        return $this;
    }

    public function withHashPasswordResult(string $hashPasswordResult): UserOutsideBuilder
    {
        $this->hashPasswordResult = $hashPasswordResult;
        return $this;
    }

    public function withIsPasswordValidResult(bool $isPasswordValidResult): UserOutsideBuilder
    {
        $this->isPasswordValidResult = $isPasswordValidResult;
        return $this;
    }

    public function withDoesUserWithEmailExistResult(bool $doesUserWithEmailExistResult): UserOutsideBuilder
    {
        $this->doesUserWithEmailExistResult = $doesUserWithEmailExistResult;
        return $this;
    }

    public function withGetCurrentDateTimeResult(DateTime $getCurrentDateTimeResult): UserOutsideBuilder
    {
        $this->getCurrentDateTimeResult = $getCurrentDateTimeResult;
        return $this;
    }

    public function withHasRoleAccessResult(bool $hasRoleAccessResult): UserOutsideBuilder
    {
        $this->hasRoleAccessResult = $hasRoleAccessResult;
        return $this;
    }

    public function build(): UserOutsideInterface
    {
        return $this->doBuild(UserOutsideInterface::class);
    }
}
