<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User\Builders;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Email\Email;
use Grifix\Ip\IpAddress;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Mockery\MockInterface;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Domain\User\User;
use Modules\Security\Domain\User\UserOutsideInterface;

final class UserBuilder
{
    private Uuid $id;

    private UserOutsideInterface|MockInterface $outside;

    private Email $email;

    private string $password = 'password';

    private bool $shouldHaveCompletedRegistration = false;

    private bool $shouldBeDisabled = false;

    private ?Email $shouldHaveInitiatedEmailChange = null;

    private ?int $shouldHaveFailedAuthenticationAttempts = null;

    private ?Uuid $shouldHaveAssignedRole = null;

    private ?DateInterval $shouldBeSuspended = null;

    private bool $shouldHaveInitiatedPasswordReset = false;

    private function __construct()
    {
        $this->email = Email::create('user@example.com');
        $this->id = Uuid::createFromString('d6a40721-b3ff-46eb-b569-484298b9be63');
        $this->outside = UserOutsideBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function withId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function shouldHaveInitiatedPasswordReset(): self
    {
        $this->shouldHaveInitiatedPasswordReset = true;
        return $this;
    }

    public function shouldHaveInitiatedEmailChange(?Email $email = null): self
    {
        $this->shouldHaveCompletedRegistration();
        if (null === $email) {
            $email = Email::create('user@example.com');
        }
        $this->shouldHaveInitiatedEmailChange = $email;
        return $this;
    }

    public function shouldHaveCompletedRegistration(): self
    {
        $this->shouldHaveCompletedRegistration = true;
        return $this;
    }

    public function shouldHaveFailedAuthenticationAttempts(int $attemptsNumber = 1): self
    {
        $this->shouldHaveFailedAuthenticationAttempts = $attemptsNumber;
        return $this;
    }

    public function shouldHaveAssignedRole(?Uuid $roleId = null): self
    {
        if (null === $roleId) {
            $roleId = Uuid::createRandom();
        }
        $this->shouldHaveAssignedRole = $roleId;
        return $this;
    }

    public function shouldBeDisabled(): self
    {
        $this->shouldBeDisabled = true;
        return $this;
    }

    public function shouldBeSuspended(?DateInterval $interval = null): self
    {
        if (null === $interval) {
            $interval = DateInterval::create(minutes: 1);
        }
        $this->shouldBeSuspended = $interval;
        return $this;
    }

    public function withOutside(UserOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function withEmail(Email $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function withPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function build(): User
    {
        $result = new User(
            $this->outside, //@phpstan-ignore-line
            $this->id,
            $this->email,
            $this->password,
        );

        if ($this->shouldHaveCompletedRegistration) {
            $result->completeRegistration();
        }

        if ($this->shouldBeDisabled) {
            $result->disable();
        }

        if ($this->shouldHaveInitiatedEmailChange) {
            $result->initiateEmailChange($this->shouldHaveInitiatedEmailChange, 'password');
        }

        if ($this->shouldBeSuspended) {
            $result->suspend($this->shouldBeSuspended);
        }

        if ($this->shouldHaveInitiatedPasswordReset) {
            $result->initiatePasswordReset();
        }

        if ($this->shouldHaveFailedAuthenticationAttempts) {
            $this->outside->allows('isPasswordValid')->byDefault()->andReturn(false); //@phpstan-ignore-line
            for ($i = 0; $i < $this->shouldHaveFailedAuthenticationAttempts; $i++) {
                try {
                    $result->authenticate('wrong_password', IpAddress::create('127.0.0.1'));
                } catch (WrongPasswordException) {
                }
            }
        }

        if ($this->shouldHaveAssignedRole) {
            $result->assignRole($this->shouldHaveAssignedRole);
        }

        EventCollector::clearEvents($result);
        return $result;
    }
}
