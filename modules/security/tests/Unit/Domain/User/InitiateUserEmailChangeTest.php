<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Email\UserEmailIsNotUniqueException;
use Modules\Security\Domain\User\Events\UserEmailChangeInitiatedEvent;
use Modules\Security\Domain\User\Exceptions\RegistrationIsNotCompletedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class InitiateUserEmailChangeTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('74667409-c1b9-4d01-bca2-a86a2d74fc93');
        $newEmail = Email::create('admin@example.com');
        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveCompletedRegistration()
            ->withEmail(Email::create('user@example.com'))
            ->withOutside(
                UserOutsideBuilder::create()
                    ->build(),
            )
            ->build();

        $user->initiateEmailChange($newEmail, 'password');

        self::assertEventsWerePublished(
            $user,
            new UserEmailChangeInitiatedEvent(
                $userId,
                $newEmail,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        Email $newEmail,
        string $password,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->initiateEmailChange($newEmail, $password);
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'user is disabled' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration()
                    ->shouldBeDisabled()
                ,
                Email::create('admin@example.com'),
                'password',
                UserIsDisabledException::class,
                'User is disabled!',
            ],
            'user is suspended' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration()
                    ->shouldBeSuspended(DateInterval::create(hours: 1))
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withGetCurrentDateTimeResult(DateTime::create(2001, 1, 1))
                            ->build(),
                    )
                ,
                Email::create('admin@example.com'),
                'password',
                UserIsSuspendedException::class,
                'User is suspended until [2001-01-01T01:00:00+00:00]!',
            ],
            'not unique email' => [
                UserBuilder::create()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withDoesUserWithEmailExistResult(true)
                            ->build(),
                    )
                    ->shouldHaveCompletedRegistration()
                ,
                Email::create('admin@example.com'),
                'password',
                UserEmailIsNotUniqueException::class,
                'Email is not unique!',
            ],
            'wrong password' => [
                UserBuilder::create()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withIsPasswordValidResult(false)
                            ->build(),
                    )
                    ->shouldHaveCompletedRegistration()
                ,
                Email::create('admin@example.com'),
                'wrong_password',
                WrongPasswordException::class,
                'Wrong password!',
            ],
            'has not completed registration' => [
                UserBuilder::create(),
                Email::create('admin@example.com'),
                'password',
                RegistrationIsNotCompletedException::class,
                'Registration is not completed!',
            ],
        ];
    }
}
