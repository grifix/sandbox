<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserDisabledEvent;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class DisableUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('6a7f8b08-e4f7-4cac-a874-7f665833e98b');
        $user = UserBuilder::create()
            ->withId($userId)
            ->build();

        $user->disable();

        self::assertEventsWerePublished($user, new UserDisabledEvent($userId));
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->disable();
    }

    public function failsProvider(): array
    {
        return [
            'already disabled' => [
                UserBuilder::create()
                    ->shouldBeDisabled()
                ,
                UserIsDisabledException::class,
                'User is disabled!'
            ]
        ];
    }
}
