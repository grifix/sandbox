<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserResumedEvent;
use Modules\Security\Domain\User\Exceptions\UserIsNotSuspendedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class ResumeUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('f1e53ef9-d5d5-4584-b22e-f5308001aa28');

        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldBeSuspended()
            ->build();

        $user->resume();
        self::assertEventsWerePublished(
            $user,
            new UserResumedEvent($userId),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->resume();
    }

    public function failsProvider(): array
    {
        return [
            'not suspended user' => [
                UserBuilder::create(),
                UserIsNotSuspendedException::class,
                'User is not suspended!',
            ],
        ];
    }
}
