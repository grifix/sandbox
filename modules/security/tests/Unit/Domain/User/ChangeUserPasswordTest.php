<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserPasswordChangedEvent;
use Modules\Security\Domain\User\Exceptions\RegistrationIsNotCompletedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordIsInsecureException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordMismatchException;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class ChangeUserPasswordTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('d801bd7a-4461-45a0-a9de-b12afb9b4bdd');
        $passwordHash = 'password_hash';
        $user = UserBuilder::create()
            ->withId($userId)
            ->withPassword('password')
            ->shouldHaveCompletedRegistration()
            ->withOutside(
                UserOutsideBuilder::create()
                    ->withHashPasswordResult($passwordHash)
                    ->build(),
            )
            ->build();

        $user->changePassword('password', 'new_password', 'new_password');

        self::assertEventsWerePublished(
            $user,
            new UserPasswordChangedEvent(
                $userId,
                $passwordHash,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $currentPassword,
        string $newPassword,
        string $newPasswordConfirmation,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->changePassword($currentPassword, $newPassword, $newPasswordConfirmation);
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'suspended user' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration()
                    ->shouldBeSuspended(),
                'current_password',
                'new_password',
                'new_password',
                UserIsSuspendedException::class,
            ],
            'disabled user' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration()
                    ->shouldBeDisabled(),
                'current_password',
                'new_password',
                'new_password',
                UserIsDisabledException::class,
                'User is disabled!',
            ],
            'wrong password' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withIsPasswordValidResult(false)
                            ->build(),
                    )
                ,
                'current_password',
                'new_password',
                'new_password',
                WrongPasswordException::class,
                'Wrong password!',
            ],
            'password mismatch' => [
                UserBuilder::create()
                    ->shouldHaveCompletedRegistration(),
                'current_password',
                'new_password',
                'wrong_new_password',
                PasswordMismatchException::class,
                'Password mismatch!',
            ],
            'insecure password' => [
                UserBuilder::create()
                    ->withId(Uuid::createFromString('e9a71ff4-1c89-428a-b82b-b5a48413c5ca'))
                    ->shouldHaveCompletedRegistration()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withIsPasswordSecureResult(false)
                            ->build(),
                    )
                ,
                'current_password',
                'new_password',
                'new_password',
                PasswordIsInsecureException::class,
                'Password is insecure!',
            ],
            'not completed registration' => [
                UserBuilder::create(),
                'current_password',
                'new_password',
                'new_password',
                RegistrationIsNotCompletedException::class,
                'Registration is not completed!',
            ],
        ];
    }
}
