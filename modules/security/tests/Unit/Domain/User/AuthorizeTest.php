<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Ip\IpAddress;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserAuthorizedEvent;
use Modules\Security\Domain\User\Exceptions\UserAuthorizationFailedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class AuthorizeTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('dc890f7c-0018-4d28-bb79-a4967b440222');
        $roleId = Uuid::createFromString('0bc0cf7b-a8fe-4046-a834-462a17a06c76');
        $permission = 'item.delete';
        $ip = IpAddress::create('127.0.0.1');
        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveAssignedRole($roleId)
            ->withOutside(UserOutsideBuilder::create()->withHasRoleAccessResult(true)->build())
            ->build();
        $user->authorize($permission, $ip);
        self::assertEventsWerePublished(
            $user,
            new UserAuthorizedEvent(
                $userId,
                $roleId,
                $permission,
                $ip,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $permission,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->authorize($permission, IpAddress::create('127.0.0.1'));
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'disabled user' => [
                UserBuilder::create()->shouldBeDisabled(),
                'item.delete',
                UserIsDisabledException::class,
            ],
            'suspended user' => [
                UserBuilder::create()->shouldBeSuspended(),
                'item.delete',
                UserIsSuspendedException::class,
            ],
            'without any role' => [
                UserBuilder::create(),
                'item.delete',
                UserAuthorizationFailedException::class,
            ],
            'role has no access' => [
                UserBuilder::create()
                    ->shouldHaveAssignedRole()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withHasRoleAccessResult(false)
                            ->build(),
                    )
                ,
                'item.delete',
                UserAuthorizationFailedException::class,
            ],
        ];
    }
}
