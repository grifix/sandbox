<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Email\UserEmailIsNotUniqueException;
use Modules\Security\Domain\User\Events\UserCreatedEvent;
use Modules\Security\Domain\User\Password\Exceptions\PasswordIsInsecureException;
use Modules\Security\Domain\User\User;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class CreateUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('b9342949-d246-4524-8c12-194227bd33bc');
        $email = Email::create('user@example.com');
        $password = '12345678';
        $passwordHash = 'passwordHash';
        $createdAt = DateTime::fromString('2001-01-01 00:00:00');
        $user = new User(
            UserOutsideBuilder::create()
                ->withHashPasswordResult($passwordHash)
                ->withGetCurrentDateTimeResult($createdAt)
                ->build(),
            $userId,
            $email,
            $password,
        );

        self::assertEventsWerePublished(
            $user,
            new UserCreatedEvent(
                $userId,
                $email,
                $passwordHash,
                $createdAt,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'insecure password' => [
                UserBuilder::create()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withIsPasswordSecureResult(false)
                            ->build(),
                    ),
                PasswordIsInsecureException::class,
                'Password is insecure!',
            ],

            'not unique email' => [
                UserBuilder::create()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withDoesUserWithEmailExistResult(true)
                            ->build(),
                    ),
                UserEmailIsNotUniqueException::class,
                'Email is not unique!',
            ],
        ];
    }
}
