<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Ip\IpAddress;
use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserPasswordResetCompletedEvent;
use Modules\Security\Domain\User\Exceptions\PasswordResetIsNotInitiatedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordMismatchException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class CompleteResetPasswordTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('39db0477-860a-42f1-bb96-cf94bcc0594a');
        $userIp = IpAddress::create('127.0.0.1');
        $encryptedPassword = 'encrypted_password';
        $user = UserBuilder::create()
            ->withId($userId)
            ->withPassword('password')
            ->shouldHaveInitiatedPasswordReset()
            ->withOutside(
                UserOutsideBuilder::create()
                    ->withHashPasswordResult($encryptedPassword)
                    ->build(),
            )
            ->build();

        $user->completePasswordReset('new password', 'new password', $userIp);

        self::assertEventsWerePublished(
            $user,
            new UserPasswordResetCompletedEvent(
                $userId,
                $userIp,
                $encryptedPassword,
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $password,
        string $passwordConfirmation,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->completePasswordReset(
            $password,
            $passwordConfirmation,
            IpAddress::create('127.0.0.1'),
        );
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'disabled user' => [
                UserBuilder::create()
                    ->shouldHaveInitiatedPasswordReset()
                    ->shouldBeDisabled(),
                'password',
                'password',
                UserIsDisabledException::class,
            ],
            'suspended user' => [
                UserBuilder::create()
                    ->shouldHaveInitiatedPasswordReset()
                    ->shouldBeSuspended(),
                'password',
                'password',
                UserIsSuspendedException::class,
            ],
            'password mismatch' => [
                UserBuilder::create()
                    ->shouldHaveInitiatedPasswordReset()
                ,
                'password',
                'different_password',
                PasswordMismatchException::class,
            ],
            'not initiated' => [
                UserBuilder::create(),
                'password',
                'password',
                PasswordResetIsNotInitiatedException::class,
            ],
        ];
    }
}
