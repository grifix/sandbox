<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Test\AbstractTestCase;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserEnabledEvent;
use Modules\Security\Domain\User\Exceptions\UserAlreadyEnabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;

final class EnableUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('6f9fda9e-4ea9-4b22-8dc6-7a69036f0811');

        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldBeDisabled()
            ->build();

        $user->enable();

        self::assertEventsWerePublished(
            $user,
            new UserEnabledEvent($userId),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->enable();
    }

    /**
     * @return mixed[]
     */
    public function failsProvider(): array
    {
        return [
            'suspend user' => [
                UserBuilder::create()
                    ->shouldBeSuspended(),
                UserIsSuspendedException::class,
            ],
            'not disabled user' => [
                UserBuilder::create(),
                UserAlreadyEnabledException::class,
                'User is already enabled!',
            ],
        ];
    }
}
