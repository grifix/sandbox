<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Ip\IpAddress;
use Grifix\Test\AbstractTestCase;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Mockery\MockInterface;
use Modules\Security\Domain\User\Events\UserAuthenticatedEvent;
use Modules\Security\Domain\User\Events\UserAuthenticationFailedEvent;
use Modules\Security\Domain\User\Events\UserResumedEvent;
use Modules\Security\Domain\User\Events\UserSuspendedEvent;
use Modules\Security\Domain\User\Exceptions\UserEmailIsNotConfirmedException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;
use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class AuthenticateUserTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('25022680-2e44-4a39-ac67-39137c6ab824');
        $userIp = IpAddress::create('127.0.0.1');
        $currentDate = DateTime::create(2001, 5, 21);

        $user = UserBuilder::create()
            ->withId($userId)
            ->withPassword('password')
            ->withOutside(
                UserOutsideBuilder::create()
                    ->withGetCurrentDateTimeResult($currentDate)
                    ->build(),
            )
            ->build();

        $user->authenticate('test', $userIp);

        self::assertEventsWerePublished(
            $user,
            new UserAuthenticatedEvent(
                $userId,
                $userIp,
            ),
        );
    }

    public function testItWorksAfterMultipleFailedAttempts(): void
    {
        $userId = Uuid::createFromString('9a9be397-319b-4e59-9e8f-d41de198f20b');
        $userIp = IpAddress::create('127.0.0.1');
        $currentDate = DateTime::create(2001, 11, 15, 15, 45);
        $nextDate = $currentDate->add(DateInterval::create(minutes: 5));

        $outside = UserOutsideBuilder::create()
            ->withGetCurrentDateTimeResult($currentDate)
            ->build();

        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveFailedAuthenticationAttempts(4)
            ->withOutside($outside)
            ->build();

        /** @var MockInterface $outside */
        $outside->allows(UserOutsideBuilder::isPasswordValid)->andReturn(true); //@phpstan-ignore-line
        $outside->allows(UserOutsideBuilder::getCurrentDateTime)->andReturn($nextDate); //@phpstan-ignore-line

        $user->authenticate('valid_password', $userIp);

        self::assertEventsWerePublished(
            $user,
            new UserResumedEvent($userId),
            new UserAuthenticatedEvent($userId, $userIp),
        );
    }

    public function testItResumesAndFailsWithSuspendAfterMultipleFailedAttempts(): void
    {
        $userId = Uuid::createFromString('9a9be397-319b-4e59-9e8f-d41de198f20b');
        $userIp = IpAddress::create('127.0.0.1');
        $currentDate = DateTime::create(2001, 11, 15, 15, 45);
        $expectedWakeUpDate = $currentDate->add(DateInterval::create(minutes: 3));
        $outside = UserOutsideBuilder::create()
            ->withIsPasswordValidResult(false)
            ->withGetCurrentDateTimeResult($currentDate)
            ->build();
        $user = UserBuilder::create()
            ->withId($userId)
            ->shouldHaveFailedAuthenticationAttempts(3)
            ->withOutside($outside)
            ->build();

        try {
            $user->authenticate('wrong_password', $userIp);
        } catch (WrongPasswordException) {
        }

        self::assertEventsWerePublished(
            $user,
            new UserAuthenticationFailedEvent(
                $userId,
                4,
                $userIp,
            ),
            new UserSuspendedEvent(
                $userId,
                $expectedWakeUpDate,
            ),
        );
        EventCollector::clearEvents($user);

        /** @var MockInterface $outside */
        $outside->allows(UserOutsideBuilder::getCurrentDateTime) //@phpstan-ignore-line
        ->andReturn($expectedWakeUpDate->add(DateInterval::create(seconds: 1)));
        try {
            $user->authenticate('wrong_password', $userIp);
        } catch (WrongPasswordException) {
        }

        self::assertEventsWerePublished(
            $user,
            new UserResumedEvent(
                $userId,
            ),
            new UserAuthenticationFailedEvent(
                $userId,
                5,
                $userIp,
            ),
            new UserSuspendedEvent(
                $userId,
                $expectedWakeUpDate->add(DateInterval::create(minutes: 3, seconds: 1)),
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        string $password,
        string $expectedException,
        ?string $expectedExceptionMessage = null,
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->authenticate($password, IpAddress::create('127.0.0.1'));
    }

    /**
     * @return mixed[]>
     */
    public function failsProvider(): array
    {
        return [
            'user is suspended' => [
                UserBuilder::create()
                    ->shouldBeSuspended(),
                'password',
                UserIsSuspendedException::class,
            ],
            'user is disabled' => [
                UserBuilder::create()
                    ->shouldBeDisabled()
                ,
                'password',
                UserIsDisabledException::class,
            ],

            'wrong password' => [
                UserBuilder::create()
                    ->withOutside(
                        UserOutsideBuilder::create()
                            ->withIsPasswordValidResult(false)
                            ->build(),
                    ),
                'wrong_password',
                WrongPasswordException::class,
                'Wrong password!',
            ],
        ];
    }
}
