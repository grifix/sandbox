<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Stubs;

use Grifix\Email\Email;
use Modules\Security\Application\Common\Ports\MailSender\MailSenderInterface;

final class MailSenderStub implements MailSenderInterface
{
    /** @var array<array{to: string, payload: object}> */
    private array $sentEmails = [];

    public function send(Email $to, object $payload): void
    {
        $this->sentEmails[] = [
            'to' => $to->toString(),
            'payload' => $payload,
        ];
    }

    public function hasEmailSentTo(Email $email, string $emailPayloadClass): bool
    {
        foreach ($this->sentEmails as $sentEmail) {
            if ($sentEmail['to'] !== $email->toString()) {
                continue;
            }

            if (!($sentEmail['payload'] instanceof $emailPayloadClass)) {
                continue;
            }

            return true;
        }

        return false;
    }

    /**
     * @return array<object>
     */
    public function getEmailsSentTo(Email $email, string $emailPayloadClass): array
    {
        $result = [];

        foreach ($this->sentEmails as $sentEmail) {
            if ($sentEmail['to'] !== $email->toString()) {
                continue;
            }

            if (!($sentEmail['payload'] instanceof $emailPayloadClass)) {
                continue;
            }

            $result[] = $sentEmail['payload'];
        }

        return $result;
    }
}
