<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral;

use Grifix\Email\Email;
use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Commands\Revoke\RevokeRefreshAccessSessionCommand;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionDto;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;
use Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\DecodeRefreshTokenQuery;
use Modules\Security\Application\RefreshAccessSession\Queries\Find\FindRefreshAccessSessionsQuery;
use Modules\Security\Application\RefreshAccessSession\Queries\Get\GetRefreshAccessSessionQuery;
use Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken\GetRefreshTokenQuery;
use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Queries\Get\GetRoleQuery;
use Modules\Security\Application\User\Commands\Authenticate\AuthenticateUserCommand;
use Modules\Security\Application\User\Commands\InitiateEmailChange\InitiateUserEmailChangeCommand;
use Modules\Security\Application\User\Commands\InitiatePasswordReset\InitiatePasswordResetCommand;
use Modules\Security\Application\User\Ports\Projector\UserDto;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\DecodeAccessToken\DecodeAccessTokenQuery;
use Modules\Security\Application\User\Queries\Find\FindUsersQuery;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Modules\Security\Application\User\Queries\GetAccessToken\GetAccessTokenQuery;
use Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken\GetEmailChangeConfirmationTokenQuery;
use Modules\Security\Application\User\Queries\GetPasswordResetToken\GetPasswordResetTokenQuery;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;

final readonly class SecurityApplicationTestClient
{
    public function __construct(
        private ApplicationInterface $application,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    public function getRole(Uuid $roleId): RoleDto
    {
        return $this->application->ask(
            new GetRoleQuery(RoleFilter::create()->setRoleId($roleId)),
        )->role;
    }

    public function getUser(Uuid $userId): UserDto
    {
        return $this->application->ask(
            new GetUserQuery(
                UserFilter::create()
                    ->withUserId($userId)
                    ->withPermissions(true),
            ),
        )->user;
    }

    public function isUserPasswordEqualTo(Uuid $userId, string $password): bool
    {
        try {
            $this->application->tell(
                new AuthenticateUserCommand(
                    $userId,
                    $password,
                    new IpAddress('127.0.0.1'),
                ),
            );
        } catch (WrongPasswordException) {
            return false;
        }
        return true;
    }

    public function hasUserWithPassword(string $userAlias, string $password): bool
    {
        $users = $this->application->ask(
            new FindUsersQuery(
                UserFilter::create()
                    ->withUserId($this->fixtureRegistry->getUserFixture($userAlias)->getId())
                    ->withPassword($password),
            ),
        )->users;

        return count($users) === 1;
    }

    public function changeUserEmail(string $userAlias, ?Email $email = null): void
    {
        if (null === $email) {
            $email = Email::create(uniqid('user_') . '@example.com');
        }
        $userFixture = $this->fixtureRegistry->getUserFixture($userAlias);
        $this->application->tell(
            new InitiateUserEmailChangeCommand(
                $userFixture->getId(),
                $email,
                $userFixture->getPassword(),
            ),
        );
    }

    public function hasActiveRefreshAccessSession(string $userAlias): bool
    {
        $sessions = $this->application->ask(
            new FindRefreshAccessSessionsQuery(
                RefreshAccessSessionFilter::create()
                    ->setUserId($this->fixtureRegistry->getUserFixture($userAlias)->getId())
                    ->setIsActive(true),
            ),
        )->refreshAccessSessions;

        return count($sessions) > 0;
    }

    public function getUserIdFromAccessToken(string $accessToken): Uuid
    {
        return $this->application->ask(
            new DecodeAccessTokenQuery($accessToken),
        )->userId;
    }

    public function getUserAccessToken(string $userAlias): string
    {
        return $this->application->ask(
            new GetAccessTokenQuery(
                $this->fixtureRegistry->getUserFixture($userAlias)->getId(),
            ),
        )->token;
    }

    public function getEmailConfirmationToken(string $userAlias): string
    {
        $userFixture = $this->fixtureRegistry->getUserFixture($userAlias);
        return $this->application->ask(
            new GetEmailChangeConfirmationTokenQuery($userFixture->getId()),
        )->token;
    }

    public function initiatePasswordReset(string $userAlias): void
    {
        $this->application->tell(
            new InitiatePasswordResetCommand(
                $this->fixtureRegistry->getUserFixture($userAlias)->getId(),
            ),
        );
    }

    public function getPasswordResetToken(string $userAlias): string
    {
        return $this->application->ask(
            new GetPasswordResetTokenQuery(
                $this->fixtureRegistry->getUserFixture($userAlias)->getId(),
            ),
        )->token;
    }

    public function getUserRefreshToken(string $userAlias): string
    {
        return $this->application->ask(
            new GetRefreshTokenQuery(
                $this->fixtureRegistry->getUserFixture($userAlias)->getId(),
            ),
        )->refreshToken;
    }

    public function getRefreshAccessSessionId(string $refreshToken): Uuid
    {
        return $this->application->ask(
            new DecodeRefreshTokenQuery($refreshToken),
        )->refreshAccessSessionId;
    }

    public function getRefreshAccessSessionById(?Uuid $createdRefreshAccessSessionId): RefreshAccessSessionDto
    {
        return $this->application->ask(
            new GetRefreshAccessSessionQuery(
                RefreshAccessSessionFilter::create()
                    ->setRefreshAccessSessionId($createdRefreshAccessSessionId),
            ),
        )->refreshAccessSession;
    }

    public function revokeAllUserRefreshAccessSessions(string $userAlias): void
    {
        $sessions = $this->application->ask(
            new FindRefreshAccessSessionsQuery(
                RefreshAccessSessionFilter::create()
                    ->setUserId($this->fixtureRegistry->getUserFixture($userAlias)->getId())
                    ->setIsActive(true),
            ),
        )->refreshAccessSessions;
        foreach ($sessions as $session) {
            $this->application->tell(new RevokeRefreshAccessSessionCommand($session->id));
        }
    }
}
