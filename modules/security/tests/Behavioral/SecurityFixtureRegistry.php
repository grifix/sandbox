<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral;

use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Modules\Security\Tests\Behavioral\Fixtures\RoleFixture;
use Modules\Security\Tests\Behavioral\Fixtures\UserFixture;

final readonly class SecurityFixtureRegistry
{

    public function __construct(private ObjectRegistryInterface $objectRegistry)
    {
    }

    public function getRoleFixture(string $alias): RoleFixture
    {
        return $this->objectRegistry->getObject(RoleFixture::class, $alias);
    }

    public function getUserFixture(string $alias): UserFixture
    {
        return $this->objectRegistry->getObject(UserFixture::class, $alias);
    }
}
