Feature: create the refresh access session

  Background:
    Given the time is "2001-01-01 00:00:00"
    Given there is an authenticated user "Joe"

  Scenario: success
    When user "Joe" creates a new refresh access session
    Then user "Joe" should have the new refresh access session
    And there should be refresh and access tokens obtained

  Scenario: wrong token
    When user creates a new refresh access session with wrong token
    Then there should be returned an error that the refresh token is wrong

  Scenario: expired session
    Given the time is "2001-03-01 00:00:00"
    When user "Joe" creates a new refresh access session
    Then there should be a security error "Expired refresh token!"

  Scenario: revoked session
    Given all user "Joe" refresh access sessions are revoked
    When user "Joe" creates a new refresh access session
    Then there should be a security error "Refresh access session is revoked!"

