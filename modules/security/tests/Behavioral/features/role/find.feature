Feature: Find roles

  Background:
    And there are the following roles:
      | alias   | id                                   | name    |
      | Admin   | d372ec80-a966-4ff5-a3de-ed59a6430b7f | Admin   |
      | User    | 1a2b3c4d-5e6f-7a8b-9c0d-e1f2a3b4c5d6 | User    |
      | Manager | 0f1539e6-0472-4564-8310-499b05bef566 | Manager |
    And there is a user "Admin" with role "Admin"
    And the role "Admin" has the following permissions:
    """
    - security.role.read
    - security.role.read.protectedProperty
    """
    And the user "Admin" is authenticated

  Scenario: find all
    When the user "Admin" finds roles
    Then the following roles should be found:
    """
    - Admin
    - User
    - Manager
    """

  Scenario: find by name
    When the user "Admin" finds roles by name "Admin"
    Then the following roles should be found:
    """
    - Admin
    """

  Scenario: limit and offset
    When the user "Admin" finds roles with the following params:
    """
    offset: 1
    limit: 1
    """
    Then the following roles should be found:
    """
    - User
    """

  Scenario: data structure
    When the user "Admin" finds roles with the following params:
    """
    name: Admin
    withPermissions: true
    countTotal: true
    """
    Then the returned result should be:
    """
    roles:
      - id: d372ec80-a966-4ff5-a3de-ed59a6430b7f
        name: Admin
        permissions:
          - security.role.read
          - security.role.read.protectedProperty
    total: 1
    """

  Scenario: forbidden
    Given there is a user "Mike"
    And the user "Mike" is authenticated
    When the user "Mike" finds roles
    Then there should be an error that "Forbidden!"

  Scenario: forbidden property
    Given there is a user "Mike"
    And the user "Mike" has permission "security.role.read"
    And the user "Mike" is authenticated
    When the user "Mike" finds roles with the following params:
    """
    offset: 0
    limit: 1
    withPermissions: true
    countTotal: true
    """
    Then the returned result should be:
    """
    roles:
      - id: d372ec80-a966-4ff5-a3de-ed59a6430b7f
        name: Admin
        permissions: __FORBIDDEN_PROPERTY__
    total: 4
    """
