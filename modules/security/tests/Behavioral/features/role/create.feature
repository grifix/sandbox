Feature: create role

  Background:
    Given there is an authenticated user "Admin" with permission "security.role.create"

  Scenario: create role
    When user "Admin" creates a new role with name "Tester"
    Then there should be a new role with name "Tester"
    And there should be a confirmation about successfully created role

  Scenario: role with same name
    Given there is a role with name "Tester"
    When user "Admin" creates a new role with name "Tester"
    Then there should be an error that "Role with the same name already exists!"

  Scenario: forbidden
    Given there is an registered authenticated user "Guest"
    When user "Guest" creates a new role
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized user creates a new role
    Then there should be an error that "Unauthorized!"

  Scenario: empty input
    When user "Admin" creates a new role with empty input
    Then there should be an error that "name" is required
