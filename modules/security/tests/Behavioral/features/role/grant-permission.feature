Feature: grant permission

  Background:
    Given there is an authenticated user "Admin" with permission "security.role.permission.manage"
    And there is a role "User"
    And there is a user "Joe" with role "User"
    And there is a user "Mike" with role "User"
    And there is a user "Nick"

  Scenario: grant permission
    When user "Admin" grants permission "items.delete" for role "User"
    Then there should be a confirmation about successfully granted permission
    And role "User" should have permission "items.delete"
    And user "Joe" should have permission "items.delete"
    And user "Mike" should have permission "items.delete"
    And user "Nick" should not have permission "items.delete"


  Scenario: permission already granted
    Given there is a role "Guests" with permission "items.create"
    When user "Admin" grants permission "items.create" for role "Guests"
    Then there should be an error that "Permission is already granted!"

  Scenario: forbidden
    Given there is an registered authenticated user "Guest"
    When user "Guest" grants permission "items.delete" for role "User"
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized grants permission for the role
    Then there should be an error that "Unauthorized!"

