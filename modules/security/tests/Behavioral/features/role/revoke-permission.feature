Feature: revoke permission

  Background:
    Given there is an authenticated user "Admin" with permission "security.role.permission.manage"
    And there is a role "User" with permission "items.delete"
    And there is a role "Items manager" with permission "items.delete"
    And there is a user "Joe" with role "User"
    And there is a user "Mike" with role "User"
    And there is a user "Nick" with role "Items manager"

  Scenario: revoke permission
    When user "Admin" revokes permission "items.delete" for role "User"
    Then role "User" should not have permission "items.delete"
    And user "Joe" should not have permission "items.delete"
    And user "Mike" should not have permission "items.delete"
    And user "Nick" should have permission "items.delete"
    And there should be a confirmation about successfully revoked permission

  Scenario: not granted permission
    Given there is a role "Guests"
    When user "Admin" revokes permission "items.create" for role "Guests"
    Then there should be an error that "Permission is not granted!"

  Scenario: forbidden
    Given there is an registered authenticated user "Guest"
    When user "Guest" revokes permission "items.delete" for role "User"
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized revokes permission for the role
    Then there should be an error that "Unauthorized!"
