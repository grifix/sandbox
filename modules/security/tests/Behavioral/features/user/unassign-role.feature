Feature: unassign user role

  Background:
    Given the time is "12:00"
    And there is an authenticated user "Admin" with permission "security.user.role.manage"
    And there is a role "Manager" with permission "documents.manage"
    And there is a user "Joe" with role "Manager"
    And there is a user "Jack" with role "Manager"

  Scenario: successfully unassigned role
    When user "Admin" unassigns role "Manager" from user "Joe"
    Then user "Joe" should not have role "Manager"
    And user "Joe" should not have permission "documents.manage"
    And user "Jack" should have role "Manager"
    And user "Jack" should have permission "documents.manage"
    And there should ba a confirmation about successfully unassigned role

  Scenario: role already unassigned
    Given there is a registered user "Alex"
    When user "Admin" unassigns role "Manager" from user "Alex"
    Then there should be an error that "User has no role!"

  Scenario: forbidden
    Given there is an registered and authenticated user "Guest"
    When user "Guest" unassigns role "Manager" from user "Joe"
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When unauthorized user unassigns role from user
    Then there should be an error that "Unauthorized!"

