Feature: change user password

  Background:
    Given the time is "2001-01-01 12:00"
    And there is an authenticated user "Joe" with password "vyON!3sk5fW9" and completed registration

  Scenario: change password
    When user "Joe" changes his password with data:
    """
    currentPassword: vyON!3sk5fW9
    newPassword: G8PJi10G!bty
    newPasswordConfirmation: G8PJi10G!bty
    """
    Then user "Joe" should have password "G8PJi10G!bty"
    And user "Joe" should have no active refresh access sessions

  Scenario: not completed registration
    Given there is an authenticated user "Alex" with not completed registration
    When user "Alex" changes his password
    Then there should be a security error "Registration is not completed!"
    And user "Alex" password should be unchanged

  Scenario: suspended user
    Given there is an authenticated suspended user "Alex" with completed registration
    When user "Alex" changes his password
    Then there should be a security error "User is suspended!" with the message "User is suspended until [2001-01-01T13:00:00+00:00]!"

  Scenario: disabled user
    Given there is an authenticated disabled user "Alex" with completed registration
    When user "Alex" changes his password
    Then there should be a security error "User is disabled!"

  Scenario: password mismatch
    When user "Joe" changes his password with data:
    """
    currentPassword: vyON!3sk5fW9
    newPassword: G8PJi10G!bty
    newPasswordConfirmation: 5fRA8@6@lcb^
    """
    Then there should be a security error "Password mismatch!"

  Scenario: wrong password
    When user "Joe" changes his password with current password "G8PJi10G!bty"
    Then there should be a security error "Wrong password!"

  Scenario: insecure password
    When user "Joe" changes his password with new password "qwerty"
    Then there should be a security error "Password is insecure!"

  Scenario: unauthorized
    When unauthorized user changes his password
    Then there should be an error that "Unauthorized!"





