Feature: authenticate

  Background:
    Given the time is "2023-01-23 7:00:00"
    And there is a user "Mike" with email "mike@example.com" and password "vyON!3sk5fW9"
    And there is a user "Joe" with email "joe@example.com" and password "OaE2qP8*06*F"

  Scenario: successful authentication
    When I authenticate with email "mike@example.com" and password "vyON!3sk5fW9"
    Then I should be authenticated as user "Mike"
    And new refresh access session for user "Mike" should be created
    And access and refresh tokens should be obtained

  Scenario: wrong password
    When I authenticate with email "mike@example.com" and password "vyON!3sk5fW9_wrong_password"
    Then there should be a security error "User does not exist!"

  Scenario: brute force attack
    When I authenticate with email "mike@example.com" and password "vyON!3sk5fW9_wrong_password" for 4 times
    Then there should be a security error "User does not exist!"
    And user "Mike" should be suspended for 3 minutes

  Scenario: user is suspended
    Given there is suspended user "Alex" with email "alex@example.com" and password "vyON!3sk5fW9"
    When I authenticate with email "alex@example.com" and password "vyON!3sk5fW9"
    Then there should be a security error "User is suspended!" with the message "User is suspended until [2023-01-23T08:00:00+00:00]!"

  Scenario: user is disabled
    Given there is disabled user "Alex" with email "alex@example.com" and password "vyON!3sk5fW9"
    When I authenticate with email "alex@example.com" and password "vyON!3sk5fW9"
    Then there should be a security error "User is disabled!"

  Scenario: second session
    Given there is an authenticated user "Alex" with email "alex@example.com" and password "1Qw*ertY28"
    When I authenticate with email "alex@example.com" and password "1Qw*ertY28"
    Then I should be authenticated as user "Alex"

