Feature: complete registration
  Background:
    Given the time is "2001-01-01 00:00"
    And there is an user "Joe"

  Scenario: success
    When an unauthenticated user completes user "Joe" registration
    Then there should be a confirmation about successfully completed registration
    And user "Joe" should be registered
    
  Scenario: already completed
    Given there is a registered user "Alex"
    When an unauthenticated user completes user "Alex" registration
    Then there should be a security error "Registration is already completed!"

  Scenario: disabled user
    Given there a disabled user "Alex"
    When an unauthenticated user completes user "Alex" registration
    Then there should be a security error "User is disabled!"
    And user "Alex" should not be registered

  Scenario: suspended user
    Given there a suspended user "Alex"
    When an unauthenticated user completes user "Alex" registration
    Then there should be a security error "User is suspended!" with the message "User is suspended until [2001-01-01T01:00:00+00:00]!"
    And user "Alex" should not be registered

  Scenario: expired token
    When an unauthenticated user completes user "Joe" registration at "2001-01-01 00:16"
    Then there should be a security error "Expired registration confirmation token!"
    And user "Joe" should not be registered

  Scenario: invalid token
    When an unauthenticated user completes registration with invalid token
    Then there should be a security error "Invalid registration confirmation token!"
