Feature: authorize user

  Scenario: success
    Given there is a registered user "Joe"
    And the user "Joe" is authenticated
    When user "Joe" requests information about himself
    Then user should be successfully authorized

  Scenario: expired token
    Given the time is "2101-01-01 12:00:00"
    And there is a registered user "Joe"
    And the user "Joe" is authenticated
    And the time is "2101-01-01 13:00:00"
    When user "Joe" requests information about himself
    Then there should be a security error "Expired access token!"

  Scenario: invalid token
    And there is a registered user "Joe"
    And the user "Joe" is authenticated
    When user "Joe" requests information about himself with invalid token
    Then there should be a security error "Invalid access token!"

