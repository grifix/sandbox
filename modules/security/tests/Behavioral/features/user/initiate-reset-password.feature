Feature: initiate the password reset

  Background:
    Given there is a user "Joe" with the email "joe@example.com"

  Scenario: success
    When an unauthenticated user initiates a password reset with the email "joe@example.com"
    Then the password reset should be initiated for the user "Joe"
    And there should be a confirmation about the successful password reset initiation
    And there should be sent a message with password reset token to the email "joe@example.com"

  Scenario: disabled user
    Given there is an registered and disabled user "Max"
    When an unauthenticated user initiates a password reset for the user "Max"
    Then there should be an error that "User is disabled!"

  Scenario: suspended user
    Given the time is "2001-01-01 15:00"
    And there is an registered and suspended user "Max"
    When an unauthenticated user initiates a password reset for the user "Max"
    Then there should be an error that "User is suspended until [2001-01-01T16:00:00+00:00]!"

