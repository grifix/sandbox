Feature: create an email confirmation

  Background:
    Given the time is "2001-01-01 12:00"
    And there is an registered user "Joe" with email "joe@example.com" and initiated email change to "joe.smith@example.com"

  Scenario: success
    When user "Joe" confirms his email change
    Then there should be a confirmation about successfully confirmed email change
    And user "Joe" should have email "joe.smith@example.com"

  Scenario: expired token
    Given the time is "2001-01-01 13:00"
    When user "Joe" confirms his email change at "2001-01-01 15:00"
    Then there should be a security error "Expired email change confirmation token!"
    And user "Joe" should have email "joe@example.com"

  Scenario: invalid token
    When user confirms his email change with invalid token
    Then there should be a security error "Invalid email change confirmation token!"
    And user "Joe" should have email "joe@example.com"

  Scenario: email change has not been started
    Given there is an registered and authenticated user "Alex"
    When user "Alex" confirms his email change
    Then there should be a security error "Email change is not initiated!"

  Scenario: suspended user
    Given there a registered and suspended user "Alex" with email "alex@example.com" who has initiated email change to "alex.white@example.com"
    When user "Alex" confirms his email change
    And there should be a security error "User is suspended!" with the message "User is suspended until [2001-01-01T13:00:00+00:00]!"
    Then user "Alex" should have email "alex@example.com"

  Scenario: disabled user
    Given there a registered disabled user "Alex" with email "alex@example.com" who has initiated email change to "alex.white@example.com"
    When user "Alex" confirms his email change
    And there should be a security error "User is disabled!"
    Then user "Alex" should have email "alex@example.com"

