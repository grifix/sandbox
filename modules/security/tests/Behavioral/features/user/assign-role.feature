Feature: assign user role

  Background:
    Given there is an authenticated user "Admin" with permission "security.user.role.manage"
    And there is a user "Joe"
    And there is a user "Jack"
    And there is a role "Manager" with permission "documents.manage"

  Scenario: assign role
    When user "Admin" assigns role "Manager" to user "Joe"
    Then there should ba a confirmation about successfully assigned role
    And user "Joe" should have role "Manager"
    And user "Joe" should have permission "documents.manage"
    And user "Jack" should not have role "Manager"
    And user "Jack" should not have permission "documents.manage"

  Scenario: role already assigned
    Given there is a user "Alex" with role "Manager"
    When user "Admin" assigns role "Manager" to user "Alex"
    Then there should be a security error "Role already assigned!"

  Scenario: forbidden
    Given there is an authenticated user "Guest"
    When user "Guest" assigns role "Manager" to user "Joe"
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized user assigns role to user
    Then there should be an error that "Unauthorized!"


