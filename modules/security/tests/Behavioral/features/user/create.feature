Feature: create user
  Scenario: successfully created user
    Given the time is "12:00"
    When I create a new user:
    """
    email: user@example.com
    password: ybnNq!93bd0iykFMKDPH5
    """
    And there should be a new user with not completed registration and the following data:
    """
    email: user@example.com
    password: ybnNq!93bd0iykFMKDPH5
    """
    And there should be sent an email with registration confirmation token to "user@example.com"
    And there should ba a confirmation about successfully created user

  Scenario: insecure password
    When I create a new user with password "123456"
    Then there should be a security error "Password is insecure!"

  Scenario: not unique email
    Given the time is "13:00"
    And there is a user with email "user@example.com"
    When I create a new user with email "user@example.com"
    Then there should be a security error "Email is not unique!"
