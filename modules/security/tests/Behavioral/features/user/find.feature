Feature: find users

  Background:
    Given the time is "2001-01-01 14:00:00"
    And there are the following users:
      | alias | id                                   | email            | createdAt           |
      | Mike  | b8ffcded-0f52-4b56-8b28-b5cc1b4c40a0 | mike@example.com | 2001-01-01 12:00:00 |
      | Joe   | 6acaf1e2-4ab1-4202-b874-2c891daa4781 | joe@example.com  | 2001-01-01 13:00:00 |
      | Alex  | 24754f76-6afa-4605-83cb-eb96402b1748 | alex@example.com | 2001-01-01 14:00:00 |
    And the user "Alex" is authenticated
    And the user "Alex" has the following permissions:
    """
    - security.user.read
    - security.user.read.protectedProperty
    """

  Scenario: find all
    When the user "Alex" finds users
    Then the following users should be found:
    """
    - Mike
    - Joe
    - Alex
    """

  Scenario: find by email
    When the user "Alex" finds users with the following params:
    """
    email: joe@example.com
    """
    Then the following users should be found:
    """
    - Joe
    """

  Scenario: limit and offset
    When the user "Alex" finds users with the following params:
    """
    offset: 1
    limit: 1
    """
    Then the following users should be found:
    """
    - Joe
    """

  Scenario: sorting
    When the user "Alex" finds users with the following params:
    """
    sortBy: email
    """
    Then the following users should be found:
    """
    - Alex
    - Joe
    - Mike
    """

  Scenario: data structure
    When the user "Alex" finds users with the following params:
    """
    email: alex@example.com
    withPermissions: true
    countTotal: true
    """
    Then the returned result should be:
    """
    users:
      - id: 24754f76-6afa-4605-83cb-eb96402b1748
        email: alex@example.com
        disabled: false
        createdAt: '2001-01-01T14:00:00+00:00'
        resumeDate: null
        registrationCompleted: false
        passwordResetInitiated: false
        permissions:
          - security.user.read
          - security.user.read.protectedProperty
    total: 1
    """
    
  Scenario: forbidden
    Given the user "Mike" is authenticated
    When the user "Mike" finds users
    Then there should be an error that "Forbidden!"

  Scenario: forbidden property
    Given the user "Joe" has permission "security.user.read"
    And the user "Joe" is authenticated
    When the user "Joe" finds users with the following params:
    """
    offset: 0
    limit: 1
    withPermissions: true
    countTotal: true
    """
    Then the returned result should be:
    """
    users:
      - id: b8ffcded-0f52-4b56-8b28-b5cc1b4c40a0
        email: __FORBIDDEN_PROPERTY__
        disabled: __FORBIDDEN_PROPERTY__
        createdAt: __FORBIDDEN_PROPERTY__
        resumeDate: __FORBIDDEN_PROPERTY__
        permissions: __FORBIDDEN_PROPERTY__
        registrationCompleted: __FORBIDDEN_PROPERTY__
        passwordResetInitiated: __FORBIDDEN_PROPERTY__
    total: 3
    """

    





