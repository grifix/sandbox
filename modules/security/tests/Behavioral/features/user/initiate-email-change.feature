Feature: initiate email change

  Background:
    Given the time is "2001-01-01 12:00"
    And there is an registered and authenticated user "Joe" with email "joe@example.com" and password "vyON!3sk5fW9"

  Scenario: success
    When user "Joe" initiates his email change to "joe2@example.com" with password "vyON!3sk5fW9"
    Then user "Joe" should have initiated email change to email "joe2@example.com"
    And there should ba a confirmation about successfully initiated email change
    And there should be sent a message with email confirmation token to the email "joe2@example.com"

  Scenario: disabled user
    Given there is an authenticated registered and disabled user "Jack"
    When user "Jack" initiates his email change
    Then there should be an error that "User is disabled!"
    And there should be no initiated email change for user "Jack"

  Scenario: suspended user
    Given there is an authenticated registered and suspended user "Jack"
    When user "Jack" initiates his email change
    Then there should be an error that "User is suspended until [2001-01-01T13:00:00+00:00]!"
    And there should be no initiated email change for user "Jack"

  Scenario: not unique email
    Given there is a user with email "alex@example.com"
    When user "Joe" initiates his email change to "alex@example.com" with password "vyON!3sk5fW9"
    Then there should be an error that "Email is not unique!"
    And there should be no initiated email change for user "Joe"

  Scenario: wrong password
    When user "Joe" initiates his email change to "joe2@example.com" with password "WronG!2PassworD"
    Then there should be an error that "Wrong password!"
    And there should be no initiated email change for user "Joe"

  Scenario: unauthorized
    When an unauthorized user initiates his email change
    Then there should be an error that "Unauthorized!"

  Scenario: not completed registration
    Given there is an authenticated user "Jack" with not completed registration
    When user "Jack" initiates his email change
    Then there should be an error that "Registration is not completed!"
