Feature: disable user

  Background:
    Given the time is "12:00"
    Given there is an authenticated user "Admin" with permission "security.user.manage"
    And there is a user "Joe"
    And there is a role "Manager" with permission "documents.manage"

  Scenario: successfully disabled
    When user "Admin" disables user "Joe"
    Then user "Joe" should be disabled
    And there should be a confirmation about successfully disabled user

  Scenario: already disabled
    Given there is a disabled user "Tom"
    When user "Admin" disables user "Tom"
    Then there should be a security error "User is disabled!"

  Scenario: forbidden
    Given there is an authenticated user "Guest"
    When user "Guest" disables user
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized user disables user
    Then there should be an error that "Unauthorized!"
