Feature: complete password reset

  Background:
    Given the time is "2001-01-01 00:00"
    And there is an registered user "Joe" with initiated password reset

  Scenario: success
    When an unauthenticated user completes user "Joe" password reset with data:
    """
    newPassword: "7S7)yAwSV^D8b$>"
    newPasswordConfirmation: "7S7)yAwSV^D8b$>"
    """
    Then user "Joe" should have password "7S7)yAwSV^D8b$>"
    And there should ba a confirmation about successfully completed password reset

  Scenario: disabled user
    Given there a disabled registered user "Alex" with initiated password reset
    When an unauthenticated user completes user "Alex" password reset
    Then there should be a security error "User is disabled!"
    And user "Alex" password should be unchanged

  Scenario: password mismatch
    When an unauthenticated user completes user "Joe" password reset with data:
    """
    newPassword: "7S7)yAwSV^D8b$>"
    newPasswordConfirmation: "rRpduBPMaH7CTfhj"
    """
    Then there should be a security error "Password mismatch!"
    And user "Joe" password should be unchanged

  Scenario: not initiated password reset
    Given there is a registered user "Alex"
    When an unauthenticated user completes user "Alex" password reset
    Then there should be a security error "Password reset is not initiated!"

  Scenario: expired token
    When an unauthenticated user completes user "Joe" password reset at "2001-01-01 00:06"
    Then there should be a security error "Expired password reset token!"
