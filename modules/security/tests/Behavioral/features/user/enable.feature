Feature: enable user

  Background:
    Given the time is "2001-01-01 12:00"
    Given there is an authenticated user "Admin" with permission "security.user.manage"
    And there is a disabled user "Joe"
    And there is a role "Manager" with permission "documents.manage"

  Scenario: successfully enabled
    When user "Admin" enables user "Joe"
    Then user "Joe" should be enabled
    And there should be a confirmation about successfully enabled user

  Scenario: suspended
    Given there is a suspended user "Alex"
    When user "Admin" enables user "Alex"
    Then there should be a security error "User is suspended!" with the message "User is suspended until [2001-01-01T13:00:00+00:00]!"

  Scenario: not disabled
    Given there is a enabled user "Tom"
    When user "Admin" enables user "Tom"
    Then there should be an error that "User is already enabled!"

  Scenario: forbidden
    Given there is an authenticated user "Guest"
    When user "Guest" enables user
    Then there should be an error that "Forbidden!"

  Scenario: unauthorized
    When an unauthorized user enables user
    Then there should be an error that "Unauthorized!"
