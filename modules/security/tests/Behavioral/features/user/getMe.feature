Feature: get information about authenticated user
  Scenario: success
    Given there ia a registered user "Joe" with the following data:
    """
    id: '860dc78b-6806-4ba4-aff0-b91c813e48e8'
    email: joe@example.com
    """
    And the user "Joe" is authenticated
    When user "Joe" requests information about himself
    Then there should be the following data received:
    """
    id: '860dc78b-6806-4ba4-aff0-b91c813e48e8'
    email: joe@example.com
    disabled: __FORBIDDEN_PROPERTY__
    createdAt: __FORBIDDEN_PROPERTY__
    resumeDate: __FORBIDDEN_PROPERTY__
    permissions: __FORBIDDEN_PROPERTY__
    registrationCompleted: __FORBIDDEN_PROPERTY__
    passwordResetInitiated: __FORBIDDEN_PROPERTY__
    """

  Scenario: unauthorized
    When an unauthorized user requests information about himself
    Then there should be an error that "Unauthorized!"
