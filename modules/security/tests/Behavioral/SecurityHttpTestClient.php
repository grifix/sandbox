<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Test\TestHttpClient;
use Grifix\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final readonly class SecurityHttpTestClient
{
    public function __construct(private TestHttpClient $httpClient)
    {
    }

    /**
     * @param mixed[] $payload
     */
    public function createRole(array $payload = [], ?string $accessToken = null): Response
    {
        return $this->httpClient->post(
            $this->createUrl('roles'),
            (string)json_encode($payload),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param mixed[] $override
     * @param mixed[] $remove
     *
     * @return mixed[]
     */
    public function buildCreateRolePayload(array $override = [], array $remove = []): array
    {
        return $this->httpClient->buildPayload(
            [
                'name' => uniqid('role_'),
            ],
            $override,
            $remove,
        );
    }

    /**
     * @param mixed[] $override
     * @param mixed[] $remove
     *
     * @return mixed[]
     */
    public function buildCreateUserPayload(array $override = [], array $remove = []): array
    {
        return $this->httpClient->buildPayload(
            [
                'email' => uniqid('user_') . '@example.com',
                'password' => 'qE78!3458_dfIdnDFi$#fsdi',
            ],
            $override,
            $remove,
        );
    }

    public function getLastResponseJson(): ArrayWrapper
    {
        return $this->httpClient->getLastResponseJson();
    }

    public function getLastResponseStatusCode(): int
    {
        return $this->httpClient->getLastResponseStatusCode();
    }

    /**
     * @param array{} $payload
     */
    public function createUser(array $payload): Response
    {
        return $this->httpClient->post(
            $this->createUrl('users'),
            (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function grantRolePermission(
        ?Uuid $roleId = null,
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if (null === $roleId) {
            $roleId = Uuid::createRandom();
        }
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'permission' => 'user.delete',
                ],
                $payload,
            );
        }
        return $this->httpClient->post(
            $this->createUrl('roles/' . $roleId->toString() . '/permissions'),
            (string)json_encode($payload),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function revokeRolePermission(
        ?Uuid $roleId = null,
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if (null === $roleId) {
            $roleId = Uuid::createRandom();
        }
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'permission' => 'user.delete',
                ],
                $payload,
            );
        }
        return $this->httpClient->delete(
            $this->createUrl('roles/' . $roleId->toString() . '/permissions'),
            headers: $this->createHeaders($accessToken),
            body: (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function assignRoleToUser(
        Uuid $userId = null,
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if (null === $userId) {
            $userId = Uuid::createRandom();
        }
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'roleId' => Uuid::createRandom()->toString(),
                ],
                $payload,
            );
        }

        return $this->httpClient->post(
            $this->createUrl('users/' . $userId->toString() . '/roles'),
            body: (string)json_encode($payload),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @return array{Authorization:string}|mixed[]
     */
    private function createHeaders(?string $accessToken = null): array
    {
        $result = [];
        if ($accessToken) {
            $result['Authorization'] = 'Bearer ' . $accessToken;
        }
        return $result;
    }

    /**
     * @param mixed[] $payload
     */
    public function unAssignRoleFromUser(
        ?Uuid $userId = null,
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if (null === $userId) {
            $userId = Uuid::createRandom();
        }
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'roleId' => Uuid::createRandom()->toString(),
                ],
                $payload,
            );
        }
        return $this->httpClient->delete(
            $this->createUrl('users/' . $userId->toString() . '/roles'),
            headers: $this->createHeaders($accessToken),
            body: (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function initiateUserEmailChange(
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'email' => uniqid('email') . '@example.com',
                    'password' => 'J8u1aw5Q&5hq',
                ],
                $payload,
            );
        }
        return $this->httpClient->path(
            $this->createUrl('users/email'),
            body: (string)json_encode($payload),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param array{
     *     email:string,
     *     password: string,
     * } $payload
     */
    public function authenticate(array $payload): Response
    {
        return $this->httpClient->post(
            $this->createUrl('users/authenticate'),
            body: (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function changePassword(
        array $payload = [],
        ?string $accessToken = null,
        bool $useDefaultPayload = true,
    ): Response {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'currentPassword' => 'F8!3Z4t@1@t9',
                    'newPassword' => '2Q4mcDB0yhI!',
                    'newPasswordConfirmation' => '2Q4mcDB0yhI!',
                ],
                $payload,
            );
        }
        return $this->httpClient->path(
            $this->createUrl('users/password'),
            (string)json_encode($payload),
            headers: $this->createHeaders($accessToken),
        );
    }

    private function createUrl(string $path): string
    {
        return '/api/v1/security/' . $path;
    }

    /**
     * @param mixed[] $payload
     */
    public function confirmEmail(array $payload = [], bool $useDefaultPayload = true): Response
    {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'token' => uniqid(),
                ],
                $payload,
            );
        }
        return $this->httpClient->path(
            $this->createUrl('users/email/confirm'),
            (string)json_encode($payload),
        );
    }

    public function disableUser(?Uuid $userId = null, ?string $accessToken = null): Response
    {
        if (null === $userId) {
            $userId = Uuid::createRandom();
        }

        return $this->httpClient->path(
            $this->createUrl(sprintf('users/%s/disable', $userId->toString())),
            headers: $this->createHeaders($accessToken),
        );
    }

    public function enableUser(Uuid $userId = null, ?string $accessToken = null): Response
    {
        if (null === $userId) {
            $userId = Uuid::createRandom();
        }

        return $this->httpClient->path(
            $this->createUrl(sprintf('users/%s/enable', $userId->toString())),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function completePasswordReset(array $payload = [], bool $useDefaultPayload = true): Response
    {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'token' => uniqid(),
                    'newPassword' => 'NpDhphyj85ecWpPE',
                    'newPasswordConfirmation' => 'NpDhphyj85ecWpPE',
                ],
                $payload,
            );
        }

        return $this->httpClient->path(
            $this->createUrl('users/password/reset/complete'),
            (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function initiatePasswordReset(array $payload = [], bool $useDefaultPayload = true): Response
    {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'email' => uniqid('user_') . '@example.com',
                ],
                $payload,
            );
        }

        return $this->httpClient->post(
            $this->createUrl('users/password/reset/initiate'),
            (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function confirmUserEmailChange(
        array $payload = [],
        bool $useDefaultPayload = true,
    ): Response {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'token' => uniqid(),
                ],
                $payload,
            );
        }
        return $this->httpClient->path(
            $this->createUrl('users/email/confirm'),
            (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function createRefreshAccessSession(
        array $payload = [],
        bool $useDefaultPayload = true,
    ): Response {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'refreshToken' => uniqid(),
                ],
                $payload,
            );
        }
        return $this->httpClient->post(
            $this->createUrl('refreshAccessSession'),
            body: (string)json_encode($payload),
        );
    }

    public function getMe(?string $accessToken = null): Response
    {
        return $this->httpClient->get(
            $this->createUrl('users/me'),
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param mixed[] $queryString
     */
    public function findUsers(array $queryString = [], ?string $accessToken = null): Response
    {
        return $this->httpClient->get(
            $this->createUrl('users'),
            queryString: $queryString,
            headers: $this->createHeaders($accessToken),
        );
    }

    /**
     * @param mixed[] $payload
     */
    public function completeRegistration(
        array $payload = [],
        bool $useDefaultPayload = true,
    ): Response {
        if ($useDefaultPayload) {
            $payload = $this->httpClient->buildPayload(
                [
                    'token' => uniqid(),
                ],
                $payload,
            );
        }
        return $this->httpClient->path(
            $this->createUrl('users/registration/complete'),
            (string)json_encode($payload),
        );
    }

    /**
     * @param mixed[] $queryString
     */
    public function findRoles(array $queryString = [], ?string $accessToken = null): Response
    {
        return $this->httpClient->get(
            $this->createUrl('roles'),
            queryString: $queryString,
            headers: $this->createHeaders($accessToken),
        );
    }
}
