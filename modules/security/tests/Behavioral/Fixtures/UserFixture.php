<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Fixtures;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken\GetRefreshTokenQuery;
use Modules\Security\Application\User\Commands\AssignRole\AssignRoleCommand;
use Modules\Security\Application\User\Commands\Authenticate\AuthenticateUserCommand;
use Modules\Security\Application\User\Commands\CompleteRegistration\CompleteUserRegistrationCommand;
use Modules\Security\Application\User\Commands\Create\CreateUserCommand;
use Modules\Security\Application\User\Commands\Disable\DisableUserCommand;
use Modules\Security\Application\User\Commands\InitiateEmailChange\InitiateUserEmailChangeCommand;
use Modules\Security\Application\User\Commands\InitiatePasswordReset\InitiatePasswordResetCommand;
use Modules\Security\Application\User\Commands\Suspend\SuspendUserCommand;
use Modules\Security\Application\User\Ports\Projector\UserDto;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Modules\Security\Application\User\Queries\GetAccessToken\GetAccessTokenQuery;
use Modules\Security\Application\User\Queries\GetRegistrationConfirmationToken\GetRegistrationConfirmationTokenQuery;

final class UserFixture extends AbstractFixture
{
    private Uuid $id;

    private Email $email;

    private string $password = '6ro&bPr61i2v';

    /**
     * @var string[]
     */
    private array $permissions = [];
    /**
     * @var Uuid[]
     */
    private array $assignedRoles = [];

    private bool $shouldBeSuspended = false;

    private bool $shouldBeDisabled = false;
    private bool $shouldBeAuthenticated = false;

    private bool $completedRegistration = false;

    private ?Email $initiatedEmailChangeTo = null;

    private bool $initiatedPasswordReset = false;

    private ?string $accessToken = null;

    private ?string $refreshToken = null;

    public function withInitiatedPasswordReset(): self
    {
        $this->assertIsNotBuilt();
        $this->initiatedPasswordReset = true;
        return $this;
    }

    public function withId(Uuid $id): self
    {
        $this->assertIsNotBuilt();
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed[] $data
     */
    public function withRawData(array $data): self
    {
        $this->assertIsNotBuilt();
        $data = ArrayWrapper::create($data);
        if ($data->hasElement('id')) {
            $this->withId(Uuid::createFromString($data->getAsString('id')));
        }
        if ($data->hasElement('email')) {
            $this->withEmail(Email::create($data->getAsString('email')));
        }
        if ($data->hasElement('createdAt')) {
            $this->withCreatedAt(DateTime::fromString($data->getAsString('createdAt')));
        }

        return $this;
    }

    public function isRegistered(): bool
    {
        $this->assertIsBuilt();
        return $this->fetchState()->registrationCompleted === true;
    }

    protected function init(): void
    {
        $this->id = Uuid::createRandom();
        $this->email = Email::create(uniqid('email') . '@example.com');
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function withEmail(Email $email): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->email = $email;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function withPassword(string $password): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->password = $password;
        return $this;
    }

    public function withInitiatedEmailChangeTo(Email $email): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->withCompletedRegistration();
        $this->initiatedEmailChangeTo = $email;
        return $this;
    }

    public function withPermissions(string ...$permissions): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->permissions = $permissions;
        return $this;
    }

    public function withAssignedRoles(Uuid ...$roles): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->assignedRoles = $roles;
        return $this;
    }

    public function withCompletedRegistration(): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->completedRegistration = true;
        return $this;
    }

    public function shouldBeAuthenticated(): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->shouldBeAuthenticated = true;
        return $this;
    }

    public function shouldBeSuspended(): UserFixture
    {
        $this->assertIsNotBuilt();
        $this->shouldBeSuspended = true;
        return $this;
    }

    public function shouldBeDisabled(): UserFixture
    {
        $this->shouldBeDisabled = true;
        return $this;
    }

    public function getAccessToken(): string
    {
        if (null === $this->accessToken) {
            throw new \RuntimeException('User is not authenticated!');
        }
        return $this->accessToken;
    }

    public function getRefreshToken(): string
    {
        if (null === $this->refreshToken) {
            throw new \RuntimeException('User is not authenticated!');
        }
        return $this->refreshToken;
    }

    protected function create(): void
    {
        $this->assertIsNotBuilt();
        $this->application->tell(
            new CreateUserCommand(
                $this->id,
                $this->email,
                $this->password,
            ),
        );
    }

    public function grantPermission(string $permission): self
    {
        $this->assertIsBuilt();
        $roleFixture = $this->fixtureFactory
            ->createFixture(RoleFixture::class)
            ->build()
            ->grantPermission($permission);

        $this->assignRole($roleFixture->getId());
        return $this;
    }

    public function assignRole(Uuid $roleId): self
    {
        $this->assertIsBuilt();
        $this->application->tell(
            new AssignRoleCommand(
                $this->id,
                $roleId,
            ),
        );
        return $this;
    }

    public function suspend(?DateInterval $dateInterval = null): self
    {
        $this->assertIsBuilt();
        if (null === $dateInterval) {
            $dateInterval = DateInterval::create(hours: 1);
        }
        $this->application->tell(
            new SuspendUserCommand(
                $this->id,
                $dateInterval,
            ),
        );
        return $this;
    }

    public function disable(): self
    {
        $this->assertIsBuilt();
        $this->application->tell(
            new DisableUserCommand(
                $this->id,
            ),
        );
        return $this;
    }

    public function getRegistrationConfirmationToken(): string
    {
        $this->assertIsBuilt();
        return $this->application->ask(new GetRegistrationConfirmationTokenQuery($this->id))->token;
    }

    public function authenticate(): self
    {
        $this->application->tell(
            new AuthenticateUserCommand(
                $this->id,
                $this->password,
            ),
        );

        $this->accessToken = $this->application->ask(
            new GetAccessTokenQuery($this->id),
        )->token;

        $this->refreshToken = $this->application->ask(
            new GetRefreshTokenQuery($this->id),
        )->refreshToken;

        return $this;
    }

    public function completeRegistration(): self
    {
        $this->assertIsBuilt();
        $this->application->tell(
            new CompleteUserRegistrationCommand(
                $this->id,
            ),
        );
        return $this;
    }

    public function initiatePasswordReset(): self
    {
        $this->assertIsBuilt();
        $this->application->tell(
            new InitiatePasswordResetCommand(
                $this->id,
            ),
        );
        return $this;
    }

    protected function afterCreate(): void
    {
        if ($this->completedRegistration) {
            $this->completeRegistration();
        }

        if ($this->initiatedEmailChangeTo) {
            $this->initiateEmailChange($this->initiatedEmailChangeTo);
        }

        if ($this->initiatedPasswordReset) {
            $this->initiatePasswordReset();
        }

        if ($this->permissions) {
            foreach ($this->permissions as $permission) {
                $this->grantPermission($permission);
            }
        }

        if ($this->assignedRoles) {
            foreach ($this->assignedRoles as $roleId) {
                $this->assignRole($roleId);
            }
        }

        if ($this->shouldBeAuthenticated) {
            $this->authenticate();
        }

        if ($this->shouldBeSuspended) {
            $this->suspend();
        }

        if ($this->shouldBeDisabled) {
            $this->disable();
        }
    }

    public function initiateEmailChange(Email $shouldHaveInitiatedEmailChangeTo): self
    {
        $this->application->tell(
            new InitiateUserEmailChangeCommand(
                $this->id,
                $shouldHaveInitiatedEmailChangeTo,
                $this->password,
            ),
        );
        return $this;
    }

    public function fetchState(): UserDto
    {
        return $this->application->ask(
            new GetUserQuery(
                UserFilter::create()
                    ->withUserId($this->id)
                    ->withPermissions(true),
            ),
        )->user;
    }
}
