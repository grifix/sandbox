<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Fixtures;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Commands\Create\CreateRoleCommand;
use Modules\Security\Application\Role\Commands\GrantPermission\GrantPermissionCommand;
use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Queries\Get\GetRoleQuery;

final class RoleFixture extends AbstractFixture
{
    private Uuid $id;

    private string $name;

    /** @var string[] */
    private array $permissions = [];

    protected function init(): void
    {
        $this->name = uniqid('Group_');
        $this->id = Uuid::createRandom();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function withId(Uuid $id): RoleFixture
    {
        $this->assertIsNotBuilt();
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function withName(string $name): RoleFixture
    {
        $this->assertIsNotBuilt();
        $this->name = $name;
        return $this;
    }

    public function withPermissions(string ...$permissions): RoleFixture
    {
        $this->assertIsNotBuilt();
        $this->permissions = $permissions;
        return $this;
    }

    public function grantPermission(string $permission): RoleFixture
    {
        $this->assertIsBuilt();
        $this->application->tell(
            new GrantPermissionCommand(
                $this->id,
                $permission,
            ),
        );
        return $this;
    }

    protected function create(): void
    {
        $this->assertIsNotBuilt();
        $this->application->tell(
            new CreateRoleCommand(
                $this->id,
                $this->name,
            ),
        );
    }

    protected function afterCreate(): void
    {
        if ($this->permissions) {
            foreach ($this->permissions as $permission) {
                $this->grantPermission($permission);
            }
        }
    }

    public function fetchState(): RoleDto
    {
        $this->assertIsBuilt();
        return $this->application->ask(new GetRoleQuery(RoleFilter::create()->setRoleId($this->id)))->role;
    }

    /**
     * @param array<mixed> $data
     */
    public function withRawData(array $data): self
    {
        $this->assertIsNotBuilt();
        $data = ArrayWrapper::create($data);
        if ($data->hasElement('id')) {
            $this->withId(Uuid::createFromString($data->getAsString('id')));
        }

        if ($data->hasElement('name')) {
            $this->withName($data->getAsString('name'));
        }
        return $this;
    }
}
