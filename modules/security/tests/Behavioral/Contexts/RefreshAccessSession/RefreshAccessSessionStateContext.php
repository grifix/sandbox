<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\RefreshAccessSession;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityVariableRegistry;
use PHPUnit\Framework\Assert;

final readonly class RefreshAccessSessionStateContext implements Context
{
    public function __construct(
        private SecurityApplicationTestClient $applicationClient,
        private SecurityVariableRegistry $variableRegistry,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @Given /^new refresh access session for user "([^"]*)" should be created$/
     */
    public function newRefreshAccessSessionForUserShouldBeCreated(string $userAlias): void
    {
        Assert::assertTrue($this->applicationClient->hasActiveRefreshAccessSession($userAlias));
    }

    /**
     * @Then /^user "([^"]*)" should have the new refresh access session$/
     */
    public function userShouldHaveTheNewRefreshAccessSession(string $userAlias): void
    {
        Assert::assertNotNull($this->variableRegistry->createdRefreshAccessSessionId);
        $refreshAccessSession = $this->applicationClient->getRefreshAccessSessionById(
            $this->variableRegistry->createdRefreshAccessSessionId,
        );
        Assert::assertTrue(
            $refreshAccessSession->userId->isEqualTo($this->fixtureRegistry->getUserFixture($userAlias)->getId()),
        );
    }

    /**
     * @Given /^all user "([^"]*)" refresh access sessions are revoked$/
     */
    public function allUserRefreshAccessSessionsAreRevoked(string $userAlias): void
    {
        $this->applicationClient->revokeAllUserRefreshAccessSessions($userAlias);
    }
}
