<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\RefreshAccessSession\Http;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Grifix\Clock\FrozenClock;
use Modules\Security\Domain\Common\SecurityErrorCodes;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use Modules\Security\Tests\Behavioral\SecurityVariableRegistry;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class CreateRefreshAccessSessionHttpContext implements Context
{
    /**
     * @param FrozenClock $clock
     */
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private SecurityVariableRegistry $variableRegistry,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^user "([^"]*)" creates a new refresh access session$/
     */
    public function userCreatesANewRefreshAccessSession(string $userAlias): void
    {
        $this->httpClient->createRefreshAccessSession(
            [
                'refreshToken' => $this->fixtureRegistry->getUserFixture($userAlias)->getRefreshToken(),
            ],
        );

        $response = $this->httpClient->getLastResponseJson();
        if ($response->hasElement('refreshToken')) {
            $this->variableRegistry->createdRefreshAccessSessionId = $this->applicationClient->getRefreshAccessSessionId(
                $response->getElement('refreshToken'), //@phpstan-ignore-line
            );
        }
    }

    /**
     * @Given /^there should be refresh and access tokens obtained$/
     */
    public function thereShouldBeRefreshAndAccessTokensObtained(): void
    {
        Assert::assertEquals($this->httpClient->getLastResponseStatusCode(), Response::HTTP_CREATED);
        $response = $this->httpClient->getLastResponseJson();
        Assert::assertNotNull($response->getElement('refreshToken'));
        Assert::assertNotNull($response->getElement('accessToken'));
    }

    /**
     * @When /^user creates a new refresh access session with wrong token$/
     */
    public function userCreatesANewRefreshAccessSessionWithWrongToken(): void
    {
        $this->httpClient->createRefreshAccessSession();
    }

    /**
     * @Then /^there should be returned an error that the refresh token is wrong$/
     */
    public function thereShouldBeReturnedAnErrorThatTheRefreshTokenIsWrong(): void
    {
        Assert::assertEquals(Response::HTTP_BAD_REQUEST, $this->httpClient->getLastResponseStatusCode());
        $response = $this->httpClient->getLastResponseJson();
        Assert::assertEquals(
            SecurityErrorCodes::invalidRefreshToken->value,
            $response->getElement('error.code'),
        );
        Assert::assertEquals(
            'Invalid refresh token!',
            $response->getElement('error.message'),
        );
    }
}
