<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class AuthenticateUserHttpContext implements Context
{
    private ?string $accessToken = null;

    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityApplicationTestClient $applicationClient,
        private readonly SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^I authenticate with email "([^"]*)" and password "([^"]*)"$/
     */
    public function iAuthenticateWithEmailAndPassword(string $email, string $password): void
    {
        $response = $this->httpClient->authenticate([
            'email' => $email,
            'password' => $password,
        ]);

        if ($response->getContent()) {
            /** @var \stdClass $content */
            $content = json_decode($response->getContent());
            if (isset($content->accessToken)) {
                $this->accessToken = $content->accessToken;
            }
        }
    }

    /**
     * @Then /^I should be authenticated as user "([^"]*)"$/
     */
    public function iShouldBeAuthenticatedAsUser(string $userAlias): void
    {
        Assert::assertNotNull($this->accessToken);
        Assert::assertEquals(
            $this->fixtureRegistry->getUserFixture($userAlias)->getId(),
            $this->applicationClient->getUserIdFromAccessToken($this->accessToken),
        );
    }

    /**
     * @When /^I authenticate with email "([^"]*)" and password "([^"]*)" for (\d+) times$/
     */
    public function iAuthenticateWithEmailAndPasswordTimes(string $email, string $password, int $attempts): void
    {
        for ($i = 0; $i < $attempts; $i++) {
            $this->httpClient->authenticate([
                'email' => $email,
                'password' => $password,
            ]);
        }
    }

    /**
     * @Given /^access and refresh tokens should be obtained$/
     */
    public function accessAndRefreshTokensShouldBeObtained(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        $response = $this->httpClient->getLastResponseJson();
        Assert::assertNotNull($response->getElement('accessToken'));
        Assert::assertNotNull($response->getElement('refreshToken'));
    }
    
}
