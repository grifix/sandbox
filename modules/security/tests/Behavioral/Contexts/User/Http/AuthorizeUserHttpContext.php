<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class AuthorizeUserHttpContext implements Context
{
    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
    ) {
    }

    /**
     * @Then /^user should be successfully authorized$/
     */
    public function userShouldBeSuccessfullyAuthorized(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^user "([^"]*)" requests information about himself with invalid token$/
     */
    public function userRequestsInformationAboutHimselfWithInvalidToken(string $userAlias): void
    {
        $this->httpClient->getMe('invalid_token');
    }
}
