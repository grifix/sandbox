<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class DisableUserHttpContext implements Context
{

    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^user "([^"]*)" disables user "([^"]*)"$/
     */
    public function userDisablesUser(string $userAlias, string $targetUserAlias): void
    {
        $this->httpClient->disableUser(
            $this->fixtureRegistry->getUserFixture($targetUserAlias)->getId(),
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should be a confirmation about successfully disabled user$/
     */
    public function thereShouldBeAConfirmationAboutSuccessfullyDisabledUser(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^user "([^"]*)" disables user$/
     */
    public function userDisablesAnyUser(string $userAlias): void
    {
        $this->httpClient->disableUser(
            accessToken: $this->applicationClient->getUserAccessToken($userAlias)
        );
    }

    /**
     * @When /^an unauthorized user disables user$/
     */
    public function anUnauthorizedUserDisablesUser(): void
    {
        $this->httpClient->disableUser();
    }
}
