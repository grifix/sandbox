<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;

final class GetMeHttpContext implements Context
{
    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^user "([^"]*)" requests information about himself$/
     */
    public function userRequestsInformationAboutHimself(string $userAlias): void
    {
        $this->httpClient->getMe(
            accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken(),
        );
    }

    /**
     * @When /^an unauthorized user requests information about himself$/
     */
    public function anUnauthorizedUserRequestsInformationAboutHimself(): void
    {
        $this->httpClient->getMe();
    }

}
