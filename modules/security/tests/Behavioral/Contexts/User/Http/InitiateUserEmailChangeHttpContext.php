<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class InitiateUserEmailChangeHttpContext implements Context
{
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
    ) {
    }

    /**
     * @When /^user "([^"]*)" initiates his email change to "([^"]*)" with password "([^"]*)"$/
     */
    public function userInitiatesHisEmailChangeToWithPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->httpClient->initiateUserEmailChange(
            [
                'email' => $email,
                'password' => $password,
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should ba a confirmation about successfully initiated email change$/
     */
    public function thereShouldBaAConfirmationAboutSuccessfullyInitiatedEmailChange(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^user "([^"]*)" initiates his email change$/
     */
    public function userInitiatesHisEmailChange(string $userAlias): void
    {
        $this->httpClient->initiateUserEmailChange(
            accessToken: $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^an unauthorized user initiates his email change$/
     */
    public function anUnauthorizedUserInitiatesHisEmailChange(): void
    {
        $this->httpClient->initiateUserEmailChange();
    }
}
