<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use Symfony\Component\Yaml\Yaml;

final readonly class ChangeUserPasswordHttpContext implements Context
{
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^user "([^"]*)" changes his password with data:$/
     */
    public function userChangesHisPasswordWithData(string $userAlias, string $data): void
    {
        $data = Yaml::parse($data);
        /** @var mixed[] $data */
        $this->httpClient->changePassword(
            $data,
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^user "([^"]*)" changes his password$/
     */
    public function userChangesHisPassword(string $userAlias): void
    {
        $this->httpClient->changePassword(
            [
                'currentPassword' => $this->fixtureRegistry->getUserFixture($userAlias)->getPassword(),
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^user "([^"]*)" changes his password with current password "([^"]*)"$/
     */
    public function userChangesHisPasswordWithCurrentPassword(string $userAlias, string $password): void
    {
        $this->httpClient->changePassword(
            [
                'currentPassword' => $password,
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^user "([^"]*)" changes his password with new password "([^"]*)"$/
     */
    public function userChangesHisPasswordWithNewPassword(string $userAlias, string $newPassword): void
    {
        $this->httpClient->changePassword(
            [
                'currentPassword' => $this->fixtureRegistry->getUserFixture($userAlias)->getPassword(),
                'newPassword' => $newPassword,
                'newPasswordConfirmation' => $newPassword,
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^unauthorized user changes his password$/
     */
    public function unauthorizedUserChangesHisPassword(): void
    {
        $this->httpClient->changePassword();
    }
}
