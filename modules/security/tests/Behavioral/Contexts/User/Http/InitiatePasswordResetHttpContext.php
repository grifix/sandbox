<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class InitiatePasswordResetHttpContext implements Context
{
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^an unauthenticated user initiates a password reset with the email "([^"]*)"$/
     */
    public function anUnauthenticatedUserInitiatesAPasswordResetWithTheEmail(string $email): void
    {
        $this->httpClient->initiatePasswordReset(
            [
                'email' => $email,
            ],
        );
    }

    /**
     * @Given /^there should be a confirmation about the successful password reset initiation$/
     */
    public function thereShouldBeAConfirmationAboutTheSuccessfulPasswordResetInitiation(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthenticated user initiates a password reset for the user "([^"]*)"$/
     */
    public function anUnauthenticatedUserInitiatesAPasswordResetForTheUser(string $userAlias): void
    {
        $this->httpClient->initiatePasswordReset(
            [
                'email' => $this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->email->toString(),
            ],
        );
    }
}
