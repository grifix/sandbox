<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Domain\Token\Dto\TokenTypeDto;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class AssignRoleHttpContext implements Context
{
    public function __construct(
        private readonly SecurityFixtureRegistry $fixtureRegistry,
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityApplicationTestClient $applicationClient,
    ) {
    }

    /**
     * @When /^user "([^"]*)" assigns role "([^"]*)" to user "([^"]*)"$/
     */
    public function userAssignsRoleToUser(string $userAlias, string $roleAlias, string $toUserAlias): void
    {
        $this->httpClient->assignRoleToUser(
            $this->fixtureRegistry->getUserFixture($toUserAlias)->getId(),
            [
                'roleId' => $this->fixtureRegistry->getRoleFixture($roleAlias)->getId()->toString(),
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should ba a confirmation about successfully assigned role$/
     */
    public function thereShouldBaAConfirmationAboutSuccessfullyAssignedRole(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthorized user assigns role to user$/
     */
    public function anUnauthorizedAssignsRoleToUser(): void
    {
        $this->httpClient->assignRoleToUser();
    }
}
