<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Grifix\Uuid\Uuid;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use Modules\Security\Tests\Behavioral\SecurityVariableRegistry;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final class CreateUserHttpContext implements Context
{
    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityApplicationTestClient $applicationClient,
        private readonly SecurityVariableRegistry $variableRegistry,
    ) {
    }

    /**
     * @When /^I create a new user:$/
     */
    public function iCreateANewUser(string $user): void
    {
        $response = $this->httpClient->createUser(Yaml::parse($user));
        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $this->variableRegistry->createdUserId = Uuid::createFromString(
                $this->httpClient->getLastResponseJson()->getElement('userId'),
            );
        }
    }

    /**
     * @When /^I create a new user with password "([^"]*)"$/
     */
    public function iCreateANewUserWithPassword(string $password): void
    {
        $this->httpClient->createUser(
            $this->httpClient->buildCreateUserPayload(
                [
                    'password' => $password,
                ],
            ),
        );
    }

    /**
     * @When /^I create a new user with email "([^"]*)"$/
     */
    public function iCreateANewUserWithEmail(string $email): void
    {
        $this->httpClient->createUser(
            $this->httpClient->buildCreateUserPayload(
                [
                    'email' => $email,
                ],
            ),
        );
    }

    /**
     * @Given /^there should ba a confirmation about successfully created user$/
     */
    public function thereShouldBaAConfirmationAboutSuccessfullyCreatedUser(): void
    {
        Assert::assertEquals(Response::HTTP_CREATED, $this->httpClient->getLastResponseStatusCode());
    }
}
