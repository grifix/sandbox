<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\TestHelper;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class CompleteRegistrationHttpContext implements Context
{
    /**
     * @param FrozenClock $clock
     */
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityFixtureRegistry $fixtureRegistry,
        private ClockInterface $clock,
    ) {
    }

    /**
     * @Then /^there should be a confirmation about successfully completed registration$/
     */
    public function thereShouldBeAConfirmationAboutSuccessfullyCompletedRegistration(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthenticated user completes user "([^"]*)" registration$/
     */
    public function anUnauthenticatedUserCompletesUserRegistration(string $userAlias): void
    {
        $this->httpClient->completeRegistration(
            [
                'token' => $this->fixtureRegistry->getUserFixture($userAlias)->getRegistrationConfirmationToken(),
            ],
            false,
        );
    }

    /**
     * @When /^an unauthenticated user completes user "([^"]*)" registration at "([^"]*)"$/
     */
    public function anUnauthenticatedUserCompletesUserRegistrationAt(string $userAlias, string $time): void
    {
        $token = $this->fixtureRegistry->getUserFixture($userAlias)->getRegistrationConfirmationToken();
        $date = DateTime::fromString($time);
        $this->clock->freezeDate($date);
        TestHelper::freezeCurrentDate($date);
        $this->httpClient->completeRegistration(
            [
                'token' => $token,
            ],
            false,
        );
    }

    /**
     * @When /^an unauthenticated user completes registration with invalid token$/
     */
    public function anUnauthenticatedUserCompletesRegistrationWithInvalidToken(): void
    {
        $this->httpClient->completeRegistration();
    }
}
