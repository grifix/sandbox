<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final readonly class FindUsersHttpContext implements Context
{
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @Then /^the following users should be found:$/
     */
    public function theFollowingUsersShouldBeFound(string $expectedUsersAliasesYaml): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        /** @var string[] $expectedUsersAliases */
        $expectedUsersAliases = Yaml::parse($expectedUsersAliasesYaml);
        $actualUsers = $this->httpClient->getLastResponseJson()->getAsArray('users');
        Assert::assertEquals(count($expectedUsersAliases), count($actualUsers));
        $expectedUserIds = array_map(
            fn (string $userAlias) => $this->fixtureRegistry->getUserFixture($userAlias)->getId()->toString(),
            $expectedUsersAliases,
        );
        $actualUserIds = array_map(
            fn (array $user) => $user['id'],
            $actualUsers,
        );
        Assert::assertEquals($expectedUserIds, $actualUserIds);
    }

    /**
     * @When /^the user "([^"]*)" finds users$/
     */
    public function theUserFindsUsers(string $userAlias): void
    {
        $this->httpClient->findUsers(accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken());
    }

    /**
     * @When /^the user "([^"]*)" finds users with the following params:$/
     */
    public function theUserFindsUsersWithTheFollowingParams(string $userAlias, string $paramsYaml): void
    {
        /** @var mixed[] $params */
        $params = Yaml::parse($paramsYaml);
        $this->httpClient->findUsers(
            queryString: $params,
            accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken(),
        );
    }
}
