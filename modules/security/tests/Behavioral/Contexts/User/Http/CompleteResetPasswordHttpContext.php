<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\TestHelper;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final readonly class CompleteResetPasswordHttpContext implements Context
{
    /**
     * @param FrozenClock $clock
     */
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private ClockInterface $clock,
    ) {
    }

    /**
     * @When /^an unauthenticated user completes user "([^"]*)" password reset with data:$/
     */
    public function anUnauthenticatedUserCompletesUserPasswordResetWithData(
        string $userAlias,
        string $data,
    ): void {
        $data = Yaml::parse($data);
        $data['token'] = $this->applicationClient->getPasswordResetToken($userAlias);
        $this->httpClient->completePasswordReset($data);
    }

    /**
     * @Given /^there should ba a confirmation about successfully completed password reset$/
     */
    public function thereShouldBaAConfirmationAboutSuccessfullyCompletedPasswordReset(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthenticated user completes user "([^"]*)" password reset$/
     */
    public function anUnauthenticatedUserCompletesUserPasswordReset(string $userAlias): void
    {
        $this->httpClient->completePasswordReset(
            [
                'token' => $this->applicationClient->getPasswordResetToken($userAlias),
            ],
        );
    }

    /**
     * @When an unauthenticated user completes user :userAlias password reset at :time
     */
    public function anUnauthenticatedUserCompletesUserPasswordResetAt(string $userAlias, string $time): void
    {
        $token = $this->applicationClient->getPasswordResetToken($userAlias);
        $date = DateTime::fromString($time);
        $this->clock->freezeDate($date);
        TestHelper::freezeCurrentDate($date);
        $this->httpClient->completePasswordReset([
            'token' => $token,
        ]);
    }
}
