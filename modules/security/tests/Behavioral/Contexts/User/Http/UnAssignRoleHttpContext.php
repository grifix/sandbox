<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class UnAssignRoleHttpContext implements Context
{
    public function __construct(
        private SecurityFixtureRegistry $fixtureRegistry,
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationTestClient,
    ) {
    }

    /**
     * @When /^user "([^"]*)" unassigns role "([^"]*)" from user "([^"]*)"$/
     */
    public function userUnAssignsRoleFromUser(string $userAlias, string $roleAlias, string $fromUserAlias): void
    {
        $this->httpClient->unAssignRoleFromUser(
            $this->fixtureRegistry->getUserFixture($fromUserAlias)->getId(),
            [
                'roleId' => $this->fixtureRegistry->getRoleFixture($roleAlias)->getId()->toString(),
            ],
            $this->applicationTestClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should ba a confirmation about successfully unassigned role$/
     */
    public function thereShouldBaAConfirmationAboutSuccessfullyUnassignedRole(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^unauthorized user unassigns role from user$/
     */
    public function unauthorizedUserUnassignsRoleFromUser(): void
    {
        $this->httpClient->unAssignRoleFromUser();
    }
}
