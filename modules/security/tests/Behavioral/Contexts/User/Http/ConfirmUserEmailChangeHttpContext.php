<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\TestHelper as JwtTestHelper;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class ConfirmUserEmailChangeHttpContext implements Context
{
    /**
     * @param FrozenClock $clock
     */
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private ClockInterface $clock,
    ) {
    }

    /**
     * @When /^user "([^"]*)" confirms his email change$/
     */
    public function userConfirmsHisEmailChange(string $userAlias): void
    {
        $this->httpClient->confirmUserEmailChange([
            'token' => $this->applicationClient->getEmailConfirmationToken($userAlias),
        ]);
    }

    /**
     * @Then /^there should be a confirmation about successfully confirmed email change$/
     */
    public function thereShouldBeAConfirmationAboutSuccessfullyConfirmedEmailChange(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When user :userAlias confirms his email change at :time
     */
    public function userConfirmsHisEmailChangeAt(string $userAlias, string $time): void
    {
        $token = $this->applicationClient->getEmailConfirmationToken($userAlias);
        $date = DateTime::fromString($time);
        $this->clock->freezeDate($date);
        JwtTestHelper::freezeCurrentDate($date);
        $this->httpClient->confirmUserEmailChange([
            'token' => $token,
        ]);
    }

    /**
     * @When /^user confirms his email change with invalid token$/
     */
    public function userConfirmsHisEmailChangeWithInvalidToken(): void
    {
        $this->httpClient->confirmUserEmailChange(
            [
                'token' => 'invalid_token',
            ],
        );
    }
}
