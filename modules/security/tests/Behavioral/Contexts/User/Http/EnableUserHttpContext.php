<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final readonly class EnableUserHttpContext implements Context
{
    public function __construct(
        private SecurityHttpTestClient $httpClient,
        private SecurityApplicationTestClient $applicationClient,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @When /^user "([^"]*)" enables user "([^"]*)"$/
     */
    public function userEnablesUser(string $userAlias, string $targetUserAlias): void
    {
        $this->httpClient->enableUser(
            $this->fixtureRegistry->getUserFixture($targetUserAlias)->getId(),
            $this->applicationClient->getUserAccessToken($userAlias)
        );
    }

    /**
     * @Given /^there should be a confirmation about successfully enabled user$/
     */
    public function thereShouldBeAConfirmationAboutSuccessfullyEnabledUser(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^user "([^"]*)" enables user$/
     */
    public function userEnablesAnyUser(string $userAlias): void
    {
        $this->httpClient->enableUser(
            accessToken: $this->applicationClient->getUserAccessToken($userAlias)
        );
    }

    /**
     * @When /^an unauthorized user enables user$/
     */
    public function anUnauthorizedUserEnablesUser(): void
    {
        $this->httpClient->enableUser();
    }
}
