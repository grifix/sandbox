<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Grifix\Email\Email;
use Grifix\Test\Fixture\FixtureFactory;
use Modules\Security\Tests\Behavioral\Fixtures\UserFixture;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Symfony\Component\Yaml\Yaml;

final readonly class UserFixtureContext implements Context
{
    public function __construct(
        private FixtureFactory $fixtureFactory,
        private SecurityFixtureRegistry $fixtureRegistry,
        private SecurityApplicationTestClient $applicationClient,
    ) {
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)" with permission "([^"]*)"$/
     */
    public function thereIsAUserWithPermission(string $userAlias, string $permission): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withPermissions($permission)
            ->build($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)"$/
     */
    public function thereIsAUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->build($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)" with role "([^"]*)"$/
     */
    public function thereIsAUserWithRole(string $userAlias, string $roleAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withAssignedRoles($this->fixtureRegistry->getRoleFixture($roleAlias)->getId())
            ->build($userAlias);
    }

    /**
     * @Given /^there is a user with email "([^"]*)"$/
     */
    public function thereIsAUserWithEmail(string $email): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->withEmail(Email::create($email))->build();
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)" with email "([^"]*)" and password "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedUserWithEmailAndPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withPassword($password)
            ->shouldBeAuthenticated()
            ->build($userAlias);
    }

    /**
     * @Given /^there is suspended user "([^"]*)" with email "([^"]*)" and password "([^"]*)"$/
     */
    public function thereIsSuspendedUserWithEmailAndPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withPassword($password)
            ->shouldBeSuspended()
            ->build($userAlias);
    }

    /**
     * @Given /^there is disabled user "([^"]*)" with email "([^"]*)" and password "([^"]*)"$/
     */
    public function thereIsDisabledUserWithEmailAndPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withPassword($password)
            ->shouldBeDisabled()
            ->build($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)" with email "([^"]*)" and password "([^"]*)"$/
     */
    public function thereIsAUserWithEmailAndPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withPassword($password)
            ->build($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)" with confirmed email "([^"]*)"$/
     */
    public function thereIsAUserWithConfirmedEmailValue(string $userAlias, string $email): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->shouldHaveConfirmedEmail()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated disabled user "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedDisabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->shouldBeDisabled()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated suspended user "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedSuspendedUserWithPassword(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->shouldBeSuspended()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)" with password "([^"]*)" and confirmed email$/
     */
    public function thereIsAnAuthenticatedUserWithPassword(string $userAlias, string $password): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->shouldHaveConfirmedEmail()
            ->setPassword($password)
            ->build($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)" with not confirmed email$/
     */
    public function thereIsAUserWithNotConfirmedEmail(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->build($userAlias);
    }

    /**
     * @Given /^there a suspended user "([^"]*)" with not confirmed email$/
     */
    public function thereASuspendedUserWithNotConfirmedEmail(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->shouldBeSuspended()->build($userAlias);
    }

    /**
     * @Given /^there a disabled user "([^"]*)" with not confirmed email$/
     */
    public function thereADisabledUserWithNotConfirmedEmail(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->shouldBeDisabled()->build($userAlias);
    }

    /**
     * @Given /^there is a disabled user "([^"]*)"$/
     */
    public function thereIsADisabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->shouldBeDisabled()->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->shouldBeAuthenticated()->build($userAlias);
    }

    /**
     * @Given /^there is a enabled user "([^"]*)"$/
     */
    public function thereIsAEnabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->build($userAlias);
    }

    /**
     * @Given /^there is a suspended user "([^"]*)"$/
     */
    public function thereIsASuspendedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)->shouldBeSuspended()->build($userAlias);
    }

    /**
     * @Given /^user "([^"]*)" has changed his email$/
     */
    public function userHasChangedHisEmail(string $userAlias): void
    {
        $this->applicationClient->changeUserEmail($userAlias);
    }

    /**
     * @Given /^there is a user "([^"]*)" with the email "([^"]*)"$/
     */
    public function thereIsAUserWithTheEmail(string $userAlias, string $email): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered and authenticated user "([^"]*)" with email "([^"]*)" and password "([^"]*)"$/
     */
    public function thereIsAnRegisteredAndAuthenticatedUserWithEmailAndPassword(
        string $userAlias,
        string $email,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withPassword($password)
            ->withCompletedRegistration()
            ->shouldBeAuthenticated()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated registered and suspended user "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedRegisteredAndSuspendedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withCompletedRegistration()
            ->shouldBeSuspended()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated registered and disabled user "([^"]*)"$/
     */
    public function thereIsAnAuthenticatedRegisteredAndDisabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withCompletedRegistration()
            ->shouldBeDisabled()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)" with not completed registration$/
     */
    public function thereIsAnAuthenticatedUserWithNotCompletedRegistration(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated user "([^"]*)" with password "([^"]*)" and completed registration$/
     */
    public function thereIsAnAuthenticatedUserWithPasswordAndCompletedRegistration(
        string $userAlias,
        string $password,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withCompletedRegistration()
            ->withPassword($password)
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated suspended user "([^"]*)" with completed registration$/
     */
    public function thereIsAnAuthenticatedSuspendedUserWithCompletedRegistration(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->shouldBeSuspended()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an authenticated disabled user "([^"]*)" with completed registration$/
     */
    public function thereIsAnAuthenticatedDisabledUserWithCompletedRegistration(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->shouldBeDisabled()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered user "([^"]*)" with email "([^"]*)" and initiated email change to "([^"]*)"$/
     */
    public function thereIsAnRegisteredUserWithEmailAndInitiatedEmailChangeTo(
        string $userAlias,
        string $email,
        string $newEmail,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withCompletedRegistration()
            ->withInitiatedEmailChangeTo(Email::create($newEmail))
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered authenticated user "([^"]*)"$/
     */
    public function thereIsAnRegisteredAuthenticatedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered and authenticated user "([^"]*)"$/
     */
    public function thereIsAnRegisteredAndAuthenticatedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeAuthenticated()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there a registered and suspended user "([^"]*)" with email "([^"]*)" who has initiated email change to "([^"]*)"$/
     */
    public function thereARegisteredAndSuspendedUserWithEmailWhoHasInitiatedEmailChangeTo(
        string $userAlias,
        string $email,
        string $newEmail,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withCompletedRegistration()
            ->shouldBeSuspended()
            ->withInitiatedEmailChangeTo(Email::create($newEmail))
            ->build($userAlias);
    }

    /**
     * @Given /^there a registered disabled user "([^"]*)" with email "([^"]*)" who has initiated email change to "([^"]*)"$/
     */
    public function thereARegisteredDisabledUserWithEmailWhoHasInitiatedEmailChangeTo(
        string $userAlias,
        string $email,
        string $newEmail,
    ): void {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withEmail(Email::create($email))
            ->withCompletedRegistration()
            ->shouldBeDisabled()
            ->withInitiatedEmailChangeTo(Email::create($newEmail))
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered and disabled user "([^"]*)"$/
     */
    public function thereIsAnRegisteredAndDisabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeDisabled()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered and suspended user "([^"]*)"$/
     */
    public function thereIsAnRegisteredAndSuspendedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeSuspended()
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there is an registered user "([^"]*)" with initiated password reset$/
     */
    public function thereIsAnRegisteredUserWithInitiatedPasswordReset(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withCompletedRegistration()
            ->withInitiatedPasswordReset()
            ->build($userAlias);
    }

    /**
     * @Given /^there a disabled registered user "([^"]*)" with initiated password reset$/
     */
    public function thereADisabledRegisteredUserWithInitiatedPasswordReset(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeDisabled()
            ->withCompletedRegistration()
            ->withInitiatedPasswordReset()
            ->build($userAlias);
    }

    /**
     * @Given /^there is a registered user "([^"]*)"$/
     */
    public function thereIsARegisteredUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withCompletedRegistration()
            ->build($userAlias);
    }

    /**
     * @Given /^there ia a registered user "([^"]*)" with the following data:$/
     */
    public function thereIaAUserWithTheFollowingData(string $userAlias, string $data): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withCompletedRegistration()
            ->withRawData(Yaml::parse($data)) //@phpstan-ignore-line
            ->build($userAlias);
    }

    /**
     * @Given /^the user "([^"]*)" is authenticated$/
     */
    public function theUserIsAuthenticated(string $userAlias): void
    {
        $this->fixtureRegistry->getUserFixture($userAlias)->authenticate();
    }

    /**
     * @Given /^there are the following users:$/
     */
    public function thereAreTheFollowingUsers(TableNode $table): void
    {
        foreach ($table->getHash() as $row) {
            $this->fixtureFactory->createFixture(UserFixture::class)
                ->withRawData($row)
                ->build($row['alias'] ?? null);
        }
    }

    /**
     * @Given /^the user "([^"]*)" has the following permissions:$/
     */
    public function theUserHasTheFollowingPermissions(string $userAlias, string $permissionsYaml): void
    {
        /** @var string[] $permissions */
        $permissions = Yaml::parse($permissionsYaml);
        foreach ($permissions as $permission) {
            $this->fixtureRegistry->getUserFixture($userAlias)->grantPermission($permission);
        }
    }

    /**
     * @Given /^the user "([^"]*)" has permission "([^"]*)"$/
     */
    public function theUserHasPermission(string $userAlias, string $permission): void
    {
        $this->fixtureRegistry->getUserFixture($userAlias)->grantPermission($permission);
    }

    /**
     * @Given /^there is an user "([^"]*)"$/
     */
    public function thereIsAnUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->build($userAlias);
    }

    /**
     * @Given /^there a disabled user "([^"]*)"$/
     */
    public function thereADisabledUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeDisabled()
            ->build($userAlias);
    }

    /**
     * @Given /^there a suspended user "([^"]*)"$/
     */
    public function thereASuspendedUser(string $userAlias): void
    {
        $this->fixtureFactory->createFixture(UserFixture::class)
            ->shouldBeSuspended()
            ->build($userAlias);
    }
}
