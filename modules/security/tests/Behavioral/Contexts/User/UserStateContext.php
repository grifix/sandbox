<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\User;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Clock\ClockInterface;
use Grifix\Uuid\Uuid;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityVariableRegistry;
use PHPUnit\Framework\Assert;
use Symfony\Component\Yaml\Yaml;

final readonly class UserStateContext implements Context
{
    public function __construct(
        private SecurityApplicationTestClient $applicationClient,
        private ClockInterface $clock,
        private SecurityFixtureRegistry $fixtureRegistry,
        private SecurityVariableRegistry $variableRegistry,
    ) {
    }

    /**
     * @Given /^user "([^"]*)" should not have permission "([^"]*)"$/
     */
    public function userShouldNotHavePermission(string $userAlias, string $permission): void
    {
        /** @var string[] $permissions */
        $permissions = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->permissions;
        Assert::assertNotContains(
            $permission,
            $permissions,
        );
    }

    /**
     * @Then /^user "([^"]*)" should have permission "([^"]*)"$/
     */
    public function userShouldHavePermission(string $userAlias, string $permission): void
    {
        /** @var string[] $permissions */
        $permissions = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->permissions;
        Assert::assertContains(
            $permission,
            $permissions,
        );
    }

    /**
     * @Then /^user "([^"]*)" should have role "([^"]*)"$/
     */
    public function userShouldHaveRole(string $userAlias, string $roleAlias): void
    {
        $user = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState();
        $role = $this->fixtureRegistry->getRoleFixture($roleAlias)->fetchState();
        /** @var string[] $roles */
        $roles = array_map(fn (Uuid $roleId) => $roleId->toString(), $user->roles);
        Assert::assertContains($role->id->toString(), $roles);
    }

    /**
     * @Given /^user "([^"]*)" should not have role "([^"]*)"$/
     */
    public function userShouldNotHaveRole(string $userAlias, string $roleAlias): void
    {
        $user = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState();
        $role = $this->fixtureRegistry->getRoleFixture($roleAlias)->fetchState();
        $roles = array_map(fn (Uuid $roleId) => $roleId->toString(), $user->roles);
        Assert::assertNotContains($role->id->toString(), $roles);
    }

    /**
     * @Then user :userAlias should be suspended for :minutes minutes
     */
    public function userShouldBeSuspended(string $userAlias, int $minutes): void
    {
        $user = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState();
        Assert::assertNotNull($user->resumeDate);
        Assert::assertTrue($user->resumeDate->isAfter($this->clock->getCurrentDate()));
        Assert::assertEquals($minutes, $user->resumeDate->diff($this->clock->getCurrentDate())->getMinutes());
    }

    /**
     * @Then /^user "([^"]*)" should have password "([^"]*)"$/
     */
    public function userShouldHavePassword(string $userAlias, string $expectedPassword): void
    {
        Assert::assertTrue($this->applicationClient->hasUserWithPassword($userAlias, $expectedPassword));
    }

    /**
     * @Given /^user "([^"]*)" password should be unchanged$/
     */
    public function userPasswordShouldBeUnchanged(string $userAlias): void
    {
        Assert::assertTrue(
            $this->applicationClient->hasUserWithPassword(
                $userAlias,
                $this->fixtureRegistry->getUserFixture($userAlias)->getPassword(),
            ),
        );
    }

    /**
     * @Then /^user "([^"]*)" should be disabled$/
     */
    public function userShouldBeDisabled(string $userAlias): void
    {
        Assert::assertTrue($this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->disabled);
    }

    /**
     * @Then /^user "([^"]*)" should be enabled$/
     */
    public function userShouldBeEnabled(string $userAlias): void
    {
        Assert::assertFalse($this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->disabled);
    }

    /**
     * @Given /^there should be a new user with not completed registration and the following data:$/
     */
    public function thereShouldBeANewUserWithNotCompletedRegistrationAndTheFollowingData(string $data): void
    {
        $data = ArrayWrapper::create(Yaml::parse($data)); // @phpstan-ignore-line
        Assert::assertNotNull($this->variableRegistry->createdUserId);
        $user = $this->applicationClient->getUser($this->variableRegistry->createdUserId);
        Assert::assertEquals($data->getElement('email'), $user->email->toString());
        Assert::assertTrue(
            $this->applicationClient->isUserPasswordEqualTo(
                $user->id,
                $data->getElement('password'), // @phpstan-ignore-line
            ),
        );
    }

    /**
     * @Then /^user "([^"]*)" should have initiated email change to email "([^"]*)"$/
     */
    public function userShouldHaveInitiatedEmailChangeToEmail(string $userAlias, string $email): void
    {
        $newEmail = $this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->newEmail;
        Assert::assertNotNull($newEmail);
        Assert::assertEquals($email, $newEmail->toString());
    }

    /**
     * @Given /^there should be no initiated email change for user "([^"]*)"$/
     */
    public function thereShouldBeNoInitiatedEmailChangeForUser(string $userAlias): void
    {
        Assert::assertNull($this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->newEmail);
    }

    /**
     * @Given /^user "([^"]*)" should have no active refresh access sessions$/
     */
    public function userShouldHaveNoActiveRefreshAccessSessions(string $userAlias): void
    {
        Assert::assertFalse($this->applicationClient->hasActiveRefreshAccessSession($userAlias));
    }

    /**
     * @Given /^user "([^"]*)" should have email "([^"]*)"$/
     */
    public function userShouldHaveEmail(string $userAlias, string $email): void
    {
        Assert::assertEquals($email, $this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->email->toString());
    }

    /**
     * @Then /^the password reset should be initiated for the user "([^"]*)"$/
     */
    public function thePasswordResetShouldBeInitiatedForTheUser(string $userAlias): void
    {
        Assert::assertTrue($this->fixtureRegistry->getUserFixture($userAlias)->fetchState()->passwordResetInitiated);
    }

    /**
     * @Given /^user "([^"]*)" should be registered$/
     */
    public function userShouldBeRegistered(string $userAlias): void
    {
        Assert::assertTrue($this->fixtureRegistry->getUserFixture($userAlias)->isRegistered());
    }

    /**
     * @Given /^user "([^"]*)" should not be registered$/
     */
    public function userShouldNotBeRegistered(string $userAlias): void
    {
        Assert::assertFalse($this->fixtureRegistry->getUserFixture($userAlias)->isRegistered());
    }
}
