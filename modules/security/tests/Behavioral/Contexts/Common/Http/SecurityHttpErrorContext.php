<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Common\Http;

use Behat\Behat\Context\Context;
use Grifix\Test\TestHttpClient;
use Modules\Security\Domain\Common\SecurityErrorCodes;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class SecurityHttpErrorContext implements Context
{
    /**
     * @var array<string, array{int, int}>
     */
    private array $errors = [
        'Role already assigned!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::roleAlreadyAssigned->value,
        ],
        'User does not exist!' => [
            Response::HTTP_NOT_FOUND,
            SecurityErrorCodes::userDoesNotExist->value,
        ],
        'User is suspended!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::userIsSuspended->value,
        ],
        'User is disabled!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::userIsDisabled->value,
        ],
        'Registration is not completed!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::registrationIsNotCompleted->value,
        ],
        'Password mismatch!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::passwordMismatch->value,
        ],
        'Wrong password!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::wrongPassword->value,
        ],
        'Password is insecure!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::passwordIsInsecure->value,
        ],
        'Password reset is not initiated!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::passwordResetIsNotInitiated->value,
        ],
        'Invalid access token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::invalidAccessToken->value,
        ],
        'Email change is not initiated!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::emailChangeIsNotInitiated->value,
        ],
        'Email is not unique!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::userEmailIsNotUnique->value,
        ],
        'Expired access token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::expiredAccessToken->value,
        ],
        'Expired refresh token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::expiredRefreshToken->value,
        ],
        'Invalid refresh token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::invalidRefreshToken->value,
        ],
        'Expired password reset token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::expiredPasswordResetToken->value,
        ],
        'Invalid password reset token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::invalidPasswordResetToken->value,
        ],
        'Expired email change confirmation token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::expiredEmailChangeConfirmationToken->value,
        ],
        'Invalid email change confirmation token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::invalidEmailChangeConfirmationToken->value,
        ],
        'Refresh access session is revoked!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::refreshAccessSessionRevoked->value,
        ],
        'Registration is already completed!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::registrationIsAlreadyCompleted->value,
        ],
        'Expired registration confirmation token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::expiredRegistrationConfirmationToken->value,
        ],
        'Invalid registration confirmation token!' => [
            Response::HTTP_BAD_REQUEST,
            SecurityErrorCodes::invalidRegistrationConfirmationToken->value,
        ],
    ];

    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }

    /**
     * @Then /^there should be a security error "([^"]*)"$/
     */
    public function thereShouldBeASecurityError(string $errorKey): void
    {
        $this->assertError($errorKey);
    }

    private function assertError(string $errorKey, ?string $message = null): void
    {
        if (!array_key_exists($errorKey, $this->errors)) {
            throw new \Exception('Unknown error!');
        }
        if (null === $message) {
            $message = $errorKey;
        }
        $error = $this->errors[$errorKey];
        Assert::assertEquals($error[0], $this->httpClient->getLastResponseStatusCode());
        $response = $this->httpClient->getLastResponseJson();
        Assert::assertEquals($message, $response->getElement('error.message'));
        Assert::assertEquals($error[1], $response->getElement('error.code'));
    }

    /**
     * @Then /^there should be a security error "([^"]*)" with the message "([^"]*)"$/
     */
    public function thereShouldBeASecurityErrorWithTheMessage(string $errorKey, string $message): void
    {
        $this->assertError($errorKey, $message);
    }
}
