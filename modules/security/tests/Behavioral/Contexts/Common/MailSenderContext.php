<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Common;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Grifix\Email\Email;
use Modules\Security\Application\Common\Ports\MailSender\Emails\EmailChangeConfirmationEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\Emails\PasswordResetEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\Emails\RegistrationConfirmationEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\MailSenderInterface;
use Modules\Security\Tests\Stubs\MailSenderStub;
use PHPUnit\Framework\Assert;

final readonly class MailSenderContext implements Context
{
    /**
     * @param MailSenderStub $mailSender
     */
    public function __construct(
        private MailSenderInterface $mailSender,
    ) {
    }

    /**
     * @Given /^there should be sent an email with registration confirmation token to "([^"]*)"$/
     */
    public function thereShouldBeSentAnEmailWithRegistrationConfirmationTokenTo(string $email): void
    {
        Assert::assertTrue(
            $this->mailSender->hasEmailSentTo(
                Email::create($email),
                RegistrationConfirmationEmailPayloadDto::class,
            ),
        );
    }

    /**
     * @Given /^there should be sent a message with email confirmation token to the email "([^"]*)"$/
     */
    public function thereShouldBeSentAMessageWithEmailConfirmationTokenToTheEmail(string $email): void
    {
        Assert::assertTrue(
            $this->mailSender->hasEmailSentTo(
                Email::create($email),
                EmailChangeConfirmationEmailPayloadDto::class,
            ),
        );
    }

    /**
     * @Given /^there should be sent a message with password reset token to the email "([^"]*)"$/
     */
    public function thereShouldBeSentAMessageWithPasswordResetTokenToTheEmail(string $email): void
    {
        Assert::assertTrue(
            $this->mailSender->hasEmailSentTo(
                Email::create($email),
                PasswordResetEmailPayloadDto::class,
            ),
        );
    }
}
