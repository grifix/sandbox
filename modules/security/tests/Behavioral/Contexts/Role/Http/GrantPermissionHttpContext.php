<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Role\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Domain\Token\Dto\TokenTypeDto;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class GrantPermissionHttpContext implements Context
{
    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityFixtureRegistry $fixtureRegistry,
        private readonly SecurityApplicationTestClient $applicationClient,
    ) {
    }

    /**
     * @When /^user "([^"]*)" grants permission "([^"]*)" for role "([^"]*)"$/
     */
    public function userGrantsPermissionForRole(string $userAlias, string $permission, string $roleAlias): void
    {
        $this->httpClient->grantRolePermission(
            $this->fixtureRegistry->getRoleFixture($roleAlias)->getId(),
            [
                'permission' => $permission,
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should be a confirmation about successfully granted permission$/
     */
    public function thereShouldBeAConfirmationAboutGrantedPermission(): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthorized grants permission for the role$/
     */
    public function anUnauthorizedGrantsPermissionForTheRole(): void
    {
        $this->httpClient->grantRolePermission();
    }
}
