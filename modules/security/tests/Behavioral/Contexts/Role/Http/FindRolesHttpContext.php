<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Role\Http;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final readonly class FindRolesHttpContext implements Context
{
    public function __construct(
        private SecurityFixtureRegistry $fixtureRegistry,
        private SecurityHttpTestClient $httpClient,
    ) {
    }

    /**
     * @Then /^the following roles should be found:$/
     */
    public function theFollowingRolesShouldBeFound(string $expectedRoleAliasesYaml): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        /** @var string[] $expectedUsersAliases */
        $expectedUsersAliases = Yaml::parse($expectedRoleAliasesYaml);
        $actualRoles = $this->httpClient->getLastResponseJson()->getAsArray('roles');
        Assert::assertEquals(count($expectedUsersAliases), count($actualRoles));
        $expectedRolesIds = array_map(
            fn (string $userAlias) => $this->fixtureRegistry->getRoleFixture($userAlias)->getId()->toString(),
            $expectedUsersAliases,
        );
        $actualRolesIds = array_map(
            fn (array $role) => $role['id'],
            $actualRoles,
        );
        Assert::assertEquals($expectedRolesIds, $actualRolesIds);
    }

    /**
     * @When /^the user "([^"]*)" finds roles$/
     */
    public function theUserFindsRoles(string $userAlias): void
    {
        $this->httpClient->findRoles(accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken());
    }

    /**
     * @When /^the user "([^"]*)" finds roles by name "([^"]*)"$/
     */
    public function theUserFindsRolesByName(string $userAlias, string $name): void
    {
        $this->httpClient->findRoles(
            queryString: [
                'name' => $name,
            ],
            accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken(),
        );
    }

    /**
     * @When /^the user "([^"]*)" finds roles with the following params:$/
     */
    public function theUserFindsRolesWithTheFollowingParams(string $userAlias, string $paramsYaml): void
    {
        /** @var mixed[] $params */
        $params = Yaml::parse($paramsYaml);
        $this->httpClient->findRoles(
            queryString: $params,
            accessToken: $this->fixtureRegistry->getUserFixture($userAlias)->getAccessToken(),
        );
    }
}
