<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Role\Http;

use Behat\Behat\Context\Context;
use Grifix\Uuid\Uuid;
use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class CreateRoleHttpContext implements Context
{
    private ?Uuid $createdRoleId = null;

    public function __construct(
        private readonly SecurityHttpTestClient $httpClient,
        private readonly SecurityApplicationTestClient $applicationClient,
    ) {
    }

    /**
     * @When /^user "([^"]*)" creates a new role with name "([^"]*)"$/
     */
    public function userCreatesANewRoleWithName(string $userAlias, string $roleName): void
    {
        $response = $this->httpClient->createRole(
            [
                'name' => $roleName,
            ],
            $this->applicationClient->getUserAccessToken($userAlias),
        );

        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $this->createdRoleId = Uuid::createFromString(
                (string)$this->httpClient->getLastResponseJson()->getElement('roleId'),
            );
        }
    }

    /**
     * @Then /^there should be a new role with name "([^"]*)"$/
     */
    public function thereShouldBeANewRole(string $roleName): void
    {
        Assert::assertEquals(
            $roleName,
            $this->applicationClient->getRole($this->createdRoleId)->name,
        );
    }

    /**
     * @When /^user "([^"]*)" creates a new role$/
     */
    public function userCreatesANewRole(string $userAlias): void
    {
        $this->httpClient->createRole(
            $this->httpClient->buildCreateRolePayload(),
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @When /^user "([^"]*)" creates a new role with empty input$/
     */
    public function userCreatesANewRoleWithEmptyInput(string $userAlias): void
    {
        $this->httpClient->createRole(
            [],
            $this->applicationClient->getUserAccessToken($userAlias),
        );
    }

    /**
     * @Given /^there should be a confirmation about successfully created role$/
     */
    public function thereShouldBeAConfirmationAboutSuccessfullyCreatedRole(): void
    {
        Assert::assertEquals(Response::HTTP_CREATED, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^an unauthorized user creates a new role$/
     */
    public function anUnauthorizedUserCreatesANewRole()
    {
        $this->httpClient->createRole();
    }
}
