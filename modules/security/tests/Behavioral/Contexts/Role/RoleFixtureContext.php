<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Role;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Uuid\Uuid;
use Modules\Security\Tests\Behavioral\Fixtures\RoleFixture;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use Symfony\Component\Yaml\Yaml;

final readonly class RoleFixtureContext implements Context
{
    public function __construct(
        private FixtureFactory $fixtureFactory,
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @Given /^there is a role with name "([^"]*)"$/
     */
    public function thereIsARoleWithName(string $name): void
    {
        $this->fixtureFactory->createFixture(RoleFixture::class)
            ->withName($name)
            ->build();
    }

    /**
     * @Given /^there is a role "([^"]*)"$/
     */
    public function thereIsARole(string $alias): void
    {
        $this->fixtureFactory
            ->createFixture(RoleFixture::class)
            ->build($alias);
    }

    /**
     * @Given /^there is a role "([^"]*)" with permission "([^"]*)"$/
     */
    public function thereIsARoleWithPermission(string $roleAlias, string $permission): void
    {
        $this->fixtureFactory->createFixture(RoleFixture::class)
            ->withPermissions($permission)
            ->build($roleAlias);
    }

    /**
     * @Given /^there are the following roles:$/
     */
    public function thereAreTheFollowingRoles(TableNode $table): void
    {
        foreach ($table->getHash() as $row) {
            $this->fixtureFactory->createFixture(RoleFixture::class)
                ->withId(Uuid::createFromString($row['id']))
                ->withName($row['name'])
                ->build($row['alias']);
        }
    }

    /**
     * @Given /^the role "([^"]*)" has the following permissions:$/
     */
    public function theRoleHasTheFollowingPermissions(string $roleAlias, string $permissionsYaml): void
    {
        /** @var string[] $permissions */
        $permissions = Yaml::parse($permissionsYaml);
        foreach ($permissions as $permission) {
            $this->fixtureRegistry->getRoleFixture($roleAlias)->grantPermission($permission);
        }
    }
}
