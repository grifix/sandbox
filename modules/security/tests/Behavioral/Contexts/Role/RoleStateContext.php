<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral\Contexts\Role;

use Behat\Behat\Context\Context;
use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
use PHPUnit\Framework\Assert;

final readonly class RoleStateContext implements Context
{
    public function __construct(
        private SecurityFixtureRegistry $fixtureRegistry,
    ) {
    }

    /**
     * @Then /^role "([^"]*)" should have permission "([^"]*)"$/
     */
    public function roleShouldHavePermission(string $roleAlias, string $permission): void
    {
        Assert::assertContains(
            $permission,
            $this->fixtureRegistry->getRoleFixture($roleAlias)->fetchState()->permissions,
        );
    }

    /**
     * @Then /^role "([^"]*)" should not have permission "([^"]*)"$/
     */
    public function roleShouldNotHavePermission(string $roleAlias, string $permission): void
    {
        Assert::assertNotContains(
            $permission,
            $this->fixtureRegistry->getRoleFixture($roleAlias)->fetchState()->permissions,
        );
    }
}
