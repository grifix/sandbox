<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Behavioral;

use Grifix\Uuid\Uuid;

final class SecurityVariableRegistry
{
    public ?Uuid $createdUserId = null;

    public ?Uuid $createdRefreshAccessSessionId = null;

}
