<?php

declare(strict_types=1);

namespace {

    use Grifix\Framework\Config\ModuleServicesConfigurator;
    use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
    use Modules\Security\Application\Common\Ports\MailSender\MailSenderInterface;
    use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;
    use Modules\Security\Application\RefreshAccessSession\Ports\Repository\RefreshAccessSessionRepositoryInterface;
    use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;
    use Modules\Security\Application\Role\Ports\Repository\RoleRepositoryInterface;
    use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;
    use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;
    use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSessionOutsideInterface;
    use Modules\Security\Domain\Role\RoleOutsideInterface;
    use Modules\Security\Domain\User\UserOutsideInterface;
    use Modules\Security\Infrastructure\Common\Application\Ports\JwtTokenManager;
    use Modules\Security\Infrastructure\Common\Application\Ports\SymfonyMailSender;
    use Modules\Security\Infrastructure\Common\Connectors\PasswordGenerator\HackzillaPasswordGeneratorConnector;
    use Modules\Security\Infrastructure\Common\Connectors\PasswordGenerator\PasswordGeneratorConnectorInterface;
    use Modules\Security\Infrastructure\Common\Connectors\PasswordHash\PasswordHashConnectorInterface;
    use Modules\Security\Infrastructure\Common\Connectors\PasswordHash\PhpPasswordHashConnector;
    use Modules\Security\Infrastructure\RefreshAccessSession\Application\Ports\RefreshAccessSessionProjector;
    use Modules\Security\Infrastructure\RefreshAccessSession\Application\Ports\RefreshAccessSessionRepository;
    use Modules\Security\Infrastructure\RefreshAccessSession\Domain\RefreshAccessSessionOutside;
    use Modules\Security\Infrastructure\Role\Application\Ports\RoleProjector;
    use Modules\Security\Infrastructure\Role\Application\Ports\RoleRepository;
    use Modules\Security\Infrastructure\Role\Domain\RoleOutside;
    use Modules\Security\Infrastructure\User\Application\Ports\UserProjector;
    use Modules\Security\Infrastructure\User\Application\Ports\UserRepository;
    use Modules\Security\Infrastructure\User\Domain\UserOutside;
    use Modules\Security\Tests\Behavioral\SecurityApplicationTestClient;
    use Modules\Security\Tests\Behavioral\SecurityFixtureRegistry;
    use Modules\Security\Tests\Behavioral\SecurityHttpTestClient;
    use Modules\Security\Tests\Behavioral\SecurityVariableRegistry;
    use Modules\Security\Tests\Stubs\MailSenderStub;
    use Modules\Security\Ui\Role\Vault\GuardedRoleFactory;
    use Modules\Security\Ui\Role\Vault\GuardedRoleVault;
    use Modules\Security\Ui\SecurityExceptionConverterProvider;
    use Modules\Security\Ui\User\Vault\GuardedUserFactory;
    use Modules\Security\Ui\User\Vault\GuardedUserVault;
    use Sandbox\Security\MessageSender\MessageSender;
    use Sandbox\Security\MessageSender\MessageSenderInterface;
    use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

    return static function (ContainerConfigurator $configurator) {
        ModuleServicesConfigurator::create($configurator)
            ->configure('security', 'Security');

        $services = $configurator->services();
        $services->defaults()->autowire()->autoconfigure();

        $services->set(RoleRepositoryInterface::class, RoleRepository::class);
        $services->set(RoleOutsideInterface::class, RoleOutside::class)->public();
        $services->set(RoleProjectorInterface::class, RoleProjector::class);

        $services->set(UserRepositoryInterface::class, UserRepository::class);
        $services->set(UserOutsideInterface::class, UserOutside::class)->public();
        $services->set(UserProjectorInterface::class, UserProjector::class);

        $services->set(RefreshAccessSessionRepositoryInterface::class, RefreshAccessSessionRepository::class);
        $services->set(RefreshAccessSessionProjectorInterface::class, RefreshAccessSessionProjector::class);
        $services->set(RefreshAccessSessionOutsideInterface::class, RefreshAccessSessionOutside::class)->public();

        $services->set(PasswordGeneratorConnectorInterface::class, HackzillaPasswordGeneratorConnector::class);
        $services->set(PasswordHashConnectorInterface::class, PhpPasswordHashConnector::class);

        $services->set(SecurityExceptionConverterProvider::class);

        $services->set(MessageSenderInterface::class, MessageSender::class);
        $services->set(JwtTokenManagerInterface::class, JwtTokenManager::class);

        $services->set(MailSenderInterface::class, SymfonyMailSender::class);

        $services->set(GuardedUserVault::class);

        $services->set(GuardedUserFactory::class);

        $services->set(GuardedRoleVault::class);

        $services->set(GuardedRoleFactory::class);

        if ($configurator->env() === 'test') {
            $services->set(SecurityHttpTestClient::class);
            $services->set(SecurityFixtureRegistry::class);
            $services->set(SecurityApplicationTestClient::class);
            $services->set(SecurityVariableRegistry::class);
            $services->set(MailSenderInterface::class, MailSenderStub::class);
        }
    };
}
