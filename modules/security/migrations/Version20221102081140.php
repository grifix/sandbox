<?php

declare(strict_types=1);

namespace Security;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Grifix\EntityManager\EntityMigrationTrait;

final class Version20221102081140 extends AbstractMigration
{
    use EntityMigrationTrait;

    private const SCHEMA = 'security';

    public function up(Schema $schema): void
    {
        $this->addSql('create schema ' . self::SCHEMA);
        $this->createEntityTable(self::SCHEMA, 'roles');
        $this->createRoleProjections();
        $this->createEntityTable(self::SCHEMA, 'users');
        $this->createUserProjection();
        $this->createUserRoleProjection();
        $this->createEntityTable(self::SCHEMA, 'refresh_access_sessions');
        $this->createRefreshAccessSessionProjection();
    }

    public function down(Schema $schema): void
    {
        $this->dropTable(self::SCHEMA, 'roles');
        $this->dropTable(self::SCHEMA, 'p_roles');
        $this->dropTable(self::SCHEMA, 'users');
        $this->dropTable(self::SCHEMA, 'p_users_roles');
        $this->dropTable(self::SCHEMA, 'p_users');
        $this->dropTable(self::SCHEMA, 'refresh_access_sessions');
        $this->dropTable(self::SCHEMA, 'p_refresh_access_sessions');
        $this->addSql('drop schema ' . self::SCHEMA);
    }

    private function createRoleProjections(): void
    {
        $sql = <<<SQL
create table security.p_roles
(
    id          uuid    not null
        constraint security_p_roles_pk
            primary key,
    "name" varchar not null,
    "permissions" jsonb not null default '[]',
    "created_at" timestamp not null default now()
);
SQL;
        $this->addSql($sql);
    }

    private function createUserProjection(): void
    {
        $sql = <<<SQL
create table security.p_users
(
    id uuid not null constraint security_p_users_pk primary key,
    email varchar not null,
    new_email varchar null,
    encrypted_password varchar not null,
    registration_completed bool not null default false,
    roles jsonb default '[]'::jsonb not null,
    resume_date timestamp,
    disabled bool not null default false,
    password_reset_initiated bool not null default false,
    created_at timestamp not null
)
SQL;
        $this->addSql($sql);
    }

    private function createUserRoleProjection(): void
    {
        $sql = <<<SQL
create table security.p_user_roles
(
    user_id uuid not null
        constraint security_p_user_roles_p_users_null_fk
            references security.p_users,
    role_id uuid not null
        constraint security_p_user_roles_p_roles_null_fk
            references security.p_roles,
    constraint security_p_user_roles_pk
        primary key (role_id, user_id)
);


SQL;
        $this->addSql($sql);
    }

    private function createRefreshAccessSessionProjection(): void
    {
        $sql = <<<SQL
create table security.p_refresh_access_sessions
(
    id          uuid    not null
        constraint security_p_refresh_access_sessions_pk
            primary key,
    "user_id" uuid not null,
    "revoked" boolean not null,
    "expires_at" timestamp not null
);
SQL;
        $this->addSql($sql);
    }
}
