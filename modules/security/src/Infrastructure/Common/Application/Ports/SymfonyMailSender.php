<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Application\Ports;

use Grifix\Email\Email;
use Modules\Security\Application\Common\Ports\MailSender\MailSenderInterface;

final readonly class SymfonyMailSender implements MailSenderInterface
{
    public function __construct()
    {
    }

    public function send(Email $to, object $payload): void
    {
        throw new \Exception('Not implemented yet');
    }
}
