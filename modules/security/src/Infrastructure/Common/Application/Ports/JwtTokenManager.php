<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Application\Ports;

use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\Exceptions\TokenExpiredException;
use Grifix\Jwt\JwtInterface;
use Grifix\Jwt\Payload;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;

final readonly class JwtTokenManager implements JwtTokenManagerInterface
{
    public function __construct(private JwtInterface $jwt)
    {
    }

    /**
     * @inheritdoc
     */
    public function decodeToken(string $token): array
    {
        try {
            return $this->jwt->decode($token)->toArray();
        } catch (TokenExpiredException $exception) {
            throw new ExpiredTokenException($exception);
        } catch (\Throwable $exception) {
            throw new InvalidTokenException($exception);
        }
    }

    /**
     * @inheritdoc
     */
    public function encodeToken(array $payload, DateTime $expiresAt): string
    {
        return $this->jwt->encode(
            Payload::create($payload, expiresAt: $expiresAt),
        );
    }
}
