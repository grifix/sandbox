<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Connectors\PasswordHash;

interface PasswordHashConnectorInterface
{
    public function hash(string $password): string;

    public function verify(string $password, string $hash):bool;
}
