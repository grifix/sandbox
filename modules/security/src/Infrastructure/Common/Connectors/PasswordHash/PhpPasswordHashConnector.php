<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Connectors\PasswordHash;

final class PhpPasswordHashConnector implements PasswordHashConnectorInterface
{

    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function verify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}
