<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Connectors\PasswordGenerator;

use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;

final class HackzillaPasswordGeneratorConnector implements PasswordGeneratorConnectorInterface
{
    private readonly RequirementPasswordGenerator $generator;

    public function __construct()
    {
        $this->generator = new RequirementPasswordGenerator();
        $this->generator
            ->setLength(8)
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols()
            ->setMinimumCount(ComputerPasswordGenerator::OPTION_UPPER_CASE, 1)
            ->setMinimumCount(ComputerPasswordGenerator::OPTION_LOWER_CASE, 1)
            ->setMinimumCount(ComputerPasswordGenerator::OPTION_NUMBERS, 1)
            ->setMinimumCount(ComputerPasswordGenerator::OPTION_SYMBOLS, 1);
    }

    public function generatePassword(): string
    {
        return $this->generator->generatePassword();
    }

    public function isPasswordSecure(string $password): bool
    {
        return $this->generator->validatePassword($password);
    }
}
