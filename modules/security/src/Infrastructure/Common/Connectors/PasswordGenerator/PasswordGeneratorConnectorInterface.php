<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Common\Connectors\PasswordGenerator;

interface PasswordGeneratorConnectorInterface
{
    public function generatePassword(): string;

    public function isPasswordSecure(string $password): bool;
}
