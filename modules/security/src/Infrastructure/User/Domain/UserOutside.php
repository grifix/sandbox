<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\User\Domain;

use Grifix\Clock\ClockInterface;
use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Encryptor\Encryptor\EncryptorInterface;
use Grifix\EventStore\EventStoreInterface;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;
use Modules\Security\Domain\User\Events\UserEventInterface;
use Modules\Security\Domain\User\User;
use Modules\Security\Domain\User\UserOutsideInterface;
use Modules\Security\Infrastructure\Common\Connectors\PasswordGenerator\PasswordGeneratorConnectorInterface;
use Modules\Security\Infrastructure\Common\Connectors\PasswordHash\PasswordHashConnectorInterface;

final class UserOutside implements UserOutsideInterface
{
    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly PasswordGeneratorConnectorInterface $passwordGeneratorConnector,
        private readonly PasswordHashConnectorInterface $passwordHashConnector,
        private readonly RoleProjectorInterface $roleProjector,
        private readonly ClockInterface $clock,
        private readonly UserProjectorInterface $userProjector,
    ) {
    }

    public function generatePassword(): string
    {
        return $this->passwordGeneratorConnector->generatePassword();
    }

    public function isPasswordSecure(string $password): bool
    {
        return $this->passwordGeneratorConnector->isPasswordSecure($password);
    }

    public function hashPassword(string $password): string
    {
        return $this->passwordHashConnector->hash($password);
    }

    public function isPasswordValid(string $password, string $hash): bool
    {
        return $this->passwordHashConnector->verify($password, $hash);
    }

    public function publishEvent(UserEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, User::class, $event->getUserId());
    }

    public function doesUserWithEmailExist(Email $email): bool
    {
        return $this->userProjector->has(UserFilter::create()->withEmail($email));
    }

    public function getCurrentDateTime(): DateTime
    {
        return $this->clock->getCurrentDate();
    }


    public function hasRoleAccess(Uuid $roleId, string $permission): bool
    {
        $role = $this->roleProjector->getOne(
            RoleFilter::create()->setRoleId($roleId),
        );
        return (in_array($permission, $role->permissions));
    }
}
