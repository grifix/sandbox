<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\User\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\Projector\CreateUserDto;
use Modules\Security\Application\User\Ports\Projector\Exceptions\FilterByPasswordIsNotSupportedException;
use Modules\Security\Application\User\Ports\Projector\Exceptions\UserDoesNotExistException;
use Modules\Security\Application\User\Ports\Projector\UserDto;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;
use Modules\Security\Infrastructure\Common\Connectors\PasswordHash\PasswordHashConnectorInterface;
use Modules\Security\Infrastructure\Role\Application\Ports\RoleProjector;

final class UserProjector implements UserProjectorInterface
{
    private const TABLE_USERS = 'security.p_users';
    private const TABLE_ROLES = 'security.p_user_roles';

    public function __construct(
        private readonly Connection $connection,
        private readonly PasswordHashConnectorInterface $passwordHashConnector,
    ) {
    }

    public function find(UserFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $this->createDto($row);
        }
        if ($filter->getPassword()) {
            $result = $this->filterByPassword($filter->getPassword(), $result);
        }
        return $result;
    }

    public function assignRole(Uuid $userId, Uuid $roleId): void
    {
        $roleIds = $this->fetchRoles($userId);
        $roleIds[] = $roleId->toString();
        $this->updateRoles($userId, $roleIds);

        $this->connection->insert(
            self::TABLE_ROLES,
            [
                'user_id' => $userId->toString(),
                'role_id' => $roleId->toString(),
            ],
        );
    }

    public function unAssignRole(Uuid $userId, Uuid $roleId): void
    {
        $roles = $this->fetchRoles($userId);
        $key = array_search($roleId->toString(), $roles);
        if ($key !== false) {
            unset($roles[$key]);
            $this->updateRoles($userId, $roles);
        }

        $this->connection->delete(
            self::TABLE_ROLES,
            [
                'user_id' => $userId->toString(),
                'role_id' => $roleId->toString(),
            ],
        );
    }

    /**
     * @return string[]
     * @throws Exception
     */
    private function fetchRoles(Uuid $userId): array
    {
        /** @var string[] $result */
        $result = $this->connection->createQueryBuilder()
            ->select('role_id')
            ->from(self::TABLE_ROLES)
            ->where('user_id = :id')
            ->setParameter('id', $userId->toString())
            ->fetchFirstColumn();
        return $result;
    }

    /**
     * @param string[] $roles
     * @throws Exception
     */
    private function updateRoles(Uuid $userId, array $roles): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'roles' => json_encode($roles),
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function suspend(Uuid $userId, DateTime $resumeDate): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'resume_date' => $resumeDate->toPgTimestamp(),
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function resume(Uuid $userId): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'resume_date' => null,
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function disable(Uuid $userId): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'disabled' => 'true',
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function enable(Uuid $userId): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'disabled' => 'false',
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function getOne(UserFilter $filter): UserDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            throw new UserDoesNotExistException();
        }
        $result = $this->createDto($row);
        if ($filter->getPassword() && !$this->isPasswordEqual($result, $filter->getPassword())) {
            throw new UserDoesNotExistException();
        }
        return $result;
    }

    public function findOne(UserFilter $filter): ?UserDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            return null;
        }
        $result = $this->createDto($row);
        if ($filter->getPassword() && !$this->isPasswordEqual($result, $filter->getPassword())) {
            return null;
        }
        return $result;
    }

    public function count(UserFilter $filter): int
    {
        if ($filter->getPassword() !== null) {
            throw new FilterByPasswordIsNotSupportedException('count');
        }
        return $this->createQueryBuilder($filter)->executeQuery()->rowCount();
    }

    public function createUser(CreateUserDto $user): void
    {
        $this->connection->insert(
            self::TABLE_USERS,
            [
                'id' => $user->id->toString(),
                'email' => $user->email->toString(),
                'encrypted_password' => $user->encryptedPassword,
                'disabled' => 'false',
                'created_at' => $user->createdAt->toPgTimestamp(),
            ],
        );
    }

    public function has(UserFilter $filter): bool
    {
        return count($this->find($filter)) > 0;
    }

    public function confirmEmailChange(Uuid $userId): void
    {
        $this->connection->executeQuery(
            'UPDATE ' . self::TABLE_USERS . ' SET email = new_email, new_email = null WHERE id = :id',
            [
                'id' => $userId->toString(),
            ],
        );
    }

    private function createQueryBuilder(UserFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('users.*')->from(self::TABLE_USERS, 'users');
        if ($filter->getUserId()) {
            $result->andWhere('users.id = :id')
                ->setParameter('id', $filter->getUserId()->toString());
        }

        if ($filter->getEmail()) {
            $result->andWhere('users.email = :email')
                ->setParameter('email', $filter->getEmail()->toString());
        }

        if ($filter->getWithPermissions()) {
            $result->addSelect('json_agg(roles.permissions) as permissions');
            $result->leftJoin(
                'users',
                self::TABLE_ROLES,
                'user_roles',
                'user_roles.user_id = users.id',
            );
            $result->leftJoin(
                'user_roles',
                RoleProjector::TABLE,
                'roles',
                'roles.id = user_roles.role_id',
            );
            $result->groupBy('1', '2', '3', '4');
        }

        if (null !== $filter->getOffset()) {
            $result->setFirstResult($filter->getOffset());
        }

        if (null !== $filter->getLimit()) {
            $result->setMaxResults($filter->getLimit());
        }

        if (null !== $filter->getSortBy()) {
            $result->orderBy($filter->getSortBy(), $filter->getSortDirection() ?? 'asc');
        }

        return $result;
    }

    /**
     * @param mixed[] $row
     */
    private function createDto(array $row): UserDto
    {
        /**
         * @var array{
         *     id:string,
         *     email:string,
         *     encrypted_password:string,
         *     password_reset_initiated:string,
         *     registration_completed:string,
         *     roles: string,
         *     permissions: string,
         *     resume_date: string|null,
         *     disabled: bool,
         * } $row
         */

        $permissions = null;
        if (array_key_exists('permissions', $row)) {
            $permissions = $this->parsePermissionString($row['permissions']);
        }
        $resumeDate = null;
        if ($row['resume_date']) {
            $resumeDate = DateTime::fromPgTimestamp($row['resume_date']);
        }

        return new UserDto(
            Uuid::createFromString($row['id']),
            Email::create($row['email']),
            DateTime::fromPgTimestamp($row['created_at']),
            $row['encrypted_password'],
            (bool)$row['disabled'],
            (bool)$row['password_reset_initiated'],
            (bool)$row['registration_completed'],
            $this->parseRoleString($row['roles']),
            $row['new_email'] ? Email::create($row['new_email']) : null,
            $permissions,
            $resumeDate,
        );
    }

    /**
     * @return string[]
     */
    private function parsePermissionString(string $permissionString): array
    {
        /** @var array<int, ?string[]> $permissions */
        $permissions = json_decode($permissionString, true);

        $result = array_reduce(
            $permissions,
            function (array $carry, ?array $item) {
                if (null === $item) {
                    return $carry;
                }
                return array_merge($carry, $item);
            },
            [],
        );
        return array_unique($result);
    }

    /**
     * @return Uuid[]
     */
    private function parseRoleString(string $roleString): array
    {
        /** @var Uuid[] $result */
        $result = [];
        /** @var string[] $roleStrings */
        $roles = json_decode($roleString, true);
        foreach ($roles as $role) {
            $result[] = Uuid::createFromString($role);
        }
        return $result;
    }

    /**
     * @param UserDto[] $users
     * @return UserDto[] $users
     */
    private function filterByPassword(string $password, array $users): array
    {
        return array_filter($users, function (UserDto $user) use ($password) {
            return $this->isPasswordEqual($user, $password);
        });
    }

    private function isPasswordEqual(UserDto $user, string $password): bool
    {
        return $this->passwordHashConnector->verify($password, $user->encryptedPassword);
    }

    public function updatePassword(Uuid $userId, string $password): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'encrypted_password' => $password,
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function initiateEmailChange(Uuid $userId, Email $newEmail): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'new_email' => $newEmail->toString(),
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function initiatePasswordReset(Uuid $userId): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'password_reset_initiated' => true,
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }

    public function completeRegistration(Uuid $userId): void
    {
        $this->connection->update(
            self::TABLE_USERS,
            [
                'registration_completed' => true,
            ],
            [
                'id' => $userId->toString(),
            ],
        );
    }
}
