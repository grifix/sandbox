<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\User\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\Repository\Exceptions\UserDoesNotExistException;
use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;
use Modules\Security\Domain\User\User;

final class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    public function add(User $user): void
    {
        $this->doAdd($user);
    }

    public function get(Uuid $id): User
    {
        return $this->doGet($id, UserDoesNotExistException::class, User::class);
    }
}
