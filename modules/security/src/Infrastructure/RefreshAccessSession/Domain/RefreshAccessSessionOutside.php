<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\RefreshAccessSession\Domain;

use Grifix\Clock\ClockInterface;
use Grifix\Date\DateTime\DateTime;
use Grifix\EventStore\EventStoreInterface;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionEventInterface;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSession;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSessionOutsideInterface;

final readonly class RefreshAccessSessionOutside implements RefreshAccessSessionOutsideInterface
{
    public function __construct(
        private ClockInterface $clock,
        private EventStoreInterface $eventStore,
    ) {
    }

    public function publishEvent(RefreshAccessSessionEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, RefreshAccessSession::class, $event->getRefreshAccessSessionId());
    }

    public function getCurrentTime(): DateTime
    {
        return $this->clock->getCurrentDate();
    }
}
