<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\RefreshAccessSession\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Ports\Repository\Exceptions\RefreshAccessSessionDoesNotExistException;
use Modules\Security\Application\RefreshAccessSession\Ports\Repository\RefreshAccessSessionRepositoryInterface;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSession;

final class RefreshAccessSessionRepository extends AbstractRepository implements RefreshAccessSessionRepositoryInterface
{
    public function get(Uuid $refreshAccessSessionId): RefreshAccessSession
    {
        return $this->doGet(
            $refreshAccessSessionId,
            RefreshAccessSessionDoesNotExistException::class,
            RefreshAccessSession::class,
        );
    }

    public function add(RefreshAccessSession $refreshAccessSession): void
    {
        $this->doAdd($refreshAccessSession);
    }
}
