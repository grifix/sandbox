<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\RefreshAccessSession\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Clock\ClockInterface;
use Grifix\Date\DateTime\DateTime;
use Grifix\Date\DateTime\Exceptions\InvalidDateFormatException;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions\MultipleRefreshAccessSessionsFoundException;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions\RefreshAccessSessionProjectionDoesNotExistException;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionDto;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;

final readonly class RefreshAccessSessionProjector implements RefreshAccessSessionProjectorInterface
{
    private const TABLE = 'security.p_refresh_access_sessions';

    public function __construct(
        private Connection $connection,
        private ClockInterface $clock,
    ) {
    }

    /**
     * @inheritdoc
     */
    public function get(RefreshAccessSessionFilter $filter): RefreshAccessSessionDto
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        if (\count($rows) < 1) {
            throw new RefreshAccessSessionProjectionDoesNotExistException();
        }
        if (\count($rows) > 1) {
            throw new MultipleRefreshAccessSessionsFoundException();
        }
        return $this->createDto($rows[0]);
    }

    /**
     * @inheritdoc
     */
    public function getFirst(RefreshAccessSessionFilter $filter): RefreshAccessSessionDto
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        if (\count($rows) < 1) {
            throw new RefreshAccessSessionProjectionDoesNotExistException();
        }
        return $this->createDto($rows[0]);
    }

    public function find(RefreshAccessSessionFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $this->createDto($row);
        }

        return $result;
    }

    private function createQueryBuilder(RefreshAccessSessionFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('sessions.*')->from(self::TABLE, 'sessions');
        if ($filter->getRefreshAccessSessionId()) {
            $result->andWhere('sessions.id = :id')
                ->setParameter('id', $filter->getRefreshAccessSessionId()->toString());
        }

        if ($filter->getUserId()) {
            $result->andWhere('sessions.user_id = :user_id')
                ->setParameter('user_id', $filter->getUserId()->toString());
        }

        if ($filter->getIsActive() === true) {
            $result
                ->andWhere('sessions.revoked = false')
                ->andWhere('expires_at > :expiresAt')
                ->setParameter('expiresAt', $this->clock->getCurrentDate()->toPgTimestamp());
        }

        $result->orderBy('expires_at', 'desc');
        return $result;
    }

    /**
     * @param mixed[] $row
     * @throws InvalidDateFormatException
     */
    private function createDto(array $row): RefreshAccessSessionDto
    {
        /** @var array{
         *      id: string,
         *      user_id:string,
         *      revoked:boolean,
         *      expires_at: string
         * } $row
         */

        return new RefreshAccessSessionDto(
            Uuid::createFromString($row['id']),
            Uuid::createFromString($row['user_id']),
            DateTime::fromPgTimestamp($row['expires_at']),
            (bool)$row['revoked'],
        );
    }

    public function create(RefreshAccessSessionDto $dto): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $dto->id->toString(),
                'user_id' => $dto->userId->toString(),
                'revoked' => $dto->isRevoked,
                'expires_at' => $dto->expiresAt->toPgTimestamp(),
            ],
            [
                'revoked' => 'bool',
            ],
        );
    }

    public function revoke(Uuid $refreshAccessSessionId): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'revoked' => true,
            ],
            [
                'id' => $refreshAccessSessionId->toString(),
            ],
        );
    }
}
