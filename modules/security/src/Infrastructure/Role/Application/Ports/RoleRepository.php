<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Role\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Repository\Exceptions\RoleDoesNotExistException;
use Modules\Security\Application\Role\Ports\Repository\RoleRepositoryInterface;
use Modules\Security\Domain\Role\Role;

final class RoleRepository extends AbstractRepository implements RoleRepositoryInterface
{

    public function add(Role $role): void
    {
        $this->doAdd($role);
    }

    public function get(Uuid $id): Role
    {
        return $this->doGet($id, RoleDoesNotExistException::class, Role::class);
    }
}
