<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Role\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Projector\Exceptions\RoleDoesNotExistException;
use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;

final class RoleProjector implements RoleProjectorInterface
{
    public const TABLE = 'security.p_roles';

    public function __construct(private readonly Connection $connection)
    {
    }

    public function createRole(RoleDto $role): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $role->id->toString(),
                'name' => $role->name,
            ],
        );
    }

    public function grantPermission(Uuid $roleId, string $permission): void
    {
        $permissions = $this->fetchPermissions($roleId);
        $permissions[] = $permission;
        $this->updatePermissions($roleId, $permissions);
    }

    /**
     * @return string[]
     * @throws Exception
     */
    private function fetchPermissions(Uuid $roleId): array
    {
        /** @var string $permissions */
        $permissions = $this->connection->createQueryBuilder()
            ->select('permissions')
            ->from(self::TABLE)
            ->where('id = :id')
            ->setParameter('id', $roleId->toString())
            ->fetchOne();
        /** @var string[] $result */
        $result = json_decode($permissions, true);
        return $result;
    }

    /**
     * @param string[] $permissions
     * @throws Exception
     */
    private function updatePermissions(Uuid $roleId, array $permissions): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'permissions' => json_encode($permissions),
            ],
            [
                'id' => $roleId->toString(),
            ],
        );
    }

    public function revokePermission(Uuid $roleId, string $permission): void
    {
        $permissions = $this->fetchPermissions($roleId);
        $key = array_search($permission, $permissions);
        if (false !== $key) {
            unset($permissions[$key]);
            $this->updatePermissions($roleId, $permissions);
        }
    }

    public function find(RoleFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            /** @var array{} $row */
            $result[] = $this->createDto($row);
        }
        return $result;
    }

    public function getOne(RoleFilter $filter): RoleDto
    {
        /** @var array{}|false $row */
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            throw new RoleDoesNotExistException();
        }
        return $this->createDto($row);
    }

    public function findOne(RoleFilter $filter): ?RoleDto
    {
        /** @var array{}|false $row
         */
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            return null;
        }
        return $this->createDto($row);
    }

    public function count(RoleFilter $filter): int
    {
        return $this->createQueryBuilder($filter)->executeQuery()->rowCount();
    }

    public function has(RoleFilter $filter): bool
    {
        return false !== $this->createQueryBuilder($filter)->fetchOne();
    }

    private function createQueryBuilder(RoleFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('roles.id, roles.name, roles.permissions')->from(self::TABLE, 'roles');
        if ($filter->getRoleId()) {
            $result->andWhere('id = :id')->setParameter('id', $filter->getRoleId()->toString());
        }

        if ($filter->getRoleIds()) {
            $ids = array_map(fn (Uuid $roleId) => $roleId->toString(), $filter->getRoleIds());
            $result->andWhere('id in (:ids)')->setParameter('ids', $ids, Connection::PARAM_STR_ARRAY);
        }

        if ($filter->getName()) {
            $result->andWhere('name = :name')->setParameter('name', $filter->getName());
        }

        if (null !== $filter->getOffset()) {
            $result->setFirstResult($filter->getOffset());
        }

        if (null !== $filter->getLimit()) {
            $result->setMaxResults($filter->getLimit());
        }

        if (null !== $filter->getSortBy()) {
            $result->orderBy($filter->getSortBy(), $filter->getSortDirection() ?? 'asc');
        }

        return $result;
    }

    /**
     * @param array{} $row
     */
    private function createDto(array $row): RoleDto
    {
        /**
         * @var array{
         * id: string,
         * name: string,
         * permissions: string
         * } $row
         */

        /** @var string[] $permissions */
        $permissions = json_decode($row['permissions'], true);
        return new RoleDto(
            Uuid::createFromString($row['id']),
            $row['name'],
            $permissions,
        );
    }
}
