<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\Role\Domain;

use Grifix\EventStore\EventStoreInterface;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;
use Modules\Security\Domain\Role\Events\RoleEventInterface;
use Modules\Security\Domain\Role\Role;
use Modules\Security\Domain\Role\RoleOutsideInterface;

final class RoleOutside implements RoleOutsideInterface
{
    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly RoleProjectorInterface $roleProjector,
    ) {
    }

    public function doesRoleWithSameNameExist(string $name): bool
    {
        return $this->roleProjector->has(RoleFilter::create()->withName($name));
    }

    public function publishEvent(RoleEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, Role::class, $event->getRoleId());
    }
}
