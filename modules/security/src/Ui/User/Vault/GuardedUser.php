<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\Vault;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Framework\Ui\NormalizerTrait;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\Projector\UserDto;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\Security\ForbiddenProperty;
use Sandbox\SharedKernel\Ui\Security\Security;

final readonly class GuardedUser
{
    use NormalizerTrait;

    public function __construct(
        private Security $security,
        private UserDto $user,
    ) {
    }

    public function getId(): Uuid
    {
        return $this->user->id;
    }

    public function getEmail(): Email|ForbiddenProperty
    {
        if ($this->security->getUserId()->isEqualTo($this->user->id)) {
            return $this->user->email;
        }
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->email;
    }

    /**
     * @return string[]|ForbiddenProperty|null
     */
    public function getPermissions(): array|ForbiddenProperty|null
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->permissions;
    }

    public function isDisabled(): bool|ForbiddenProperty
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->disabled;
    }

    public function isPasswordResetInitiated(): bool|ForbiddenProperty
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->passwordResetInitiated;
    }

    public function isRegistrationCompleted(): bool|ForbiddenProperty
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->registrationCompleted;
    }

    public function getResumeDate(): DateTime|ForbiddenProperty|null
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->resumeDate;
    }

    public function getCreatedAt(): DateTime|ForbiddenProperty
    {
        if (!$this->security->hasPermission(SecurityPermission::readUserProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->user->createdAt;
    }

    /**
     * @return array<string, mixed>
     */
    public function normalize(): array
    {
        return [
            'id' => $this->normalizeValue($this->getId()),
            'email' => $this->normalizeValue($this->getEmail()),
            'disabled' => $this->normalizeValue($this->isDisabled()),
            'createdAt' => $this->normalizeValue($this->getCreatedAt()),
            'resumeDate' => $this->normalizeValue($this->getResumeDate()),
            'permissions' => $this->normalizeValue($this->getPermissions()),
            'registrationCompleted' => $this->normalizeValue($this->isRegistrationCompleted()),
            'passwordResetInitiated' => $this->normalizeValue($this->isPasswordResetInitiated()),
        ];
    }
}
