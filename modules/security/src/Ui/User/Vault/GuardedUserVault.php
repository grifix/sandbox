<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\Vault;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\Count\CountUsersQuery;
use Modules\Security\Application\User\Queries\Find\FindUsersQuery;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;

final readonly class GuardedUserVault
{
    public function __construct(
        private ApplicationInterface $application,
        private GuardedUserFactory $guardedUserFactory,
    ) {
    }

    /**
     * @return GuardedUser[]
     */
    public function find(UserFilter $filter): array
    {
        $queryResult = $this->application->ask(new FindUsersQuery($filter));
        $result = [];
        foreach ($queryResult->users as $userDto) {
            $result[] = $this->guardedUserFactory->createGuardedUser($userDto);
        }
        return $result;
    }

    public function count(UserFilter $filter): int
    {
        $filter->withLimit(null);
        $filter->withOffset(null);
        return $this->application->ask(new CountUsersQuery($filter))->value;
    }

    public function get(Uuid $userId): GuardedUser
    {
        $filter = UserFilter::create()->withUserId($userId);
        return $this->guardedUserFactory->createGuardedUser(
            $this->application->ask(new GetUserQuery($filter))->user,
        );
    }
}
