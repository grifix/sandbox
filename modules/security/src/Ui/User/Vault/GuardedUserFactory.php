<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\Vault;

use Modules\Security\Application\User\Ports\Projector\UserDto;
use Sandbox\SharedKernel\Ui\Security\Security;

final readonly class GuardedUserFactory
{
    public function __construct(
        private Security $security,
    ) {
    }

    public function createGuardedUser(UserDto $user): GuardedUser
    {
        return new GuardedUser($this->security, $user);
    }
}
