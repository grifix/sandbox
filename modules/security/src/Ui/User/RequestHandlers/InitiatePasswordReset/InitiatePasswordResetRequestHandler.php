<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\InitiatePasswordReset;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\PasswordResetProcessManager\Commands\Start\StartResetPasswordProcessCommand;
use Modules\Security\Application\User\Commands\InitiatePasswordReset\InitiatePasswordResetCommand;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class InitiatePasswordResetRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/password/reset/initiate',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, InitiatePasswordResetInput::class);
        $this->application->tell(
            new InitiatePasswordResetCommand(
                $this->getUserId($input->getEmail()),
            ),
        );
        return new Response();
    }

    private function getUserId(Email $email): Uuid
    {
        return $this->application->ask(
            new GetUserQuery(
                UserFilter::create()->withEmail($email),
            ),
        )->user->id;
    }
}
