<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\ConfirmEmailChange;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class ConfirmEmailChangeInput extends AbstractInput
{
    public function getToken(): string
    {
        return $this->getValue('token', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'token' => StringInputType::createConstraint(),
            ],
        ]);
    }
}
