<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\ConfirmEmailChange;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\ConfirmEmailChange\ConfirmEmailChangeCommand;
use Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken\DecodeEmailChangeConfirmationTokenQuery;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ConfirmEmailChangeRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/email/confirm',
        methods: ['PATH'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, ConfirmEmailChangeInput::class);
        $this->application->tell(
            new ConfirmEmailChangeCommand(
                $this->getUserId($input->getToken()),
            ),
        );

        return new JsonResponse();
    }

    private function getUserId(string $token): Uuid
    {
        return $this->application->ask(
            new DecodeEmailChangeConfirmationTokenQuery(
                $token,
            ),
        )->userId;
    }
}
