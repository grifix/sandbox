<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Enable;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\Enable\EnableUserCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class EnableUserRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/{userId}/enable',
        methods: ['PATH'],
    )]
    public function __invoke(string $userId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageUser->value);
        $this->application->tell(
            new EnableUserCommand(
                Uuid::createFromString($userId),
            )
        );

        return new JsonResponse();
    }
}
