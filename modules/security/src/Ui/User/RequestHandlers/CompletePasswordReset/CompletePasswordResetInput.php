<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\CompletePasswordReset;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class CompletePasswordResetInput extends AbstractInput
{
    public function getToken(): string
    {
        return $this->getValue('token', StringInputType::class)->toString();
    }

    public function getNewPassword(): string
    {
        return $this->getValue('newPassword', StringInputType::class)->toString();
    }

    public function getNewPasswordConfirmation(): string
    {
        return $this->getValue('newPasswordConfirmation', StringInputType::class)->ToString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'token' => StringInputType::createConstraint(),
                'newPassword' => StringInputType::createConstraint(),
                'newPasswordConfirmation' => StringInputType::createConstraint(),
            ],
        ]);
    }
}
