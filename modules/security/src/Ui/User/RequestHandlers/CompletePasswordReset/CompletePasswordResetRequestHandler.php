<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\CompletePasswordReset;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Token\Queries\DecodeToken\DecodeTokenQuery;
use Modules\Security\Application\User\Commands\CompetePasswordReset\CompletePasswordResetCommand;
use Modules\Security\Application\User\Queries\DecodePasswordResetToken\DecodePasswordResetTokenQuery;
use Modules\Security\Domain\Token\Dto\TokenTypeDto;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CompletePasswordResetRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/password/reset/complete',
        methods: ['PATH'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, CompletePasswordResetInput::class);
        $this->application->tell(
            new CompletePasswordResetCommand(
                $this->getUserId($input->getToken()),
                $input->getNewPassword(),
                $input->getNewPasswordConfirmation(),
                $this->getUserIp($request),
            ),
        );

        return new Response();
    }

    private function getUserIp(Request $request): ?IpAddress
    {
        if (null === $request->getClientIp()) {
            return null;
        }
        return IpAddress::create($request->getClientIp());
    }

    private function getUserId(string $token): Uuid
    {
        return $this->application->ask(
            new DecodePasswordResetTokenQuery(
                $token,
            ),
        )->userId;
    }
}
