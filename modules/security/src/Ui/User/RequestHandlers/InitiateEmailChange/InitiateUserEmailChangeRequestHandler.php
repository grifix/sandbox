<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\InitiateEmailChange;

use Modules\Security\Application\User\Commands\InitiateEmailChange\InitiateUserEmailChangeCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class InitiateUserEmailChangeRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/email',
        methods: ['PATH'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, InitiateUserEmailChangeInput::class);

        $this->application->tell(
            new InitiateUserEmailChangeCommand(
                $this->getAuthenticatedUserId(),
                $input->getEmail(),
                $input->getPassword(),
            ),
        );

        return new JsonResponse();
    }
}
