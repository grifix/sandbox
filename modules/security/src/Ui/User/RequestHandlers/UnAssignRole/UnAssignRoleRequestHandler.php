<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\UnAssignRole;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\UnAssignRole\UnAssignRoleCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class UnAssignRoleRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/{userId}/roles',
        methods: ['DELETE'],
    )]
    public function __invoke(string $userId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageUserRole->value);
        $input = $this->createInputFromBody($request, UnAssignRoleInput::class);
        $this->application->tell(
            new UnAssignRoleCommand(
                Uuid::createFromString($userId),
                $input->getRoleId(),
            ),
        );

        return new JsonResponse();
    }
}
