<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Create;

use Grifix\Email\Email;
use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\EmailInputType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class CreateUserInput extends AbstractInput
{
    public function getEmail(): Email
    {
        return $this->getValue('email', EmailInputType::class)->toEmail();
    }

    public function getPassword(): string
    {
        return $this->getValue('password', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'email' => EmailInputType::createConstraint(),
                'password' => StringInputType::createConstraint()
            ]
        ]);
    }
}
