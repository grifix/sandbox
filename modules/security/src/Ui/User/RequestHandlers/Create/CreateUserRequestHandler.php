<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Create;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\Create\CreateUserCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateUserRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, CreateUserInput::class);
        $userId = Uuid::createRandom();
        $this->application->tell(
            new CreateUserCommand(
                $userId,
                $input->getEmail(),
                $input->getPassword(),
            ),
        );

        return new JsonResponse(
            [
                'userId' => $userId->toString(),
            ],
            Response::HTTP_CREATED,
        );
    }
}
