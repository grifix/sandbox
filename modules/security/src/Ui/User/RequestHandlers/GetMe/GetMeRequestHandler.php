<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\GetMe;

use Modules\Security\Ui\User\Vault\GuardedUserVault;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

final class GetMeRequestHandler extends AbstractRequestHandler
{
    #[Required]
    public GuardedUserVault $userVault;

    #[Route(
        '/api/v1/security/users/me',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        return new JsonResponse($this->userVault->get($this->getAuthenticatedUserId())->normalize());
    }
}
