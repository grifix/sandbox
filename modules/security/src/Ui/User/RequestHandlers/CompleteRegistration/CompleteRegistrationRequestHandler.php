<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\CompleteRegistration;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\CompleteRegistration\CompleteUserRegistrationCommand;
use Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken\DecodeRegistrationConfirmationTokenQuery;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CompleteRegistrationRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/registration/complete',
        methods: ['PATH'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, CompleteRegistrationInput::class);
        $this->application->tell(
            new CompleteUserRegistrationCommand(
                $this->getUserId($input->getToken()),
            ),
        );

        return new JsonResponse();
    }

    private function getUserId(string $token): Uuid
    {
        return $this->application->ask(
            new DecodeRegistrationConfirmationTokenQuery(
                $token,
            ),
        )->userId;
    }
}
