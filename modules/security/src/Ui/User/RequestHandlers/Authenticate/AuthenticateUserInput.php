<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Authenticate;

use Grifix\Email\Email;
use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\EmailInputType;
use Grifix\Framework\Ui\Input\Types\IpAddressInputType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Grifix\Ip\IpAddress;
use Symfony\Component\Validator\Constraints\Collection;

final class AuthenticateUserInput extends AbstractInput
{
    public function getEmail(): Email
    {
        return $this->getValue('email', EmailInputType::class)->toEmail();
    }

    public function getPassword(): string
    {
        return $this->getValue('password', StringInputType::class)->toString();
    }

    public function getClientIp(): ?IpAddress
    {
        if (!$this->hasValue('clientIp')) {
            return null;
        }
        return $this->getValue('clientIp', IpAddressInputType::class)->toIpAddress();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'email' => EmailInputType::createConstraint(),
                'password' => StringInputType::createConstraint()
            ]
        ]);
    }
}
