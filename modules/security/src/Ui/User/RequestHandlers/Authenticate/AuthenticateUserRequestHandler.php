<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Authenticate;

use Grifix\Email\Email;
use Grifix\Repeater\Repeater;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions\RefreshAccessSessionProjectionDoesNotExistException;
use Modules\Security\Application\RefreshAccessSession\Ports\Repository\Exceptions\RefreshAccessSessionDoesNotExistException;
use Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken\GetRefreshTokenQuery;
use Modules\Security\Application\Token\Ports\Projector\TokenFilter;
use Modules\Security\Application\Token\Queries\Find\FindTokensQuery;
use Modules\Security\Application\User\Commands\Authenticate\AuthenticateUserCommand;
use Modules\Security\Application\User\Ports\Projector\Exceptions\UserDoesNotExistException;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Modules\Security\Application\User\Queries\GetAccessToken\GetAccessTokenQuery;
use Modules\Security\Domain\Token\Dto\TokenTypeDto;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AuthenticateUserRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/authenticate',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, AuthenticateUserInput::class);
        $userId = $this->fetchUserId($input->getEmail());
        try {
            $this->application->tell(
                new AuthenticateUserCommand(
                    $userId,
                    $input->getPassword(),
                    $input->getClientIp(),
                ),
            );
        } catch (WrongPasswordException) {
            throw new UserDoesNotExistException();
        }

        return new JsonResponse([
            'accessToken' => $this->getAccessToken($userId),
            'refreshToken' => $this->getRefreshToken($userId),
        ]);
    }

    private function fetchUserId(Email $userEmail): Uuid
    {
        return $this->application->ask(
            new GetUserQuery(
                UserFilter::create()
                    ->withEmail($userEmail),
            ),
        )->user->id;
    }

    private function getRefreshToken(Uuid $userId): ?string
    {
        /** @var ?string $result */
        $result = Repeater::repeatUntilNotNull(function () use ($userId): ?string {
            try {
                return $this->application->ask(
                    new GetRefreshTokenQuery($userId),
                )->refreshToken;
            } catch (RefreshAccessSessionProjectionDoesNotExistException) {
                sleep(1);
                return null;
            }
        });
        return $result;
    }

    private function getAccessToken(Uuid $userId): string
    {
        return $this->application->ask(new GetAccessTokenQuery($userId))->token;
    }
}
