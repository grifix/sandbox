<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\AssignRole;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\AssignRole\AssignRoleCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AssignRoleRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/{userId}/roles',
        methods: ['POST'],
    )]
    public function __invoke(string $userId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageUserRole->value);
        $input = $this->createInputFromBody($request, AssignRoleInput::class);
        $this->application->tell(
            new AssignRoleCommand(
                Uuid::createFromString($userId),
                $input->getRoleId(),
            ),
        );

        return new JsonResponse();
    }
}
