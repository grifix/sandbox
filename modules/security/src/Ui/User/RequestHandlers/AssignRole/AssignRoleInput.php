<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\AssignRole;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\UuidInputType;
use Grifix\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\Collection;

final class AssignRoleInput extends AbstractInput
{
    public function getRoleId(): Uuid
    {
        return $this->getValue('roleId', UuidInputType::class)->toUuid();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'roleId' => UuidInputType::createConstraint(),
            ],
        ]);
    }
}
