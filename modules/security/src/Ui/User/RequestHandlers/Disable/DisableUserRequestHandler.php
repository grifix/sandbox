<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Disable;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\Disable\DisableUserCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DisableUserRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/{userId}/disable',
        methods: ['PATH'],
    )]
    public function __invoke(string $userId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageUser->value);
        $this->application->tell(
            new DisableUserCommand(
                Uuid::createFromString($userId),
            ),
        );

        return new JsonResponse();
    }
}
