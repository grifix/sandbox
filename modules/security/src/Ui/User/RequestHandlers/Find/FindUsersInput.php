<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Find;

use Grifix\Email\Email;
use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\BoolInputType;
use Grifix\Framework\Ui\Input\Types\EmailInputType;
use Grifix\Framework\Ui\Input\Types\IntInputType;
use Grifix\Framework\Ui\Input\Types\PositiveIntType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Regex;

final class FindUsersInput extends AbstractInput
{
    public function getEmail(): ?Email
    {
        return $this->getValueOrNull('email', EmailInputType::class)?->toEmail();
    }

    public function getOffset(): int
    {
        return $this->getValue('offset', PositiveIntType::class, 0)->toInt();
    }

    public function getLimit(): int
    {
        return $this->getValue('limit', PositiveIntType::class, 10)->toInt();
    }

    public function getWithPermissions(): bool
    {
        return $this->getValue('withPermissions', BoolInputType::class, false)->toBool();
    }

    public function getCountTotal(): bool
    {
        return $this->getValue('countTotal', BoolInputType::class, false)->toBool();
    }

    public function getSortBy(): string
    {
        return $this->getValue('sortBy', StringInputType::class, 'created_at')->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => true,
            'fields' => [
                'email' => new Optional(EmailInputType::createConstraint()),
                'offset' => new Optional(
                    [
                        IntInputType::createConstraint(),
                        new GreaterThanOrEqual(0),
                    ],
                ),
                'limit' => new Optional(
                    [
                        IntInputType::createConstraint(),
                        new GreaterThanOrEqual(1),
                        new LessThanOrEqual(100),
                    ],
                ),
                'withPermissions' => new Optional(
                    [
                        BoolInputType::createConstraint(),
                    ],
                ),
                'countTotal' => new Optional(
                    [
                        BoolInputType::createConstraint(),
                    ],
                ),
                'sortBy' => new Optional(
                    [
                        StringInputType::createConstraint(),
                        new Regex('/^(created_at|email)$/'),
                    ],
                ),
            ],
        ]);
    }
}
