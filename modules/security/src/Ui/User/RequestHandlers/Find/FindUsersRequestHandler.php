<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\Find;

use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Ui\SecurityPermission;
use Modules\Security\Ui\User\Vault\GuardedUser;
use Modules\Security\Ui\User\Vault\GuardedUserVault;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

final class FindUsersRequestHandler extends AbstractRequestHandler
{
    #[Required]
    public GuardedUserVault $userVault;

    #[Route(
        '/api/v1/security/users',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        $this->authorize(SecurityPermission::readUser->value);
        $input = $this->createInputFromQueryString($request, FindUsersInput::class);
        $filter = UserFilter::create()
            ->withEmail($input->getEmail())
            ->withLimit($input->getLimit())
            ->withOffset($input->getOffset())
            ->sortBy($input->getSortBy())
            ->withPermissions($input->getWithPermissions());

        $result = [
            'users' => $this->findUsers($filter),
        ];
        if ($input->getCountTotal()) {
            $result['total'] = $this->userVault->count($filter);
        }

        return new JsonResponse($result);
    }

    /**
     * @return mixed[]
     */
    private function findUsers(UserFilter $filter): array
    {
        return array_map(fn (GuardedUser $user): array => $user->normalize(), $this->userVault->find($filter));
    }

}
