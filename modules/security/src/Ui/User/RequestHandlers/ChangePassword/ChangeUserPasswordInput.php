<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\ChangePassword;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class ChangeUserPasswordInput extends AbstractInput
{
    public function getCurrentPassword(): string
    {
        return $this->getValue('currentPassword', StringInputType::class)->toString();
    }

    public function getNewPassword(): string
    {
        return $this->getValue('newPassword', StringInputType::class)->toString();
    }

    public function getNewPasswordConfirmation(): string
    {
        return $this->getValue('newPasswordConfirmation', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'currentPassword' => StringInputType::createConstraint(),
                'newPassword' => StringInputType::createConstraint(),
                'newPasswordConfirmation' => StringInputType::createConstraint(),
            ],
        ]);
    }
}
