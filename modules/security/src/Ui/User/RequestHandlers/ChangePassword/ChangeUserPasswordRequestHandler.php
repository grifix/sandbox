<?php

declare(strict_types=1);

namespace Modules\Security\Ui\User\RequestHandlers\ChangePassword;

use Modules\Security\Application\User\Commands\ChangePassword\ChangeUserPasswordCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ChangeUserPasswordRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/users/password',
        methods: ['PATH'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, ChangeUserPasswordInput::class);
        $this->application->tell(
            new ChangeUserPasswordCommand(
                $this->getAuthenticatedUserId(),
                $input->getCurrentPassword(),
                $input->getNewPassword(),
                $input->getNewPasswordConfirmation(),
            ),
        );

        return new JsonResponse();
    }
}
