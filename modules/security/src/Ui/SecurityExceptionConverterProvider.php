<?php

declare(strict_types=1);

namespace Modules\Security\Ui;

use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions\RefreshAccessSessionProjectionDoesNotExistException;
use Modules\Security\Application\Role\Ports\Projector\Exceptions\RoleDoesNotExistException;
use Modules\Security\Application\Role\Ports\Repository\Exceptions\RoleDoesNotExistException as RoleAggregateDoesNotExistExceptionAlias;
use Modules\Security\Application\User\Ports\Projector\Exceptions\UserDoesNotExistException;
use Modules\Security\Application\User\Ports\Repository\Exceptions\UserDoesNotExistException as UserAggregateDoesNotExistExceptionAlias;
use Modules\Security\Domain\User\Exceptions\UserAuthorizationFailedException;
use Sandbox\SharedKernel\Ui\Security\Exceptions\ForbiddenException;
use Sandbox\SharedKernel\Ui\Security\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class SecurityExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(
                RefreshAccessSessionProjectionDoesNotExistException::class,
                Response::HTTP_NOT_FOUND,
            ),
            ExceptionConverter::create(
                ForbiddenException::class,
                Response::HTTP_FORBIDDEN,
            ),
            ExceptionConverter::create(
                UnauthorizedException::class,
                Response::HTTP_UNAUTHORIZED,
            ),
            ExceptionConverter::create(
                UserAuthorizationFailedException::class,
                Response::HTTP_UNAUTHORIZED,
            ),
            ExceptionConverter::create(
                RoleAggregateDoesNotExistExceptionAlias::class,
                Response::HTTP_NOT_FOUND,
            ),
            ExceptionConverter::create(
                RoleDoesNotExistException::class,
                Response::HTTP_NOT_FOUND,
            ),
            ExceptionConverter::create(
                UserAggregateDoesNotExistExceptionAlias::class,
                Response::HTTP_NOT_FOUND,
            ),
            ExceptionConverter::create(
                UserDoesNotExistException::class,
                Response::HTTP_NOT_FOUND,
            ),
            ExceptionConverter::create(
                'Modules\Security\Domain',
                Response::HTTP_BAD_REQUEST,
            ),
            ExceptionConverter::create(
                'Modules\Security\Application',
                Response::HTTP_BAD_REQUEST,
            ),
            ExceptionConverter::create(
                'Modules\Security\Ui',
                Response::HTTP_BAD_REQUEST,
            ),
        ];
    }
}
