<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\Vault;

use Grifix\Framework\Ui\NormalizerTrait;
use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\Security\ForbiddenProperty;
use Sandbox\SharedKernel\Ui\Security\Security;

final readonly class GuardedRole
{
    use NormalizerTrait;

    public function __construct(
        private Security $security,
        private RoleDto $role,
    ) {
    }

    public function getId(): string
    {
        return $this->role->id->toString();
    }

    public function getName(): string
    {
        return $this->role->name;
    }

    /**
     * @return string[]|ForbiddenProperty|null
     */
    public function getPermissions(): array|ForbiddenProperty|null
    {
        if (!$this->security->hasPermission(SecurityPermission::readRoleProtectedProperty->value)) {
            return new ForbiddenProperty();
        }
        return $this->role->permissions;
    }

    /**
     * @return array<string, mixed>
     */
    public function normalize(): array
    {
        return [
            'id' => $this->normalizeValue($this->getId()),
            'name' => $this->normalizeValue($this->getName()),
            'permissions' => $this->normalizeValue($this->getPermissions()),
        ];
    }
}
