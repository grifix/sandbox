<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\Vault;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Application\Role\Queries\Count\CountRolesQuery;
use Modules\Security\Application\Role\Queries\Find\FindRolesQuery;
use Modules\Security\Application\Role\Queries\Get\GetRoleQuery;

final readonly class GuardedRoleVault
{
    public function __construct(
        private ApplicationInterface $application,
        private GuardedRoleFactory $guardedRoleFactory,
    ) {
    }

    /**
     * @return GuardedRole[]
     */
    public function find(RoleFilter $filter): array
    {
        $queryResult = $this->application->ask(new FindRolesQuery($filter));
        $result = [];
        foreach ($queryResult->roles as $roleDto) {
            $result[] = $this->guardedRoleFactory->createGuardedRole($roleDto);
        }
        return $result;
    }

    public function count(RoleFilter $filter): int
    {
        $filter->withLimit(null);
        $filter->withOffset(null);
        return $this->application->ask(new CountRolesQuery($filter))->value;
    }

    public function get(Uuid $roleId): GuardedRole
    {
        $filter = RoleFilter::create()->setRoleId($roleId);
        return $this->guardedRoleFactory->createGuardedRole(
            $this->application->ask(new GetRoleQuery($filter))->role,
        );
    }
}
