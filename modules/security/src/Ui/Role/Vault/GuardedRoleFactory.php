<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\Vault;

use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Sandbox\SharedKernel\Ui\Security\Security;

final readonly class GuardedRoleFactory
{
    public function __construct(
        private Security $security,
    ) {
    }

    public function createGuardedRole(RoleDto $role): GuardedRole
    {
        return new GuardedRole($this->security, $role);
    }
}
