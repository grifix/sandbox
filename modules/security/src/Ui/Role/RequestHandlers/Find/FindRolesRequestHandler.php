<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\Find;

use Modules\Security\Application\Role\Ports\Projector\RoleFilter;
use Modules\Security\Ui\Role\Vault\GuardedRole;
use Modules\Security\Ui\Role\Vault\GuardedRoleVault;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

final class FindRolesRequestHandler extends AbstractRequestHandler
{
    #[Required]
    public GuardedRoleVault $roleVault;

    #[Route(
        '/api/v1/security/roles',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        $this->authorize(SecurityPermission::readRole->value);
        $input = $this->createInputFromQueryString($request, FindRolesInput::class);
        $filter = RoleFilter::create()
            ->withName($input->getName())
            ->withLimit($input->getLimit())
            ->withOffset($input->getOffset())
            ->sortBy($input->getSortBy());
        $result = [
            'roles' => $this->findRoles($filter),
        ];
        if ($input->getCountTotal()) {
            $result['total'] = $this->roleVault->count($filter);
        }

        return new JsonResponse($result);
    }

    /**
     * @return mixed[]
     */
    private function findRoles(RoleFilter $filter): array
    {
        return array_map(fn (GuardedRole $role): array => $role->normalize(), $this->roleVault->find($filter));
    }
}
