<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\GrantPermission;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class GrantPermissionInput extends AbstractInput
{
    /**
     * @return mixed[]
     */
    public static function getJsonSchema(): array
    {
        return [
            'type' => 'object',
            'properties' => [
                'permission' => [
                    'type' => 'string',
                ],
            ],
            'required' => [
                'permission',
            ],
            'additionalProperties' => false,
        ];
    }

    public function getPermission(): string
    {
        return $this->getValue('permission', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'permission' => StringInputType::createConstraint()
            ]
        ]);
    }
}
