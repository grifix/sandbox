<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\GrantPermission;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Commands\GrantPermission\GrantPermissionCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class GrantPermissionRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/roles/{roleId}/permissions',
        methods: ['POST'],
    )]
    public function __invoke(string $roleId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageRolePermission->value);
        $input = $this->createInputFromBody($request, GrantPermissionInput::class);
        $this->application->tell(
            new GrantPermissionCommand(
                Uuid::createFromString($roleId),
                $input->getPermission(),
            ),
        );

        return new JsonResponse();
    }
}
