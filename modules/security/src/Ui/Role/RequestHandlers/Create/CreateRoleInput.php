<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\Create;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class CreateRoleInput extends AbstractInput
{
    public function getName(): string
    {
        return $this->getValue('name', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'name' => StringInputType::createConstraint(),
            ],
        ]);
    }
}
