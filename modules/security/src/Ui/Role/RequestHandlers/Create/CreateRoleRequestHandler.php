<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\Create;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Commands\Create\CreateRoleCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateRoleRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/roles',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $this->authorize(SecurityPermission::createRole->value);
        $input = $this->createInputFromBody($request, CreateRoleInput::class);
        $roleId = Uuid::createRandom();
        $this->application->tell(
            new CreateRoleCommand(
                $roleId,
                $input->getName(),
            ),
        );

        return new JsonResponse(
            [
                'roleId' => $roleId->toString(),
            ],
            Response::HTTP_CREATED,
        );
    }
}
