<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\RevokePermission;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class RevokePermissionInput extends AbstractInput
{
    public function getPermission(): string
    {
        return $this->getValue('permission', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'permission' => StringInputType::createConstraint()
            ]
        ]);
    }
}
