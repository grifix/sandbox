<?php

declare(strict_types=1);

namespace Modules\Security\Ui\Role\RequestHandlers\RevokePermission;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Commands\RevokePermission\RevokePermissionCommand;
use Modules\Security\Ui\SecurityPermission;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class RevokePermissionRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/roles/{roleId}/permissions',
        methods: ['DELETE'],
    )]
    public function __invoke(string $roleId, Request $request): Response
    {
        $this->authorize(SecurityPermission::manageRolePermission->value);
        $input = $this->createInputFromBody($request, RevokePermissionInput::class);
        $this->application->tell(
            new RevokePermissionCommand(
                Uuid::createFromString($roleId),
                $input->getPermission(),
            ),
        );

        return new JsonResponse();
    }
}
