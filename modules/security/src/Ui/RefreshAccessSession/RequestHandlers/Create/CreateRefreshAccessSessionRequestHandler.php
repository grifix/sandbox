<?php

declare(strict_types=1);

namespace Modules\Security\Ui\RefreshAccessSession\RequestHandlers\Create;

use Grifix\Repeater\Repeater;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Commands\Verify\VerifyRefreshAccessSessionCommand;
use Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\DecodeRefreshTokenQuery;
use Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken\GetRefreshTokenQuery;
use Modules\Security\Application\User\Queries\GetAccessToken\GetAccessTokenQuery;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateRefreshAccessSessionRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/security/refreshAccessSession',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, CreateRefreshAccessSessionInput::class);
        $token = $this->application->ask(new DecodeRefreshTokenQuery($input->getRefreshToken()));
        $this->application->tell(new VerifyRefreshAccessSessionCommand($token->refreshAccessSessionId));
        return new JsonResponse([
            'accessToken' => $this->getAccessToken($token->userId),
            'refreshToken' => $this->getRefreshToken($token->userId),
        ], Response::HTTP_CREATED);
    }

    private function getAccessToken(Uuid $userId): string
    {
        return $this->application->ask(new GetAccessTokenQuery($userId))->token;
    }

    private function getRefreshToken(Uuid $userId): ?string
    {
        /** @var ?string $result */
        $result = Repeater::repeatUntilNotNull(function () use ($userId): ?string {
            $result = $this->application->ask(
                new GetRefreshTokenQuery($userId),
            )->refreshToken;
            if ($result === null) {
                sleep(1);
                return null;
            }
            return $result;
        });
        return $result;
    }
}
