<?php

declare(strict_types=1);

namespace Modules\Security\Ui\RefreshAccessSession\RequestHandlers\Create;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;

final class CreateRefreshAccessSessionInput extends AbstractInput
{
    public function getRefreshToken(): string
    {
        return $this->getValue('refreshToken', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'refreshToken' => StringInputType::createConstraint(),
            ],
        ]);
    }
}
