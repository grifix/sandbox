<?php

declare(strict_types=1);

namespace Modules\Security\Ui;

enum SecurityPermission: string
{
    case manageUserRole = 'security.user.role.manage';
    case createRole = 'security.role.create';
    case manageRolePermission = 'security.role.permission.manage';
    case manageUser = 'security.user.manage';
    case readUser = 'security.user.read';
    case readUserProtectedProperty = 'security.user.read.protectedProperty';
    case readRole = 'security.role.read';
    case readRoleProtectedProperty = 'security.role.read.protectedProperty';
}
