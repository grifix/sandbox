<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Subscribers;

use Grifix\Framework\Application\QueryBusInterface;
use Modules\Security\Application\Common\Ports\MailSender\Emails\EmailChangeConfirmationEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\Emails\PasswordResetEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\Emails\RegistrationConfirmationEmailPayloadDto;
use Modules\Security\Application\Common\Ports\MailSender\MailSenderInterface;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken\GetEmailChangeConfirmationTokenQuery;
use Modules\Security\Application\User\Queries\GetPasswordResetToken\GetPasswordResetTokenQuery;
use Modules\Security\Application\User\Queries\GetRegistrationConfirmationToken\GetRegistrationConfirmationTokenQuery;
use Modules\Security\Domain\User\Events\UserCreatedEvent;
use Modules\Security\Domain\User\Events\UserEmailChangeInitiatedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetInitiatedEvent;

final readonly class UserEmailsSubscriber
{
    public function __construct(
        private MailSenderInterface $mailSender,
        private QueryBusInterface $queryBus,
    ) {
    }

    public function onCreated(UserCreatedEvent $event): void
    {
        $token = $this->queryBus->ask(
            new GetRegistrationConfirmationTokenQuery($event->userId),
        )->token;

        $this->mailSender->send(
            $event->email,
            new RegistrationConfirmationEmailPayloadDto(
                $token,
            ),
        );
    }

    public function onEmailChangeInitiated(UserEmailChangeInitiatedEvent $event): void
    {
        $token = $this->queryBus->ask(
            new GetEmailChangeConfirmationTokenQuery(
                $event->userId,
            ),
        )->token;

        $this->mailSender->send(
            $event->newEmail,
            new EmailChangeConfirmationEmailPayloadDto(
                $token,
            ),
        );
    }

    public function onPasswordResetInitiated(UserPasswordResetInitiatedEvent $event): void
    {
        $token = $this->queryBus->ask(
            new GetPasswordResetTokenQuery(
                $event->userId,
            ),
        )->token;

        $user = $this->queryBus->ask(
            new GetUserQuery(UserFilter::create()->withUserId($event->userId)),
        )->user;

        $this->mailSender->send(
            $user->email,
            new PasswordResetEmailPayloadDto(
                $token,
            ),
        );
    }
}
