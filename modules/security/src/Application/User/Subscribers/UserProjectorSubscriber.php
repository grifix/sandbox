<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Subscribers;

use Modules\Security\Application\User\Ports\Projector\CreateUserDto;
use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;
use Modules\Security\Domain\User\Events\UserCreatedEvent;
use Modules\Security\Domain\User\Events\UserDisabledEvent;
use Modules\Security\Domain\User\Events\UserEmailChangeConfirmedEvent;
use Modules\Security\Domain\User\Events\UserEmailChangeInitiatedEvent;
use Modules\Security\Domain\User\Events\UserEnabledEvent;
use Modules\Security\Domain\User\Events\UserPasswordChangedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetCompletedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetInitiatedEvent;
use Modules\Security\Domain\User\Events\UserRegistrationCompletedEvent;
use Modules\Security\Domain\User\Events\UserResumedEvent;
use Modules\Security\Domain\User\Events\UserRoleAssignedEvent;
use Modules\Security\Domain\User\Events\UserRoleUnAssignedEvent;
use Modules\Security\Domain\User\Events\UserSuspendedEvent;

final readonly class UserProjectorSubscriber
{
    public function __construct(private UserProjectorInterface $projector)
    {
    }

    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->projector->createUser(
            new CreateUserDto($event->userId, $event->email, $event->encryptedPassword, $event->createdAt),
        );
    }

    public function onRoleAssigned(UserRoleAssignedEvent $event): void
    {
        $this->projector->assignRole($event->userId, $event->roleId);
    }

    public function onRoleUnassigned(UserRoleUnAssignedEvent $event): void
    {
        $this->projector->unAssignRole($event->userId, $event->roleId);
    }

    public function onSuspended(UserSuspendedEvent $event): void
    {
        $this->projector->suspend($event->userId, $event->resumeDate);
    }

    public function onResume(UserResumedEvent $event): void
    {
        $this->projector->resume($event->userId);
    }

    public function onEmailChanged(UserEmailChangeInitiatedEvent $event): void
    {
        $this->projector->initiateEmailChange($event->userId, $event->newEmail);
    }

    public function onPasswordChanged(UserPasswordChangedEvent $event): void
    {
        $this->projector->updatePassword($event->userId, $event->newEncryptedPassword);
    }

    public function onDisabled(UserDisabledEvent $event): void
    {
        $this->projector->disable($event->userId);
    }

    public function onEnabled(UserEnabledEvent $event): void
    {
        $this->projector->enable($event->userId);
    }

    public function onResetPassword(UserPasswordResetCompletedEvent $event): void
    {
        $this->projector->updatePassword($event->userId, $event->newEncryptedPassword);
    }

    public function onEmailChangeConfirmed(UserEmailChangeConfirmedEvent $event): void
    {
        $this->projector->confirmEmailChange($event->userId);
    }

    public function onPasswordResetInitiated(UserPasswordResetInitiatedEvent $event): void
    {
        $this->projector->initiatePasswordReset($event->userId);
    }

    public function onRegistrationCompleted(UserRegistrationCompletedEvent $event): void
    {
        $this->projector->completeRegistration($event->userId);
    }
}
