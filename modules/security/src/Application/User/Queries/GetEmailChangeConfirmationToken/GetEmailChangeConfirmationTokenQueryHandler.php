<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken;

use Grifix\Clock\ClockInterface;
use Grifix\Date\DateInterval\DateInterval;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;

final readonly class GetEmailChangeConfirmationTokenQueryHandler
{
    public const TOKEN_TYPE = 'emailChangeConfirmation';

    public function __construct(
        private JwtTokenManagerInterface $jwtTokenManager,
        private ClockInterface $clock,
    ) {
    }

    public function __invoke(GetEmailChangeConfirmationTokenQuery $query): GetEmailChangeConfirmationTokenQueryResult
    {
        $token = $this->jwtTokenManager->encodeToken(
            [
                'userId' => $query->userId->toString(),
                'type' => self::TOKEN_TYPE,
            ],
            $this->clock->getCurrentDate()->add(DateInterval::create(minutes: 15)),
        );

        return new GetEmailChangeConfirmationTokenQueryResult($token);
    }
}
