<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken;

final readonly class GetEmailChangeConfirmationTokenQueryResult
{
    public function __construct(public string $token)
    {
    }
}
