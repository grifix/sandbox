<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken;

use Grifix\Uuid\Uuid;

final readonly class GetEmailChangeConfirmationTokenQuery
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
