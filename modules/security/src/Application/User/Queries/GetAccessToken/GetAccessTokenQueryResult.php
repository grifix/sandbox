<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetAccessToken;

final readonly class GetAccessTokenQueryResult
{
    public function __construct(public string $token)
    {
    }
}
