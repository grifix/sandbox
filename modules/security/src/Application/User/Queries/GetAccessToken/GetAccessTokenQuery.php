<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetAccessToken;

use Grifix\Uuid\Uuid;

final readonly class GetAccessTokenQuery
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
