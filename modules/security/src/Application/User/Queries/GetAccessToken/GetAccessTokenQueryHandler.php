<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetAccessToken;

use Grifix\Clock\ClockInterface;
use Grifix\Date\DateInterval\DateInterval;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;

final readonly class GetAccessTokenQueryHandler
{
    public const TOKEN_TYPE = 'access';

    public function __construct(
        private JwtTokenManagerInterface $tokenManager,
        private ClockInterface $clock,
    ) {
    }

    public function __invoke(GetAccessTokenQuery $query): GetAccessTokenQueryResult
    {
        return new GetAccessTokenQueryResult(
            $this->tokenManager->encodeToken(
                [
                    'userId' => $query->userId->toString(),
                    'type' => self::TOKEN_TYPE,
                ],
                $this->clock->getCurrentDate()->add(DateInterval::create(minutes: 15)),
            ),
        );
    }
}
