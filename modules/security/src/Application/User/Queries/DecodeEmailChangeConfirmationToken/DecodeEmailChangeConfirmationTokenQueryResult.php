<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken;

use Grifix\Uuid\Uuid;

final readonly class DecodeEmailChangeConfirmationTokenQueryResult
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
