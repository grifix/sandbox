<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken;

final readonly class DecodeEmailChangeConfirmationTokenQuery
{
    public function __construct(
        public string $token,
    ) {
    }
}
