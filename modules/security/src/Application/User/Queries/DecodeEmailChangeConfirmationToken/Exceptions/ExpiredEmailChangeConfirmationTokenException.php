<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class ExpiredEmailChangeConfirmationTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Expired email change confirmation token!',
            SecurityErrorCodes::expiredEmailChangeConfirmationToken->value,
        );
    }
}
