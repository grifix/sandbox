<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken\Exceptions\ExpiredEmailChangeConfirmationTokenException;
use Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken\Exceptions\InvalidEmailChangeConfirmationTokenException;
use Modules\Security\Application\User\Queries\GetEmailChangeConfirmationToken\GetEmailChangeConfirmationTokenQueryHandler;

final readonly class DecodeEmailChangeConfirmationTokenQueryHandler
{
    public function __construct(
        private JwtTokenManagerInterface $jwtTokenManager,
    ) {
    }

    public function __invoke(
        DecodeEmailChangeConfirmationTokenQuery $query,
    ): DecodeEmailChangeConfirmationTokenQueryResult {
        try {
            $payload = $this->jwtTokenManager->decodeToken($query->token);
        } catch (ExpiredTokenException) {
            throw new ExpiredEmailChangeConfirmationTokenException();
        } catch (InvalidTokenException) {
            throw new InvalidEmailChangeConfirmationTokenException();
        }

        if (!isset($payload['type'])) {
            throw new InvalidEmailChangeConfirmationTokenException();
        }
        if ($payload['type'] !== GetEmailChangeConfirmationTokenQueryHandler::TOKEN_TYPE) {
            throw new InvalidEmailChangeConfirmationTokenException();
        }
        if (!isset($payload['userId'])) {
            throw new InvalidEmailChangeConfirmationTokenException();
        }
        return new DecodeEmailChangeConfirmationTokenQueryResult(
            Uuid::createFromString($payload['userId']),
        );
    }
}
