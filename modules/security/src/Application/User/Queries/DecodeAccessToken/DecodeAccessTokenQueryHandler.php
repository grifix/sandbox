<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeAccessToken;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\User\Queries\DecodeAccessToken\Exceptions\ExpiredAccessTokenException;
use Modules\Security\Application\User\Queries\DecodeAccessToken\Exceptions\InvalidAccessTokenException;
use Modules\Security\Application\User\Queries\GetAccessToken\GetAccessTokenQueryHandler;

final readonly class DecodeAccessTokenQueryHandler
{
    public function __construct(
        private JwtTokenManagerInterface $tokenManager,
    ) {
    }

    public function __invoke(DecodeAccessTokenQuery $query): DecodeAccessTokenResult
    {
        try {
            $payload = $this->tokenManager->decodeToken($query->token);
        } catch (ExpiredTokenException) {
            throw new ExpiredAccessTokenException();
        } catch (InvalidTokenException) {
            throw new InvalidAccessTokenException();
        }

        if (!isset($payload['type'])) {
            throw new InvalidAccessTokenException();
        }
        if ($payload['type'] !== GetAccessTokenQueryHandler::TOKEN_TYPE) {
            throw new InvalidAccessTokenException();
        }
        if (!isset($payload['userId'])) {
            throw new InvalidAccessTokenException();
        }
        return new DecodeAccessTokenResult(
            Uuid::createFromString($payload['userId']),
        );
    }
}
