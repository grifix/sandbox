<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeAccessToken;

use Grifix\Uuid\Uuid;

final readonly class DecodeAccessTokenResult
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
