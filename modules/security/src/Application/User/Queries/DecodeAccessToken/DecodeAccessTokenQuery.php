<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeAccessToken;

final readonly class DecodeAccessTokenQuery
{
    public function __construct(
        public string $token,
    ) {
    }
}
