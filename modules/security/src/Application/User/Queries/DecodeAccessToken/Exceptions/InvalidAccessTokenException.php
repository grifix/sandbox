<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeAccessToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class InvalidAccessTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Invalid access token!', SecurityErrorCodes::invalidAccessToken->value);
    }
}
