<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Count;

use Modules\Security\Application\User\Ports\Projector\UserFilter;

final readonly class CountUsersQuery
{
    public function __construct(
        public UserFilter $filter,
    ) {
    }
}
