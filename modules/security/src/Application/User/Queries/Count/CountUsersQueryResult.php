<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Count;

final readonly class CountUsersQueryResult
{
    public function __construct(
        public int $value,
    ) {
    }
}
