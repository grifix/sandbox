<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Count;

use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;

final readonly class CountUsersQueryHandler
{
    public function __construct(
        private UserProjectorInterface $userProjector,
    ) {
    }

    public function __invoke(CountUsersQuery $query): CountUsersQueryResult
    {
        return new CountUsersQueryResult($this->userProjector->count($query->filter));
    }
}
