<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken;

final readonly class DecodeRegistrationConfirmationTokenQuery
{
    public function __construct(
        public string $token,
    ) {
    }
}
