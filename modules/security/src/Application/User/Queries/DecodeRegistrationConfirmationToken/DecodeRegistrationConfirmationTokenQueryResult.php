<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken;

use Grifix\Uuid\Uuid;

final readonly class DecodeRegistrationConfirmationTokenQueryResult
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
