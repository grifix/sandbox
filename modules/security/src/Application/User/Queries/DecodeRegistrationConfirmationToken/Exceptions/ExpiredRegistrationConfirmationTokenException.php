<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class ExpiredRegistrationConfirmationTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Expired registration confirmation token!',
            SecurityErrorCodes::expiredRegistrationConfirmationToken->value,
        );
    }
}
