<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\User\Queries\DecodeEmailChangeConfirmationToken\Exceptions\ExpiredEmailChangeConfirmationTokenException;
use Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken\Exceptions\ExpiredRegistrationConfirmationTokenException;
use Modules\Security\Application\User\Queries\DecodeRegistrationConfirmationToken\Exceptions\InvalidRegistrationConfirmationTokenException;
use Modules\Security\Application\User\Queries\GetRegistrationConfirmationToken\GetRegistrationConfirmationTokenQueryHandler;

final readonly class DecodeRegistrationConfirmationTokenQueryHandler
{
    public function __construct(
        private JwtTokenManagerInterface $jwtTokenManager,
    ) {
    }

    public function __invoke(
        DecodeRegistrationConfirmationTokenQuery $query,
    ): DecodeRegistrationConfirmationTokenQueryResult {
        try {
            $payload = $this->jwtTokenManager->decodeToken($query->token);
        } catch (ExpiredTokenException) {
            throw new ExpiredRegistrationConfirmationTokenException();
        } catch (InvalidTokenException) {
            throw new InvalidRegistrationConfirmationTokenException();
        }

        if (!isset($payload['type'])) {
            throw new InvalidRegistrationConfirmationTokenException();
        }
        if ($payload['type'] !== GetRegistrationConfirmationTokenQueryHandler::TOKEN_TYPE) {
            throw new InvalidRegistrationConfirmationTokenException();
        }
        if (!isset($payload['userId'])) {
            throw new InvalidRegistrationConfirmationTokenException();
        }
        return new DecodeRegistrationConfirmationTokenQueryResult(
            Uuid::createFromString($payload['userId']),
        );
    }
}
