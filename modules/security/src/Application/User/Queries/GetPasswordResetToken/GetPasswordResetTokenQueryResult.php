<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetPasswordResetToken;

final readonly class GetPasswordResetTokenQueryResult
{
    public function __construct(
        public string $token,
    ) {
    }
}
