<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetPasswordResetToken;

use Grifix\Clock\ClockInterface;
use Grifix\Date\DateInterval\DateInterval;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;

final readonly class GetPasswordResetTokenQueryHandler
{
    public const TOKEN_TYPE = 'passwordReset';

    public function __construct(
        private JwtTokenManagerInterface $jwtTokenManager,
        private ClockInterface $clock,
    ) {
    }

    public function __invoke(GetPasswordResetTokenQuery $query): GetPasswordResetTokenQueryResult
    {
        $token = $this->jwtTokenManager->encodeToken(
            [
                'userId' => $query->userId->toString(),
                'type' => self::TOKEN_TYPE,
            ],
            $this->clock->getCurrentDate()->add(DateInterval::create(minutes: 5)),
        );

        return new GetPasswordResetTokenQueryResult($token);
    }
}
