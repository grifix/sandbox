<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetPasswordResetToken;

use Grifix\Uuid\Uuid;

final readonly class GetPasswordResetTokenQuery
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
