<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Find;

use Modules\Security\Application\User\Ports\Projector\UserDto;

final readonly class FindUsersQueryResult
{

    /**
     * @param UserDto[] $users
     */
    public function __construct(
        public array $users,
    ) {
    }
}
