<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Find;

use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;

final readonly class FindUsersQueryHandler
{
    public function __construct(
        public UserProjectorInterface $userProjector,
    ) {
    }

    public function __invoke(FindUsersQuery $query): FindUsersQueryResult
    {
        return new FindUsersQueryResult(
            $this->userProjector->find($query->filter),
        );
    }
}
