<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Find;

use Modules\Security\Application\User\Ports\Projector\UserFilter;

final readonly class FindUsersQuery
{
    public function __construct(
        public UserFilter $filter,
    ) {
    }
}
