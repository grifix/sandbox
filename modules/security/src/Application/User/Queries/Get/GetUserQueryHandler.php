<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Get;

use Modules\Security\Application\User\Ports\Projector\UserProjectorInterface;

final class GetUserQueryHandler
{
    public function __construct(private readonly UserProjectorInterface $projector)
    {
    }

    public function __invoke(GetUserQuery $query): GetUserQueryResult
    {
        return new GetUserQueryResult(
            $this->projector->getOne($query->filter)
        );
    }
}
