<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Get;

use Modules\Security\Application\User\Ports\Projector\UserDto;

final class GetUserQueryResult
{
    public function __construct(public readonly UserDto $user)
    {
    }
}
