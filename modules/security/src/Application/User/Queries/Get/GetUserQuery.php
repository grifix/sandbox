<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\Get;

use Modules\Security\Application\User\Ports\Projector\UserFilter;

final class GetUserQuery
{

    public function __construct(public readonly UserFilter $filter)
    {
    }
}
