<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodePasswordResetToken;

final readonly class DecodePasswordResetTokenQuery
{
    public function __construct(
        public string $token,
    ) {
    }
}
