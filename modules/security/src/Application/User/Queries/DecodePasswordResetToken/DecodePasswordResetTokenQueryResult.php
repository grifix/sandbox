<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodePasswordResetToken;

use Grifix\Uuid\Uuid;

final readonly class DecodePasswordResetTokenQueryResult
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
