<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodePasswordResetToken;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\User\Queries\DecodePasswordResetToken\Exceptions\ExpiredPasswordResetTokenException;
use Modules\Security\Application\User\Queries\DecodePasswordResetToken\Exceptions\InvalidPasswordResetTokenException;
use Modules\Security\Application\User\Queries\GetPasswordResetToken\GetPasswordResetTokenQueryHandler;

final readonly class DecodePasswordResetTokenQueryHandler
{
    public function __construct(
        private JwtTokenManagerInterface $jwtTokenManager,
    ) {
    }

    public function __invoke(DecodePasswordResetTokenQuery $query): DecodePasswordResetTokenQueryResult
    {
        try {
            $payload = $this->jwtTokenManager->decodeToken($query->token);
        } catch (ExpiredTokenException) {
            throw new ExpiredPasswordResetTokenException();
        } catch (InvalidTokenException) {
            throw new InvalidPasswordResetTokenException();
        }

        if (!isset($payload['type'])) {
            throw new InvalidPasswordResetTokenException();
        }
        if ($payload['type'] !== GetPasswordResetTokenQueryHandler::TOKEN_TYPE) {
            throw new InvalidPasswordResetTokenException();
        }

        if (!isset($payload['userId'])) {
            throw new InvalidPasswordResetTokenException();
        }

        return new DecodePasswordResetTokenQueryResult(Uuid::createFromString($payload['userId']));
    }
}
