<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\DecodePasswordResetToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class InvalidPasswordResetTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Invalid password reset token!',
            SecurityErrorCodes::invalidPasswordResetToken->value,
        );
    }
}
