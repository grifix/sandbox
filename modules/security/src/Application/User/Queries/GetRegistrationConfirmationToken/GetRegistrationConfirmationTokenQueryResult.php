<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetRegistrationConfirmationToken;

final readonly class GetRegistrationConfirmationTokenQueryResult
{
    public function __construct(
        public string $token,
    ) {
    }
}
