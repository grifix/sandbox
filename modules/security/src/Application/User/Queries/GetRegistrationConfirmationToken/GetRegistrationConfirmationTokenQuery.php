<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Queries\GetRegistrationConfirmationToken;

use Grifix\Uuid\Uuid;

final readonly class GetRegistrationConfirmationTokenQuery
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
