<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Disable;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final class DisableUserCommandHandler
{
    public function __construct(public readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(DisableUserCommand $command): void
    {
        $this->userRepository->get($command->userId)->disable();
    }
}
