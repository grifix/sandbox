<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Disable;

use Grifix\Uuid\Uuid;

final class DisableUserCommand
{

    public function __construct(public readonly Uuid $userId)
    {
    }
}
