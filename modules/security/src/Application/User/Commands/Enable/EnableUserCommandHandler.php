<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Enable;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class EnableUserCommandHandler
{

    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(EnableUserCommand $command): void
    {
        $this->userRepository->get($command->userId)->enable();
    }
}
