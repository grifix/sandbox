<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Enable;

use Grifix\Uuid\Uuid;

final readonly class EnableUserCommand
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
