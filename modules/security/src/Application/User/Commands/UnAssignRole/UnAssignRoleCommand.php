<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\UnAssignRole;

use Grifix\Uuid\Uuid;

final class UnAssignRoleCommand
{
    public function __construct(
        public readonly Uuid $userId,
        public readonly Uuid $roleId,
    ) {
    }
}
