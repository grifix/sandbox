<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\UnAssignRole;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final class UnAssignRoleCommandHandler
{
    public function __construct(private readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(UnAssignRoleCommand $command): void
    {
        $this->userRepository->get($command->userId)->unAssignRole($command->roleId);
    }
}
