<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\ConfirmEmailChange;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class ConfirmEmailChangeCommandHandler
{
    public function __construct(private UserRepositoryInterface $repository)
    {
    }

    public function __invoke(ConfirmEmailChangeCommand $command): void
    {
        $this->repository->get($command->userId)->confirmEmailChange();
    }
}
