<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\ConfirmEmailChange;

use Grifix\Uuid\Uuid;

final readonly class ConfirmEmailChangeCommand
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
