<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\InitiateEmailChange;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class InitiateUserEmailChangeCommandHandler
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(InitiateUserEmailChangeCommand $command): void
    {
        $this->userRepository->get($command->userId)->initiateEmailChange($command->email, $command->password);
    }
}
