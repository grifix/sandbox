<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\InitiateEmailChange;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class InitiateUserEmailChangeCommand
{
    public function __construct(
        public Uuid $userId,
        public Email $email,
        public string $password,
    ) {
    }
}
