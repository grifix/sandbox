<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Authorize;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final class AuthorizeUserCommandHandler
{

    public function __construct(private readonly UserRepositoryInterface $repository)
    {
    }

    public function __invoke(AuthorizeUserCommand $command): void
    {
        $this->repository->get($command->userId)->authorize($command->permission, $command->ipAddress);
    }
}
