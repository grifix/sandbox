<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Authorize;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final class AuthorizeUserCommand
{
    public function __construct(
        public readonly Uuid $userId,
        public readonly string $permission,
        public readonly ?IpAddress $ipAddress,
    ) {
    }
}
