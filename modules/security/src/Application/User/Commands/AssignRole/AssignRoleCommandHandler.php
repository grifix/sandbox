<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\AssignRole;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final class AssignRoleCommandHandler
{

    public function __construct(
        private readonly UserRepositoryInterface $repository,
    ) {
    }

    public function __invoke(AssignRoleCommand $command): void
    {
        $this->repository->get($command->userId)->assignRole($command->roleId);
    }
}
