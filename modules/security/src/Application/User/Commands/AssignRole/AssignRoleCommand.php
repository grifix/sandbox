<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\AssignRole;

use Grifix\Uuid\Uuid;

final class AssignRoleCommand
{

    public function __construct(
        public readonly Uuid $userId,
        public readonly Uuid $roleId,
    ) {
    }
}
