<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\ChangePassword;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class ChangeUserPasswordCommandHandler
{

    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(ChangeUserPasswordCommand $command): void
    {
        $this->userRepository->get($command->userId)->changePassword(
            $command->currentPassword,
            $command->newPassword,
            $command->newPasswordConfirmation
        );
    }
}
