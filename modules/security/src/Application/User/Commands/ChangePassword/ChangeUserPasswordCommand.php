<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\ChangePassword;

use Grifix\Uuid\Uuid;

final readonly class ChangeUserPasswordCommand
{

    public function __construct(
        public Uuid $userId,
        public string $currentPassword,
        public string $newPassword,
        public string $newPasswordConfirmation,
    ) {
    }
}
