<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\CompetePasswordReset;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class CompletePasswordResetCommandHandler
{
    public function __construct(
        private UserRepositoryInterface $repository,
    ) {
    }

    public function __invoke(CompletePasswordResetCommand $command): void
    {
        $this->repository->get($command->userId)->completePasswordReset(
            $command->newPassword,
            $command->newPasswordConfirmation,
            $command->userIp,
        );
    }
}
