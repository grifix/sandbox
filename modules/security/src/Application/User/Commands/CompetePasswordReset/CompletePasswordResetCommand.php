<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\CompetePasswordReset;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final readonly class CompletePasswordResetCommand
{
    public function __construct(
        public Uuid $userId,
        public string $newPassword,
        public string $newPasswordConfirmation,
        public ?IpAddress $userIp = null,
    ) {
    }
}
