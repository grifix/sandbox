<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\InitiatePasswordReset;

use Grifix\Uuid\Uuid;

final readonly class InitiatePasswordResetCommand
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
