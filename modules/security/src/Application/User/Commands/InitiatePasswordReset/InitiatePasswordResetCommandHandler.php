<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\InitiatePasswordReset;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class InitiatePasswordResetCommandHandler
{
    public function __construct(private UserRepositoryInterface $repository)
    {
    }

    public function __invoke(InitiatePasswordResetCommand $command): void
    {
        $this->repository->get($command->userId)->initiatePasswordReset();
    }
}
