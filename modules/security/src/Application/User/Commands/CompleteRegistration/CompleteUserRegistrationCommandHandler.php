<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\CompleteRegistration;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class CompleteUserRegistrationCommandHandler
{
    public function __construct(
        private UserRepositoryInterface $repository,
    ) {
    }

    public function __invoke(CompleteUserRegistrationCommand $command): void
    {
        $user = $this->repository->get($command->userId);
        $user->completeRegistration();
    }
}
