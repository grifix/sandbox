<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\CompleteRegistration;

use Grifix\Uuid\Uuid;

final readonly class CompleteUserRegistrationCommand
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
