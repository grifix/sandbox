<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Suspend;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final class SuspendUserCommandHandler
{
    public function __construct(private readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(SuspendUserCommand $command): void
    {
        $this->userRepository->get($command->userId)->suspend($command->period);
    }
}
