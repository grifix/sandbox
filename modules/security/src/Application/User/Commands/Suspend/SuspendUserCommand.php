<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Suspend;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Uuid\Uuid;

final class SuspendUserCommand
{
    public function __construct(
        public readonly Uuid $userId,
        public readonly DateInterval $period,
    ) {
    }
}
