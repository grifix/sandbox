<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Authenticate;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final readonly class AuthenticateUserCommand
{
    public function __construct(
        public Uuid $userId,
        public string $password,
        public ?IpAddress $ip = null,
    ) {
    }
}
