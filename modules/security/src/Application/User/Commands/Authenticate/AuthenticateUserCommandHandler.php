<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Authenticate;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;

final readonly class AuthenticateUserCommandHandler
{

    public function __construct(private UserRepositoryInterface $repository)
    {
    }

    public function __invoke(AuthenticateUserCommand $command): void
    {
        $this->repository->get($command->userId)->authenticate($command->password, $command->ip);
    }
}
