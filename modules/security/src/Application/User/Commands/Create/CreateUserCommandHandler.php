<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Create;

use Modules\Security\Application\User\Ports\Repository\UserRepositoryInterface;
use Modules\Security\Domain\User\UserFactory;

final class CreateUserCommandHandler
{

    public function __construct(
        private readonly UserFactory $factory,
        private readonly UserRepositoryInterface $repository,
    ) {
    }

    public function __invoke(CreateUserCommand $command): void
    {
        $this->repository->add(
            $this->factory->createUser(
                $command->id,
                $command->email,
                $command->password,
            ),
        );
    }
}
