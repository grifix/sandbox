<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Commands\Create;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class CreateUserCommand
{
    public function __construct(
        public Uuid $id,
        public Email $email,
        public string $password,
    ) {
    }
}
