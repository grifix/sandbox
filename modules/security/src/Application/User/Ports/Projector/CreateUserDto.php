<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class CreateUserDto
{

    public function __construct(
        public Uuid $id,
        public Email $email,
        public string $encryptedPassword,
        public DateTime $createdAt,
    ) {
    }
}
