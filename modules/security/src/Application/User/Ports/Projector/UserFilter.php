<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final class UserFilter
{
    private ?Uuid $userId = null;

    private ?Email $email = null;


    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $sortBy = null;

    private ?string $sortDirection = null;

    private ?string $password = null;

    private bool $withPermissions = false;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getUserId(): ?Uuid
    {
        return $this->userId;
    }

    public function withUserId(?Uuid $userId): UserFilter
    {
        $this->userId = $userId;
        return $this;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function withEmail(?Email $email): UserFilter
    {
        $this->email = $email;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function withOffset(?int $offset): UserFilter
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function withLimit(?int $limit): UserFilter
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function sortBy(?string $sortBy): UserFilter
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function withSortDirection(?string $sortDirection): UserFilter
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }


    public function getWithPermissions(): bool
    {
        return $this->withPermissions;
    }

    public function withPermissions(bool $withPermissions): UserFilter
    {
        $this->withPermissions = $withPermissions;
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function withPassword(?string $password): UserFilter
    {
        $this->password = $password;
        return $this;
    }

}
