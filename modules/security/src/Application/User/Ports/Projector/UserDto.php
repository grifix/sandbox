<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class UserDto
{
    /**
     * @param Uuid[] $roles
     * @param string[] $permissions
     */
    public function __construct(
        public Uuid $id,
        public Email $email,
        public DateTime $createdAt,
        public string $encryptedPassword,
        public bool $disabled = false,
        public bool $passwordResetInitiated = false,
        public bool $registrationCompleted = false,
        public array $roles = [],
        public ?Email $newEmail = null,
        public ?array $permissions = null,
        public ?DateTime $resumeDate = null,
    ) {
    }
}
