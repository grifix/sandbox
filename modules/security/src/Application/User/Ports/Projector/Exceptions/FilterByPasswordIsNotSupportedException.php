<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class FilterByPasswordIsNotSupportedException extends \Exception
{
    public function __construct(string $method)
    {
        parent::__construct(
            sprintf('Filter by password is not supported for method [%s]!', $method),
            SecurityErrorCodes::filterByPasswordIsNotSupported->value,
        );
    }
}
