<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserDoesNotExistException extends \Exception
{

    public function __construct()
    {
        parent::__construct('User does not exist!', SecurityErrorCodes::userDoesNotExist->value);
    }
}
