<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Projector;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

interface UserProjectorInterface
{
    /**
     * @return UserDto[]
     */
    public function find(UserFilter $filter): array;

    public function getOne(UserFilter $filter): UserDto;

    public function findOne(UserFilter $filter): ?UserDto;

    public function count(UserFilter $filter): int;

    public function createUser(CreateUserDto $user): void;

    public function has(UserFilter $filter): bool;

    public function assignRole(Uuid $userId, Uuid $roleId): void;

    public function unAssignRole(Uuid $userId, Uuid $roleId): void;

    public function suspend(Uuid $userId, DateTime $resumeDate): void;

    public function resume(Uuid $userId): void;

    public function confirmEmailChange(Uuid $userId): void;

    public function updatePassword(Uuid $userId, string $password): void;

    public function disable(Uuid $userId): void;

    public function enable(Uuid $userId): void;

    public function initiateEmailChange(Uuid $userId, Email $newEmail): void;

    public function completeRegistration(Uuid $userId): void;
}
