<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Repository\Exceptions;

use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserDoesNotExistException extends \Exception
{
    public function __construct(Uuid $tokenId)
    {
        parent::__construct(
            sprintf('User with id [%s] does not exist!', $tokenId->toString()),
            SecurityErrorCodes::userDoesNotExist->value,
        );
    }
}
