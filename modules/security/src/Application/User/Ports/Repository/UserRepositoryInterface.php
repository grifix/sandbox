<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\Repository\Exceptions\UserDoesNotExistException;
use Modules\Security\Domain\User\User;

interface UserRepositoryInterface
{
    public function add(User $user): void;

    /**
     * @throws UserDoesNotExistException
     */
    public function get(Uuid $id): User;
}
