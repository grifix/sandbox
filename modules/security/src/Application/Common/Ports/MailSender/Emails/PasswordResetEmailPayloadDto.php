<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\MailSender\Emails;

final readonly class PasswordResetEmailPayloadDto
{
    public function __construct(
        public string $passwordResetToken,
    ) {
    }
}
