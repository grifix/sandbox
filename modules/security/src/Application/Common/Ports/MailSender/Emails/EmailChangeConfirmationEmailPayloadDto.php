<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\MailSender\Emails;

final class EmailChangeConfirmationEmailPayloadDto
{
    public function __construct(
        public string $emailConfirmationToken,
    ) {
    }
}
