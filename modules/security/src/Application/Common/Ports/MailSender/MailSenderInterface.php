<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\MailSender;

use Grifix\Email\Email;

interface MailSenderInterface
{
    public function send(Email $to, object $payload): void;
}
