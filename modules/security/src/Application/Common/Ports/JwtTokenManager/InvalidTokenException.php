<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\JwtTokenManager;

final class InvalidTokenException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct('Invalid token!', previous: $previous);
    }
}
