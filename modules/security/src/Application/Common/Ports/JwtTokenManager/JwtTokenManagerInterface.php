<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\JwtTokenManager;

use Grifix\Date\DateTime\DateTime;

interface JwtTokenManagerInterface
{
    /**
     * @return mixed[]
     * @throws InvalidTokenException
     * @throws ExpiredTokenException
     */
    public function decodeToken(string $token): array;

    /**
     * @param mixed[] $payload
     */
    public function encodeToken(array $payload, DateTime $expiresAt): string;
}
