<?php

declare(strict_types=1);

namespace Modules\Security\Application\Common\Ports\JwtTokenManager;

final class ExpiredTokenException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct('Expired token!', previous: $previous);
    }
}
