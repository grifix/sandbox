<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Subscribers;

use Modules\Security\Application\Role\Ports\Projector\RoleDto;
use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;
use Modules\Security\Domain\Role\Events\RoleCreatedEvent;
use Modules\Security\Domain\Role\Events\RolePermissionGrantedEvent;
use Modules\Security\Domain\Role\Events\RolePermissionRevokedEvent;

final class RoleProjectorSubscriber
{
    public function __construct(private readonly RoleProjectorInterface $projector)
    {
    }

    public function onRoleCreated(RoleCreatedEvent $event): void
    {
        $this->projector->createRole(
            new RoleDto(
                $event->roleId,
                $event->name,
            ),
        );
    }

    public function onPermissionGranted(RolePermissionGrantedEvent $event): void
    {
        $this->projector->grantPermission($event->roleId, $event->permission);
    }

    public function onPermissionRevoked(RolePermissionRevokedEvent $event): void
    {
        $this->projector->revokePermission($event->roleId, $event->permission);
    }
}
