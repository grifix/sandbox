<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Repository\Exceptions\RoleDoesNotExistException;
use Modules\Security\Domain\Role\Role;

interface RoleRepositoryInterface
{
    public function add(Role $role): void;

    /**
     * @throws RoleDoesNotExistException
     */
    public function get(Uuid $id): Role;
}
