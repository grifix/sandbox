<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Repository\Exceptions;

use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RoleDoesNotExistException extends \Exception
{
    public function __construct(Uuid $roleId)
    {
        parent::__construct(
            sprintf('Role with is [%s] does not exist!.', $roleId->toString()),
            SecurityErrorCodes::roleDoesNotExist->value,
        );
    }
}
