<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Projector;

use Grifix\Uuid\Uuid;

final class RoleFilter
{
    private ?Uuid $roleId = null;

    /** @var Uuid[] */
    private array $roleIds = [];

    private ?string $name = null;

    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $sortBy = null;

    private ?string $sortDirection = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getRoleId(): ?Uuid
    {
        return $this->roleId;
    }

    public function setRoleId(?Uuid $roleId): RoleFilter
    {
        $this->roleId = $roleId;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function withName(?string $name): RoleFilter
    {
        $this->name = $name;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function withOffset(?int $offset): RoleFilter
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function withLimit(?int $limit): RoleFilter
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function sortBy(?string $sortBy): RoleFilter
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function withSortDirection(?string $sortDirection): RoleFilter
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    /**
     * @param Uuid[] $roleIds
     */
    public function withRoleIds(array $roleIds): RoleFilter
    {
        $this->roleIds = $roleIds;
        return $this;
    }

    /**
     * @return Uuid[]
     */
    public function getRoleIds(): array
    {
        return $this->roleIds;
    }
}
