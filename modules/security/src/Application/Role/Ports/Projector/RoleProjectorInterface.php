<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Projector;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Role\Ports\Projector\Exceptions\RoleDoesNotExistException;
use Modules\Security\Infrastructure\Role\Application\Ports\RoleProjector;

interface RoleProjectorInterface
{
    public function createRole(RoleDto $role): void;

    /**
     * @return RoleDto[]
     */
    public function find(RoleFilter $filter): array;

    /**
     * @throws RoleDoesNotExistException
     */
    public function getOne(RoleFilter $filter): RoleDto;

    public function has(RoleFilter $filter): bool;

    public function findOne(RoleFilter $filter): ?RoleDto;

    public function grantPermission(Uuid $roleId, string $permission): void;

    public function revokePermission(Uuid $roleId, string $permission): void;

    public function count(RoleFilter $filter): int;
}
