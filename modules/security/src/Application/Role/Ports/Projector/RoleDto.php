<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Projector;

use Grifix\Uuid\Uuid;

final readonly class RoleDto
{
    /**
     * @param string[] $permissions
     */
    public function __construct(
        public Uuid $id,
        public string $name,
        public array $permissions = [],
    ) {
    }
}
