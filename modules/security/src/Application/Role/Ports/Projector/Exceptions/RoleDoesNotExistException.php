<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Ports\Projector\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RoleDoesNotExistException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Role does not exist!',
            SecurityErrorCodes::roleDoesNotExist->value,
        );
    }
}
