<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Count;

final readonly class CountRolesQueryResult
{
    public function __construct(
        public int $value,
    ) {
    }
}
