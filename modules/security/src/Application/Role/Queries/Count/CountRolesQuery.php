<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Count;

use Modules\Security\Application\Role\Ports\Projector\RoleFilter;

final readonly class CountRolesQuery
{
    public function __construct(
        public RoleFilter $filter,
    ) {
    }
}
