<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Count;

use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;

final readonly class CountRolesQueryHandler
{
    public function __construct(
        private RoleProjectorInterface $roleProjector,
    ) {
    }

    public function __invoke(CountRolesQuery $query): CountRolesQueryResult
    {
        return new CountRolesQueryResult($this->roleProjector->count($query->filter));
    }
}
