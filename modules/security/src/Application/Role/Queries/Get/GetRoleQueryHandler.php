<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Get;

use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;

final class GetRoleQueryHandler
{
    public function __construct(private readonly RoleProjectorInterface $projector)
    {
    }

    public function __invoke(GetRoleQuery $query): GetRoleQueryResult
    {
        return new GetRoleQueryResult(
            $this->projector->getOne($query->filter),
        );
    }
}
