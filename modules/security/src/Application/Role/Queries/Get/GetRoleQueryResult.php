<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Get;

use Modules\Security\Application\Role\Ports\Projector\RoleDto;

final class GetRoleQueryResult
{

    public function __construct(public readonly RoleDto $role)
    {
    }
}
