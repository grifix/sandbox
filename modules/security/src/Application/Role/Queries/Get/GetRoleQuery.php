<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Get;

use Modules\Security\Application\Role\Ports\Projector\RoleFilter;

final class GetRoleQuery
{
    public function __construct(public readonly RoleFilter $filter)
    {
    }
}
