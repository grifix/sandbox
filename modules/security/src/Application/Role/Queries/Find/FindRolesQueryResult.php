<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Find;

use Modules\Security\Application\Role\Ports\Projector\RoleDto;

final readonly class FindRolesQueryResult
{
    /**
     * @param RoleDto[] $roles
     */
    public function __construct(
        public array $roles,
    ) {
    }
}
