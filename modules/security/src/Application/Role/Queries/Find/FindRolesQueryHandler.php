<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Find;

use Modules\Security\Application\Role\Ports\Projector\RoleProjectorInterface;

final readonly class FindRolesQueryHandler
{
    public function __construct(
        private RoleProjectorInterface $roleProjector,
    ) {
    }

    public function __invoke(FindRolesQuery $query): FindRolesQueryResult
    {
        return new FindRolesQueryResult($this->roleProjector->find($query->filter));
    }
}
