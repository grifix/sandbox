<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Queries\Find;

use Modules\Security\Application\Role\Ports\Projector\RoleFilter;

final readonly class FindRolesQuery
{
    public function __construct(
        public RoleFilter $filter,
    ) {
    }
}
