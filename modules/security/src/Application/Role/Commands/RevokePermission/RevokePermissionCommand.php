<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\RevokePermission;

use Grifix\Uuid\Uuid;

final class RevokePermissionCommand
{

    public function __construct(
        public readonly Uuid $roleId,
        public readonly string $permission,
    ) {
    }
}
