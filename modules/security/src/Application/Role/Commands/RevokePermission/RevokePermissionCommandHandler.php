<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\RevokePermission;

use Modules\Security\Application\Role\Ports\Repository\RoleRepositoryInterface;

final class RevokePermissionCommandHandler
{

    public function __construct(public readonly RoleRepositoryInterface $roleRepository)
    {
    }

    public function __invoke(RevokePermissionCommand $command): void
    {
        $this->roleRepository->get($command->roleId)->revokePermission($command->permission);
    }
}
