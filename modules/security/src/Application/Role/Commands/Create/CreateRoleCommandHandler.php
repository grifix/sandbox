<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\Create;

use Modules\Security\Application\Role\Ports\Repository\RoleRepositoryInterface;
use Modules\Security\Domain\Role\RoleFactory;

final class CreateRoleCommandHandler
{
    public function __construct(
        private readonly RoleRepositoryInterface $roleRepository,
        private readonly RoleFactory $roleFactory,
    ) {
    }

    public function __invoke(CreateRoleCommand $command): void
    {
        $this->roleRepository->add(
            $this->roleFactory->createRole(
                $command->roleId,
                $command->name,
            ),
        );
    }
}
