<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\Create;

use Grifix\Uuid\Uuid;

final class CreateRoleCommand
{
    public function __construct(
        public readonly Uuid $roleId,
        public readonly string $name,
    ) {
    }
}
