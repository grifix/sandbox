<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\GrantPermission;

use Modules\Security\Application\Role\Ports\Repository\RoleRepositoryInterface;

final class GrantPermissionCommandHandler
{
    public function __construct(private readonly RoleRepositoryInterface $repository)
    {
    }

    public function __invoke(GrantPermissionCommand $command): void
    {
        $this->repository->get($command->roleId)->grantPermission($command->permission);
    }
}
