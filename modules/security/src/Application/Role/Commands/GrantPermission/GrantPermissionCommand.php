<?php

declare(strict_types=1);

namespace Modules\Security\Application\Role\Commands\GrantPermission;

use Grifix\Uuid\Uuid;

final class GrantPermissionCommand
{

    public function __construct(
        public readonly Uuid $roleId,
        public readonly string $permission,
    ) {
    }
}
