<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Repository\Exceptions;

use Exception;
use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RefreshAccessSessionDoesNotExistException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Refresh access session does not exist!',
            SecurityErrorCodes::refreshAccessSessionDoesNotExist->value,
        );
    }
}
