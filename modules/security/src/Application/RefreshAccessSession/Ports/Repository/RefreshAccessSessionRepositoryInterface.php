<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSession;

interface RefreshAccessSessionRepositoryInterface
{
    public function get(Uuid $refreshAccessSessionId): RefreshAccessSession;

    public function add(RefreshAccessSession $refreshAccessSession): void;
}
