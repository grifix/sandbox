<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RefreshAccessSessionProjectionDoesNotExistException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Refresh access session projection does not exist!',
            SecurityErrorCodes::refreshAccessSessionDoesNotExist->value,
        );
    }
}
