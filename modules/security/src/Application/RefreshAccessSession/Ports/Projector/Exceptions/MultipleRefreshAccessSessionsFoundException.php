<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions;

use Exception;
use Modules\Security\Domain\Common\SecurityErrorCodes;

final class MultipleRefreshAccessSessionsFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct(
            'Multiple refresh access sessions found!',
            SecurityErrorCodes::multipleRefreshAccessSessionsFound->value,
        );
    }
}
