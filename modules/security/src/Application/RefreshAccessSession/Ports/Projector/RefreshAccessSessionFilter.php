<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Projector;

use Grifix\Uuid\Uuid;

final class RefreshAccessSessionFilter
{
    private ?Uuid $refreshAccessSessionId = null;

    private ?Uuid $userId = null;

    private ?bool $isActive = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getRefreshAccessSessionId(): ?Uuid
    {
        return $this->refreshAccessSessionId;
    }

    public function setRefreshAccessSessionId(?Uuid $refreshAccessSessionId): RefreshAccessSessionFilter
    {
        $this->refreshAccessSessionId = $refreshAccessSessionId;
        return $this;
    }

    public function getUserId(): ?Uuid
    {
        return $this->userId;
    }

    public function setUserId(?Uuid $userId): RefreshAccessSessionFilter
    {
        $this->userId = $userId;
        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): RefreshAccessSessionFilter
    {
        $this->isActive = $isActive;
        return $this;
    }
}
