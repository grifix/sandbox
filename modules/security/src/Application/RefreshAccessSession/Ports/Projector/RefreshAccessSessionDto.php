<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Projector;

use Grifix\Date\DateTime\DateTime;
use Grifix\Uuid\Uuid;

final readonly class RefreshAccessSessionDto
{
    public function __construct(
        public Uuid $id,
        public Uuid $userId,
        public DateTime $expiresAt,
        public bool $isRevoked,
    ) {
    }
}
