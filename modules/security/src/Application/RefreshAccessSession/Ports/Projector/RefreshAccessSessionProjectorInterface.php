<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Ports\Projector;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\Exceptions\MultipleRefreshAccessSessionsFoundException;
use Modules\Security\Application\RefreshAccessSession\Ports\Repository\Exceptions\RefreshAccessSessionDoesNotExistException;

interface RefreshAccessSessionProjectorInterface
{
    /**
     * @throws RefreshAccessSessionDoesNotExistException
     * @throws MultipleRefreshAccessSessionsFoundException
     */
    public function get(RefreshAccessSessionFilter $filter): RefreshAccessSessionDto;

    /**
     * @return RefreshAccessSessionDto[]
     */
    public function find(RefreshAccessSessionFilter $filter): array;

    public function create(RefreshAccessSessionDto $dto): void;

    public function revoke(Uuid $refreshAccessSessionId): void;

    /**
     * @throws RefreshAccessSessionDoesNotExistException
     */
    public function getFirst(RefreshAccessSessionFilter $filter): RefreshAccessSessionDto;
}
