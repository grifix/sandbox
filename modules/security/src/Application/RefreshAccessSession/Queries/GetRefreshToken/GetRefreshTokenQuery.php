<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken;

use Grifix\Uuid\Uuid;

final readonly class GetRefreshTokenQuery
{
    public function __construct(
        public Uuid $userId,
    ) {
    }
}
