<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken;

use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;

final readonly class GetRefreshTokenQueryHandler
{
    public function __construct(
        private RefreshAccessSessionProjectorInterface $projector,
        private JwtTokenManagerInterface $tokenManager,
    ) {
    }

    public function __invoke(GetRefreshTokenQuery $query): GetRefreshTokenQueryResult
    {
        $session = $this->projector->getFirst(
            RefreshAccessSessionFilter::create()
                ->setUserId($query->userId)
                ->setIsActive(true),
        );
        return new GetRefreshTokenQueryResult(
            $this->tokenManager->encodeToken(
                [
                    'refreshAccessSessionId' => $session->id->toString(),
                    'userId' => $session->userId->toString(),
                    'type' => 'refreshAccessSession',
                ],
                $session->expiresAt,
            ),
        );
    }
}
