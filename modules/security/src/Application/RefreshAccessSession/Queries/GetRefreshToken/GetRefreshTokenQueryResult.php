<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\GetRefreshToken;

final readonly class GetRefreshTokenQueryResult
{
    public function __construct(
        public string $refreshToken,
    ) {
    }
}
