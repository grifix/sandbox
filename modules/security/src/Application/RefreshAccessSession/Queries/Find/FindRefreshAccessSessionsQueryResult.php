<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Find;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionDto;

final readonly class FindRefreshAccessSessionsQueryResult
{
    /**
     * @param RefreshAccessSessionDto[] $refreshAccessSessions
     */
    public function __construct(
        public array $refreshAccessSessions,
    ) {
    }
}
