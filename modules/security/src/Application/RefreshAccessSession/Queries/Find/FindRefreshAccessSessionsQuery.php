<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Find;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;

final readonly class FindRefreshAccessSessionsQuery
{
    public function __construct(
        public RefreshAccessSessionFilter $filter,
    ) {
    }
}
