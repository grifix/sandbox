<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Find;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;

final readonly class FindRefreshAccessSessionsQueryHandler
{
    public function __construct(
        private RefreshAccessSessionProjectorInterface $projector,
    ) {
    }

    public function __invoke(FindRefreshAccessSessionsQuery $query): FindRefreshAccessSessionsQueryResult
    {
        return new FindRefreshAccessSessionsQueryResult(
            $this->projector->find($query->filter),
        );
    }
}
