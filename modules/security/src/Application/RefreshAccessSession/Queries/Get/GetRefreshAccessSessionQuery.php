<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Get;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;

final readonly class GetRefreshAccessSessionQuery
{
    public function __construct(
        public RefreshAccessSessionFilter $filter,
    ) {
    }
}
