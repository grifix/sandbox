<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Get;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;

final readonly class GetRefreshAccessSessionQueryHandler
{
    public function __construct(
        private RefreshAccessSessionProjectorInterface $projector,
    ) {
    }

    public function __invoke(GetRefreshAccessSessionQuery $query): GetRefreshAccessSessionQueryResult
    {
        return new GetRefreshAccessSessionQueryResult(
            $this->projector->get($query->filter),
        );
    }
}
