<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\Get;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionDto;

final readonly class GetRefreshAccessSessionQueryResult
{
    public function __construct(
        public RefreshAccessSessionDto $refreshAccessSession,
    ) {
    }
}
