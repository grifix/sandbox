<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken;

final readonly class DecodeRefreshTokenQuery
{
    public function __construct(public string $token)
    {
    }
}
