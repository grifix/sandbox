<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken;

use Grifix\Uuid\Uuid;
use Modules\Security\Application\Common\Ports\JwtTokenManager\ExpiredTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\InvalidTokenException;
use Modules\Security\Application\Common\Ports\JwtTokenManager\JwtTokenManagerInterface;
use Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\Exceptions\ExpiredRefreshTokenException;
use Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\Exceptions\InvalidRefreshTokenException;

final readonly class DecodeRefreshTokenQueryHandler
{
    public function __construct(
        public JwtTokenManagerInterface $tokenManager,
    ) {
    }

    public function __invoke(DecodeRefreshTokenQuery $query): DecodeRefreshTokenQueryResult
    {
        try {
            $payload = $this->tokenManager->decodeToken($query->token);
        } catch (ExpiredTokenException) {
            throw new ExpiredRefreshTokenException();
        } catch (InvalidTokenException) {
            throw new InvalidRefreshTokenException();
        }

        if (!isset($payload['type'])) {
            throw new InvalidRefreshTokenException();
        }
        if ($payload['type'] !== 'refreshAccessSession') {
            throw new InvalidRefreshTokenException();
        }

        if (!isset($payload['refreshAccessSessionId'])) {
            throw new InvalidRefreshTokenException();
        }

        if (!isset($payload['userId'])) {
            throw new InvalidRefreshTokenException();
        }

        return new DecodeRefreshTokenQueryResult(
            Uuid::createFromString($payload['refreshAccessSessionId']),
            Uuid::createFromString($payload['userId']),
        );
    }
}
