<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken;

use Grifix\Uuid\Uuid;

final readonly class DecodeRefreshTokenQueryResult
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
        public Uuid $userId,
    ) {
    }
}
