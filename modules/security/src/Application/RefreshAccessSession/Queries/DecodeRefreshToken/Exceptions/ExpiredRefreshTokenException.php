<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class ExpiredRefreshTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Expired refresh token!', SecurityErrorCodes::expiredRefreshToken->value);
    }
}
