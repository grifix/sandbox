<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Queries\DecodeRefreshToken\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class InvalidRefreshTokenException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Invalid refresh token!', SecurityErrorCodes::invalidRefreshToken->value);
    }
}
