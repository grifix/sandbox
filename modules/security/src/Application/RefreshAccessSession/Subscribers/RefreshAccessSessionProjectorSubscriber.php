<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Subscribers;

use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionDto;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionCreatedEvent;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionRevokedEvent;

final readonly class RefreshAccessSessionProjectorSubscriber
{
    public function __construct(
        private RefreshAccessSessionProjectorInterface $projector,
    ) {
    }

    public function onCreate(RefreshAccessSessionCreatedEvent $event): void
    {
        $this->projector->create(
            new RefreshAccessSessionDto(
                $event->refreshAccessSessionId,
                $event->userId,
                $event->expiresAt,
                false,
            ),
        );
    }

    public function onRevoke(RefreshAccessSessionRevokedEvent $event): void
    {
        $this->projector->revoke($event->refreshAccessSessionId);
    }
}
