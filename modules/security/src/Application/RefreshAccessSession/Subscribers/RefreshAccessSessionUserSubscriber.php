<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Subscribers;

use Grifix\Framework\Application\CommandBusInterface;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\RefreshAccessSession\Commands\Create\CreateRefreshAccessSessionCommand;
use Modules\Security\Application\RefreshAccessSession\Commands\Revoke\RevokeRefreshAccessSessionCommand;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionFilter;
use Modules\Security\Application\RefreshAccessSession\Ports\Projector\RefreshAccessSessionProjectorInterface;
use Modules\Security\Domain\User\Events\UserAuthenticatedEvent;
use Modules\Security\Domain\User\Events\UserPasswordChangedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetCompletedEvent;

final readonly class RefreshAccessSessionUserSubscriber
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private RefreshAccessSessionProjectorInterface $projector,
    ) {
    }

    public function onUserAuthenticated(UserAuthenticatedEvent $event): void
    {
        $this->commandBus->tell(
            new CreateRefreshAccessSessionCommand(
                Uuid::createRandom(),
                $event->userId,
            ),
        );
    }

    public function onPasswordResetCompleted(UserPasswordResetCompletedEvent $event): void
    {
        $this->revokeAllActiveSessions($event->userId);
    }

    public function onPasswordChanged(UserPasswordChangedEvent $event): void
    {
        $this->revokeAllActiveSessions($event->userId);
    }

    private function revokeAllActiveSessions(Uuid $userId): void
    {
        $activeSessions = $this->projector->find(
            RefreshAccessSessionFilter::create()
                ->setIsActive(true)
                ->setUserId($userId),
        );
        foreach ($activeSessions as $session) {
            $this->commandBus->tell(new RevokeRefreshAccessSessionCommand($session->id));
        }
    }
}
