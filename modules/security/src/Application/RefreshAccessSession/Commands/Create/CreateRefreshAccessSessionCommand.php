<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Create;

use Grifix\Uuid\Uuid;

final readonly class CreateRefreshAccessSessionCommand
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
        public Uuid $userId,
    ) {
    }
}
