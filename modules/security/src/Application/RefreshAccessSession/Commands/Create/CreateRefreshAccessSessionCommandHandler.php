<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Create;

use Modules\Security\Application\RefreshAccessSession\Ports\Repository\RefreshAccessSessionRepositoryInterface;
use Modules\Security\Domain\RefreshAccessSession\RefreshAccessSessionFactory;

final readonly class CreateRefreshAccessSessionCommandHandler
{
    public function __construct(
        private RefreshAccessSessionFactory $factory,
        private RefreshAccessSessionRepositoryInterface $repository,
    ) {
    }

    public function __invoke(CreateRefreshAccessSessionCommand $command): void
    {
        $this->repository->add(
            $this->factory->createRefreshAccessSession($command->refreshAccessSessionId, $command->userId),
        );
    }
}
