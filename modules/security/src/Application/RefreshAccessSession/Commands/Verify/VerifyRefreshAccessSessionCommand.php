<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Verify;

use Grifix\Uuid\Uuid;

final readonly class VerifyRefreshAccessSessionCommand
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
    ) {
    }
}
