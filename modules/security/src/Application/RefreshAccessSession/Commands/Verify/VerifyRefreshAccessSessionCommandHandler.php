<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Verify;

use Modules\Security\Application\RefreshAccessSession\Ports\Repository\RefreshAccessSessionRepositoryInterface;

final readonly class VerifyRefreshAccessSessionCommandHandler
{
    public function __construct(
        private RefreshAccessSessionRepositoryInterface $repository,
    ) {
    }

    public function __invoke(VerifyRefreshAccessSessionCommand $command): void
    {
        $this->repository->get($command->refreshAccessSessionId)->verify();
    }
}
