<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Revoke;

use Modules\Security\Application\RefreshAccessSession\Ports\Repository\RefreshAccessSessionRepositoryInterface;

final readonly class RevokeRefreshAccessSessionCommandHandler
{
    public function __construct(
        private RefreshAccessSessionRepositoryInterface $repository,
    ) {
    }

    public function __invoke(RevokeRefreshAccessSessionCommand $command): void
    {
        $this->repository->get($command->refreshAccessSessionId)->revoke();
    }
}
