<?php

declare(strict_types=1);

namespace Modules\Security\Application\RefreshAccessSession\Commands\Revoke;

use Grifix\Uuid\Uuid;

final readonly class RevokeRefreshAccessSessionCommand
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
    ) {
    }
}
