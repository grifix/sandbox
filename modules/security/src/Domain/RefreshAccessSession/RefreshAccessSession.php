<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateTime\DateTime;
use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionCreatedEvent;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionRevokedEvent;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionVerifiedEvent;
use Modules\Security\Domain\RefreshAccessSession\Exceptions\RefreshAccessSessionExpiredException;
use Modules\Security\Domain\RefreshAccessSession\Exceptions\RefreshAccessSessionRevokedException;

final class RefreshAccessSession
{
    private readonly DateTime $expiresAt;

    private bool $revoked = false;

    public function __construct(
        private readonly RefreshAccessSessionOutsideInterface $outside,
        private readonly Uuid $id,
        private readonly Uuid $userId,
    ) {
        $this->expiresAt = $this->outside->getCurrentTime()->add(DateInterval::create(days: 30));
        $this->outside->publishEvent(
            new RefreshAccessSessionCreatedEvent(
                $this->id,
                $this->userId,
                $this->expiresAt,
            ),
        );
    }

    public function revoke(): void
    {
        $this->assertIsNotRevoked();
        $this->revoked = true;
        $this->outside->publishEvent(new RefreshAccessSessionRevokedEvent($this->id));
    }

    public function verify(?IpAddress $ipAddress = null): void
    {
        $this->assertIsNotRevoked();
        $this->assertIsNotExpired();
        $this->outside->publishEvent(new RefreshAccessSessionVerifiedEvent($this->id, $ipAddress));
    }

    private function assertIsNotRevoked(): void
    {
        if ($this->revoked) {
            throw new RefreshAccessSessionRevokedException();
        }
    }

    private function assertIsNotExpired(): void
    {
        if ($this->outside->getCurrentTime()->isAfter($this->expiresAt)) {
            throw new RefreshAccessSessionExpiredException();
        }
    }
}
