<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession;

use Grifix\Uuid\Uuid;

final readonly class RefreshAccessSessionFactory
{
    public function __construct(private RefreshAccessSessionOutsideInterface $outside)
    {
    }

    public function createRefreshAccessSession(Uuid $id, Uuid $userId): RefreshAccessSession
    {
        return new RefreshAccessSession($this->outside, $id, $userId);
    }
}
