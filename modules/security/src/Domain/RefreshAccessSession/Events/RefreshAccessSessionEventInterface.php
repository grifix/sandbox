<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession\Events;

use Grifix\Uuid\Uuid;

interface RefreshAccessSessionEventInterface
{
    public function getRefreshAccessSessionId(): Uuid;
}
