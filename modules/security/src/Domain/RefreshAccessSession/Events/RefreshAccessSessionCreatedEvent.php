<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession\Events;

use Grifix\Date\DateTime\DateTime;
use Grifix\Uuid\Uuid;

final readonly class RefreshAccessSessionCreatedEvent implements RefreshAccessSessionEventInterface
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
        public Uuid $userId,
        public DateTime $expiresAt,
    ) {
    }

    public function getRefreshAccessSessionId(): Uuid
    {
        return $this->refreshAccessSessionId;
    }
}
