<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession\Events;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final readonly class RefreshAccessSessionVerifiedEvent implements RefreshAccessSessionEventInterface
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
        public ?IpAddress $ipAddress,
    ) {
    }

    public function getRefreshAccessSessionId(): Uuid
    {
        return $this->refreshAccessSessionId;
    }
}
