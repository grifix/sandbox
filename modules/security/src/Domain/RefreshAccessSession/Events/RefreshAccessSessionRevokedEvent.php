<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession\Events;

use Grifix\Uuid\Uuid;

final readonly class RefreshAccessSessionRevokedEvent implements RefreshAccessSessionEventInterface
{
    public function __construct(
        public Uuid $refreshAccessSessionId,
    ) {
    }

    public function getRefreshAccessSessionId(): Uuid
    {
        return $this->refreshAccessSessionId;
    }
}
