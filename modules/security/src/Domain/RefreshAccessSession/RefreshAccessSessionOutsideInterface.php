<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession;

use Grifix\Date\DateTime\DateTime;
use Modules\Security\Domain\RefreshAccessSession\Events\RefreshAccessSessionEventInterface;

interface RefreshAccessSessionOutsideInterface
{
    public function publishEvent(RefreshAccessSessionEventInterface $event): void;

    public function getCurrentTime(): DateTime;
}
