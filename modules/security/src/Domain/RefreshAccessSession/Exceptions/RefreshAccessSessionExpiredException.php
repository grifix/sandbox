<?php

declare(strict_types=1);

namespace Modules\Security\Domain\RefreshAccessSession\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RefreshAccessSessionExpiredException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Refresh access session is expired!',
            SecurityErrorCodes::refreshAccessSessionExpired->value,
        );
    }
}
