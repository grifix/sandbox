<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Events\UserEventInterface;

interface UserOutsideInterface
{
    public function generatePassword(): string;

    public function isPasswordSecure(string $password): bool;

    public function hashPassword(string $password): string;

    public function isPasswordValid(string $password, string $hash): bool;

    public function publishEvent(UserEventInterface $event): void;

    public function doesUserWithEmailExist(Email $email): bool;

    public function getCurrentDateTime(): DateTime;

    public function hasRoleAccess(Uuid $roleId, string $permission): bool;
}
