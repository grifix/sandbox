<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final class UserFactory
{

    public function __construct(private readonly UserOutsideInterface $outside)
    {
    }

    public function createUser(Uuid $id, Email $email, string $password): User
    {
        return new User($this->outside, $id, $email, $password);
    }
}
