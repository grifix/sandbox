<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final readonly class UserAuthenticatedEvent implements UserEventInterface
{

    public function __construct(
        public Uuid $userId,
        public ?IpAddress $ip,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
