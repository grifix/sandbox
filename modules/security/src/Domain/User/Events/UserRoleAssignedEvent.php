<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Uuid\Uuid;

final readonly class UserRoleAssignedEvent implements UserEventInterface
{
    public function __construct(
        public Uuid $userId,
        public Uuid $roleId,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
