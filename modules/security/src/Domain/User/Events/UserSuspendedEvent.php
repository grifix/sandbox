<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Date\DateTime\DateTime;
use Grifix\Uuid\Uuid;

final readonly class UserSuspendedEvent implements UserEventInterface
{
    public function __construct(
        public Uuid $userId,
        public DateTime $resumeDate,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
