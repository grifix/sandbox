<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Uuid\Uuid;

final class UserEnabledEvent implements UserEventInterface
{

    public function __construct(
        public readonly Uuid $userId
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
