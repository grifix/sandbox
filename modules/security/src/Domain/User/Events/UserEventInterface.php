<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Uuid\Uuid;

interface UserEventInterface
{
    public function getUserId(): Uuid;
}
