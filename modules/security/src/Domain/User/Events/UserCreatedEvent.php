<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class UserCreatedEvent implements UserEventInterface
{
    public function __construct(
        public Uuid $userId,
        public Email $email,
        public string $encryptedPassword,
        public DateTime $createdAt,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
