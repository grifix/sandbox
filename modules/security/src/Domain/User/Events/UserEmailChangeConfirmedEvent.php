<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Events;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class UserEmailChangeConfirmedEvent implements UserEventInterface
{
    public function __construct(
        public Uuid $userId,
        public Email $newEmail,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
