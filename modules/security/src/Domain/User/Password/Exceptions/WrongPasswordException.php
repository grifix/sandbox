<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Password\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class WrongPasswordException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Wrong password!',
            SecurityErrorCodes::wrongPassword->value,
        );
    }
}
