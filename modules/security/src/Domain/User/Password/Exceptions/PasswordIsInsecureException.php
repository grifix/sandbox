<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Password\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class PasswordIsInsecureException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Password is insecure!',
            SecurityErrorCodes::passwordIsInsecure->value,
        );
    }
}
