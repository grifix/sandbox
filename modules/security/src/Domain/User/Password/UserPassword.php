<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Password;

use Modules\Security\Domain\User\Password\Exceptions\PasswordIsInsecureException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordMismatchException;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Domain\User\UserOutsideInterface;

final class UserPassword
{
    private string $hash;

    /**
     * @throws PasswordIsInsecureException
     */
    public function __construct(
        private readonly UserOutsideInterface $outside,
        string $value,
    ) {
        if (!$this->outside->isPasswordSecure($value)) {
            throw new PasswordIsInsecureException();
        }
        $this->hash = $this->outside->hashPassword($value);
    }

    /**
     * @throws WrongPasswordException
     */
    public function verify(string $password): void
    {
        if (!$this->outside->isPasswordValid($password, $this->hash)) {
            throw new WrongPasswordException();
        }
    }

    /**
     * @throws PasswordIsInsecureException
     * @throws PasswordMismatchException
     * @throws WrongPasswordException
     */
    public function change(string $oldValue, string $newValue, string $newValueConfirmation): self
    {
        $this->verify($oldValue);
        $this->assertNoMismatch($newValue, $newValueConfirmation);
        return new self($this->outside, $newValue);
    }

    public function toString(): string
    {
        return $this->hash;
    }

    public function reset(string $newValue, string $newValueConfirmation): self
    {
        $this->assertNoMismatch($newValue, $newValueConfirmation);
        return new self($this->outside, $newValue);
    }

    /**
     * @throws PasswordMismatchException
     */
    private function assertNoMismatch(string $value, string $valueConfirmation): void
    {
        if ($value !== $valueConfirmation) {
            throw new PasswordMismatchException();
        }
    }
}
