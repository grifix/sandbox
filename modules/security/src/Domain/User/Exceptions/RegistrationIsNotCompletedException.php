<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RegistrationIsNotCompletedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Registration is not completed!',
            SecurityErrorCodes::registrationIsNotCompleted->value,
        );
    }
}
