<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Grifix\Date\DateTime\DateTime;
use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserIsSuspendedException extends \Exception
{
    public function __construct(DateTime $resumeDate)
    {
        parent::__construct(
            sprintf('User is suspended until [%s]!', $resumeDate->toAtom()),
            SecurityErrorCodes::userIsSuspended->value,
        );
    }
}
