<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class EmailChangeIsNotInitiatedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Email change is not initiated!',
            SecurityErrorCodes::emailChangeIsNotInitiated->value,
        );
    }
}
