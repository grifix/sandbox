<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserIsDisabledException extends \Exception
{

    public function __construct()
    {
        parent::__construct(
            'User is disabled!',
            SecurityErrorCodes::userIsDisabled->value,
        );
    }
}
