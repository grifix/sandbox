<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RoleAlreadyAssignedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Role already assigned!',
            SecurityErrorCodes::roleAlreadyAssigned->value,
        );
    }
}
