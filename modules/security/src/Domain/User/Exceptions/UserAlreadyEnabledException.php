<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserAlreadyEnabledException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'User is already enabled!',
            SecurityErrorCodes::userAlreadyEnabled->value,
        );
    }
}
