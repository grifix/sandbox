<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Email;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class UserEmailIsNotUniqueException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Email is not unique!',
            SecurityErrorCodes::userEmailIsNotUnique->value,
        );
    }
}
