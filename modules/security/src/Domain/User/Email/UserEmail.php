<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User\Email;

use Grifix\Email\Email;
use Modules\Security\Domain\User\UserOutsideInterface;

final readonly class UserEmail
{
    /**
     * @throws UserEmailIsNotUniqueException
     */
    public function __construct(
        private UserOutsideInterface $outside,
        private Email $value,
    ) {
        if ($this->outside->doesUserWithEmailExist($this->value)) {
            throw new UserEmailIsNotUniqueException();
        }
    }

    /**
     * @throws UserEmailIsNotUniqueException
     */
    public function change(Email $newEmail): self
    {
        return new self($this->outside, $newEmail);
    }

    /**
     * @return Email
     */
    public function getValue(): Email
    {
        return $this->value;
    }
}
