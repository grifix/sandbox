<?php

declare(strict_types=1);

namespace Modules\Security\Domain\User;

use Grifix\Date\DateInterval\DateInterval;
use Grifix\Date\DateInterval\Exceptions\InvalidDateIntervalException;
use Grifix\Date\DateTime\DateTime;
use Grifix\Email\Email;
use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\User\Email\UserEmail;
use Modules\Security\Domain\User\Email\UserEmailIsNotUniqueException;
use Modules\Security\Domain\User\Events\UserAuthenticatedEvent;
use Modules\Security\Domain\User\Events\UserAuthenticationFailedEvent;
use Modules\Security\Domain\User\Events\UserAuthorizedEvent;
use Modules\Security\Domain\User\Events\UserCreatedEvent;
use Modules\Security\Domain\User\Events\UserDisabledEvent;
use Modules\Security\Domain\User\Events\UserEmailChangeConfirmedEvent;
use Modules\Security\Domain\User\Events\UserEmailChangeInitiatedEvent;
use Modules\Security\Domain\User\Events\UserEnabledEvent;
use Modules\Security\Domain\User\Events\UserPasswordChangedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetCompletedEvent;
use Modules\Security\Domain\User\Events\UserPasswordResetInitiatedEvent;
use Modules\Security\Domain\User\Events\UserRegistrationCompletedEvent;
use Modules\Security\Domain\User\Events\UserResumedEvent;
use Modules\Security\Domain\User\Events\UserRoleAssignedEvent;
use Modules\Security\Domain\User\Events\UserRoleUnAssignedEvent;
use Modules\Security\Domain\User\Events\UserSuspendedEvent;
use Modules\Security\Domain\User\Exceptions\EmailChangeIsNotInitiatedException;
use Modules\Security\Domain\User\Exceptions\PasswordResetIsNotInitiatedException;
use Modules\Security\Domain\User\Exceptions\RegistrationIsAlreadyCompletedException;
use Modules\Security\Domain\User\Exceptions\RegistrationIsNotCompletedException;
use Modules\Security\Domain\User\Exceptions\RoleAlreadyAssignedException;
use Modules\Security\Domain\User\Exceptions\UserAlreadyEnabledException;
use Modules\Security\Domain\User\Exceptions\UserAuthorizationFailedException;
use Modules\Security\Domain\User\Exceptions\UserHasNoRoleException;
use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;
use Modules\Security\Domain\User\Exceptions\UserIsNotSuspendedException;
use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordIsInsecureException;
use Modules\Security\Domain\User\Password\Exceptions\PasswordMismatchException;
use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;
use Modules\Security\Domain\User\Password\UserPassword;

final class User
{
    private const MAX_AUTHENTICATION_FAILS = 3;

    private UserPassword $password;

    private UserEmail $email;

    private ?UserEmail $newEmail = null;
    private bool $registrationCompleted = false;

    private int $authenticationFails = 0;

    private bool $enabled = true;

    private ?DateTime $resumeDate = null;

    private bool $passwordResetInitiated = false;

    /**
     * @var Uuid[]
     */
    private array $roles = [];

    private DateTime $createdAt;

    /**
     * @throws PasswordIsInsecureException
     * @throws UserEmailIsNotUniqueException
     */
    public function __construct(
        private readonly UserOutsideInterface $outside,
        private readonly Uuid $id,
        Email $email,
        string $password,
    ) {
        $this->password = new UserPassword($this->outside, $password);
        $this->email = new UserEmail($this->outside, $email);
        $this->createdAt = $this->outside->getCurrentDateTime();
        $this->outside->publishEvent(
            new UserCreatedEvent(
                $this->id,
                $this->email->getValue(),
                $this->password->toString(),
                $this->createdAt,
            ),
        );
    }

    /**
     * @throws RegistrationIsAlreadyCompletedException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     */
    public function completeRegistration(): void
    {
        if ($this->registrationCompleted) {
            throw new RegistrationIsAlreadyCompletedException();
        }
        $this->assertIsNotSuspended();
        $this->assertIsEnabled();
        $this->registrationCompleted = true;
        $this->outside->publishEvent(
            new UserRegistrationCompletedEvent(
                $this->id,
            ),
        );
    }

    /**
     * @throws UserEmailIsNotUniqueException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     * @throws WrongPasswordException
     * @throws RegistrationIsNotCompletedException
     */
    public function initiateEmailChange(Email $newEmail, string $password): void
    {
        $this->assertRegistrationIsCompleted();
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        $this->password->verify($password);
        $this->newEmail = new UserEmail($this->outside, $newEmail);
        $this->outside->publishEvent(
            new UserEmailChangeInitiatedEvent(
                $this->id,
                $this->newEmail->getValue(),
            ),
        );
    }

    /**
     * @throws UserEmailIsNotUniqueException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     * @throws EmailChangeIsNotInitiatedException
     */
    public function confirmEmailChange(): void
    {
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        if (null === $this->newEmail) {
            throw new EmailChangeIsNotInitiatedException();
        }
        $this->email = $this->email->change($this->newEmail->getValue());
        $this->outside->publishEvent(
            new UserEmailChangeConfirmedEvent(
                $this->id,
                $this->email->getValue(),
            ),
        );
    }

    /**
     * @throws PasswordIsInsecureException
     * @throws PasswordMismatchException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     * @throws WrongPasswordException
     */
    public function changePassword(string $currentPassword, string $newPassword, string $newPasswordConfirmation): void
    {
        $this->assertRegistrationIsCompleted();
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        $this->password = $this->password->change($currentPassword, $newPassword, $newPasswordConfirmation);
        $this->outside->publishEvent(new UserPasswordChangedEvent($this->id, $this->password->toString()));
    }

    /**
     * @throws UserIsDisabledException
     */
    public function disable(): void
    {
        $this->assertIsEnabled();
        $this->enabled = false;
        $this->outside->publishEvent(new UserDisabledEvent($this->id));
    }

    /**
     * @throws UserAlreadyEnabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     */
    public function enable(): void
    {
        $this->assertIsNotSuspended();
        if ($this->enabled) {
            throw new UserAlreadyEnabledException();
        }
        $this->enabled = true;
        $this->outside->publishEvent(new UserEnabledEvent($this->id));
    }

    /**
     * @throws InvalidDateIntervalException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     * @throws WrongPasswordException
     */
    public function authenticate(string $password, ?IpAddress $ip): void
    {
        $this->assertIsNotSuspended();
        $this->assertIsEnabled();
        try {
            $this->password->verify($password);
        } catch (WrongPasswordException $e) {
            $this->authenticationFails++;
            $this->outside->publishEvent(
                new UserAuthenticationFailedEvent(
                    $this->id,
                    $this->authenticationFails,
                    $ip,
                ),
            );
            if ($this->authenticationFails > self::MAX_AUTHENTICATION_FAILS) {
                $this->suspend(DateInterval::create(minutes: 3));
            }
            throw $e;
        }
        $this->authenticationFails = 0;

        $this->outside->publishEvent(
            new UserAuthenticatedEvent(
                $this->id,
                $ip,
            ),
        );
    }

    /**
     * @return void
     * @throws UserIsDisabledException
     */
    private function assertIsEnabled(): void
    {
        if (!$this->enabled) {
            throw new UserIsDisabledException();
        }
    }

    /**
     * @return void
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     */
    private function assertIsNotSuspended(): void
    {
        if (null === $this->resumeDate) {
            return;
        }

        if ($this->resumeDate->isBefore($this->outside->getCurrentDateTime())) {
            $this->resume();
            return;
        }
        throw new UserIsSuspendedException($this->resumeDate);
    }

    /**
     * @param DateInterval $period
     * @return void
     */
    public function suspend(DateInterval $period): void
    {
        $this->resumeDate = $this->outside->getCurrentDateTime()->add($period);

        $this->outside->publishEvent(
            new UserSuspendedEvent(
                $this->id,
                $this->resumeDate,
            ),
        );
    }

    /**
     * @return void
     * @throws UserIsNotSuspendedException
     */
    public function resume(): void
    {
        if (null === $this->resumeDate) {
            throw new UserIsNotSuspendedException();
        }
        $this->resumeDate = null;
        $this->outside->publishEvent(new UserResumedEvent($this->id));
    }

    /**
     * @throws RoleAlreadyAssignedException
     */
    public function assignRole(Uuid $roleId): void
    {
        if ($this->hasRole($roleId)) {
            throw new RoleAlreadyAssignedException();
        }
        $this->roles[$roleId->toString()] = $roleId;
        $this->outside->publishEvent(
            new UserRoleAssignedEvent(
                $this->id,
                $roleId,
            ),
        );
    }

    public function unAssignRole(Uuid $roleId): void
    {
        if (!$this->hasRole($roleId)) {
            throw new UserHasNoRoleException();
        }
        unset($this->roles[$roleId->toString()]);
        $this->outside->publishEvent(
            new UserRoleUnAssignedEvent(
                $this->id,
                $roleId,
            ),
        );
    }

    private function hasRole(Uuid $roleId): bool
    {
        return array_key_exists($roleId->toString(), $this->roles);
    }

    /**
     * @throws UserAuthorizationFailedException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     */
    public function authorize(string $permission, ?IpAddress $ip): void
    {
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        foreach ($this->roles as $roleId) {
            if ($this->outside->hasRoleAccess($roleId, $permission)) {
                $this->outside->publishEvent(
                    new UserAuthorizedEvent(
                        $this->id,
                        $roleId,
                        $permission,
                        $ip,
                    ),
                );
                return;
            }
        }
        throw new UserAuthorizationFailedException();
    }

    public function initiatePasswordReset(): void
    {
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        $this->passwordResetInitiated = true;
        $this->outside->publishEvent(
            new UserPasswordResetInitiatedEvent(
                $this->id,
            ),
        );
    }

    /**
     * @throws PasswordResetIsNotInitiatedException
     * @throws UserIsDisabledException
     * @throws UserIsNotSuspendedException
     * @throws UserIsSuspendedException
     */
    public function completePasswordReset(
        string $newPassword,
        string $newPasswordConfirmation,
        ?IpAddress $ip,
    ): void {
        if (!$this->passwordResetInitiated) {
            throw new PasswordResetIsNotInitiatedException();
        }
        $this->assertIsEnabled();
        $this->assertIsNotSuspended();
        $this->password = $this->password->reset($newPassword, $newPasswordConfirmation);
        $this->passwordResetInitiated = false;
        $this->outside->publishEvent(
            new UserPasswordResetCompletedEvent(
                $this->id,
                $ip,
                $this->password->toString(),
            ),
        );
    }

    /**
     * @throws RegistrationIsNotCompletedException
     */
    private function assertRegistrationIsCompleted(): void
    {
        if (!$this->registrationCompleted) {
            throw new RegistrationIsNotCompletedException();
        }
    }
}
