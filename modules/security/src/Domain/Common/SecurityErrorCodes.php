<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Common;

enum SecurityErrorCodes: int
{
    case emailChangeIsNotInitiated = 1;
    case passwordResetIsNotInitiated = 2;
    case registrationIsAlreadyCompleted = 3;
    case registrationIsNotCompleted = 4;
    case roleAlreadyAssigned = 5;
    case userAlreadyEnabled = 6;
    case userAuthorizationFailed = 7;
    case userHasNoRole = 8;
    case userIsDisabled = 9;
    case userIsNotSuspended = 10;
    case userIsSuspended = 11;
    case passwordIsInsecure = 12;
    case passwordMismatch = 13;
    case wrongPassword = 14;
    case filterByPasswordIsNotSupported = 15;
    case userDoesNotExist = 16;
    case userEmailIsNotUnique = 17;
    case permissionAlreadyGranted = 18;
    case permissionNotGranted = 19;
    case roleWithSameNameAlreadyExists = 20;
    case refreshAccessSessionExpired = 21;
    case refreshAccessSessionRevoked = 22;
    case refreshAccessSessionDoesNotExist = 23;
    case multipleRefreshAccessSessionsFound = 24;
    case roleDoesNotExist = 25;
    case invalidAccessToken = 26;
    case expiredAccessToken = 27;
    case expiredRefreshToken = 28;
    case invalidRefreshToken = 29;
    case invalidEmailChangeConfirmationToken = 30;
    case expiredEmailChangeConfirmationToken = 31;
    case invalidPasswordResetToken = 32;
    case expiredPasswordResetToken = 33;

    case invalidRegistrationConfirmationToken = 34;
    case expiredRegistrationConfirmationToken = 35;
}
