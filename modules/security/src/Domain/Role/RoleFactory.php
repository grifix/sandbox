<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role;

use Grifix\Uuid\Uuid;

final class RoleFactory
{

    public function __construct(private readonly RoleOutsideInterface $roleOutside)
    {
    }

    public function createRole(Uuid $id, string $name): Role
    {
        return new Role($this->roleOutside, $id, $name);
    }
}
