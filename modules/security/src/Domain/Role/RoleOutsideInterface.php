<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role;

use Modules\Security\Domain\Role\Events\RoleEventInterface;

interface RoleOutsideInterface
{
    public function doesRoleWithSameNameExist(string $name): bool;

    public function publishEvent(RoleEventInterface $event): void;
}
