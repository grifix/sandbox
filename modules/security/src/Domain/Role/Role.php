<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role;

use Grifix\Date\DateTime\DateTime;
use Grifix\Uuid\Uuid;
use Modules\Security\Domain\Role\Events\RoleCreatedEvent;
use Modules\Security\Domain\Role\Events\RolePermissionGrantedEvent;
use Modules\Security\Domain\Role\Events\RolePermissionRevokedEvent;
use Modules\Security\Domain\Role\Exceptions\PermissionAlreadyGrantedException;
use Modules\Security\Domain\Role\Exceptions\PermissionNotGrantedException;
use Modules\Security\Domain\Role\Exceptions\RoleWithSameNameAlreadyExistsException;

final class Role
{
    /**
     * @var string[]
     */
    private array $permissions = [];
    /**
     * @throws RoleWithSameNameAlreadyExistsException
     */
    public function __construct(
        private readonly RoleOutsideInterface $outside,
        private readonly Uuid $id,
        private readonly string $name,
    ) {
        $this->assertRoleWithSameNameDoesNotExists($name);
        $this->outside->publishEvent(
            new RoleCreatedEvent(
                $this->id,
                $this->name,
            ),
        );
    }

    /**
     * @throws PermissionAlreadyGrantedException
     */
    public function grantPermission(string $permission): void
    {
        $this->assertPermissionIsNotGranted($permission);
        $this->permissions[] = $permission;
        $this->outside->publishEvent(
            new RolePermissionGrantedEvent(
                $this->id,
                $permission,
            ),
        );
    }

    /**
     * @throws PermissionNotGrantedException
     */
    public function revokePermission(string $permission): void
    {
        $this->assertPermissionGranted($permission);
        unset($this->permissions[$permission]);
        $this->outside->publishEvent(
            new RolePermissionRevokedEvent(
                $this->id,
                $permission,
            ),
        );
    }

    private function isPermissionGranted(string $permission): bool
    {
        return in_array($permission, $this->permissions);
    }

    /**
     * @throws PermissionAlreadyGrantedException
     */
    private function assertPermissionIsNotGranted(string $permission): void
    {
        if ($this->isPermissionGranted($permission)) {
            throw new PermissionAlreadyGrantedException();
        }
    }

    /**
     * @throws PermissionNotGrantedException
     */
    private function assertPermissionGranted(string $permission): void
    {
        if (!$this->isPermissionGranted($permission)) {
            throw new PermissionNotGrantedException();
        }
    }

    private function assertRoleWithSameNameDoesNotExists(string $name): void
    {
        if ($this->outside->doesRoleWithSameNameExist($name)) {
            throw new RoleWithSameNameAlreadyExistsException();
        }
    }
}
