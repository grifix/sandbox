<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class RoleWithSameNameAlreadyExistsException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Role with the same name already exists!',
            SecurityErrorCodes::roleWithSameNameAlreadyExists->value,
        );
    }
}
