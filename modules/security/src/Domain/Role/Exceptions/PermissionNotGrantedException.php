<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class PermissionNotGrantedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Permission is not granted!',
            SecurityErrorCodes::permissionNotGranted->value,
        );
    }
}
