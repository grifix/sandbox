<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Exceptions;

use Modules\Security\Domain\Common\SecurityErrorCodes;

final class PermissionAlreadyGrantedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Permission is already granted!',
            SecurityErrorCodes::permissionAlreadyGranted->value,
        );
    }
}
