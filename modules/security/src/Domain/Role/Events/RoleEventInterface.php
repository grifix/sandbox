<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Events;

use Grifix\Uuid\Uuid;

interface RoleEventInterface
{
    public function getRoleId(): Uuid;
}
