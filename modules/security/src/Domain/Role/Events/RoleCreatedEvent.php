<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Events;

use Grifix\Uuid\Uuid;

final readonly class RoleCreatedEvent implements RoleEventInterface
{

    public function __construct(
        public Uuid $roleId,
        public string $name
    ) {
    }

    public function getRoleId(): Uuid
    {
        return $this->roleId;
    }

}
