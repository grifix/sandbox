<?php

declare(strict_types=1);

namespace Modules\Security\Domain\Role\Events;

use Grifix\Uuid\Uuid;

final class RolePermissionRevokedEvent implements RoleEventInterface
{

    public function __construct(
        public readonly Uuid $roleId,
        public readonly string $permission
    ) {
    }

    public function getRoleId(): Uuid
    {
        return $this->roleId;
    }
}
