<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Events\ProductCreatedEvent;
use Modules\Catalog\Domain\Product\Events\ProductDeletedEvent;
use Modules\Catalog\Domain\Product\Events\ProductNameChangedEvent;
use Modules\Catalog\Domain\Product\Events\ProductPriceChangedEvent;
use Modules\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use Modules\Catalog\Domain\Product\Name\ProductName;

final class Product
{
    private ProductName $name;

    private bool $deleted = false;

    public function __construct(
        private readonly ProductOutsideInterface $outside,
        private readonly Uuid $id,
        string $name,
        private Money $price
    ) {
        $this->name = new ProductName($this->outside, $name);
        $this->outside->publishEvent(
            new ProductCreatedEvent(
                $this->id,
                $this->price,
                $this->name->toString()
            )
        );
    }

    public function changeName(string $newName): void
    {
        $this->assertIsNotDeleted();
        if ($newName === $this->name->toString()) {
            return;
        }
        $this->name = $this->name->change($newName);
        $this->outside->publishEvent(
            new ProductNameChangedEvent(
                $this->id,
                $newName
            )
        );
    }

    public function changePrice(Money $newPrice): void
    {
        $this->assertIsNotDeleted();
        $this->price = $newPrice;
        $this->outside->publishEvent(
            new ProductPriceChangedEvent(
                $this->id,
                $newPrice
            )
        );
    }

    public function delete(): void
    {
        $this->assertIsNotDeleted();
        $this->deleted = true;
        $this->outside->publishEvent(new ProductDeletedEvent($this->id));
    }

    private function assertIsNotDeleted(): void
    {
        if ($this->deleted) {
            throw new ProductDeletedException($this->id);
        }
    }
}
