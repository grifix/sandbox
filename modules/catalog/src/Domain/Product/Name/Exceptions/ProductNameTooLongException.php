<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Name\Exceptions;

final class ProductNameTooLongException extends \Exception
{
    public function __construct(int $minChars)
    {
        parent::__construct(sprintf('Product name cannot have more than [%s] characters!', $minChars));
    }
}
