<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Name\Exceptions;

final class ProductNameIsNotUniqueException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Product name is not unique!');
    }
}
