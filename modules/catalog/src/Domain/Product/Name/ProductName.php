<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Name;

use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameIsNotUniqueException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooLongException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooShortException;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductName
{
    public function __construct(
        private readonly ProductOutsideInterface $outside,
        private readonly string $value
    ) {
        $this->assertMinChars();
        $this->assertMaxChars();
        $this->assertIsUnique();
    }

    public function change(string $value): self
    {
        return new self($this->outside, $value);
    }

    private function assertMinChars(): void
    {
        $minChars = $this->outside->getProductNameMinChars();
        if (mb_strlen($this->value) < $minChars) {
            throw new ProductNameTooShortException($minChars);
        }
    }

    private function assertMaxChars(): void
    {
        $maxChars = $this->outside->getProductNameMaxChars();
        if (mb_strlen($this->value) > $maxChars) {
            throw new ProductNameTooLongException($maxChars);
        }
    }

    public function assertIsUnique(): void
    {
        if (!$this->outside->isProductNameUnique($this->value)) {
            throw new ProductNameIsNotUniqueException();
        }
    }

    public function toString(): string
    {
        return $this->value;
    }
}
