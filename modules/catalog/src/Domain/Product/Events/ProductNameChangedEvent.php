<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Events;

use Grifix\Uuid\Uuid;

final class ProductNameChangedEvent implements ProductEventInterface
{

    public function __construct(
        public readonly Uuid $productId,
        public readonly string $newName
    )
    {
    }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
