<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Events;

use Grifix\Uuid\Uuid;

final class ProductDeletedEvent implements ProductEventInterface
{
    public function __construct(public readonly Uuid $productId)
    {
    }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
