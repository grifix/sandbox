<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Events;

use Grifix\Uuid\Uuid;

interface ProductEventInterface
{
    public function getProductId(): Uuid;
}
