<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Events;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductCreatedEvent implements ProductEventInterface
{

    public function __construct(
        public readonly Uuid $productId,
        public readonly Money $price,
        public readonly string $name
    ) {
    }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
