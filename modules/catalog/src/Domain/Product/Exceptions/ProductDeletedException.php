<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Exceptions;

use Grifix\Uuid\Uuid;

final class ProductDeletedException extends \Exception
{
    public function __construct(Uuid $productId)
    {
        parent::__construct(sprintf('Product with id [%s] has been deleted!', $productId->toString()));
    }
}
