<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product;


use Modules\Catalog\Domain\Product\Events\ProductEventInterface;

interface ProductOutsideInterface
{
    public function publishEvent(ProductEventInterface $event);

    public function getProductNameMaxChars(): int;

    public function getProductNameMinChars(): int;

    public function isProductNameUnique(string $name): bool;
}
