<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductFactory
{
    public function __construct(private readonly ProductOutsideInterface $outside)
    {
    }

    public function createProduct(Uuid $id, string $name, Money $price): Product
    {
        return new Product(
            $this->outside,
            $id,
            $name,
            $price
        );
    }
}
