<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui;

use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Modules\Catalog\Application\Product\Ports\Projector\Exceptions\ProductDoesNotExistException;
use Modules\Catalog\Application\Product\Ports\Repository\Exceptions\ProductDoesNotExistException as ProductAggregateDoesNotExistException;

final class CatalogExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(ProductDoesNotExistException::class, httpCode:404),
            ExceptionConverter::create(ProductAggregateDoesNotExistException::class, httpCode:404),
            ExceptionConverter::create('Modules\Catalog\Domain', httpCode:400),
        ];
    }
}
