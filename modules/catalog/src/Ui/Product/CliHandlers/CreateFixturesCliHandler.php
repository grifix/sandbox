<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\CliHandlers;

use Grifix\Test\Fixture\FixtureFactory;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'sandbox:catalog:fixtures')]
final class CreateFixturesCliHandler extends Command
{
    public function __construct(
        private readonly FixtureFactory $fixtureFactory,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Create catalog fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 0; $i < 20; $i++) {
            $this->fixtureFactory->createFixture(ProductFixture::class)->build();
        }
        return self::SUCCESS;
    }
}
