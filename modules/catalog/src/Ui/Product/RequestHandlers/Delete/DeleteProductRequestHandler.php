<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Delete;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\Delete\DeleteProductCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DeleteProductRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/catalog/products/{productId}',
        methods: ['DELETE'],
    )]
    public function __invoke(string $productId): Response
    {
        $this->application->tell(
            new DeleteProductCommand(Uuid::createFromString($productId))
        );

        return new JsonResponse();
    }
}
