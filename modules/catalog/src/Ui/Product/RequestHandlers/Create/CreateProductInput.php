<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Create;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\MoneyInputType;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Grifix\Money\Money\Money;
use Symfony\Component\Validator\Constraints\Collection;

final class CreateProductInput extends AbstractInput
{
    public function getName(): string
    {
        return $this->getValue('name', StringInputType::class)->toString();
    }

    public function getPrice(): Money
    {
        return $this->getValue('price', MoneyInputType::class)->toMoney();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'name' => StringInputType::createConstraint(),
                'price' => MoneyInputType::createConstraint(),
            ]
        ]);
    }
}
