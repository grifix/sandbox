<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Create;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\Create\CreateProductCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class CreateProductRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/catalog/products',
        methods: ['POST'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromBody($request, CreateProductInput::class);
        $productId = Uuid::createRandom();
        $this->application->tell(
            new CreateProductCommand(
                $productId,
                $input->getName(),
                $input->getPrice()
            )
        );

        return new JsonResponse(
            [
                'productId' => $productId->toString()
            ],
            Response::HTTP_CREATED
        );
    }
}
