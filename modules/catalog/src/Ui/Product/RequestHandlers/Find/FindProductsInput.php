<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Find;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\PositiveIntType;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;

final class FindProductsInput extends AbstractInput
{
    public function getPageNumber(): int
    {
        if (!$this->hasValue('pageNumber')) {
            return 1;
        }
        return $this->getValue('pageNumber', PositiveIntType::class)->toInt();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'fields' => [
                'pageNumber' => new Optional(
                    PositiveIntType::createConstraint()
                )
            ]
        ]);
    }
}
