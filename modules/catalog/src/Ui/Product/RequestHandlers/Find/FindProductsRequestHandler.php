<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Queries\Find\FindProductsQuery;
use Sandbox\Catalog\Dtos\ProductDto;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class FindProductsRequestHandler extends AbstractRequestHandler
{
    private const PRODUCTS_ON_PAGE = 3;

    #[Route(
        '/api/v1/catalog/products',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        $input = $this->createInputFromQueryString($request, FindProductsInput::class);

        $queryResult = $this->application->ask(
            new FindProductsQuery(
                ProductFilter::create()
                    ->setLimit(self::PRODUCTS_ON_PAGE)
                    ->setOffset(self::PRODUCTS_ON_PAGE * $input->getPageNumber() - self::PRODUCTS_ON_PAGE)
                    ->setSortBy('name')
            )
        );

        return new JsonResponse([
            'products' => $this->prepareProducts($queryResult->products)
        ]);
    }

    /**
     * @param ProductDto[] $products
     */
    private function prepareProducts(array $products): array
    {
        $result = [];
        foreach ($products as $product) {
            $result[] = [
                'name' => $product->name,
                'price' => [
                    'amount' => $product->price->getAmount(),
                    'currency' => $product->price->getCurrency()->getCode()
                ]
            ];
        }
        return $result;
    }
}
