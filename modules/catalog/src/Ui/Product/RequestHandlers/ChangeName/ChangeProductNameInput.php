<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\ChangeName;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\StringInputType;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

final class ChangeProductNameInput extends AbstractInput
{
    public function getName(): string
    {
        return $this->getValue('name', StringInputType::class)->toString();
    }

    public static function createConstraint(): Collection
    {
        return new Collection(
            [
                'allowExtraFields' => false,
                'allowMissingFields' => false,
                'fields' => [
                    'name' => new Required([
                        new NotBlank(),
                        new Type('string'),
                    ]),
                ],
            ],
        );
    }
}
