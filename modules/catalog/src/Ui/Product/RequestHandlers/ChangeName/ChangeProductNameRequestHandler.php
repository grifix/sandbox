<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\ChangeName;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\ChangeName\ChangeProductNameCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ChangeProductNameRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/catalog/products/{productId}/name',
        methods: ['POST'],
    )]
    public function __invoke(string $productId, Request $request): Response
    {
        $input = $this->createInputFromBody($request, ChangeProductNameInput::class);
        $this->application->tell(
            new ChangeProductNameCommand(
                Uuid::createFromString($productId),
                $input->getName()
            )
        );

        return new JsonResponse();
    }
}
