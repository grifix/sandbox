<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\ChangePrice;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\MoneyInputType;
use Grifix\Money\Money\Money;
use Symfony\Component\Validator\Constraints\Collection;

final class ChangeProductPriceInput extends AbstractInput
{
    public function getPrice(): Money
    {
        return $this->getValue('price', MoneyInputType::class)->toMoney();
    }

    public static function createConstraint(): Collection
    {
        return new Collection([
            'allowExtraFields' => false,
            'allowMissingFields' => false,
            'fields' => [
                'price' => MoneyInputType::createConstraint()
            ]
        ]);
    }
}
