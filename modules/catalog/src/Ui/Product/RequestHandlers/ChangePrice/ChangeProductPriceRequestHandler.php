<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\ChangePrice;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\ChangePrice\ChangeProductPriceCommand;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ChangeProductPriceRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/catalog/products/{productId}/price',
        methods: ['POST'],
    )]
    public function __invoke(string $productId, Request $request): Response
    {
        $input = $this->createInputFromBody($request, ChangeProductPriceInput::class);
        $this->application->tell(
            new ChangeProductPriceCommand(
                Uuid::createFromString($productId),
                $input->getPrice()
            )
        );

        return new JsonResponse();
    }
}
