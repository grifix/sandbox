<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\Exceptions\ProductDoesNotExistException;
use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;

final class ProductProjector implements ProductProjectorInterface
{
    private const TABLE = 'catalog.product_projections';

    public function __construct(private readonly Connection $connection)
    {
    }


    public function find(ProductFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $this->createDto($row);
        }
        return $result;
    }

    public function getOne(ProductFilter $filter): ProductDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            throw new ProductDoesNotExistException();
        }
        return $this->createDto($row);
    }

    public function findOne(ProductFilter $filter): ?ProductDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            return null;
        }
        return $this->createDto($row);
    }

    public function has(ProductFilter $filter): bool
    {
        return false !== $this->createQueryBuilder($filter)->fetchOne();
    }

    public function count(ProductFilter $filter): int
    {
        return $this->createQueryBuilder($filter)->executeQuery()->rowCount();
    }

    public function createProduct(ProductDto $product): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $product->id->toString(),
                'name' => $product->name,
                'price_amount' => $product->price->getAmount(),
                'price_currency' => $product->price->getCurrency()->getCode()
            ]
        );
    }

    public function delete(Uuid $productId): void
    {
        $this->connection->delete(self::TABLE, ['id' => $productId->toString()]);
    }

    public function changeName(Uuid $productId, string $newName): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'name' => $newName
            ],
            [
                'id' => $productId->toString(),
            ]
        );
    }

    public function changePrice(Uuid $productId, Money $newPrice): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'price_amount' => $newPrice->getAmount(),
                'price_currency' => $newPrice->getCurrency()->getCode()
            ],
            [
                'id' => $productId->toString()
            ]
        );
    }

    public function clear(): void
    {
        $this->connection->executeQuery(sprintf('truncate table %s', self::TABLE));
    }

    private function createQueryBuilder(ProductFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('*')->from(self::TABLE);

        if ($filter->getProductId()) {
            $result->andWhere('id = :id')->setParameter('id', $filter->getProductId()->toString());
        }

        if ($filter->getName()) {
            $result->andWhere('name = :name')->setParameter('name', $filter->getName());
        }

        if ($filter->getNotProductId()) {
            $result->andWhere('id != :not_id')->setParameter('not_id', $filter->getNotProductId());
        }

        if (null !== $filter->getOffset()) {
            $result->setFirstResult($filter->getOffset());
        }

        if (null !== $filter->getLimit()) {
            $result->setMaxResults($filter->getLimit());
        }

        if (null !== $filter->getSortBy()) {
            $result->orderBy($filter->getSortBy(), $filter->getSortDirection() ?? 'asc');
        }

        return $result;
    }

    private function createDto(array $row): ProductDto
    {
        return new ProductDto(
            Uuid::createFromString($row['id']),
            $row['name'],
            Money::withCurrency($row['price_amount'], $row['price_currency'])
        );
    }
}
