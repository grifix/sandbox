<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Repository\Exceptions\ProductDoesNotExistException;
use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;
use Modules\Catalog\Domain\Product\Product;

final class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{

    public function add(Product $product): void
    {
        $this->doAdd($product);
    }

    public function get(Uuid $id): Product
    {
        return $this->doGet($id, ProductDoesNotExistException::class, Product::class);
    }
}
