<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product;

use Grifix\Framework\Application\MessageBusInterface;
use Modules\Catalog\Domain\Product\Events\ProductCreatedEvent;
use Sandbox\Catalog\ChannelId;
use Sandbox\Catalog\Messages\ProductCreatedEventMessage;

final class ProductMessageSender
{
    public function __construct(private readonly MessageBusInterface $messageBus)
    {
    }

    public function onProductCreated(ProductCreatedEvent $event): void
    {
        $this->messageBus->sendMessage(
            new ProductCreatedEventMessage(
                $event->productId,
                $event->name,
                $event->price,
            ),
            ChannelId::get(),
            $event->productId,
        );
    }
}
