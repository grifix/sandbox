<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product\Domain;

use Grifix\EventStore\EventStoreInterface;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;
use Modules\Catalog\Domain\Product\Events\ProductEventInterface;
use Modules\Catalog\Domain\Product\Product;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductOutside implements ProductOutsideInterface
{

    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly int $productNameMaxChars,
        private readonly int $productNameMinChars,
        private readonly ProductProjectorInterface $productProjector
    ) {
    }

    public function publishEvent(ProductEventInterface $event)
    {
        $this->eventStore->storeEvent($event, Product::class, $event->getProductId());
    }

    public function getProductNameMaxChars(): int
    {
        return $this->productNameMaxChars;
    }

    public function getProductNameMinChars(): int
    {
        return $this->productNameMinChars;
    }

    public function isProductNameUnique(string $name): bool
    {
        return !$this->productProjector->has(ProductFilter::create()->setName($name));
    }
}
