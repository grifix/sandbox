<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Subscribers;

use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;
use Modules\Catalog\Domain\Product\Events\ProductCreatedEvent;
use Modules\Catalog\Domain\Product\Events\ProductDeletedEvent;
use Modules\Catalog\Domain\Product\Events\ProductNameChangedEvent;
use Modules\Catalog\Domain\Product\Events\ProductPriceChangedEvent;

final class ProductProjectorSubscriber
{

    public function __construct(private readonly ProductProjectorInterface $projector)
    {
    }

    public function onProductCreated(ProductCreatedEvent $event): void
    {
        $this->projector->createProduct(
            new ProductDto(
                $event->productId,
                $event->name,
                $event->price
            )
        );
    }

    public function onProductDeleted(ProductDeletedEvent $event): void
    {
        $this->projector->delete($event->productId);
    }

    public function onProductNameChanged(ProductNameChangedEvent $event): void
    {
        $this->projector->changeName($event->productId, $event->newName);
    }

    public function onProductPriceChanged(ProductPriceChangedEvent $event): void
    {
        $this->projector->changePrice($event->productId, $event->newPrice);
    }
}
