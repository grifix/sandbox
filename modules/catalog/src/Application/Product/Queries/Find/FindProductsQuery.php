<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;

final class FindProductsQuery
{
    public function __construct(public readonly ?ProductFilter $filter = null)
    {
    }
}
