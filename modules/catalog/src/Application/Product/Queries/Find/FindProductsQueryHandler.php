<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;

final class FindProductsQueryHandler
{
    public function __construct(private readonly ProductProjectorInterface $projector)
    {
    }

    public function __invoke(FindProductsQuery $query): FindProductsQueryResult
    {
        return new FindProductsQueryResult(
            $this->projector->find($query->filter)
        );
    }
}
