<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;

final class FindProductsQueryResult
{
    /**
     * @param ProductDto[] $products
     */
    public function __construct(public readonly array $products)
    {
    }
}
