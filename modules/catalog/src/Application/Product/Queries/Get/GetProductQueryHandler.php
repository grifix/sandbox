<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Get;

use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;

final class GetProductQueryHandler
{
    public function __construct(private readonly ProductProjectorInterface $productProjector)
    {
    }

    public function __invoke(GetProductQuery $query): GetProductQueryResult
    {
        return new GetProductQueryResult(
            $this->productProjector->getOne($query->filter)
        );
    }
}
