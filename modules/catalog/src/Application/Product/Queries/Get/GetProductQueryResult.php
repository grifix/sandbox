<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Get;

use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;

final class GetProductQueryResult
{
    public function __construct(public readonly ProductDto $product)
    {
    }
}
