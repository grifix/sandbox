<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Get;

use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;

final class GetProductQuery
{
    public function __construct(public readonly ProductFilter $filter)
    {
    }
}
