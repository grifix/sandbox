<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector\Exceptions;

final class ProductDoesNotExistException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Product does not exist!');
    }
}
