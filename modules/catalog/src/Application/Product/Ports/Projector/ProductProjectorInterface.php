<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\Exceptions\ProductDoesNotExistException;

interface ProductProjectorInterface
{
    /**
     * @return ProductDto[]
     */
    public function find(ProductFilter $filter): array;

    /**
     * @throws ProductDoesNotExistException
     */
    public function getOne(ProductFilter $filter): ProductDto;

    public function findOne(ProductFilter $filter): ?ProductDto;

    public function has(ProductFilter $filter): bool;

    public function count(ProductFilter $filter): int;

    public function createProduct(ProductDto $product): void;

    public function delete(Uuid $productId): void;

    public function changeName(Uuid $productId, string $newName): void;

    public function changePrice(Uuid $productId, Money $newPrice): void;

    public function clear(): void;
}
