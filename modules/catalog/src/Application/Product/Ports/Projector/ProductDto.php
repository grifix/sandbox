<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductDto
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $name,
        public readonly Money $price
    ) {
    }
}
