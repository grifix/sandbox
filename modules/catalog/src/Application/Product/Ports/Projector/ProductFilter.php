<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Uuid\Uuid;

final class ProductFilter
{
    private ?Uuid $productId = null;

    private ?Uuid $notProductId = null;

    private ?string $name = null;

    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $sortBy = null;

    private ?string $sortDirection = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getProductId(): ?Uuid
    {
        return $this->productId;
    }

    public function setProductId(?Uuid $productId): self
    {
        $this->productId = $productId;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getNotProductId(): ?Uuid
    {
        return $this->notProductId;
    }

    public function setNotProductId(?Uuid $notProductId): self
    {
        $this->notProductId = $notProductId;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function setSortBy(?string $sortBy): self
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function setSortDirection(?string $sortDirection): self
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }
}
