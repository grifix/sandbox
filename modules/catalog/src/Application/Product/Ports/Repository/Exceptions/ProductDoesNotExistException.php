<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Repository\Exceptions;

use Grifix\Uuid\Uuid;

final class ProductDoesNotExistException extends \Exception
{

    public function __construct(Uuid $productId)
    {
        parent::__construct(sprintf('Product with id [%s] does not exist!', $productId->toString()));
    }
}
