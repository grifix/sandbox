<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Product;
use Modules\Draft\Application\Person\Ports\Projector\Exceptions\PersonDoesNotExistException;

interface ProductRepositoryInterface
{
    public function add(Product $product): void;

    /**
     * @throws PersonDoesNotExistException
     */
    public function get(Uuid $id): Product;
}
