<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Delete;

use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;

final class DeleteProductCommandHandler
{

    public function __construct(private readonly ProductRepositoryInterface $productRepository)
    {
    }

    public function __invoke(DeleteProductCommand $command): void
    {
        $this->productRepository->get($command->productId)->delete();
    }
}
