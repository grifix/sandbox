<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Delete;

use Grifix\Uuid\Uuid;

final class DeleteProductCommand
{
    public function __construct(
        public readonly Uuid $productId
    ) {
    }
}
