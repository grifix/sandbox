<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\ChangeName;

use Grifix\Uuid\Uuid;

final class ChangeProductNameCommand
{

    public function __construct(
        public readonly Uuid $productId,
        public readonly string $newName
    ) {
    }
}
