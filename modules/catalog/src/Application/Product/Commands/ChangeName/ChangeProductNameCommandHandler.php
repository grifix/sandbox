<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\ChangeName;

use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;

final class ChangeProductNameCommandHandler
{
    public function __construct(private readonly ProductRepositoryInterface $productRepository)
    {
    }

    public function __invoke(ChangeProductNameCommand $command): void
    {
        $this->productRepository->get($command->productId)->changeName($command->newName);
    }
}
