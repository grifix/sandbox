<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Create;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class CreateProductCommand
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $name,
        public readonly Money $price

    ) {
    }
}
