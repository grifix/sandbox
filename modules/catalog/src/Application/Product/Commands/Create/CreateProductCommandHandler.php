<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Create;

use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;
use Modules\Catalog\Domain\Product\ProductFactory;

final class CreateProductCommandHandler
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly ProductFactory $productFactory
    ) {
    }

    public function __invoke(CreateProductCommand $command): void
    {
        $this->productRepository->add(
            $this->productFactory->createProduct($command->productId, $command->name, $command->price)
        );
    }
}
