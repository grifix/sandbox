<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\ChangePrice;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ChangeProductPriceCommand
{

    public function __construct(
        public readonly Uuid $productId,
        public readonly Money $newPrice
    ) {
    }
}
