<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\ChangePrice;

use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;

final class ChangeProductPriceCommandHandler
{
    public function __construct(private readonly ProductRepositoryInterface $productRepository)
    {
    }

    public function __invoke(ChangeProductPriceCommand $command): void
    {
        $this->productRepository->get($command->productId)->changePrice($command->newPrice);
    }
}
