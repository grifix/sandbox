<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Framework\Config\ModuleServicesConfigurator;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;
use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;
use Modules\Catalog\Infrastructure\Product\Application\Ports\ProductProjector;
use Modules\Catalog\Infrastructure\Product\Application\Ports\ProductRepository;
use Modules\Catalog\Infrastructure\Product\Domain\ProductOutside;
use Modules\Catalog\Infrastructure\Product\ProductMessageSender;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogFixtureRegistry;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use Modules\Catalog\Ui\CatalogExceptionConverterProvider;

return static function (ContainerConfigurator $configurator) {
    ModuleServicesConfigurator::create($configurator)
        ->configure('catalog', 'Catalog');

    $services = $configurator->services();
    $services->defaults()->autowire()->autoconfigure();

    $services->set(ProductRepositoryInterface::class, ProductRepository::class);
    $services->set(ProductOutsideInterface::class, ProductOutside::class)
        ->args([
            '$productNameMaxChars' => 255,
            '$productNameMinChars' => 5,
        ]);
    $services->set(ProductProjectorInterface::class, ProductProjector::class);
    $services->set(CatalogExceptionConverterProvider::class);
    $services->set(ProductMessageSender::class)->public();

    if ($configurator->env() === 'test') {
        $services->set(CatalogApplicationTestClient::class);
        $services->set(CatalogHttpTestClient::class);
        $services->set(CatalogFixtureRegistry::class);
    }
};
