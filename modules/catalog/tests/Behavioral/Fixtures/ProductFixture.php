<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Fixtures;

use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\Create\CreateProductCommand;
use Modules\Catalog\Application\Product\Commands\Delete\DeleteProductCommand;

final class ProductFixture extends AbstractFixture
{
    private Uuid $id;

    private Money $price;

    private string $name;

    private bool $shouldBeDeleted = false;

    protected function init(): void
    {
        $this->name = uniqid('Product_');
        $this->price = Money::createFromString('100 USD');
        $this->id = Uuid::createRandom();
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setPrice(Money $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function shouldBeDeleted(): self
    {
        $this->shouldBeDeleted = true;
        return $this;
    }

    protected function create(): void
    {
        $this->application->tell(
            new CreateProductCommand(
                $this->id,
                $this->name,
                $this->price,
            ),
        );
    }

    protected function afterCreate(): void
    {
        if ($this->shouldBeDeleted) {
            $this->application->tell(
                new DeleteProductCommand($this->id),
            );
        }
    }
}
