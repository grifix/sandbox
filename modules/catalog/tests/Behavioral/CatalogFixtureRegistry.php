<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral;

use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;

final class CatalogFixtureRegistry
{
    public function __construct(private readonly ObjectRegistryInterface $objectRegistry)
    {
    }

    public function getProductFixture(string $alias): ProductFixture
    {
        return $this->objectRegistry->getObject(ProductFixture::class, $alias);
    }
}
