<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogFixtureRegistry;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class ChangeProductNameHttpContext implements Context
{
    public function __construct(
        private readonly CatalogHttpTestClient $httpClient,
        private readonly CatalogApplicationTestClient $applicationClient,
        private readonly CatalogFixtureRegistry $fixtureRegistry
    ) {
    }

    /**
     * @When /^I change product "([^"]*)" name to "([^"]*)"$/
     */
    public function iChangeProductNameTo(string $productAlias, string $newName)
    {
        $this->httpClient->changeProductName(
            $this->fixtureRegistry->getProductFixture($productAlias)->getId(),
            ['name' => $newName]
        );
    }

    /**
     * @Then /^the product "([^"]*)" name should be "([^"]*)"$/
     */
    public function theProductNameShouldBe(string $productAlias, string $expectedName): void
    {
        Assert::assertEquals($expectedName, $this->applicationClient->getProductByAlias($productAlias)->name);
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^I change non\-existing product name$/
     */
    public function iChangeNonExistingProductNameTo(): void
    {
        $this->httpClient->changeProductName(
            Uuid::createRandom(),
            $this->httpClient->buildChangeProductNamePayload()
        );
    }
}
