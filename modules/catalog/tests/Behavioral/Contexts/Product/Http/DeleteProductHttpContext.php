<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogFixtureRegistry;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class DeleteProductHttpContext implements Context
{
    public function __construct(
        private readonly CatalogHttpTestClient $httpClient,
        private readonly CatalogApplicationTestClient $applicationClient,
        private readonly CatalogFixtureRegistry $fixtureRegisty
    ) {
    }

    /**
     * @Then /^there should be no product "([^"]*)"$/
     */
    public function thereShouldBeNoProduct(string $productAlias): void
    {
        Assert::assertFalse($this->applicationClient->hasProductWithAlias($productAlias));
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^I delete product "([^"]*)"$/
     */
    public function iDeleteProduct(string $productAlias): void
    {
        $this->httpClient->deleteProduct(
            $this->fixtureRegisty->getProductFixture($productAlias)->getId()
        );
    }

    /**
     * @When /^I delete non\-existing product$/
     */
    public function iDeleteNonExistingProduct(): void
    {
        $this->httpClient->deleteProduct(Uuid::createRandom());
    }
}
