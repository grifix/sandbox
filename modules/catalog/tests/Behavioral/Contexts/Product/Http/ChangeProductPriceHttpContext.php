<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogFixtureRegistry;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use PHPUnit\Framework\Assert;

final class ChangeProductPriceHttpContext implements Context
{

    public function __construct(
        private readonly CatalogHttpTestClient $httpClient,
        private readonly CatalogApplicationTestClient $applicationClient,
        private readonly CatalogFixtureRegistry $fixtureRegistry
    ) {
    }

    /**
     * @When /^I change product "([^"]*)" price to "([^"]*)"$/
     */
    public function iChangeProductPriceTo(string $productAlias, string $price): void
    {
        $price = Money::createFromString($price);
        $this->httpClient->changeProductPrice(
            $this->fixtureRegistry->getProductFixture($productAlias)->getId(),
            [
                'price' => [
                    'amount' => (int) $price->getAmount(),
                    'currency' => $price->getCurrency()->getCode()
                ]
            ]
        );
    }

    /**
     * @Then /^the product "([^"]*)" price should be "([^"]*)"$/
     */
    public function theProductPriceShouldBe(string $productAlias, string $expectedPrice): void
    {
        Assert::assertEquals(
            $expectedPrice,
            $this->applicationClient->getProductByAlias($productAlias)->price->toString()
        );
    }

    /**
     * @When /^I change non\-existing product price$/
     */
    public function iChangeNonExistingProductPrice()
    {
        $this->httpClient->changeProductPrice(
            Uuid::createRandom(),
            $this->httpClient->buildChangeProductPricePayload()
        );
    }
}
