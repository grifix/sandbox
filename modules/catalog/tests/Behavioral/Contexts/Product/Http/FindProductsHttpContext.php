<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Grifix\Money\Money\Money;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class FindProductsHttpContext implements Context
{
    public function __construct(
        private readonly CatalogHttpTestClient $httpClient
    ) {
    }

    /**
     * @Given /^the products limit on page is "([^"]*)"$/
     */
    public function theProductLimitOnPageIs(int $limit): void
    {
    }

    /**
     * @When /^I list products from page (\d+)$/
     */
    public function iListProductsFromPage(int $pageNumber): void
    {
        $this->httpClient->findProducts([
            'pageNumber' => $pageNumber
        ]);
    }

    /**
     * @Then /^I should see the following products:$/
     */
    public function iShouldSeeTheFollowingProducts(TableNode $expectedProducts): void
    {
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        $actualProducts = $this->httpClient->getLastResponseJson()->getElement('products');
        Assert::assertCount($expectedProducts->getIterator()->count(), $actualProducts);
        foreach ($expectedProducts as $i => $expectedProduct) {
            $expectedPrice = Money::createFromString($expectedProduct['price']);
            Assert::assertEquals(
                [
                    'name' => $expectedProduct['name'],
                    'price' => [
                        'amount' => $expectedPrice->getAmount(),
                        'currency' => $expectedPrice->getCurrency()->getCode()
                    ]
                ],
                $actualProducts[$i]
            );
        }
    }
}
