<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Test\TestMessageBus;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use PHPUnit\Framework\Assert;
use Sandbox\Catalog\Messages\ProductCreatedEventMessage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final class CreateProductHttpContext implements Context
{
    private Uuid $createdProductId;

    public function __construct(
        private readonly CatalogHttpTestClient $httpClient,
        private readonly CatalogApplicationTestClient $applicationClient,
        private readonly TestMessageBus $messageBus
    ) {
    }

    /**
     * @When /^I create a product:$/
     */
    public function iCreateAProduct(string $product): void
    {
        $product = Yaml::parse($product);
        $price = Money::createFromString($product['price']);
        $response = $this->httpClient->createProduct([
            'name' => $product['name'],
            'price' => [
                'amount' => (int) $price->getAmount(),
                'currency' => $price->getCurrency()->getCode()
            ]
        ]);
        if ($response->getStatusCode() === Response::HTTP_CREATED) {
            $this->createdProductId = Uuid::createFromString(
                $this->httpClient->getLastResponseJson()->getElement('productId')
            );
        }
    }

    /**
     * @Then /^the following product should be created:$/
     */
    public function theFollowingProductShouldBeCreated(string $expectedProduct): void
    {
        $expectedProduct = Yaml::parse($expectedProduct);
        $actualProduct = $this->applicationClient->getProductById($this->createdProductId);
        Assert::assertEquals($this->createdProductId, $actualProduct->id);
        Assert::assertEquals($expectedProduct['name'], $actualProduct->name);
        Assert::assertEquals($expectedProduct['price'], $actualProduct->price->toString());
        Assert::assertEquals(Response::HTTP_CREATED, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(
            $this->createdProductId->toString(),
            $this->httpClient->getLastResponseJson()->getElement('productId')
        );
        Assert::assertTrue(
            $this->messageBus->wasMessageSent(
                new ProductCreatedEventMessage(
                    $this->createdProductId,
                    $actualProduct->name,
                    $actualProduct->price
                )
            )
        );
    }

    /**
     * @When /^I create a product with name "([^"]*)"$/
     */
    public function iCreateAProductWithName(string $name): void
    {
        $this->httpClient->createProduct(
            $this->httpClient->buildCreateProductPayload(['name' => $name])
        );
    }

    /**
     * @When I create a product without :property
     */
    public function iCreateAProductWithoutProperty(string $property): void
    {
        $payload = $this->httpClient->buildCreateProductPayload(remove: [$property]);
        $this->httpClient->createProduct($payload);
    }

    /**
     * @Then /^there should be an error that product name is too short$/
     */
    public function thereShouldBeAnErrorThatProductNameIsTooShort()
    {
        Assert::assertEquals(400, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(
            'Product name is not unique!',
            $this->httpClient->getLastResponseJson()->getElement('error')
        );
    }

    /**
     * @When /^I create product with (\d+) characters name$/
     */
    public function iCreateProductWithCharactersName(int $characters): void
    {
        $this->httpClient->createProduct(
            $this->httpClient->buildCreateProductPayload([
                'name' => str_repeat('A', $characters)
            ])
        );
    }

    /**
     * @Given /^the product name min length is (\d+)$/
     */
    public function theProductNameMinLengthIs($arg1)
    {
    }

    /**
     * @Given /^the product name max length is (\d+)$/
     */
    public function theProductNameMaxLengthIs($arg1)
    {
    }
}
