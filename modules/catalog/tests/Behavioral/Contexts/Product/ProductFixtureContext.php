<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\FixtureFactory;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;

final class ProductFixtureContext implements Context
{
    public function __construct(private readonly FixtureFactory $fixtureFactory)
    {
    }

    /**
     * @Given /^there is a product with name "([^"]*)"$/
     */
    public function thereIsAProductWithName(string $name): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setName($name)->build();
    }

    /**
     * @Given /^there is a product "([^"]*)" with name "([^"]*)"$/
     */
    public function thereIsAProductAliasWithName(string $productAlias, string $name)
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setName($name)
            ->build($productAlias);
    }

    /**
     * @Given /^there is a deleted product "([^"]*)"$/
     */
    public function thereIsADeletedProduct(string $productAlias): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->shouldBeDeleted()
            ->build($productAlias);
    }

    /**
     * @Given /^there is a product "([^"]*)" with price "([^"]*)"$/
     */
    public function thereIsAProductWithPrice(string $productAlias, string $price): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setPrice(Money::createFromString($price))
            ->build($productAlias);
    }

    /**
     * @Given /^there is a product "([^"]*)"$/
     */
    public function thereIsAProduct(string $productAlias): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)->build($productAlias);
    }

    /**
     * @Given /^there are products:$/
     */
    public function thereAreProducts(TableNode $products)
    {
        foreach ($products as $product) {
            $this->fixtureFactory->createFixture(ProductFixture::class)
                ->setName($product['name'])
                ->setPrice(Money::createFromString($product['price']))
                ->build();
        }
    }
}
