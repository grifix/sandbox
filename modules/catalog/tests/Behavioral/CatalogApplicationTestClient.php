<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Queries\Find\FindProductsQuery;
use Modules\Catalog\Application\Product\Queries\Get\GetProductQuery;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;

final class CatalogApplicationTestClient
{

    public function __construct(
        private readonly ApplicationInterface $application,
        private readonly ObjectRegistryInterface $objectRegistry
    ) {
    }

    public function getProductById(Uuid $productId): ProductDto
    {
        return $this->application->ask(
            new GetProductQuery(ProductFilter::create()->setProductId($productId))
        )->product;
    }

    public function getProductByAlias(string $productAlias): ProductDto
    {
        return $this->getProductById(
            $this->objectRegistry->getObject(ProductFixture::class, $productAlias)->getId()
        );
    }

    public function hasProductWithAlias(string $productAlias): bool
    {
        $result = $this->application->ask(
            new FindProductsQuery(
                ProductFilter::create()->setProductId(
                    $this->objectRegistry->getObject(ProductFixture::class, $productAlias)->getId()
                )
            )
        );
        return count($result->products) > 0;
    }
}
