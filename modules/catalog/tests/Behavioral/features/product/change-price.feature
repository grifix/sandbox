Feature: change product name
  Background:
    Given there is a product "Book" with price "100.00 PLN"

  Scenario: change price
    When I change product "Book" price to "50.25 PLN"
    Then the product "Book" price should be "50.25 PLN"

  Scenario: change name of non existing product
    When I change non-existing product price
    Then there should be an error that "Product with id [%s] does not exist!"

  Scenario: change price for deleted product
    Given there is a deleted product "Newspaper"
    When I change product "Newspaper" price to "300 PLN"
    Then there should be an error that "Product with id [%s] has been deleted!"


