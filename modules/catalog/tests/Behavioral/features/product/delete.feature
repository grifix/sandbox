Feature: delete product
  Scenario: delete product
    Given there is a product "Book"
    When I delete product "Book"
    Then there should be no product "Book"

  Scenario: delete deleted product
    Given there is a deleted product "Book"
    When I delete product "Book"
    Then there should be an error that "Product with id [%s] has been deleted!"

  Scenario: delete non-existing product
    When I delete non-existing product
    Then there should be an error that "Product with id [%s] does not exist!"
