Feature: create product

  Scenario: create product
    When I create a product:
    """
    name: DDD in action
    price: 100.00 PLN
    """
    Then the following product should be created:
    """
    name: DDD in action
    price: 100.00 PLN
    """

  Scenario: fails to create a product with short name
    Given the product name min length is 5
    When I create product with 4 characters name
    Then there should be an error that "Product name cannot have less than [5] characters!"

  Scenario: fails to create a product with long name
    Given the product name max length is 255
    When I create product with 256 characters name
    Then there should be an error that "Product name cannot have more than [255] characters!"

  Scenario: fails to create product with existing name
    Given there is a product with name "DDD in action"
    When I create a product with name "DDD in action"
    Then there should be an error that "Product name is not unique!"

  Scenario: fails to create product without name
    When I create a product without name
    Then there should be an error that name is required

  Scenario: fails to create product without price
    When I create a product without price
    Then there should be an error that price is required

  Scenario: fails to create product without price amount
    When I create a product without price.amount
    Then there should be an error that price.amount is required

  Scenario: fails to create product without price currency
    When I create a product without price.amount
    Then there should be an error that price.amount is required
