Feature: Find products

  Background:
    Given there are products:
      | name                  | price     |
      | Cinderella            | 10.00 PLN |
      | DDD in action         | 59.99 PLN |
      | Foundation            | 45.11 PLN |
      | Foundation and Empire | 55.20 PLN |
      | Solaris               | 76.12 PLN |
      | The Godfather         | 20.00 PLN |
      | The Little Prince     | 34.20 PLN |

    And the products limit on page is "3"

  Scenario: list products from page 1
    When I list products from page 1
    Then I should see the following products:
      | name          | price     |
      | Cinderella    | 10.00 PLN |
      | DDD in action | 59.99 PLN |
      | Foundation    | 45.11 PLN |

  Scenario: list products from page 2
    When I list products from page 2
    Then I should see the following products:
      | name                  | price     |
      | Foundation and Empire | 55.20 PLN |
      | Solaris               | 76.12 PLN |
      | The Godfather         | 20.00 PLN |

  Scenario: list products from page 3
    When I list products from page 3
    Then I should see the following products:
      | name                  | price     |
      | The Little Prince     | 34.20 PLN |

  Scenario: list products from page 4
    When I list products from page 4
    Then I should see the following products:
      | name                  | price     |
