Feature: change product name
  Background:
    Given there is a product "Book" with name "DDD in action"

  Scenario: change name
    When I change product "Book" name to "DDD in action part 2"
    Then the product "Book" name should be "DDD in action part 2"

  Scenario: change name to existing name
    Given there is a product with name "Pinocchio"
    When I change product "Book" name to "Pinocchio"
    Then there should be an error that "Product name is not unique!"

  Scenario: change name of non existing product
    When I change non-existing product name
    Then there should be an error that "Product with id [%s] does not exist!"

  Scenario: change name for deleted product
    Given there is a deleted product "Newspaper"
    When I change product "Newspaper" name to "New Your Times"
    Then there should be an error that "Product with id [%s] has been deleted!"


