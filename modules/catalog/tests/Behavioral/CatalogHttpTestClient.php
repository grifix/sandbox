<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Test\TestHttpClient;
use Grifix\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final class CatalogHttpTestClient
{
    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }


    /**
     * @param string[] $remove
     */
    public function buildCreateProductPayload(
        array $override = [],
        array $remove = [],
    ): array {
        return $this->httpClient->buildPayload(
            [
                'name' => 'DDD in action',
                'price' => [
                    'amount' => '100',
                    'currency' => 'USD',
                ]
            ],
            $override,
            $remove
        );
    }

    public function createProduct(array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products',
            json_encode($payload)
        );
    }

    public function buildChangeProductNamePayload(): array
    {
        return [
            'name' => 'Cinderella'
        ];
    }

    public function changeProductName(Uuid $productId, array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products/' . $productId->toString() . '/name',
            json_encode($payload)
        );
    }

    public function buildChangeProductPricePayload(array $override = [], array $remove = []): array
    {
        $payload = [
            'price' => [
                'amount' => 100,
                'currency' => 'USD'
            ]
        ];
        return $this->httpClient->buildPayload($payload, $override, $remove);
    }

    public function changeProductPrice(Uuid $productId, array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products/' . $productId->toString() . '/price',
            json_encode($payload)
        );
    }

    public function deleteProduct(Uuid $productId): Response
    {
        return $this->httpClient->delete('/api/v1/catalog/products/' . $productId->toString());
    }

    public function findProducts(array $query = []): Response
    {
        return $this->httpClient->get('/api/v1/catalog/products', $query);
    }

    public function getLastResponse(): Response
    {
        return $this->httpClient->getLastResponse();
    }

    public function getLastResponseJson(): ArrayWrapper
    {
        return $this->httpClient->getLastResponseJson();
    }

    public function getLastResponseStatusCode(): int
    {
        return $this->httpClient->getLastResponse()->getStatusCode();
    }
}
