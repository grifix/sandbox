<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product\Builders;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Product;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductBuilder
{
    private Uuid $id;
    private string $name;
    private Money $price;
    private ProductOutsideInterface $outside;
    private bool $shouldBeDeleted = false;

    private function __construct()
    {
        $this->id = Uuid::createFromString('de3e086e-e31e-49a5-b588-3a4c773e4ff2');
        $this->name = "Alice's Adventures in Wonderland";
        $this->price = Money::usd(5220);
        $this->outside = ProductOutsideBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setOutside(ProductOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function shouldBeDeleted(): self
    {
        $this->shouldBeDeleted = true;
        return $this;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function build(): Product
    {
        $result = new Product(
            $this->outside,
            $this->id,
            $this->name,
            $this->price,
        );
        if ($this->shouldBeDeleted) {
            $result->delete();
        }
        EventCollector::clearEvents($result);
        return $result;
    }
}
