<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product\Builders;

use Grifix\Test\EventCollector;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductOutsideBuilder
{
    private int $productNameMaxChars = 255;
    private int $productNameMinChars = 5;
    private bool $productNameUnique = true;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self;
    }

    public function setProductNameMaxChars(int $productNameMaxChars): self
    {
        $this->productNameMaxChars = $productNameMaxChars;
        return $this;
    }

    public function setProductNameIsUnique(bool $productNameUnique): self
    {
        $this->productNameUnique = $productNameUnique;
        return $this;
    }

    public function setProductNameMinChars(int $productNameMinChars): self
    {
        $this->productNameMinChars = $productNameMinChars;
        return $this;
    }

    public function build(): ProductOutsideInterface
    {
        $result = \Mockery::mock(ProductOutsideInterface::class);
        $result->allows('getProductNameMaxChars')->andReturn($this->productNameMaxChars);
        $result->allows('getProductNameMinChars')->andReturn($this->productNameMinChars);
        $result->allows('isProductNameUnique')->andReturn($this->productNameUnique);
        EventCollector::injectTo($result);
        return $result;
    }
}
