<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Events\ProductCreatedEvent;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameIsNotUniqueException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooLongException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooShortException;
use Modules\Catalog\Domain\Product\Product;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductBuilder;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductOutsideBuilder;
use PHPUnit\Framework\TestCase;

final class CreateProductTest extends TestCase
{
    public function testItCreates(): void
    {
        $product = new Product(
            ProductOutsideBuilder::create()->build(),
            Uuid::createFromString('217eaa06-6f76-4f0c-aa1f-06396bbffce1'),
            'Implementing DDD',
            Money::usd(100)
        );

        $events = EventCollector::getEvents($product);

        self::assertCount(1, $events);
        self::assertEquals(
            new ProductCreatedEvent(
                Uuid::createFromString('217eaa06-6f76-4f0c-aa1f-06396bbffce1'),
                Money::usd(100),
                'Implementing DDD'
            ),
            array_shift($events)
        );
    }

    /**
     * @dataProvider itFailsToCreateDataProvider
     */
    public function testItFailsToCreate(
        ProductBuilder $builder,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $builder->build();
    }

    public function itFailsToCreateDataProvider(): array
    {
        return [
            'too short name' => [
                ProductBuilder::create()
                    ->setName('test')
                    ->setOutside(
                        ProductOutsideBuilder::create()
                            ->setProductNameMinChars(10)
                            ->setProductNameMaxChars(20)
                            ->build()
                    )
                ,
                ProductNameTooShortException::class,
                'Product name cannot have less than [10] characters!'
            ],
            'too long name' => [
                ProductBuilder::create()
                    ->setName('test')
                    ->setOutside(
                        ProductOutsideBuilder::create()
                            ->setProductNameMinChars(1)
                            ->setProductNameMaxChars(2)
                            ->build()
                    )
                ,
                ProductNameTooLongException::class,
                'Product name cannot have more than [2] characters!'
            ],
            'not unique name' => [
                ProductBuilder::create()
                ->setOutside(
                    ProductOutsideBuilder::create()
                        ->setProductNameIsUnique(false)
                    ->build()
                ),
                ProductNameIsNotUniqueException::class,
                'Product name is not unique!'
            ]
        ];
    }
}
