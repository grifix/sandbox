<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product;

use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Events\ProductDeletedEvent;
use Modules\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductBuilder;
use PHPUnit\Framework\TestCase;

final class DeleteProductTest extends TestCase
{
    public function testItDeletes(): void
    {
        $productId = Uuid::createFromString('2713b8a6-94b5-469a-ae8d-acde11128430');
        $product = ProductBuilder::create()->setId($productId)->build();

        $product->delete();
        $events = EventCollector::getEvents($product);
        self::assertCount(1, $events);
        self::assertEquals(
            new ProductDeletedEvent(
                $productId
            ),
            array_shift($events)
        );
    }

    public function testItFailsToDeleteTwice(): void
    {
        $this->expectException(ProductDeletedException::class);
        $this->expectExceptionMessage('Product with id [10bca4af-f26b-415b-a149-a18758b6fea0] has been deleted!');
        ProductBuilder::create()
            ->setId(Uuid::createFromString('10bca4af-f26b-415b-a149-a18758b6fea0'))
            ->shouldBeDeleted()
            ->build()
            ->delete();
    }
}
