<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product;

use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Events\ProductNameChangedEvent;
use Modules\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameIsNotUniqueException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooLongException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooShortException;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductBuilder;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductOutsideBuilder;
use PHPUnit\Framework\TestCase;

final class ChangeNameTest extends TestCase
{
    public function testItChangesName(): void
    {
        $productId = Uuid::createFromString('0e8dbf81-985d-4bd0-8d80-a4d9087c2185');
        $product = ProductBuilder::create()->setId($productId)->build();
        $product->changeName('new name');
        $events = EventCollector::getEvents($product);
        self::assertCount(1, $events);
        self::assertEquals(
            new ProductNameChangedEvent(
                $productId,
                'new name'
            ),
            array_shift($events)
        );
    }

    public function testItDoesNotChangeWithoutChange(): void
    {
        $product = ProductBuilder::create()->setName('old name')->build();
        $product->changeName('old name');
        self::assertCount(0, EventCollector::getEvents($product));
    }

    /**
     * @dataProvider failsDataProvider
     */
    public function testItFailsToChangeName(
        ProductBuilder $builder,
        string $newName,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);
        $builder->build()->changeName($newName);
    }

    public function failsDataProvider(): array
    {
        return [
            'product deleted' => [
                ProductBuilder::create()
                    ->setId(Uuid::createFromString('96729525-45ed-45a5-b604-22364c4b3d5c'))
                    ->shouldBeDeleted(),
                'new name',
                ProductDeletedException::class,
                'Product with id [96729525-45ed-45a5-b604-22364c4b3d5c] has been deleted!'
            ],
            'too short name' => [
                ProductBuilder::create()
                    ->setName('test')
                    ->setOutside(
                        ProductOutsideBuilder::create()
                            ->setProductNameMinChars(3)
                            ->setProductNameMaxChars(20)
                            ->build()
                    ),
                'n',
                ProductNameTooShortException::class,
                'Product name cannot have less than [3] characters!'
            ],
            'too long name' => [
                ProductBuilder::create()
                    ->setName('test')
                    ->setOutside(
                        ProductOutsideBuilder::create()
                            ->setProductNameMinChars(3)
                            ->setProductNameMaxChars(5)
                            ->build()
                    ),
                'test test',
                ProductNameTooLongException::class,
                'Product name cannot have more than [5] characters!'
            ],
            'not unique name' => [
                ProductBuilder::create()
                    ->setOutside(
                        ProductOutsideBuilder::create()
                            ->setProductNameIsUnique(false)
                            ->build()
                    ),
                'new name',
                ProductNameIsNotUniqueException::class,
                'Product name is not unique!'
            ]
        ];
    }
}
