<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Unit\Domain\Product;

use Grifix\Money\Money\Money;
use Grifix\Test\EventCollector;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Events\ProductPriceChangedEvent;
use Modules\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use Modules\Catalog\Tests\Unit\Domain\Product\Builders\ProductBuilder;
use PHPUnit\Framework\TestCase;

final class ChangePriceTest extends TestCase
{
    public function testItChangesPrice(): void
    {
        $productId = Uuid::createFromString('96005265-a010-4451-9eac-49f82650e6d5');
        $price = Money::usd(500);
        $product = ProductBuilder::create()->setId($productId)->build();
        $product->changePrice($price);
        $events = EventCollector::getEvents($product);

        self::assertCount(1, $events);
        self::assertEquals(
            new ProductPriceChangedEvent(
                $productId,
                $price
            ),
            array_shift($events)
        );
    }

    public function testItFailsToChangePriceIfDeleted():void{
        $this->expectException(ProductDeletedException::class);
        $this->expectExceptionMessage('Product with id [96005265-a010-4451-9eac-49f82650e6d5] has been deleted!');

        ProductBuilder::create()
            ->setId(Uuid::createFromString('96005265-a010-4451-9eac-49f82650e6d5'))
            ->shouldBeDeleted()
            ->build()
            ->changePrice(Money::usd(800))
        ;
    }
}
