<?php

declare(strict_types=1);

namespace Catalog;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220912125455 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('create schema catalog');
        $this->createProductTable();
        $this->createProductProjectionTable();
    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table catalog.products');
        $this->addSql('drop table catalog.product_projections');
        $this->addSql('drop schema catalog');

    }

    private function createProductTable(): void
    {
        $sql = <<<SQL
create table if not exists catalog.products
(
    id      uuid
        constraint catalog_products_pk
            primary key,
    version bigint not null,
    data    jsonb  not null
);
SQL;
        $this->addSql($sql);
    }

    private function createProductProjectionTable():void{
        $sql = <<<SQL
create table catalog.product_projections
(
    id          uuid    not null
        constraint id
            primary key,
    "name" varchar not null,
    "price_amount"  varchar not null,
    "price_currency" varchar not null
);
SQL;
        $this->addSql($sql);
    }
}
