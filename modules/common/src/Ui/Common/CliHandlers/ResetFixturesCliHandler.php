<?php

declare(strict_types=1);

namespace Modules\Common\Ui\Common\CliHandlers;

use Grifix\Framework\Ui\AbstractCliHandler;
use Modules\Common\Tests\E2e\ResetFixturesAction;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(
    name: 'sandbox:common:reset-fixtures',
    description: 'Resets fixtures for e2e tests',
)]
final class ResetFixturesCliHandler extends AbstractCliHandler
{
    #[Required]
    public ResetFixturesAction $action;

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $answer = $helper->ask(
            $input,
            $output,
            new Question(
                'This command will reset the database. Please type YES if you want to continue:',
                'n',
            ),
        );
        if ($answer !== 'YES') {
            return self::FAILURE;
        }
        $this->action->__invoke();
        return self::SUCCESS;
    }

}
