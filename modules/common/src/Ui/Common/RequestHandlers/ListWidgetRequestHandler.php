<?php

declare(strict_types=1);

namespace Modules\Common\Ui\Common\RequestHandlers;

use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ListWidgetRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/widgets/list',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        return $this->renderView(
            'list.php',
            [
                'sourceUrl' => '/widgets/list',
                'currentPage' => $request->get('page', 1),
                'pages' => [
                    1 => ['red', 'white', 'green'],
                    2 => ['yellow', 'magenta', 'black'],
                    3 => ['yellow2', 'magenta2', 'black2'],
                ]
            ]
        );
    }
}
