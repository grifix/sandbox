<?php

declare(strict_types=1);

namespace Modules\Common\Ui\Common\RequestHandlers;

use Modules\Common\Tests\E2e\ResetFixturesAction;
use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Service\Attribute\Required;

final class ResetFixturesRequestHandler extends AbstractRequestHandler
{
    #[Required]
    public ResetFixturesAction $action;

    #[Route(
        '/api/v1/reset-fixtures',
        methods: ['POST'],
    )]
    public function __invoke(
        Request $request,
    ): Response {
        try {
            $this->action->__invoke();
        } catch (\Throwable $e) {
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse('Fixtures reset completed', Response::HTTP_OK);
    }

}
