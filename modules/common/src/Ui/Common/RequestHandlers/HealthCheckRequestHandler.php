<?php

declare(strict_types=1);

namespace Modules\Common\Ui\Common\RequestHandlers;

use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HealthCheckRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/health-check',
        methods: ['GET'],
    )]
    public function __invoke(Request $request): Response
    {
        return new JsonResponse(['status' => 'ok'], Response::HTTP_OK);
    }
}
