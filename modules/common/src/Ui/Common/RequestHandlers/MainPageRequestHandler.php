<?php

declare(strict_types=1);

namespace Modules\Common\Ui\Common\RequestHandlers;

use Sandbox\SharedKernel\Ui\AbstractRequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MainPageRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/',
        methods: ['GET'],
    )]
    public function __invoke(
        Request $request,
    ): Response {
        return $this->renderView('index.php');
    }
}
