<?php

declare(strict_types=1);

use Grifix\Framework\Ui\WidgetRendererPlugin;
use Grifix\View\TemplateInterface;
use Modules\Common\Ui\Common\RequestHandlers\ListWidgetRequestHandler;

/** @var $this TemplateInterface */
$this->inherits('layout.php');
$widgetRenderer = $this->getPlugin(WidgetRendererPlugin::class);
?>

<?php
$this->startSlot('body'); ?>
<?= $widgetRenderer->renderWidget(ListWidgetRequestHandler::class, ['page' => 2]) ?>
<?php
$this->endSlot(); ?>
