<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$pages = $this->getVar('pages');
$currentPage = $this->getVar('currentPage');
$sourceUrl = $this->getVar('sourceUrl');
?>

<div data-widget="list" data-source_url="<?=$sourceUrl?>">
    <ul style="display: flex; list-style-type: none;">
        <li>
            <button data-role="reload">Reload</button>
        </li>
        <?php
        foreach (array_keys($pages) as $page): ?>
            <li>
                <button data-role="page" data-page="<?= $page ?>"><?= $page ?></button>
            </li>
        <?php
        endforeach; ?>
    </ul>
    <ul>
        <?php
        foreach ($pages[$currentPage] as $element): ?>
            <li>
                <?=$this->renderPartial('element.php', ['element' => $element])?>
            </li>
        <?php
        endforeach; ?>
    </ul>
</div>
