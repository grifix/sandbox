"use strict";

import Widget from "@grifix/widget";
import {NameClickedEvent} from "./Element";

export default class List extends Widget {
    _doInit() {
        this.#initReloadButton();
        this.#initPages();
        this._findWidgets('element').forEach(function (widget) {
            widget.on(
                'nameClicked',
                /**
                 * @param {NameClickedEvent} event
                 */
                function (event) {
                    console.log(event.title+' clicked');
                }
            )
        });

    }

    #initReloadButton() {
        const that = this;
        this._getElement('reload').addEventListener('click', async function () {
            await that.reload();
        })
    }

    #initPages() {
        const that = this;
        this._findElements('page').forEach(
            function (element) {
                element.addEventListener('click', async function () {
                    await that.reload(
                        that._getSourceUrl() + '?page=' + this.getAttribute('data-page')
                    );
                })
            }
        )
    }
}
