<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */

?>

<html lang="en">

<head>
    <title><?php $this->startSlot('title')?>Sandbox<?php $this->endSlot()?></title>
    <script src="dist/common.bundle.js"></script>
</head>
<body>
    <?php $this->startSlot('body');?>

    <?php $this->endSlot();?>
</body>
</html>
