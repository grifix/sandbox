"use strict";

import Widget from "@grifix/widget";

export default class Element extends Widget {
    _doInit() {
        this.#initButton();
    }

    #initButton() {
        const that = this;
        this._getElement('button').addEventListener(
            'click',
            function () {
                const title = this.getAttribute('data-title');
                alert(this.getAttribute('data-title'));
                that._dispatchEvent('nameClicked', new NameClickedEvent(title))
            }
        )
    }
}

export class NameClickedEvent {

    /**
     * @type {String}
     */
    title

    /**
     *
     * @param {String} title
     */
    constructor(title) {
        this.title = title;
    }
}
