<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;
use Sandbox\Catalog\Dtos\ProductDto;

/** @var $this TemplateInterface */
$this->inherits('layout.php');
/** @var ProductDto[] $products */
$products = $this->getVar('products')
?>

<?php $this->startSlot('body');?>
<table>
    <tr>
        <th>
            Name
        </th>
        <th>
            Price
        </th>
    </tr>
    <?php foreach ($products as $product):?>
       <tr>
           <td><?=$product->name?></td>
           <td><?=$product->price->toString()?></td>
       </tr>
    <?php endforeach?>
</table>
<?php $this->endSlot();?>
