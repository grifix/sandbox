import WidgetManager from "@grifix/widget/WidgetManager";
import List from "./List";
import Element from "./Element";
const manager = new WidgetManager({
    list: List,
    element: Element
})
document.addEventListener('DOMContentLoaded', function() {
    manager.bindWidgets();
});
