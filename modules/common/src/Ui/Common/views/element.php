<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
$element = $this->getVar('element');
?>

<div data-widget="element">
    <button data-title="<?= $element ?>" data-role="button"><?= $element ?></button>
</div>
