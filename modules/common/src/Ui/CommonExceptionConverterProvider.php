<?php

declare(strict_types=1);

namespace Modules\Common\Ui;

use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Sandbox\SharedKernel\Ui\Security\Exceptions\UnauthorizedException;

final class CommonExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(UnauthorizedException::class, httpCode: 401),
        ];
    }
}
