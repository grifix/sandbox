<?php

declare(strict_types=1);

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

use Grifix\Framework\Config\ModuleServicesConfigurator;
use Modules\Common\Tests\E2e\ResetFixturesAction;
use Modules\Common\Ui\CommonExceptionConverterProvider;

return static function (ContainerConfigurator $configurator) {
    ModuleServicesConfigurator::create($configurator)
        ->configure('common', 'Common');

    $services = $configurator->services();
    $services->defaults()->autowire()->autoconfigure();

    $services->set(CommonExceptionConverterProvider::class);

    $services->set(ResetFixturesAction::class)->args(
        [
            '$env' => '%env(APP_ENV)%',
        ],
    );
};
