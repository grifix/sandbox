<?php

declare(strict_types=1);

namespace Modules\Common\Tests\E2e;

use Grifix\Email\Email;
use Grifix\Test\DatabaseManager;
use Grifix\Test\Fixture\FixtureFactory;
use Modules\Security\Tests\Behavioral\Fixtures\RoleFixture;
use Modules\Security\Tests\Behavioral\Fixtures\UserFixture;
use Modules\Security\Ui\SecurityPermission;

final readonly class ResetFixturesAction
{
    public function __construct(
        private FixtureFactory $fixtureFactory,
        private DatabaseManager $databaseManager,
        private string $env,
    ) {
    }

    public function __invoke(): void
    {
        if (str_starts_with($this->env, 'prod')) {
            throw new \RuntimeException('Cannot reset fixtures on production!');
        }
        $this->databaseManager->truncateTables();

        $adminRoleFixture = $this->fixtureFactory->createFixture(RoleFixture::class)
            ->withName('Admin')
            ->build();
        foreach (SecurityPermission::cases() as $permission) {
            $adminRoleFixture->grantPermission($permission->value);
        }

        $this->fixtureFactory->createFixture(UserFixture::class)
            ->withAssignedRoles($adminRoleFixture->getId())
            ->withEmail(Email::create('admin@example.com'))
            ->withCompletedRegistration()
            ->withPassword('AdmiN!123')
            ->build();

        for ($i = 0; $i < 26; $i++) {
            $this->fixtureFactory->createFixture(UserFixture::class)->withEmail(
                Email::create('user' . $i . '@example.com'),
            )->build();
        }
    }
}
