# Table of contents

- [Application](#application)
- [Module](#module)
- [Layer](#layer)
- [Ui Layer](#ui-layer)
  - [Input Validation](#input-validation)
  - [Request Handler](#request-handler)
  - [Input](#input)
  - [CLI Handler](#cli-handler)
  - [Message Handler](#message-handler)
- [Application Layer](#application-layer)
  - [Command](#command)
  - [Command Handler](#command-handler)
  - [Query](#query)
  - [Query Handler](#query-handler)
  - [Query Result](#query-result)
  - [Projector](#projector)
  - [Projector Filter](#projector-filter)
  - [Repository](#repository)
  - [Beyond](#beyond)
  - [Subscriber](#subscriber)
- [Domain Layer](#domain-layer)
  - [Entity](#entity)
  - [Value Object](#value-object)
  - [Aggregate](#aggregate)
  - [Aggregate Root](#aggregate-root)
  - [Aggregate Factory](#aggregate-factory)
  - [Aggregate Outside](#aggregate-outside)
  - [Domain Event](#domain-event)
- [Infrastructure Layer](#infrastructure-layer)
  - [Connector](#connector)
- [DTO](#dto)
- [Generic Type](#generic-type)
- [Communication Library](#communication-library)
  - [Module client](#module-client)
  - [Message](#message)
    - [Command Message](#command-message)
    - [Event Message](#event-message)
    - [Request Message](#request-message)
    - [Reply Message](#reply-message)
- [Package](#package)
- [Library](#library)
- [Shared Kernel Library](#shared-kernel-library)
- [Application Wrapper](#application-wrapper)
- [Command Bus](#command-bus)
- [Query Bus](#query-bus)
- [Testing](#testing)
  - [Unit Tests](#unit-tests)
    - [Aggregate Builder](#aggregate-builder)
    - [DTO Builder](#dto-builder)
    - [Aggregate Outside Builder](#aggregate-outside-builder)
  - [Behavioral Tests](#behavioral-tests)
    - [Gherkin Scenario](#gherkin-scenario)
    - [Behat Contexts](#behat-contexts)
      - [Fixture Context](#fixture-context)
      - [Feature context](#feature-context)
    - [Application Test Client](#application-test-client)
    - [API Test Client](#api-test-client)
    - [Fixture Factory](#fixture-factory)
    - [Fixture](#fixture)
    - [Fixture Registry](#fixture-registry)

# <a id="application">Application</a>

`Application` is a bunch of [modules](#module) combined into one repository and deployed as one piece.

`Application` should have the following structure:

- [Module](#module)
    - [Layer](#layer)
        - [Package](#package)

Typical application directory structure:

- libraries
- modules
    - catalog
        - configs
        - migrations
        - src
            - Application
                - Product
                    - Commands
                        - Create
                        - Delete
                    - Ports
                        - Projector
                        - Repository
                    - Queries
                        - Find
                        - Get
                    - Subscribers
            - Domain
                - Product
                    - Events
                    - Exceptions
                    - Name
            - Infrastructure
                - Product
                    - Application
                        - Ports
                    - Domain
            - Ui
                - Product
                    - RequestHandlers
                        - Create
                        - Delete
                    - CliHandlers
        - tests
            - Behavioral
                - Contexts
                - features
                - Fixtures
            - Unit
                - Domain
                    - Product
                        - Builders
    - cart
        - ...

# <a id="module">Module</a>

All business functionalities must be separated into independent `modules`.
The `Module` corresponds to the [Bounded Context](https://martinfowler.com/bliki/BoundedContext.html) in terms of
[DDD](https://en.wikipedia.org/wiki/Domain-driven_design).

`Modules` cannot depend on each other directly.
They can communicate synchronously only through [Clients](#module-client) and [Connectors](#connector), and asynchronously
only through the [Message Bus](#message-bus) using the [Module Communication Library](#module-communication-library).
It means that `module` `A` cannot import any class from `module` `B`.
Asynchronous communication is preferable.

All `module` database tables must be placed into a separate schema with the `module` name.
`Module` tables cannot have relations to tables from other `modules`.

Each `Module` should be separated into [Layers](#layer).

`Module` should always be ready for extraction from the monolith `Application` and deployed as an
independent [Application](#application) with minimal effort.

# <a id="layer">Layer</a>

Each `module` should be separated into `layers`. There are the following `layers`:

- [UI](#ui-layer)
- [Application](#application-layer)
- [Domain](#domain-layer)
- [Infrastructure](#infrastructure-layer)

Each `layer` should be separated into [packages](#package) according to `module` [aggregates](#aggregate).
Classes that are not related to any `aggregate` should be placed in the `Common` `package`

# <a id="ui-layer">UI Layer</a>

This `layer` is responsible for:

- handling data from the client
- receiving [Messages](#message) from other `modules`
- [Input Validation](#input-validation)
- transforming data into [Queries](#query) and [Commands](#command)
- checking permissions
- transforming data from [Query Result](#query-result) to response

This `layer` can be coupled with the framework and third-party libraries.

This `layer` can contain:

- [Request handlers](#request-handler)
- [CLI handlers](#cli-handler)
- [Message handlers](#message-handler)
- [Inputs](#input)

`UI Layer` can communicate with other `layers` only through the [Application Wrapper](#application-wrapper)

Typical structure of `Ui Layer`:

- **User**
    - **RequestHandlers**
        - **Create**
            - CreateUserInput.php
            - CreateUserRequestHandler.php
    - **CliHandlers**
        - DeleteExpiredUsersCliHandler.php

## <a id="input-validation">Input Validation</a>

This type of validation should check only the data type and not perform any business rule validation.
It means that this type of validation should validate only separated fields, not field combinations.

## <a id="request-handler">Request Handler</a>

`Request Handler` is responsible for

- checking permissions
- [Input Validation](#input-validation)
- transforming http request into [Command](#command) or [Query](#query)
- sending [Command](#command) or [Query](#query) to the [Application Wrapper](#application-wrapper)
- transforming the [Query Result](#query-result) to the response

The `Request Handler` class must have the following name template `<Action><Aggregate>RequestHandler` for
example `DeleteUserActionHandler`

Typical `Request Handler`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\ChangePrice;

use Grifix\Framework\Ui\AbstractRequestHandler;
use Grifix\Framework\Ui\Input\InvalidInputException;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\ChangePrice\ChangeProductPriceCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ChangeProductPriceRequestHandler extends AbstractRequestHandler
{
    #[Route(
        '/api/v1/catalog/products/{productId}/price',
        methods: ['POST'],
    )]
    public function __invoke(string $productId, Request $request): Response
    {
        $input = $this->createInputFromBody($request, ChangeProductPriceInput::class);
        $this->application->tell(
            new ChangeProductPriceCommand(
                Uuid::createFromString($productId),
                $input->getPrice()
            )
        );

        return new JsonResponse();
    }
}
```

## <a id="input">Input</a>

`Input` wraps and validates data that the user passes to the [Module](#module). `Input` is responsible for:

- [Input Validation](#input-validation)
- Wrapping data
- Converting data into proper types

The `Input` class must have the following name template `<Action><Agregate>Input` for example `DeleteUserInput`

Typical `Input`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\RequestHandlers\Create;

use Grifix\Framework\Ui\Input\AbstractInput;
use Grifix\Framework\Ui\Input\Types\MoneyInputType;
use Grifix\Money\Money\Money;

final class CreateProductInput extends AbstractInput
{
    public static function getJsonSchema(): array
    {
        return [
            'type' => 'object',
            'properties' => [
                'name' => [
                    'type' => 'string'
                ],
                'price' => MoneyInputType::getJsonSchema()
            ],
            'required' => [
                'name',
                'price'
            ],
            'additionalProperties' => false
        ];
    }

    public function getName(): string
    {
        return (string)$this->data->getElement('name');
    }

    public function getPrice(): Money
    {
        return MoneyInputType::toValue($this->data->getElement('price'));
    }
}
```

## <a id="cli-handler">CLI Handler</a>

`CLI Handler` is responsible for:

- [Input Validation](#input-validation)
- transforming CLI Input into [Command](#command) or [Query](#query)
- sending [Command](#command) or [Query](#query) to [Application Wrapper](#application-wrapper)
- transforming the [Query Result](#query-result) into the CLI output

The `CLI Handler` class must have the following name template `<Action><Aggregate>CliHandler` for
example `DeleteUserCliHandler`

Typical `CLI handler`:

```php
#TODO add the example
```

## <a id="message-handler">Message Handler</a>

`Message Handler` is responsible for:

- receiving [Command](#command-message) and [Request](#request-message) `Messages` from [Message Bus](#message-bus)
- transforming the `Message` into [Command](#command) or [Query](#query)
- sending [Command](#command) or [Query](#query) to the [Application Wrapper](#application-wrapper)
- transforming [Query Result](#query-result) into [Reply Message](#reply-message) and send it to the
  [Message Bus](#message-bus)

The `Message Handler` class must have the following name template `<Name>MessageHandler` for
example `CatalogMessageHandler`

Typical `Message Handler`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Ui\Product\MessageHandlers;

use Grifix\Framework\Ui\AbstractMessageHandler;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Queries\Get\GetProductQuery;
use Sandbox\Catalog\Messages\GetProductRequestMessage;
use Sandbox\Catalog\Messages\GetProductReplyMessage;

final class ProductMessageHandler extends AbstractMessageHandler
{
    public function onGetProductMessage(GetProductRequestMessage $message): void
    {
        $product = $this->application->ask(
            new GetProductQuery(ProductFilter::create()->setProductId($message->productId))->product
        );
  
        $this->sendMessage(new GetProductReplyMessage(
            correlationId: $message->correlationId,
            product: new ProductDto(
                $product->id,
                $product->name,
                $product->price
            )
        ))
    }
}
```

# <a id="application-layer">Application Layer</a>

This `Layer` is responsible for:

- Handling [commands](#command) and passing them to [aggregates](#aggregate)
- Handling [queries](#query) and passing them to [projectors](#projector)
- Handling [domain events](#domain-event) and passing them to [aggregates](#aggregate) or [projectors](#projectors)
- Defining [ports](#port)

The `Application Layer` cannot be coupled with the framework or third-party libraries.

The `Application Layer` can be coupled only with `Application Layer` from
the [Shared Kernel Library](#shared-kernel-library) or with [generic types](#generic-type).

Typical `Application Layer` structure:

- **Application**
    - **Product**
        - **Commands**
            - **Create**
                - CreateProductCommand.php
                - CreateProductCommandHandler.php
            - **Delete**
                - DeleteProductCommand.php
                - DeleteProductCommandHandler.php
        - **Ports**
            - **Projector**
                - **Exceptions**
                    - ProductDoesNotExistException.php
                - ProductDto.php
                - ProductFilter.php
                - ProductProjectorInterface.php
            - **Repository**
                - **Exceptions**
                    - ProductDoesNotExistException.php
                - ProductRepositoryInterface.php
        - **Queries**
            - **Find**
                - FindProductsQuery.php
                - FindProductsQueryHandler.php
                - FindProductsQueryResult.php
            - **Get**
                - GetProductQuery.php
                - GetProductQueryHandler.php
                - GetProductQueryResult.php
        - **Subscribers**
            - ProductProjectorSubscriber.php

## <a id="command">Command</a>

The `Command` is the [DTO](#dto) that represents the business operation that changes the [Module](#module) state and
should be passed to the appropriate [Command Handler](#command-handler)

`Command` class should have the following name template `<Action><AggregateName>Command` for example `CreateUserCommand`

Typical `Command`:

```php

<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Create;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class CreateProductCommand
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $name,
        public readonly Money $price
    ) {
    }
}
```

## <a id="command-handler">Command Handler<a>

The `Command Handler` is responsible for:

- Handling the [Command](#command)
- Fetching the [Aggregate](#aggregate) from the [Repository](#repository)
- Perform business method on the [Aggregate](#aggregate)
- Creating a new `Aggregate` object by the [Aggregate Factory](#aggregate-factory)
- Putting the new `Aggregate` object into the [Repository](#repository)

The `Command Handler` should return `void` type

The `Command Handler` should have the following name template `<CommandClassName>Handler` for
example `CreateUserCommandHandler`

Typical `Command Handler`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Commands\Create;

use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;
use Modules\Catalog\Domain\Product\ProductFactory;

final class CreateProductCommandHandler
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
        private readonly ProductFactory $productFactory
    ) {
    }

    public function __invoke(CreateProductCommand $command): void
    {
        $this->productRepository->add(
            $this->productFactory->createProduct($command->productId, $command->name, $command->price)
        );
    }
}
```

## <a id='query'>Query</a>

`Query` is the [DTO](#dto) that represents reading operation which does not change the `Module` state and should be
passed to the appropriate [Query Handler](#query-handler)

`Query` can have the [Projection Filter](#projetion-filter) as one of they properties.

`Query` should have the following name template `<Action><Aggregate>Query` for example `FindUsersQuery`

Typical `Query`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;

final class FindProductsQuery
{
    public function __construct(public readonly ?ProductFilter $filter = null)
    {
    }
}

```

## <a id='query-handler'>Query Handler</a>

`Query Handler` is responsible for:

- Handling [Query](#query)
- Getting [DTOs](#dto) from [Projector](#projector)
- Putting it to the [Query Result](#query-result)
- Returning the [Query Result](#query-result)

`Query Handler` class must have the following name template `<QueryClassName>Handler` for
example `FindUsersQueryHandler`

Typical `Query Handler`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;

final class FindProductsQueryHandler
{
    public function __construct(private readonly ProductProjectorInterface $projector)
    {
    }

    public function __invoke(FindProductsQuery $query): FindProductsQueryResult
    {
        return new FindProductsQueryResult(
            $this->projector->find($query->filter)
        );
    }
}
```

## <a id="query-result">Query Result</a>

The `Query Result` is a [DTO](#dto) that returns result of the [Query](#query)

The `Query Result` class should have the following name template `<QueryClassName>Result` for
example `FindUsersQueryResult`

Typical `Query Result`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Queries\Find;

use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;

final class FindProductsQueryResult
{
    /**
     * @param ProductDto[] $products
     */
    public function __construct(public readonly array $products)
    {
    }
}
```

## <a id="projector">Projector<a>

The `Projector` is responsible for

- fetching data from data storage and packing them into [DTO](#dto).
- updating projection state

The [Application Layer](#application-layer) defines `projector` [Port](#port) Interface, [Dto's](#dto),
[Filter](#projector-filter) and [Exceptions](#exception) but does not have the realization of this interface
the realization must be implemented on the [Infrastructure](#infrastructure-layer) layer

Typical `Projector` package:

- **Application**
    - **User**
        - **Ports**
            - **Projector**
                - **Exceptions**
                    - ProductDoesNotExistException.php
                - ProductDto.php
                - ProductFilter.php
                - ProductProjectorInterface.php

`Projector` interface must have the following name template `<ProjectorSubject>ProjectorInterface` for
example `UserProjectorInterface`

Typical `Projector` interface:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\Exceptions\ProductDoesNotExistException;

interface ProductProjectorInterface
{
    public function find(ProductFilter $filter): array;

    /**
     * @throws ProductDoesNotExistException
     */
    public function getOne(ProductFilter $filter): ProductDto;

    public function findOne(ProductFilter $filter): ?ProductDto;

    public function has(ProductFilter $filter): bool;

    public function count(ProductFilter $filter): int;

    public function createProduct(ProductDto $product): void;

    public function delete(Uuid $productId): void;

    public function changeName(Uuid $productId, string $newName): void;

    public function changePrice(Uuid $productId, Money $newPrice): void;

    public function clear(): void;
}
```

`Projector` realization class should be placed inside the [Infrastructure layer](#infrastructure-layer)

Typical `Projector` realization:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product\Application\Ports;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\Exceptions\ProductDoesNotExistException;
use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;

final class ProductProjector implements ProductProjectorInterface
{
    private const TABLE = 'catalog.product_projections';

    public function __construct(private readonly Connection $connection)
    {
    }

    public function find(ProductFilter $filter): array
    {
        $rows = $this->createQueryBuilder($filter)->fetchAllAssociative();
        $result = [];
        foreach ($rows as $row) {
            $result[] = $this->createDto($row);
        }
        return $result;
    }

    public function getOne(ProductFilter $filter): ProductDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            throw new ProductDoesNotExistException();
        }
        return $this->createDto($row);
    }

    public function findOne(ProductFilter $filter): ?ProductDto
    {
        $row = $this->createQueryBuilder($filter)->fetchAssociative();
        if (false === $row) {
            return null;
        }
        return $this->createDto($row);
    }

    public function has(ProductFilter $filter): bool
    {
        return false !== $this->createQueryBuilder($filter)->fetchOne();
    }

    public function count(ProductFilter $filter): int
    {
        return $this->createQueryBuilder($filter)->executeQuery()->rowCount();
    }

    public function createProduct(ProductDto $product): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $product->id->toString(),
                'name' => $product->name,
                'price_amount' => $product->price->getAmount(),
                'price_currency' => $product->price->getCurrency()->getCode()
            ]
        );
    }

    public function delete(Uuid $productId): void
    {
        $this->connection->delete(self::TABLE, ['id' => $productId->toString()]);
    }

    public function changeName(Uuid $productId, string $newName): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'name' => $newName
            ],
            [
                'id' => $productId->toString(),
            ]
        );
    }

    public function changePrice(Uuid $productId, Money $newPrice): void
    {
        $this->connection->update(
            self::TABLE,
            [
                'price_amount' => $newPrice->getAmount(),
                'price_currency' => $newPrice->getCurrency()->getCode()
            ],
            [
                'id' => $productId->toString()
            ]
        );
    }

    public function clear(): void
    {
        $this->connection->executeQuery(sprintf('truncate table %s', self::TABLE));
    }

    private function createQueryBuilder(ProductFilter $filter): QueryBuilder
    {
        $result = $this->connection->createQueryBuilder();
        $result->select('*')->from(self::TABLE);

        if ($filter->getProductId()) {
            $result->andWhere('id = :id')->setParameter('id', $filter->getProductId()->toString());
        }

        if ($filter->getName()) {
            $result->andWhere('name = :name')->setParameter('name', $filter->getName());
        }

        if ($filter->getNotProductId()) {
            $result->andWhere('id != :not_id')->setParameter('not_id', $filter->getNotProductId());
        }

        if (null !== $filter->getOffset()) {
            $result->setFirstResult($filter->getOffset());
        }

        if (null !== $filter->getLimit()) {
            $result->setMaxResults($filter->getLimit());
        }

        if (null !== $filter->getSortBy()) {
            $result->orderBy($filter->getSortBy(), $filter->getSortDirection() ?? 'asc');
        }

        return $result;
    }

    private function createDto(array $row): ProductDto
    {
        return new ProductDto(
            Uuid::createFromString($row['id']),
            $row['name'],
            Money::withCurrency($row['price_amount'], $row['price_currency'])
        );
    }
}
```

## <a id="projector-filter">Projector Filter<a>

The `Projector Filter` is a class that allows us to specify search parameters.

`Projector Filter` class must have the following name template `<FilterSubject>Filter` for
example `UserFilter`

Typical `Projector Filter`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Uuid\Uuid;

final class ProductFilter
{
    private ?Uuid $productId = null;

    private ?Uuid $notProductId = null;

    private ?string $name = null;

    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $sortBy = null;

    private ?string $sortDirection = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getProductId(): ?Uuid
    {
        return $this->productId;
    }

    public function setProductId(?Uuid $productId): self
    {
        $this->productId = $productId;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getNotProductId(): ?Uuid
    {
        return $this->notProductId;
    }

    public function setNotProductId(?Uuid $notProductId): self
    {
        $this->notProductId = $notProductId;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function setSortBy(?string $sortBy): self
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function setSortDirection(?string $sortDirection): self
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }
}
```

### <a id="repository">Repository</a>

- `Repository` is responsible for [Aggregate](#aggregate) persistence
- The [Application Layer](#application-layer) defines the `repository` [Port](#port) interface
- The `Repository` realization must be placed into [Infrastructure](#infrastructure-layer) layer
- `Repository` interface name must have the following template `<Aggregate>RepositoryInterface`
- `Aggregate` can only have one `Repository`
- `Repository` interface must have only two methods:
    - `get(Uuid $id):AggregateRootClass`
    - `add(AggregateRootClass $aggregateRoot):void`

Typical `Repository` interface:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Repository;

use Grifix\Uuid\Uuid;
use Modules\Catalog\Domain\Product\Product;
use Modules\Draft\Application\Person\Ports\Projector\Exceptions\PersonDoesNotExistException;

interface ProductRepositoryInterface
{
    public function add(Product $product): void;

    /**
     * @throws PersonDoesNotExistException
     */
    public function get(Uuid $id): Product;
}

```

Typical `Repository` implementation:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Product\Application\Ports;

use Grifix\Framework\Infrastructure\AbstractRepository;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Repository\Exceptions\ProductDoesNotExistException;
use Modules\Catalog\Application\Product\Ports\Repository\ProductRepositoryInterface;
use Modules\Catalog\Domain\Product\Product;

final class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{

    public function add(Product $product): void
    {
        $this->doAdd($product);
    }

    public function get(Uuid $id): Product
    {
        return $this->doGet($id, ProductDoesNotExistException::class, Product::class);
    }
}

```


## <a id="beyond">Beyond</a>

`Beyond` is responsible for:
- perform side effects beyond the [Module](#module) boundaries

The `Beyond` is usually related to the specific [Aggregate](#aggregate) and performs side effects beyond the module
boundaries as a reaction to the [Domain Event](#domain-event)
Beyond is usually injected into the [Subscriber](#subscriber) as a dependency

typical beyond interface:

```php
<?php

<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Ports;

use Grifix\Uuid\Uuid;

interface UserBeyondInterface
{
    public function notifyAboutUserCreation(
        Uuid $userId,
        ?Uuid $correlationId = null,
    ): void;
}

```

typical beyond realization:

```php
<?php

declare(strict_types=1);

namespace Modules\Security\Infrastructure\User\Application\Ports;

use Grifix\Esb\EsbInterface;
use Grifix\Mailer\MailerInterface;
use Grifix\Security\Messages\UserCreatedEventMessage;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Ports\UserBeyondInterface;
use Modules\Security\Infrastructure\Common\SecurityChannels;

final readonly class DefaultUserBeyond implements UserBeyondInterface
{
    public function __construct(
        private EsbInterface $esb,
        private MailerInterface $mailer
    ) {
    }

    public function notifyAboutUserCreation(Uuid $userId, ?Uuid $correlationId = null): void
    {
        $this->esb->storeOutgoingMessage(
            new UserCreatedEventMessage($userId, $correlationId),
            SecurityChannels::OUTGOING_FIFO->value,
        );
        $this->mailer->send(
            'user_created',
            ['user_id' => $userId->toString()]
        );
    }
}

```

typical beyond usage:
```php
<?php

declare(strict_types=1);

namespace Modules\Security\Application\User\Subscribers;

use Modules\Security\Application\User\Ports\UserBeyondInterface;
use Modules\Security\Domain\User\Events\UserCreatedEvent;

final readonly class UserBeyondSubscriber
{
    public function __construct(
        private UserBeyondInterface $userBeyond,
    ) {
    }

    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->userBeyond->notifyAboutUserCreation($event->userId, $correlationId);
    }
}

```




## <a id="subscriber">Subscriber</a>

`Subscriber` is responsible for:

- handing [events](#domain-event) from [Aggregates](#aggregate)
- executing [Commands](#command)
- updating [Projectors](#projector)

`Subscriber` class must have the following name template `<SubscirpitonSubject>Subscriber` for
example `UserLifecycleSubscriber`

Typical `Subscriber`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Subscribers;

use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductProjectorInterface;
use Modules\Catalog\Domain\Product\Events\ProductCreatedEvent;
use Modules\Catalog\Domain\Product\Events\ProductDeletedEvent;
use Modules\Catalog\Domain\Product\Events\ProductNameChangedEvent;
use Modules\Catalog\Domain\Product\Events\ProductPriceChangedEvent;

final class ProductProjectorSubscriber
{

    public function __construct(private readonly ProductProjectorInterface $projector)
    {
    }

    public function onProductCreated(ProductCreatedEvent $event): void
    {
        $this->projector->createProduct(
            new ProductDto(
                $event->productId,
                $event->name,
                $event->price
            )
        );
    }

    public function onProductDeleted(ProductDeletedEvent $event): void
    {
        $this->projector->delete($event->productId);
    }

    public function onProductNameChanged(ProductNameChangedEvent $event): void
    {
        $this->projector->changeName($event->productId, $event->newName);
    }

    public function onProductPriceChanged(ProductPriceChangedEvent $event): void
    {
        $this->projector->changePrice($event->productId, $event->newPrice);
    }
}
```

# <a id="domain-layer">Domain Layer</a>

The `Domain Layer` is responsible for business logic and is totally independent of other `layers`.
The `Domain Layer` cannot be depended on any other [Layer](#layer) technically it means that you
**cannot import any class outside the `Domain` namespace**

`Domain Layer` cannot be coupled with the framework or third-party libraries.

`Domain Layer` can be coupled only with [generic types](#generic-type) and `Domain Layer` from
the [Shared Kernel Library](#shared-kernel-library)

## <a id="entity">Entity</a>

`Entity` is responsible for:

- storing and changing the application state
- performing business logic

`Entity` class must have the following template `<Aggregate><Entity>` for example `WalletTransfer`

Typical `Entity`:

```php
<?php

declare(strict_types=1);

namespace Modules\Garage\Domain\Vehicle\TechnicalInspection

use Grifix\Test\Tests\EventCollector\Dummies\OutsideInterface;
use Grifix\Uuid\Uuid;
use Grifix\Date\Date;
use Modules\Garage\Domain\Car\CarOutsideInterface;

final class TechnicalInspection {
   
    private function __construct(
        private CarOutsideInterface $outside
        private Uuid $id,
        private Date $date,
        private bool $successful,
    ) {
  
    }
  
    public function isSuccessful():bool{
        return $this->successful;
    }
  
    public function isValid():bool{
        return $this->date->isAfter($this->outside->currentDate()->subtract('1 year'));
    }
}
```

## <a id="value-object">Value Object</a>

`Value Object` has the same responsibility as [Entity](#entity), but it has two differences:

- it must be immutable
- it has no identifier

`Value Object` class must have the following template `<Aggregate><Entity><value_object>` for
example `WalletTransferStatus`

Typical `Value Object`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Name;

use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameIsNotUniqueException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooLongException;
use Modules\Catalog\Domain\Product\Name\Exceptions\ProductNameTooShortException;
use Modules\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductName
{
    public function __construct(
        private readonly ProductOutsideInterface $outside,
        private readonly string $value
    ) {
        $this->assertMinChars();
        $this->assertMaxChars();
        $this->assertIsUnique();
    }

    public function change(string $value): self
    {
        return new self($this->outside, $value);
    }

    private function assertMinChars(): void
    {
        $minChars = $this->outside->getProductNameMinChars();
        if (mb_strlen($this->value) < $minChars) {
            throw new ProductNameTooShortException($minChars);
        }
    }

    private function assertMaxChars(): void
    {
        $maxChars = $this->outside->getProductNameMaxChars();
        if (mb_strlen($this->value) > $maxChars) {
            throw new ProductNameTooLongException($maxChars);
        }
    }

    public function assertIsUnique(): void
    {
        if (!$this->outside->isProductNameUnique($this->value)) {
            throw new ProductNameIsNotUniqueException();
        }
    }

    public function toString(): string
    {
        return $this->value;
    }
}
```

## <a id="aggreate">Aggregate</a>

`Aggregate` is a bunch of [entities](#entity) [value objects](#value-object) that have joint business responsibility.

Each `Aggregate` must be placed into the separated [Package](#package)

All `Aggregate` `entities` and `value objects` can be used only by their `Aggregate`
should not be accessible by other `Aggregates` and should not be created outside their `Aggregate`.

[Application layer](#application-layer) cannot have access to any object of the `Aggregate` except of
the [Aggregate Root](#aggregate_root).

Typical `Aggregate` package:

- **Product**
    - **Events**
        - ProductCreatedEvent.php
        - ProductDeletedEvent.php
        - ProductEventInterface.php
    - **Exceptions**
        - ProductDeletedException.php
    - **Name**
        - **Exceptions**
            - ProductNameTooLongException.php
            - ProductNameTooShortException.php
        - ProductName.php
    - Product.php
    - ProductFactory.php
    - ProductOutsideInterface.php

## <a id="aggregate_root">Aggregate Root</a>

`Aggregate Root` is an entity that provides interface for all operations in the [Aggregate](#aggregate) and
produces [Domain Events](#domain-event)

- The `Aggregate Root` cannot have methods that return any type except of `void`
- The `Aggregate Root` cannot have `entities` or `value objects` from another `Aggregate`
- The `Aggregate Root` public method can take as arguments only:
    - scalar PHP types and arrays
    - `DTOs` from `aggregate` [package](#package)
    - PHP built in types like `DateTime`
    - [generic types](#generic-type)
    - [Aggregate Outside](#aggregate-outside) interface (only for the constructor)

Typical `Aggregate Root`:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\CartCreatedEvent;
use Modules\Cart\Domain\Cart\Events\ItemAddedToCartEvent;
use Modules\Cart\Domain\Cart\Events\ItemRemovedFromCartEvent;
use Modules\Cart\Domain\Cart\Exceptions\ItemAlreadyExistsException;
use Modules\Cart\Domain\Cart\Exceptions\ItemDoesNotExistException;
use Modules\Cart\Domain\Cart\Exceptions\ItemLimitExceedException;
use Modules\Cart\Domain\Cart\Item\CartItem;

final class Cart
{
    /**
     * @var CartItem[]
     */
    private array $items = [];

    private Money $total;

    public function __construct(private readonly CartOutsideInterface $outside, private readonly Uuid $id)
    {
        $this->total = Money::pln(0);
        $this->outside->publishEvent(new CartCreatedEvent($this->id, $this->total));
    }

    public function addItem(Uuid $productId): void
    {
        $this->assertItemLimitDoesNotExceeded();
        $this->assertHasNoItem($productId);
        $productPrice = $this->outside->getProductPrice($productId);
        $this->items[] = new CartItem($productId, $productPrice);
        $this->total = $this->total->add($productPrice);
        $this->outside->publishEvent(
            new ItemAddedToCartEvent(
                $this->id,
                $productId,
                $productPrice,
                $this->total
            )
        );
    }

    public function removeItem(Uuid $productId): void
    {
        $this->assertHasItem($productId);
        foreach ($this->items as $i => $item) {
            if ($item->productId->isEqualTo($productId)) {
                unset($this->items[$i]);
                $this->total = $this->total->subtract($item->price);
                $this->outside->publishEvent(
                    new ItemRemovedFromCartEvent(
                        $this->id,
                        $productId,
                        $this->total
                    )
                );
                return;
            }
        }
    }

    private function assertHasItem(Uuid $productId): void
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                return;
            }
        }
        throw new ItemDoesNotExistException($productId);
    }

    private function assertItemLimitDoesNotExceeded(): void
    {
        if (count($this->items) >= $this->outside->getItemLimit()) {
            throw new ItemLimitExceedException($this->outside->getItemLimit());
        }
    }

    private function assertHasNoItem(Uuid $productId): void
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                throw new ItemAlreadyExistsException($productId);
            }
        }
    }
}
```

## <a id="aggregate-factory">Aggregate Factory</a>

`Aggregate Factory` is responsible for creating [aggregates](#aggregate).

Typical `Aggregate Factory`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductFactory
{
    public function __construct(private readonly ProductOutsideInterface $outside)
    {
    }

    public function createProduct(Uuid $id, string $name, Money $price): Product
    {
        return new Product(
            $this->outside,
            $id,
            $name,
            $price
        );
    }
}
```

## <a id="aggregate-outside">Aggregate Outside</a>

`Aggregate Outside` is responsible for

- publishing [domain events](#domain-event)
- provide outside information to the [Aggregate](#aggregate)

The `Aggregate Outside` interface should be defined by the [Aggregate](#aggregate).
If `Aggregate` needs information from different sources, `Aggregate Outside` wraps these sources, converts data to the
format
defined by the `Aggregate` and pass this information to the `Aggregate`.

- The `Aggregate Outside` can be used only by `Aggregate` that defines its interface.
- The `Aggregate Outside` interface must be placed into the [Aggregate](#aggregate) [Package](#package).
- The `Aggregate Outside` interface name must have the following template `<Aggregate>OutsideInterface`
- The `Aggregate Outside` implementation must be placed into the [Infrastructure layer](#infrastructure-layer).
- The `Aggregate Outside` cannot perform operations with evident side effects, it can only ask for the data or inform
  what happened inside the `Aggregate` but not tell what to do.
- The `Aggregate Outside` should use [connectors](#connector) for communication to other [modules](#module) or external
  services

Typical `Aggregate Outside` interface:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Domain\Cart;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Domain\Cart\Events\CartEventInterface;

interface CartOutsideInterface
{
    public function getProductPrice(Uuid $productId): Money;

    public function getItemLimit(): int;

    public function publishEvent(CartEventInterface $event): void;
}
```

Typical Outside implementation:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Cart\Domain;

use Grifix\EventStore\EventStoreInterface;use Grifix\Money\Money\Money;use Grifix\Uuid\Uuid;use Modules\Cart\Domain\Cart\Cart;use Modules\Cart\Domain\Cart\CartOutsideInterface;use Modules\Cart\Domain\Cart\Events\CartEventInterface;use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;

final class CartOutside implements CartOutsideInterface
{
    public function __construct(
        private readonly int $itemLimit,
        private readonly CatalogConnectorInterface $catalogConnector,
        private readonly EventStoreInterface $eventStore
    ) {
    }

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->catalogConnector->getProductPrice($productId);
    }

    public function getItemLimit(): int
    {
        return $this->itemLimit;
    }

    public function publishEvent(CartEventInterface $event): void
    {
        $this->eventStore->storeEvent($event, Cart::class, $event->getCartId());
    }
}

```

## <a id="domain-event">Domain Event</a>

`Domain Event` is the [DTO](#dto) that represents the changes of [Aggregate](#aggregate) state after business operation.

`Domain Event` must have the following template `<Aggregate><Event>Event` for example `WalletCreditConfirmedEvent`

Typical `Domain Event`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Domain\Product\Events;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductCreatedEvent implements ProductEventInterface
{

    public function __construct(
        public readonly Uuid $productId,
        public readonly Money $price,
        public readonly string $name
    ) {
    }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
```

# <a id="infrastructure-layer">Infrastructure Layer</a>

The `Infrastructure Layer` contains:

- implementations of [ports](#port) defined in [Application layer](#application-layer)
- implementations of the [Aggregate Outside](aggregate-outside)
- [message senders](#message-senders)
- connectors for other [modules](#module) and external services

The `Infrastructure Layer` can be coupled with the framework and third-party libraries.

Typical `Infrastructure Layer` structure:

- **Infrastructure**
    - **Common**
        - **Connectors**
            - **Catalog**
                - CatalogConnector.php
                - CatalogConnectorInterface.php
            - **Facebook**
                - FacebookConnector.php
                - FacebookConnectorInterface.php
    - **Cart**
        - **Application**
            - **Ports**
                - CartProjector.php
                - CartRepository.php
                - CartBeyond.php
            - **Domain**
                - CartOutside.php
           


## <a id="connector">Connector</a>

`Connector` is responsible for communication with other [modules](#module) and external services.

It as a [Facade](https://en.wikipedia.org/wiki/Facade_pattern) that wraps another module [Client](#module-client) or  
external service provider library.

It serves as an [anti-corruption](https://deviq.com/domain-driven-design/anti-corruption-layer) layer between `modules`
and external services.

For communication with other [Module](#module) `Connector` uses this module [Client](#module-client) from
the [Communication Library](#communication-library)

Any `Connector` should have an interface for the ability to create a mock.
`Connector` interface should be placed in the same [package](#package) as the Connector implementation.

Typical `Connector`:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Infrasturcture\Common\Connectors\Catalog;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Sandbox\Catalog\CatalogClientInterface;

final class CatalogConnector implements CatalogConnectorInterface
{
    public function __construct(private readonly CatalogClientInterface $client)
    {
    }

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->client->getProduct($productId)->price;
    }

    public function getProductName(Uuid $productId): string
    {
        return $this->client->getProduct($productId)->name;
    }
}
```

Typical `Connector` mock:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Stubs;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;
use Modules\Cart\Infrasturcture\Common\Connectors\Catalog\CatalogConnectorInterface;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;

final class CatalogConnectorStub implements CatalogConnectorInterface
{
    /**
     * @var ProductFixture[]
     */
    private array $products = [];

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->findProduct($productId)->getPrice();
    }

    public function getProductName(Uuid $productId): string
    {
        return $this->findProduct($productId)->getName();
    }

    private function findProduct(Uuid $productId): ProductFixture
    {
        foreach ($this->products as $product) {
            if ($product->getId()->isEqualTo($productId)) {
                return $product;
            }
        }
        throw new \Exception('Product does not exist!');
    }

    public function createProduct(ProductFixture $product): void
    {
        $this->products[] = $product;
    }
}

```

# <a id="dto">DTO</a>

`DTO` (data transfer object) is a data structure without behavior. `DTO` class can have variables with only the
following
types:

- PHP scalar type, null or array
- PHP built-in classes like `DateTimeImmutable`
- another `DTO` from the same or child [package](#package) (except the [query result](#query-result) `DTO` that can have
  `DTO` from the [Projector](#projector) `package`)
- [Generic Type](#generic-type)

`DTO` class must have the following name template `<DtoSubject>Dto` for example `UserDto` (except of special `DTOs`
like [domain events](#domain-event) or [messages](#message))

`DTO` should be immutable

`DTO` class can only have a constructor with public readonly properties

Typical `DTO`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Application\Product\Ports\Projector;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductDto
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $name,
        public readonly Money $price
    ) {
    }
}
```

# <a id="generic-type">Generic Type</a>

`Generic Type` is the type that is known by each [application](#application) and [module](#module) in the organization.
Like `Uuid`, `Money`, `Date` etc.

`Generic Type` should be placed into the separated [libraries](#library).

`Generic Type` library should be under organization control.

`Generic Type` should be immutable.

`Generic Type` can wrap another class from the third-party library.

Typical `Generic Type`:

```php
<?php
declare(strict_types=1);

namespace Grifix\Uuid;

use Grifix\Uuid\Exceptions\InvalidUuidException;
use Ramsey\Uuid\Uuid as RamseyUuid;

final class Uuid
{
    private function __construct(private readonly string $value)
    {
        $this->assertIsValid($value);
    }

    public static function createFromString(string $value): self
    {
        return new self($value);
    }

    public static function createRandom(): self
    {
        return new self((string)RamseyUuid::uuid4());
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function isEqualTo(self $another): bool
    {
        return $this->value === $another->value;
    }

    private function assertIsValid(string $value): void
    {
        if (false === RamseyUuid::isValid($value)) {
            throw new InvalidUuidException($value);
        }
    }
}
```

# <a id="communication-library">Communication Library</a>

The `Communication Library` provides the [client](#module-client) for communication with the [module](#module)
and [messages](#message) that the `Module` can send and receive by the [Message Bus](#message-bus).

Typical `Communication Library` structure:

- **catalog**
    - **configs**
        - grifix_event_store.yaml
    - **src**
        - **Dtos**
            - CreateProductDto.php
            - ProductDto.php
        - **Messages**
            - ProductCreatedEventMessage.php
            - ProductNameChangedMessage.php
        - CatalogClientInterface.php
        - CatalogApplicationClient.php
        - CatalogHttpClient.php

## <a id="module-client">Module Client</a>

The `Module Client` provides an interface for synchronous communication with the [Module](#module)

For `modules` placed in one monolith `application`, the `Module Client` realization based on
the [Application Wrapper](#application-wrapper) is needed.

For `modules` placed in different `applications`, the `Module Client` realization based on the network protocol is
needed.

Typical `Module Client` based on the `Application Wrapper`:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Commands\Create\CreateProductCommand;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Queries\Get\GetProductQuery;
use Sandbox\Catalog\Dtos\CreateProductDto;
use Sandbox\Catalog\Dtos\ProductDto;

final class CatalogApplicationClient implements CatalogClientInterface
{
    public function __construct(private readonly ApplicationInterface $application)
    {
    }

    public function getProduct(Uuid $productId): ProductDto
    {
        $result = $this->application->ask(new GetProductQuery(ProductFilter::create()->setProductId($productId)));
        return new ProductDto(
            $result->product->id,
            $result->product->name,
            $result->product->price
        );
    }
  
    public function createProduct(CreateProductDto $dto): Uuid
    {
        $productId = Uuid::createRandom();
        $this->application->tell(
            new CreateProductCommand(
                $productId,
                $dto->name,
                $dto->price
            )
        );
        return $productId;
    }
}
```

Typical `Module Client` based on http protocol:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

use Grifix\Money\Money\Money;use Grifix\Uuid\Uuid;use Sandbox\Catalog\Dtos\CreateProductDto;use Sandbox\Catalog\Dtos\ProductDto;

final class CatalogApplicationClient implements CatalogClientInterface
{
    public function __construct(private readonly HttpClient $httpClient)
    {
    }

    public function getProduct(Uuid $productId): ProductDto
    {
        $json = $this->httpClient->get('https://products.com/v1/products/' . $productId->toString())->toJson();
        return new ProductDto(
            Uuid::fromString($json['id']),
            $json['name'],
            Money::createFromString($json['price'])
        );
    }
  
    public function createProduct(CreateProductDto $dto): Uuid
    {
        $json = $this->httpClient->post(
            'https://products.com/products/', 
            [
                'name' => $dto->name,
                'price' => $dto->price->toString()
            ]
        )->toJson();
  
        return Uuid::createFromString($json['productId'])
    }
}
```


## <a id="message">Message</a>

The `Message` is a [DTO](#dto) that serves for asynchronous communication between [modules](#module)
There are 4 types of `messages`:

### <a id="command-message"> Command message </a>

`Command Message` is used when [Module](#module) `A` wants to change the state of [Module](#module) `B`.

`Command Message` should be placed inside the [Communication Library](#communication-library) of the [Module](module)
that receives `Command Message`.

`Command Message` class must have the following name template `<Command>CommandMessage` for
example `ChangeProductNameCommandMessage`

Typical `Command Message`:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Messages;

use Grifix\Uuid\Uuid;

final class ChangeProductNameCommandMessage
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $newName,
    ) {
    }
}
```

### <a id="event-message">Event Message</a>

The `Event Message` is used when [Module](#module) `A` wants to inform other [modules](#module) about his state change.

`Event Message` should be placed inside the [Communication Library](#communication-library) of the [Module](#module)
that
sends `Event Message`.

The `Event Message` class must have the following name template `<Event>EventMessage` for
example `ProductNameChangedEventMessage`

Typical `Event Message`:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Messages;

use Grifix\Uuid\Uuid;

final class ProductNameChangedMessage
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $newName,
    ) {
    }
}
```

### <a id="request-message">Request Message</a>

The `Request Message` is used when [Module](#module) `A` wants to ask `Module` `B` about his state.

The `Request Message` should be placed inside the [Communication Library](#communication-library) of the `Module` that
receives `Request Message`.

Any `Request Message` must have the correlation identifier.

The `Request Message` class must have the following name template `<Request>RequestMessage` for
example `GetProductRequestMessage`

Typical `Request Message`:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Messages;

use Grifix\Uuid\Uuid;

final class GetProductRequestMessage
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly Uuid $correlationId,
    ) {
    }
}
```

### <a id="reply-message">Reply Message</a>

The `Reply Message` is used when [Module](#module) `A` answers to `Module` `B` on
his [Request Message](#request-message).

The `Reply Message` should be placed inside the [Communication Library](#communication-library) of the `Module` that
sends
the `Reply Message`.

Any `Reply message` must have the correlation identifier.
The correlation identifier must be the same as the correlation identifier of the [Request Message](#request-message).

The `Reply Message` class must have the following name template `<Request>ReplyMessage` for
example `GetProductReplyMessage`

Typical `Reply Message`:

```php
<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Messages\GetProductReplyMessage;

use Grifix\Uuid\Uuid;
use Sandbox\Catalog\Messages\GetProductReplyMessage\ProductDto

final class GetProductReplyMessage
{
    public function __construct(
        public readonly Uuid $correlationId,
        public readonly ProductDto $product,
    ) {
    }
}
```

# <a id="package">Package</a>

The `Package` is a bunch of classes that are strongly coupled to each other.

In most cases, the `Package` can use only its own classes or classes from child `packages`.

`Package` can group classes by functionality or type. In most cases, `Package` group classes by functionality.

Typical `Package` grouped by functionality:

- *Get*
    - GetProductQuery.php
    - GetProductQueryHandler.php
    - GetProductQueryResult.php

Typical `Package` grouped by type:

- *Commands*
    - **ChangeName**
    - **ChangePrice**
    - **Create**
    - **Delete**

# <a id="port">Port</a>

The `Port` is an interface that is defined in one [Layer](#layer) and implemented in another `Layer`.
Usually, `ports` are defined in [Application][#application-layer] and [Domain][#domain-layer] `layers` and implemented
in the [Infrastructure Layer](#infrastructure-layer)

# <a id="message-bus">MessageBus</a>

`Message Bus` is used for sending [messages](#message) between [modules](#modules).
Typically, `Message Bus` is used by [message senders](#message-sender)

You should define a stream type and stream name for sending the `Message`.

Each module has its own stream in the `Message Bus`.

Stream name should be defined in the `Stream Name` class:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Infrastructure\Common;

final class StreamName
{
    private function __construct()
    {
    }

    public static function get(): string
    {
        return 'sandbox.catalog';
    }
}
```

Event Aggregate identifier is usually used as the stream identifier:

```php
public function onProductCreated(ProductCreatedEvent $event): void
    {
        $this->messageBus->sendMessage(
            StreamName::get(),
            $event->productId,
            new ProductCreatedEventMessage(
                $event->productId,
                $event->name,
                $event->price
            )
        );
    }
```

# <a id="library"> Library</a>

The `Library` is a bunch of classes that can be used by different [modules](#module) and [applications](#application)

Till all organization `modules` are in the one monolith `Application` `libraries` can be placed in the same repository
as
`Application` in the `libraries` directory.

After moving one or more `modules` into different `applications`, libraries that are used in these `modules` should be
published as composer packages.

# <a id="shared-kernel-library">Shared Kernel Library</a>

The `Shared Kernel Library` is a [<odule](#module) separated by [layers](#layers) but placed outside
the [Application](#application) as a [Library](#library)

This `library` can contain different abstract classes and common services like the command or query bus that can be used
in appropriate `layers` in other `modules`.

In the sample application `grifix/framework` module is used as the shared kernel library.

# <a id="application-wrapper">Application Wrapper<a/>

`Application Wrapper` is a service that is used for communication between [UI](#ui-layer)
and [Application](#application-layer) `layers`.

It just wraps [command](#command-bus) and [query](#query-bus) buses.

# <a id="command-bus">Command Bus<a/>

The `Command Bus` is used for sending [commands](#command)

# <a id="query-bus">Query Bus<a/>

The `Query Bus` is used for sending [queries](#query)

# <a id="testing">Testing</a>

There are two types of tests:
[Unit tests](#unit-tests) for business logic testing.
[Behavioral tests](#behavioral-tests) for testing [Module](#module) [layers](#layer) integration.

## <a id="unit-tests"> Unit Tests</a>

- Each [Aggregate](#aggregate) must be covered by `unit tests` in 100%
- Each `aggregate` method should have a separated `Unit Test`
- `Unit Test` can have access to the `Aggregate` only by the [Aggregate Root](#aggregate-root)
- `Unit Test` can check aggregate state only by listening [domain events](#domain-event)
- `Aggregates` in tests must be created by the [aggregate builders](#aggregate-builder)
- [DTOs](#dto) that are passed to the `Aggregate` must be created by [DTO builders](#dto-builder)
- The [Aggregate Outside](#aggregate-outside) stubs must be created by the
  [aggregate outside builders](#aggregate-outside-builder)

Typical `Unit Tests` structure:

- **catalog**
    - **tests**
        - **Unit**
            - **Domain**
                - **Product**
                    - **Builders**
                        - ProductBuilder.php
                        - ProductOutsideBuilder.php
                    - ChangeNameTest.php
                    - ChangePrice.php
                    - CreateProductTest.php
                    - DeleteProductTest.php

Unit test file should have the following template `<TestingMethodName>Test.php`

Typical `Unit Test`:

```php
<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User;

use Grifix\Date\DateInterval\DateInterval;use Grifix\Date\DateTime\DateTime;use Grifix\Email\Email;use Grifix\Test\AbstractTestCase;use Grifix\Uuid\Uuid;use Modules\Security\Domain\User\Email\UserEmailIsNotUniqueException;use Modules\Security\Domain\User\Events\UserEmailChangeInitiatedEvent;use Modules\Security\Domain\User\Exceptions\UserEmailIsNotConfirmedException;use Modules\Security\Domain\User\Exceptions\UserIsDisabledException;use Modules\Security\Domain\User\Exceptions\UserIsSuspendedException;use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;use Modules\Security\Tests\Unit\Domain\User\Builders\UserBuilder;use Modules\Security\Tests\Unit\Domain\User\Builders\UserOutsideBuilder;

final class ChangeUserEmailTest extends AbstractTestCase
{
    public function testItWorks(): void
    {
        $userId = Uuid::createFromString('74667409-c1b9-4d01-bca2-a86a2d74fc93');
        $newEmail = Email::create('admin@example.com');
        $confirmationToken = 'token';
        $user = UserBuilder::create()
            ->setId($userId)
            ->setEmail(Email::create('user@example.com'))
            ->shouldHaveConfirmedEmail()
            ->setOutside(
                UserOutsideBuilder::create()
                    ->setGenerateTokenResult($confirmationToken)
                    ->build()
            )
            ->build();

        $user->initiateEmailChange($newEmail, 'password');

        self::assertEventsWerePublished(
            $user,
            new UserEmailChangeInitiatedEvent(
                $userId,
                $newEmail,
                $confirmationToken
            ),
        );
    }

    /**
     * @dataProvider failsProvider
     */
    public function testItFails(
        UserBuilder $builder,
        Email $newEmail,
        string $password,
        string $expectedException,
        ?string $expectedExceptionMessage = null
    ): void {
        $this->expectExceptionWithMessage($expectedException, $expectedExceptionMessage);
        $builder->build()->initiateEmailChange($newEmail, $password);
    }

    public function failsProvider(): array
    {
        return [
            'not confirmed email' => [
                UserBuilder::create(),
                Email::create('admin@example.com'),
                'password',
                UserEmailIsNotConfirmedException::class,
                'User email is not confirmed!'
            ],
            'user is disabled' => [
                UserBuilder::create()
                    ->shouldHaveConfirmedEmail()
                    ->shouldBeDisabled()
                ,
                Email::create('admin@example.com'),
                'password',
                UserIsDisabledException::class,
                'User is disabled!'
            ],
            'user is suspended' => [
                UserBuilder::create()
                    ->shouldHaveConfirmedEmail()
                    ->shouldBeSuspended(DateInterval::create(hours: 1))
                    ->setOutside(
                        UserOutsideBuilder::create()
                            ->setGetCurrentDateTimeResult(DateTime::create(2001, 1, 1))
                            ->build()
                    )
                ,
                Email::create('admin@example.com'),
                'password',
                UserIsSuspendedException::class,
                'User is suspended until [2001-01-01T01:00:00+00:00]!'
            ],
            'not unique email' => [
                UserBuilder::create()
                    ->setOutside(
                        UserOutsideBuilder::create()
                            ->setDoesUserWithEmailExistResult(true)
                            ->build()
                    )
                    ->shouldHaveConfirmedEmail()
                ,
                Email::create('admin@example.com'),
                'password',
                UserEmailIsNotUniqueException::class,
                'Email is not unique!'
            ],
            'wrong password' => [
                UserBuilder::create()
                    ->setOutside(
                        UserOutsideBuilder::create()
                            ->setIsPasswordValidResult(false)
                            ->build()
                    )
                    ->shouldHaveConfirmedEmail()
                ,
                Email::create('admin@example.com'),
                'wrong_password',
                WrongPasswordException::class,
                'Wrong password!'
            ],
        ];
    }
}
```

### <a id="aggregate-builder">Aggregate Builder</a>

The `Aggregate Builder` is responsible for building `Aggregate` in any valid state.

Typical `Aggregate Builder`:

```php
<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User\Builders;

use Grifix\Date\DateInterval\DateInterval;use Grifix\Email\Email;use Grifix\Ip\IpAddress;use Grifix\Test\EventCollector;use Grifix\Uuid\Uuid;use Mockery\MockInterface;use Modules\Security\Domain\User\Events\UserCreatedEvent;use Modules\Security\Domain\User\Password\Exceptions\WrongPasswordException;use Modules\Security\Domain\User\User;use Modules\Security\Domain\User\UserOutsideInterface;

final class UserBuilder
{
    private Uuid $id;

    private UserOutsideInterface|MockInterface $outside;

    private Email $email;

    private string $password = 'password';

    private bool $shouldHaveConfirmedEmail = false;

    private bool $shouldBeDisabled = false;

    private ?int $shouldHaveFailedAuthenticationAttempts = null;

    private ?Uuid $shouldHaveAssignedRole = null;

    private ?DateInterval $shouldBeSuspended = null;

    private bool $shouldHaveInitializedPasswordReset = false;

    private function __construct()
    {
        $this->email = Email::create('user@example.com');
        $this->id = Uuid::createFromString('d6a40721-b3ff-46eb-b569-484298b9be63');
        $this->outside = UserOutsideBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function shouldHaveConfirmedEmail(): self
    {
        $this->shouldHaveConfirmedEmail = true;
        return $this;
    }

    public function shouldHaveFailedAuthenticationAttempts(int $attemptsNumber = 1): self
    {
        $this->shouldHaveFailedAuthenticationAttempts = $attemptsNumber;
        $this->shouldHaveConfirmedEmail();
        return $this;
    }

    public function shouldHaveAssignedRole(?Uuid $roleId = null): self
    {
        if (null === $roleId) {
            $roleId = Uuid::createRandom();
        }
        $this->shouldHaveAssignedRole = $roleId;
        return $this;
    }

    public function shouldBeDisabled(): self
    {
        $this->shouldBeDisabled = true;
        return $this;
    }

    public function shouldBeSuspended(?DateInterval $interval = null): self
    {
        if (null === $interval) {
            $interval = DateInterval::create(minutes: 1);
        }
        $this->shouldBeSuspended = $interval;
        return $this;
    }

    public function setOutside(UserOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function setEmail(Email $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function shouldHaveInitializedPasswordReset(): self
    {
        $this->shouldHaveConfirmedEmail();
        $this->shouldHaveInitializedPasswordReset = true;
        return $this;
    }

    public function build(): User
    {
        $result = new User(
            $this->outside,
            $this->id,
            $this->email,
            $this->password
        );

        /** @var UserCreatedEvent $userCreatedEvent */
        $userCreatedEvent = EventCollector::getEvents($result)[0];

        if ($this->shouldHaveConfirmedEmail) {
            $result->confirmEmail($userCreatedEvent->emailConfirmationToken);
        }

        if ($this->shouldBeDisabled) {
            $result->disable();
        }

        if ($this->shouldBeSuspended) {
            $result->suspend($this->shouldBeSuspended);
        }

        if ($this->shouldHaveFailedAuthenticationAttempts) {
            $this->outside->allows('isPasswordValid')->byDefault()->andReturn(false);
            for ($i = 0; $i < $this->shouldHaveFailedAuthenticationAttempts; $i++) {
                try {
                    $result->authenticate('wrong_password', IpAddress::create('127.0.0.1'));
                } catch (WrongPasswordException) {
                }
            }
        }

        if ($this->shouldHaveAssignedRole) {
            $result->assignRole($this->shouldHaveAssignedRole);
        }

        if ($this->shouldHaveInitializedPasswordReset) {
            $result->initPasswordReset(IpAddress::create('127.0.0.1'));
        }

        EventCollector::clearEvents($result);
        return $result;
    }
}
```

### <a id="dto-builder">DTO Builder</a>

`DTO Builder` is used for building `aggregate` `DTOs`

Typical DTO builder:

```php
#TODO create an example
```

### <a id="aggregate-outside-builder">Aggregate Outside Builder</a>

The `Aggregate Outside Builder` is used for building [aggregate outside](#aggregate-outside) stub.

Typical `Aggregate Outside Builder`:

```php
<?php

declare(strict_types=1);

namespace Modules\Security\Tests\Unit\Domain\User\Builders;

use Grifix\Date\DateTime\DateTime;use Grifix\Test\AbstractOutsideBuilder;use Mockery\MockInterface;use Modules\Security\Domain\User\UserOutsideInterface;

final class UserOutsideBuilder extends AbstractOutsideBuilder
{
    public const isPasswordValid = 'isPasswordValid';
    public const getCurrentDateTime = 'getCurrentDateTime';

    protected string $generatePasswordResult = '94NvMCUIfdfs7878Dkjfisdkfjl';

    protected bool $isPasswordSecureResult = true;

    protected string $hashPasswordResult = '268f85305a4e47b7bb345ee63fae837e';

    protected bool $isPasswordValidResult = true;

    protected bool $doesUserWithEmailExistResult = false;

    protected bool $hasRoleAccessResult = true;

    protected DateTime $getCurrentDateTimeResult;

    protected string $generateTokenResult = '7174dc8c3a1c4375bdb25a7caaec5843';

    protected function __construct()
    {
        $this->getCurrentDateTimeResult = DateTime::create(2000, 1, 1);
    }

    public function setGeneratePasswordResult(string $generatePasswordResult): UserOutsideBuilder
    {
        $this->generatePasswordResult = $generatePasswordResult;
        return $this;
    }

    public function setIsPasswordSecureResult(bool $isPasswordSecureResult): UserOutsideBuilder
    {
        $this->isPasswordSecureResult = $isPasswordSecureResult;
        return $this;
    }

    public function setHashPasswordResult(string $hashPasswordResult): UserOutsideBuilder
    {
        $this->hashPasswordResult = $hashPasswordResult;
        return $this;
    }

    public function setIsPasswordValidResult(bool $isPasswordValidResult): UserOutsideBuilder
    {
        $this->isPasswordValidResult = $isPasswordValidResult;
        return $this;
    }

    public function setDoesUserWithEmailExistResult(bool $doesUserWithEmailExistResult): UserOutsideBuilder
    {
        $this->doesUserWithEmailExistResult = $doesUserWithEmailExistResult;
        return $this;
    }

    public function setGetCurrentDateTimeResult(DateTime $getCurrentDateTimeResult): UserOutsideBuilder
    {
        $this->getCurrentDateTimeResult = $getCurrentDateTimeResult;
        return $this;
    }

    public function setHasRoleAccessResult(bool $hasRoleAccessResult): UserOutsideBuilder
    {
        $this->hasRoleAccessResult = $hasRoleAccessResult;
        return $this;
    }


    public function setGenerateTokenResult(string $generateTokenResult): UserOutsideBuilder
    {
        $this->generateTokenResult = $generateTokenResult;
        return $this;
    }

    public function build(): UserOutsideInterface|MockInterface
    {
        return $this->doBuild(UserOutsideInterface::class);
    }
}
```

## <a id="behavioral-tests">Behavioral Tests</a>

`Behavioral tests` are responsible for describing `module` use cases and testing integration between [Module](#module)
[layers](#layer).
The Gherkin language is used for describing use cases.

Typical behavioral test structure:

- **tests**
    - **Behavioral**
        - **Contexts**
            - **Product**
                - **Http**
                    - CreateProductHttpContext.php
                    - ChangeProductNameHttpContext.php
                - **MessageBus**
                    - CreateProductMessageBusContext.php
                    - ChangeProductNameMessageBusContext.php
                - ProductFixtureContext.php
        - **features**
            - **product**
                - create.feature
                - change-name.feature
        - **Fixtures**
            - ProductFixture.php
        - behat.yml
        - CatalogApplicationClient.php
        - CatalogHttpTestClient.php

### <a id="behat-scenario">Gherkin Scenario</a>

`Gherkin Scenario` can use only business language there should not be any mention of infrastructure concepts like an HTTP request,
response, Buses, database, etc.

Bad scenario:

```
Scenario: Wrong password
    Given there is a record in "users" table:
    |login|password|
    |admin|12345678|
    When I send request to endpoint "api/singIn/":
    {
        "login": "admin"
        "password": "1111111"
    }
    Then I should get the response with code "400":
    {
        "error":"Invalid password"
    }
```

Good scenario:

```
Scenario: Wrong password
    Given there is an user with login "admin" and password "12345678"
    When i sing in with login "admin" and password "1111111"
    Then I should get an error that my password is wrong
```

One scenario should test only one interaction with the system (e.g. one request if we test HTTP API)

Bad scenario:

```
Scenario: test it all
    When I create a user with login "admin" and password "12345678"
    Then there should be user witch login "admin" and password "12345678"
    When I sing in with login "admin" and password "12345678"
    Then I should be signed in as "admin"
    When I post the message "Hello I'm here"
    Then there should be message "Hello I'm here" posed by user "admin"
```

Good scenario:

```
Scenario:  create user
    When I create a user with login "admin" and password "12345678"
    Then there should be use with login "admin" and password "12345678"

Scenario: sing in
    Given there is a user with login "admin" and password "12345678"
    When I sing in with login "admin" and password "12345678"
    Then I should be signed in as "admin"

Scenario: post a message
    Given there is a user with login "admin" and password "12345678"
    And I am logged in as user "admin"
    When I post the message "Hello I'm here"
    Then there should be message "Hello I'm here" posed by user "admin"
```

All parameters that have influence on the scenario must be described in `Given` or `Background` parts

Bad scenario:

```
Scenario: post a message (user admin was created by the build script)
    Given I am logged in as user "admin"
    When I post the message "Hello I'm here"
    Then there should be message "Hello I'm here" posed by user "admin"
```

Good scenario:

```
Scenario: post a message
    Given there is a user with login "admin" and password "12345678"
    And I am logged in as user "admin"
    When I post the message "Hello I'm here"
    Then there should be message "Hello I'm here" posed by the user "admin"
```

If the parameter has no influence on the tested scenario, it should be created by
the [Fixture Builder](#fixture-builder)
and should not be specified in the scenario

Bad scenario: (first_name and last_name don't matter in the sign scenario)

```
Scenario: sing in
    Given there is a user:
    """
        login: admin
        password: 12345678
        first_name: John
        last_name: Connor
    """
    When I sing in with login "admin" and password "12345678"
    Then I should be signed in as "admin"
```

Good scenario:

```
Scenario: sing in
    Given there is a user:
    """
        login: admin
        password: 12345678
    """
    When I sing in with login "admin" and password "12345678"
    Then I should be signed in as "admin"
```

Tabular data should be in tables, non-tabular data should be in yaml:

Bad scenario:

```
Scenario: bad
    Given there is a user:
    |login|password|
    |admin|12345678|
    And there are products:
    """
    - name: Apple
      price: 3
    - name: Potato
      price: 5
"""
```

Good scenario:

```
Scenario: good
    Given there is a user:
    """
    login: admin
    password: 12345678
    """
    And there are products:
    |name  |price|
    |apple | 3   |
    |potato| 5   |

```

### <a id="behat-contexts">Behat Contexts</a>

There are the following behat context types:

- [Fixture context](#fixture-context)
- [Feature context](#Feature-context)

`Behat Context` can interact with [Module](#module) only through [Application Wrapper](#application-wrapper) and API
that it tests.

All `Behat Context` communications with other `modules` and external services should be mocked.

The `Module` cannot use `Behat Contexts` from other `modules` except of [Shared Kernel Library](#shared-kernel-library)

#### <a id="fixture-context">Fixture Context</a>

The `Fixture Context` is responsible for all `Given` steps in [scenarios](#behat-scenario). It prepares the necessary
[Module](#module) state before interaction with API.

The `Fixture Context` can interact with the `Module` only using the [Fixture Factory](#fixture-factory)

Typical `Fixture Context`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product;

use Behat\Behat\Context\Context;
use Grifix\Money\Money\Money;
use Grifix\Test\Fixture\FixtureFactory;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;

final class ProductFixtureContext implements Context
{
    public function __construct(private readonly FixtureFactory $fixtureFactory)
    {
    }

    /**
     * @Given /^there is a product with name "([^"]*)"$/
     */
    public function thereIsAProductWithName(string $name): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setName($name)->build();
    }

    /**
     * @Given /^there is a product "([^"]*)" with name "([^"]*)"$/
     */
    public function thereIsAProductAliasWithName(string $productAlias, string $name)
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setName($name)
            ->build($productAlias);
    }

    /**
     * @Given /^there is a deleted product "([^"]*)"$/
     */
    public function thereIsADeletedProduct(string $productAlias): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->shouldBeDeleted()
            ->build($productAlias);
    }

    /**
     * @Given /^there is a product "([^"]*)" with price "([^"]*)"$/
     */
    public function thereIsAProductWithPrice(string $productAlias, string $price): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setPrice(Money::createFromString($price))
            ->build($productAlias);
    }

    /**
     * @Given /^there is a product "([^"]*)"$/
     */
    public function thereIsAProduct(string $productAlias): void
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)->build($productAlias);
    }
}
```

#### <a id="feature-context">Feature context</a>

The `Feature Context` is responsible for `When` and `Then` steps.

`When` step should send the request to the testing API using [ApiTestClient](#api-test-client)

`Then` step should check the response from the testing API and check the [module](#module) state
using the [Application Test Client](#application-test-client)

`Feature Context` name should have the following template `<Feature><ApiType>FeatureContext` for
example `ChangeProductPriceHttpContext`

Typical `Feature Context`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral\Contexts\Product\Http;

use Behat\Behat\Context\Context;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Tests\Behavioral\CatalogApplicationTestClient;
use Modules\Catalog\Tests\Behavioral\CatalogFixtureRegistry;
use Modules\Catalog\Tests\Behavioral\CatalogHttpTestClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class ChangeProductNameHttpContext implements Context
{
    public function __construct(
        private readonly CatalogHttpTestClient $httpClient,
        private readonly CatalogApplicationTestClient $applicationClient,
        private readonly CatalogFixtureRegistry $fixtureRegistry
    ) {
    }

    /**
     * @When /^I change product "([^"]*)" name to "([^"]*)"$/
     */
    public function iChangeProductNameTo(string $productAlias, string $newName)
    {
        $this->httpClient->changeProductName(
            $this->fixtureRegistry->getProductFixture($productAlias)->getId(),
            ['name' => $newName]
        );
    }

    /**
     * @Then /^the product "([^"]*)" name should be "([^"]*)"$/
     */
    public function theProductNameShouldBe(string $productAlias, string $expectedName): void
    {
        Assert::assertEquals($expectedName, $this->applicationClient->getProductByAlias($productAlias)->name);
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
    }

    /**
     * @When /^I change non\-existing product name$/
     */
    public function iChangeNonExistingProductNameTo(): void
    {
        $this->httpClient->changeProductName(
            Uuid::createRandom(),
            $this->httpClient->buildChangeProductNamePayload()
        );
    }
}
```

### <a id="application-test-client">Application Test Client</a>

`Application Test Client` wraps [Application Wrapper](#application-wrapper) to simplify
the [Behat Context](#behat-context) code.

Typical `Application Test Client`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\ProductDto;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter;
use Modules\Catalog\Application\Product\Queries\Find\FindProductsQuery;
use Modules\Catalog\Application\Product\Queries\Get\GetProductQuery;
use Modules\Catalog\Tests\Behavioral\Fixtures\ProductFixture;

final class CatalogApplicationTestClient
{

    public function __construct(
        private readonly ApplicationInterface $application,
        private readonly ObjectRegistryInterface $objectRegistry
    ) {
    }

    public function getProductById(Uuid $productId): ProductDto
    {
        return $this->application->ask(
            new GetProductQuery(ProductFilter::create()->setProductId($productId))
        )->product;
    }

    public function getProductByAlias(string $productAlias): ProductDto
    {
        return $this->getProductById(
            $this->objectRegistry->getObject(ProductFixture::class, $productAlias)->getId()
        );
    }

    public function hasProductWithAlias(string $productAlias): bool
    {
        $result = $this->application->ask(
            new FindProductsQuery(
                ProductFilter::create()->setProductId(
                    $this->objectRegistry->getObject(ProductFixture::class, $productAlias)->getId()
                )
            )
        );
        return count($result->products) > 0;
    }
}
```

### <a id="api-test-client">API Test Client</a>

`API Test Client` wraps testing API and simplify the [behat context](#behat-context) code,
builds request payload fixtures and stores last API response.

Typical `API Test Client`:

```php
<?php

declare(strict_types=1);

namespace Modules\Catalog\Tests\Behavioral;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Test\TestHttpClient;
use Grifix\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

final class CatalogHttpTestClient
{
    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }

    /**
     * @param string[] $remove
     */
    public function buildCreateProductPayload(
        array $override = [],
        array $remove = [],
    ): array {
        return $this->httpClient->buildPayload(
            [
                'name' => 'DDD in action',
                'price' => [
                    'amount' => '100',
                    'currency' => 'USD',
                ]
            ],
            $override,
            $remove
        );
    }

    public function createProduct(array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products',
            json_encode($payload)
        );
    }

    public function buildChangeProductNamePayload(): array
    {
        return [
            'name' => 'Cinderella'
        ];
    }

    public function changeProductName(Uuid $productId, array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products/' . $productId->toString() . '/name',
            json_encode($payload)
        );
    }

    public function buildChangeProductPricePayload(array $override = [], array $remove = []): array
    {
        $payload = [
            'price' => [
                'amount' => '100',
                'currency' => 'USD'
            ]
        ];
        return $this->httpClient->buildPayload($payload, $override, $remove);
    }

    public function changeProductPrice(Uuid $productId, array $payload): Response
    {
        return $this->httpClient->post(
            '/api/v1/catalog/products/' . $productId->toString() . '/price',
            json_encode($payload)
        );
    }

    public function deleteProduct(Uuid $productId): Response
    {
        return $this->httpClient->delete('/api/v1/catalog/products/' . $productId->toString());
    }

    public function findProducts(array $query = []): Response
    {
        return $this->httpClient->get('/api/v1/catalog/products', $query);
    }

    public function getLastResponse(): Response
    {
        return $this->httpClient->getLastResponse();
    }

    public function getLastResponseJson(): ArrayWrapper
    {
        return $this->httpClient->getLastResponseJson();
    }

    public function getLastResponseStatusCode(): int
    {
        return $this->httpClient->getLastResponse()->getStatusCode();
    }
}

```

### <a id="fixture-factory">Fixture Factory</a>

`Fixture Factory` is responsible for [fixture](#fixture) creation.

Typical `Fixture Factory` usage:

```php
 /**
     * @Given /^there is a product "([^"]*)" with name "([^"]*)"$/
     */
    public function thereIsAProductAliasWithName(string $productAlias, string $name)
    {
        $this->fixtureFactory->createFixture(ProductFixture::class)
            ->setName($name)
            ->build($productAlias);
    }

```

### <a id="fixture">Fixture</a>

`Fixture` is used to create the [module](#module) state using [application wrapper](#application-wrapper) or
[Connector](#connector) mocks.
The created `Fixture` can be placed into the [Fixture Registry](#fixture-registry) with an alias for use in
other [behat contexts](#behat-context) and steps.

Typical `Fixture`

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral\Fixtures;

use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;
use Modules\Cart\Application\Cart\Commands\AddItem\AddItemCommand;
use Modules\Cart\Application\Cart\Commands\Create\CreateCartCommand;

final class CartFixture extends AbstractFixture
{
    private Uuid $id;

    private ?int $addedItemQuantity = null;

    /**
     * @var string[]
     */
    private array $addedProductsAliases = [];

    /**
     * @var Uuid[]
     */
    private array $addedProductsIds = [];

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }


    public function withAddedItemQuantity(int $addedItemQuantity): self
    {
        $this->addedItemQuantity = $addedItemQuantity;
        return $this;
    }

    /**
     * @param string[] $addedProductsAliases
     */
    public function withAddedProducts(array $addedProductsAliases): self
    {
        $this->addedProductsAliases = $addedProductsAliases;
        return $this;
    }

    /**
     * @param Uuid[] $addedProductsIds
     */
    public function withAddedProductsIds(array $addedProductsIds): self
    {
        $this->addedProductsIds = $addedProductsIds;
        return $this;
    }

    protected function init(): void
    {
        $this->id = Uuid::createRandom();
    }

    protected function doBuild(): void
    {
        $this->application->tell(new CreateCartCommand($this->id));

        if (null !== $this->addedItemQuantity) {
            for ($i = 0; $i < $this->addedItemQuantity; $i++) {
                $product = $this->fixtureFactory->createFixture(ProductFixture::class)->build();
                $this->application->tell(new AddItemCommand($this->id, $product->getId()));
            }
        }

        if (!empty($this->addedProductsAliases)) {
            foreach ($this->addedProductsAliases as $addedProductsAlias) {
                $this->application->tell(
                    new AddItemCommand(
                        $this->id,
                        $this->objectRegistry->getObject(ProductFixture::class, $addedProductsAlias)->getId()
                    )
                );
            }
        }

        if (!empty($this->addedProductsIds)) {
            foreach ($this->addedProductsIds as $addedProductsId) {
                $this->application->tell(
                    new AddItemCommand(
                        $this->id,
                        $addedProductsId
                    )
                );
            }
        }
    }
}

```

### <a id="fixture-registry">Fixture Registry</a>

`Fixture Registry` provides access to all created [fixtures](#fixture) by alias.

Typical `Fixture Registry`:

```php
<?php

declare(strict_types=1);

namespace Modules\Cart\Tests\Behavioral;

use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Modules\Cart\Tests\Behavioral\Fixtures\CartFixture;
use Modules\Cart\Tests\Behavioral\Fixtures\ProductFixture;

final class CartFixtureRegistry
{
    public function __construct(private readonly ObjectRegistryInterface $objectRegistry)
    {
    }

    public function getCartFixture(string $alias): CartFixture
    {
        return $this->objectRegistry->getObject(CartFixture::class, $alias);
    }


    public function getProductFixture(string $alias): ProductFixture
    {
        return $this->objectRegistry->getObject(ProductFixture::class, $alias);
    }
}

```
