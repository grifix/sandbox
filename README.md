# The task
Create a shopping cart application with an API (HTTP).
# Guidelines
- no frontend required, just expose every action through an API
- code should be written in PHP
- you can over-engineer it, we want to see your programming skills
- tests are cool

# API interface
## Products
The catalog contains the following products:

1. The Godfather 59.99 PLN
1. Steve Jobs 49.95 PLN
1. The Return of Sherlock Holmes 39.99 PLN
1. The Little Prince 29.99 PLN
1. I Hate Myselfie! 19.99 PLN
1. The Trial 9.99 PLN

## Specification
Client should be able to:

1. add product to the catalog
1. remove product from the catalog
1. update product name
1. update product price
1. list all products in the catalog as a paginated list with at most 3 products per page
1. create a cart
1. add product to the cart
1. remove product from the cart
1. list all products in the cart
    - cart can contain a max. of 3 products
    - cart should display a total price of all products in it

