<?php

declare(strict_types=1);

namespace Sandbox\Security\Client;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Sandbox\Security\Client\Exceptions\AuthorizationFailedException;

final class SecurityClientStub implements SecurityClientInterface
{
    /**
     * @var array<string, string[]>
     */
    private array $permissions = [];

    public function authorize(string $accessToken, string $permission, ?IpAddress $ip): void
    {
        if (!in_array($permission, $this->permissions)) {
            throw new AuthorizationFailedException();
        }
    }

    public function grantPermission(string $accessToken, string $permission): void
    {
        if (!array_key_exists($accessToken, $this->permissions)) {
            $this->permissions[$accessToken] = [];
        }
        $this->permissions[$accessToken][] = $permission;
    }

    public function hasPermission(string $accessToken, string $permission): bool
    {
        if (!array_key_exists($accessToken, $this->permissions)) {
            return false;
        }

        return in_array($permission, $this->permissions[$accessToken]);
    }

    public function getUserId(string $accessToken): Uuid
    {
        return Uuid::createRandom();
    }
}
