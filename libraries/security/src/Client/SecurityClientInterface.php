<?php

declare(strict_types=1);

namespace Sandbox\Security\Client;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Sandbox\Security\Client\Exceptions\AuthorizationFailedException;

interface SecurityClientInterface
{
    /**
     * @throws AuthorizationFailedException
     */
    public function authorize(string $accessToken, string $permission, ?IpAddress $ip): void;

    public function hasPermission(string $accessToken, string $permission): bool;

    public function getUserId(string $accessToken): Uuid;
}
