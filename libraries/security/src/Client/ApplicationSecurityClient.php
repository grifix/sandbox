<?php

declare(strict_types=1);

namespace Sandbox\Security\Client;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Modules\Security\Application\User\Commands\Authorize\AuthorizeUserCommand;
use Modules\Security\Application\User\Ports\Projector\Exceptions\UserDoesNotExistException;
use Modules\Security\Application\User\Ports\Projector\UserFilter;
use Modules\Security\Application\User\Queries\DecodeAccessToken\DecodeAccessTokenQuery;
use Modules\Security\Application\User\Queries\Get\GetUserQuery;
use Modules\Security\Domain\User\Exceptions\UserAuthorizationFailedException;
use Sandbox\Security\Client\Exceptions\AuthorizationFailedException;

final readonly class ApplicationSecurityClient implements SecurityClientInterface
{
    public function __construct(
        private ApplicationInterface $application,
    ) {
    }

    /**
     * @throws AuthorizationFailedException
     */
    public function authorize(string $accessToken, string $permission, ?IpAddress $ip): void
    {
        try {
            $this->application->tell(
                new AuthorizeUserCommand(
                    $this->getUserId($accessToken),
                    $permission,
                    $ip,
                ),
            );
        } catch (UserDoesNotExistException|UserAuthorizationFailedException) {
            throw new AuthorizationFailedException();
        }
    }

    public function hasPermission(string $accessToken, string $permission): bool
    {
        $user = $this->application->ask(
            new GetUserQuery(
                UserFilter::create()
                    ->withUserId($this->getUserId($accessToken))
                    ->withPermissions(true),
            ),
        )->user;

        if (!$user->permissions) {
            return false;
        }
        return in_array($permission, $user->permissions);
    }

    public function getUserId(string $accessToken): Uuid
    {
        return $this->application->ask(new DecodeAccessTokenQuery($accessToken))->userId;
    }
}
