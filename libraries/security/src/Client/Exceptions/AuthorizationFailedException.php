<?php

declare(strict_types=1);

namespace Sandbox\Security\Client\Exceptions;

final class AuthorizationFailedException extends \Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}
