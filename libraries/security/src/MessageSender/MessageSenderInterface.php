<?php

declare(strict_types=1);

namespace Sandbox\Security\MessageSender;

use Sandbox\Security\MessageSender\Messages\User\UserMessageInterface;

interface MessageSenderInterface
{
    public function sendMessage(UserMessageInterface $message): void;
}
