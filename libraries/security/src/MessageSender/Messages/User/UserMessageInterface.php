<?php

declare(strict_types=1);

namespace Sandbox\Security\MessageSender\Messages\User;


use Grifix\Uuid\Uuid;

/** @internal */
interface UserMessageInterface
{
    public function getUserId(): Uuid;
}
