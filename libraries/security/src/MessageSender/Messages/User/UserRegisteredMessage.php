<?php

declare(strict_types=1);

namespace Sandbox\Security\MessageSender\Messages\User;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final readonly class UserRegisteredMessage implements UserMessageInterface
{
    public function __construct(
        public Uuid $userId,
        public Email $email,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
