<?php

declare(strict_types=1);

namespace Sandbox\Security\MessageSender\Messages\User;

use Grifix\Email\Email;
use Grifix\Uuid\Uuid;

final class UserEmailChangedMessage implements UserMessageInterface
{
    public function __construct(
        public Uuid $userId,
        public Email $email,
    ) {
    }

    public function getUserId(): Uuid
    {
        return $this->userId;
    }
}
