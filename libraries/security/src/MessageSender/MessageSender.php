<?php

declare(strict_types=1);

namespace Sandbox\Security\MessageSender;

use Grifix\EventStore\EventStoreInterface;
use Sandbox\Security\MessageSender\Messages\User\UserMessageInterface;

final readonly class MessageSender implements MessageSenderInterface
{
    public function __construct(private EventStoreInterface $eventStore)
    {
    }

    public static function getStreamName(): string
    {
        return 'sandbox.security';
    }

    public function sendMessage(
        UserMessageInterface $message
    ): void {
        $this->eventStore->storeEvent($message, self::class, $message->getUserId());
        $this->eventStore->flush();
    }
}
