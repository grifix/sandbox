<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui;

use Grifix\Uuid\Uuid;
use Sandbox\SharedKernel\Ui\Security\SecurityInterface;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractRequestHandler extends \Grifix\Framework\Ui\AbstractRequestHandler
{
    #[Required]
    public SecurityInterface $security;

    protected function hasPermission(string $permission): bool
    {
        return $this->security->hasPermission($permission);
    }

    protected function authorize(string $permission): void
    {
        $this->security->authorize($permission);
    }

    protected function getAuthenticatedUserId(): Uuid
    {
        return $this->security->getUserId();
    }
}
