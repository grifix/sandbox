<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Middlewares;

use Grifix\Ip\IpAddress;
use Sandbox\SharedKernel\Ui\Security\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

final class SecurityMiddleware
{
    public function __construct(
        private readonly Security $security,
    ) {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $token = $this->extractAccessToken($event->getRequest());
        if ($token) {
            $this->security->authenticate(
                $this->extractAccessToken($event->getRequest()),
                $this->getIpAddress($event->getRequest()),
            );
        }
    }

    private function extractAccessToken(Request $request): ?string
    {
        if ($request->headers->has('Authorization')) {
            /** @var string $header */
            $header = $request->headers->get('Authorization');
            return $this->getTokenFormHeader($header);
        }

        if (!$request->hasSession()) {
            return null;
        }

        if ($request->getSession()->has('access_token')) {
            /** @var string $result */
            $result = $request->getSession()->get('access_token');
            return $result;
        }
        return null;
    }

    private function getTokenFormHeader(string $header): ?string
    {
        if (!str_starts_with($header, 'Bearer ')) {
            return null;
        }
        return str_replace('Bearer ', '', $header);
    }

    private function getIpAddress(Request $request): ?IpAddress
    {
        $ip = $request->getClientIp();
        if ($ip) {
            return IpAddress::create($ip);
        }
        return null;
    }
}
