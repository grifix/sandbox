<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

interface SecurityInterface
{
    public function authenticate(string $accessToken, ?IpAddress $ipAddress = null): void;

    public function getUserId(): Uuid;

    public function hasPermission(string $permission): bool;

    public function authorize(string $permission): void;
}
