<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;

final class CachedSecurity implements SecurityInterface
{
    private ?Uuid $userId = null;

    /**
     * @var array<string, bool>
     */
    private array $hasPermissionCache = [];

    /**
     * @var array <string, true|\Throwable>
     */
    private array $authorizeCache = [];

    public function __construct(
        private readonly SecurityInterface $security,
    ) {
    }

    public function authenticate(string $accessToken, ?IpAddress $ipAddress = null): void
    {
        $this->userId = null;
        $this->hasPermissionCache = [];
        $this->security->authenticate($accessToken, $ipAddress);
    }

    public function getUserId(): Uuid
    {
        if ($this->userId === null) {
            $this->userId = $this->security->getUserId();
        }
        return $this->userId;
    }

    public function hasPermission(string $permission): bool
    {
        if (!array_key_exists($permission, $this->hasPermissionCache)) {
            $this->hasPermissionCache[$permission] = $this->security->hasPermission($permission);
        }
        return $this->hasPermissionCache[$permission];
    }

    public function authorize(string $permission): void
    {
        if(!array_key_exists($permission, $this->authorizeCache)) {
            try {
                $this->security->authorize($permission);
                $this->authorizeCache[$permission] = true;
            } catch (\Throwable $e) {
                $this->authorizeCache[$permission] = $e;
                throw $e;
            }
        }
        if ($this->authorizeCache[$permission] instanceof \Throwable) {
            throw $this->authorizeCache[$permission];
        }
    }
}
