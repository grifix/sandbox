<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security;

final class ForbiddenProperty
{
    public static function create(): self
    {
        return new self();
    }

    public function __toString(): string
    {
        return '__FORBIDDEN_PROPERTY__';
    }

    public function toString(): string
    {
        return $this->__toString();
    }
}
