<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security;

use Grifix\Ip\IpAddress;
use Grifix\Uuid\Uuid;
use Sandbox\Security\Client\Exceptions\AuthorizationFailedException;
use Sandbox\Security\Client\SecurityClientInterface;
use Sandbox\SharedKernel\Ui\Security\Exceptions\ForbiddenException;
use Sandbox\SharedKernel\Ui\Security\Exceptions\UnauthorizedException;

final class Security implements SecurityInterface
{
    private ?string $accessToken = null;
    private ?IpAddress $ipAddress = null;

    private ?Uuid $userId = null;

    public function __construct(
        private readonly SecurityClientInterface $securityClient,
    ) {
    }

    public function authenticate(string $accessToken, ?IpAddress $ipAddress = null): void
    {
        $this->userId = $this->securityClient->getUserId($accessToken);
        $this->accessToken = $accessToken;
        $this->ipAddress = $ipAddress;
    }

    private function getAccessToken(): string
    {
        if (null === $this->accessToken) {
            throw new UnauthorizedException();
        }
        return $this->accessToken;
    }

    public function getUserId(): Uuid
    {
        if (null === $this->userId) {
            throw new UnauthorizedException();
        }
        return $this->userId;
    }

    public function hasPermission(string $permission): bool
    {
        return $this->securityClient->hasPermission($this->getAccessToken(), $permission);
    }

    public function authorize(string $permission): void
    {
        try {
            $this->securityClient->authorize(
                $this->getAccessToken(),
                $permission,
                $this->ipAddress,
            );
        } catch (AuthorizationFailedException) {
            throw new ForbiddenException();
        }
    }
}
