<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security\Exceptions;

final class ForbiddenException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Forbidden!', 403);
    }
}
