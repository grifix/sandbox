<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui\Security\Exceptions;

final class UnauthorizedException extends \Exception
{

    public function __construct()
    {
        parent::__construct('Unauthorized!', 401);
    }
}
