<?php

declare(strict_types=1);

namespace Sandbox\SharedKernel\Ui;

use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenterBundle\ExceptionConverterProviderInterface;
use Sandbox\SharedKernel\Ui\Security\Exceptions\ForbiddenException;
use Sandbox\SharedKernel\Ui\Security\Exceptions\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

final class SharedExceptionConverterProvider implements ExceptionConverterProviderInterface
{
    public function getConverters(): array
    {
        return [
            ExceptionConverter::create(
                ForbiddenException::class,
                Response::HTTP_FORBIDDEN,
            ),
            ExceptionConverter::create(
                UnauthorizedException::class,
                Response::HTTP_UNAUTHORIZED,
            ),
        ];
    }
}
