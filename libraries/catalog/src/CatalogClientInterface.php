<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

use Grifix\Uuid\Uuid;
use Sandbox\Catalog\Dtos\ProductDto;

interface CatalogClientInterface
{
    public function getProduct(Uuid $productId): ProductDto;

    public function findProducts(ProductFilter $filter = null): array;
}
