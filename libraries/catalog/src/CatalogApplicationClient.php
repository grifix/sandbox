<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\Uuid\Uuid;
use Modules\Catalog\Application\Product\Ports\Projector\ProductFilter as CatalogProductFilter;
use Modules\Catalog\Application\Product\Queries\Find\FindProductsQuery;
use Modules\Catalog\Application\Product\Queries\Get\GetProductQuery;
use Sandbox\Catalog\Dtos\ProductDto;

final class CatalogApplicationClient implements CatalogClientInterface
{
    public function __construct(private readonly ApplicationInterface $application)
    {
    }

    public function getProduct(Uuid $productId): ProductDto
    {
        $result = $this->application->ask(
            new GetProductQuery(CatalogProductFilter::create()->setProductId($productId))
        );
        return new ProductDto(
            $result->product->id,
            $result->product->name,
            $result->product->price
        );
    }

    /**
     * @return ProductDto[]
     */
    public function findProducts(ProductFilter $filter = null): array
    {
        if (null == $filter) {
            $filter = ProductFilter::create();
        }
        $products = $this->application->ask(
            new FindProductsQuery(
                CatalogProductFilter::create()
                    ->setProductId($filter->getProductId())
                    ->setSortBy($filter->getSortBy())
                    ->setOffset($filter->getOffset())
                    ->setLimit($filter->getLimit())
                    ->setSortDirection($filter->getSortDirection())
            )
        )->products;

        $result = [];
        foreach ($products as $product) {
            $result[] = new ProductDto(
                $product->id,
                $product->name,
                $product->price
            );
        }

        return $result;
    }
}
