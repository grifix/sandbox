<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Dtos;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductDto
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $name,
        public readonly Money $price
    ) {
    }
}
