<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Dtos;

use Grifix\Money\Money\Money;

final class CreateProductDto
{
    public function __construct(
        public readonly string $name,
        public readonly Money $price
    )
    {
    }
}
