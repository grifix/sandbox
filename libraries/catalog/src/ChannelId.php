<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

final class ChannelId
{
    private function __construct()
    {
    }

    public static function get(): string
    {
        return 'sandbox.catalog';
    }
}
