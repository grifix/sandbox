<?php

declare(strict_types=1);

namespace Sandbox\Catalog;

use Grifix\Uuid\Uuid;

final class ProductFilter
{
    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $sortBy = null;

    private ?string $sortDirection = null;

    private ?Uuid $productId = null;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): self
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSortBy(): ?string
    {
        return $this->sortBy;
    }

    public function setSortBy(?string $sortBy): self
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    public function getSortDirection(): ?string
    {
        return $this->sortDirection;
    }

    public function setSortDirection(?string $sortDirection): self
    {
        $this->sortDirection = $sortDirection;
        return $this;
    }

    public function getProductId(): ?Uuid
    {
        return $this->productId;
    }

    public function setProductId(?Uuid $productId): self
    {
        $this->productId = $productId;
        return $this;
    }
}
