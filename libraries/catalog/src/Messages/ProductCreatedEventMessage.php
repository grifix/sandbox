<?php

declare(strict_types=1);

namespace Sandbox\Catalog\Messages;

use Grifix\Money\Money\Money;
use Grifix\Uuid\Uuid;

final class ProductCreatedEventMessage
{
    public function __construct(
        public readonly Uuid $productId,
        public readonly string $name,
        public readonly Money $price
    ) {
    }
}
